#!/bin/bash
# This script sets up the GRMON3 software to connect with the AAC LEON3 board.
# It works without any arguments provided you only have one debugger connected.
# If multiple debuggers are used, you need to differentiate the following:
# 1. Which serial to hook to (check with lsusb and grep for iSerial)
# 2. Which gdb port to assign

GDB_PORT=50001

if [ -z "$GRMON" ]; then
    # No grmon set by user, check path.
    if [ -x "$(command -v grmon)" ]; then
        GRMON=grmon
    else
        cat <<'EOF'
Error: Unable to find GRMON executable.

Plase make sure that the PATH environment variable has been set to include the
location of the 'grmon' executable, or alternatively that the 'GRMON'
environment variable has been set to point at the 'grmon' executable.

For more information please consult the ÅAC Sirius LEON3 product manual
available at http://repo.aacmicrotec.com/manuals/
EOF
    exit 1
    fi
fi

while [[ $# > 1 ]]; do
    key="$1"

    case $key in
        -s|--serial)
            FTDI_SERIAL="$2"
            echo "FTDI serial set to $FTDI_SERIAL"
            shift # past argument
            ;;
        -g|--gdb_port)
            GDB_PORT="$2"
            shift # past argument
            ;;
        *)
            # unknown option
            ;;
    esac
    shift # past argument or value
done

GRMON_CMDS="$GRMON_CMDS -ftdi -ftdigpio 0x08100000 -gdb $GDB_PORT -stack 0x04000000"

if [ ${FTDI_SERIAL+x} ]; then
    GRMON_CMDS="$GRMON_CMDS -jtagserial $FTDI_SERIAL"
fi

exec "$GRMON" $GRMON_CMDS
