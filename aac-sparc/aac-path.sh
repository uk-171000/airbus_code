#------------------------------------------------------------------------------
# Copyright (C) 2015 - 2018 ÅAC Microtec AB
#
# Filename:     aac-path.sh
# Module name:  aac-sparc-toolchain
#
# Author(s):    JaVi
# Support:      support@aacmicrotec.com
# Description:  A script for setting up all the paths necessary to run the 
#               toolchain. Usage:
#               $ source aac-path.sh
# Requirements: -
#------------------------------------------------------------------------------

# Relative path to the ÅAC toolchain directory
REL_PATH=/opt/aac-sparc

echo ""
echo "---------------------------------------------------------------------"
echo " Setting up PATHs for AAC Microtec Leon3FT SPARC toolchain"
echo "---------------------------------------------------------------------"
echo ""

# Configure PATH
PATH=$REL_PATH/gcc-rtems/bin:$REL_PATH:$PATH

# Print info that is good to remember
echo "---------------------------------------------------------------------"
echo "Remember that the AAC Leon3FT SPARC toolchain has these targets"
echo "target=sparc-aac-rtems4.11 for RTEMS compilation"
echo "---------------------------------------------------------------------"
echo ""
