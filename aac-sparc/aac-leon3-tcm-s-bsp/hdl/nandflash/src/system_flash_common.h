/*-------------------------------------------------------------------------
 * Copyright (C) 2016 - 2018 ÅAC Microtec AB
 *
 * Filename:     system_flash_common.h
 * Module name:  system_flash
 *
 * Author(s):    MaWe
 * Support:      support@aacmicrotec.com
 * Description:  Commmon declarations and defines for bare metal and RTEMS.
 *
 *               These are common items which needs to be available to both the
 *               bare metal and RTEMS driver, and additionally exposed to the
 *               application layer utilising the driver, thus cannot be in
 *               either of the bare metal or RTEMS headers since those are not
 *               allowed to include one another (in order to avoid either
 *               exposing bare metal to application layer, or making bare metal
 *               dependent on RTEMS, or duplicating defines).
 *
 * Requirements: Adheres to the ÅAC System Flash Driver Requirement
 *               Specification Rev. A
 *-----------------------------------------------------------------------*/

#ifndef _SYSTEM_FLASH_COMMON_H_
#define _SYSTEM_FLASH_COMMON_H_

#include <stdint.h>

/* The system flash controller supports Micron MT29F32G08AFABA or MT29F64G08AFAAA NAND flash
 * chips.
 * The following chip commands are supported:
 * Read/Program a page, erase a block, read chip ID, reset the device and read status.
 *
 * MT29F32G08AFABA: 4GB, 1048k raw pages with 4096 bytes each.
 * MT29F64G08AFAAA: 8GB, 1048k raw pages with 8192 bytes each.
 *
 * All sizes below are with EDAC on (half of raw page size).
 */

#define SYSFLASH_4GB_PAGE_SIZE            2048
#define SYSFLASH_4GB_PAGE_SPARE_AREA_SIZE 112

#define SYSFLASH_8GB_PAGE_SIZE            4096
#define SYSFLASH_8GB_PAGE_SPARE_AREA_SIZE 224

/* 4GB by default */
#define SYSFLASH_PAGE_SIZE            SYSFLASH_4GB_PAGE_SIZE
#define SYSFLASH_PAGE_SPARE_AREA_SIZE SYSFLASH_4GB_PAGE_SPARE_AREA_SIZE

#define SYSFLASH_PAGES_PER_BLOCK      (128)
#define SYSFLASH_BLOCKS_PER_LUN       (4096)
#define SYSFLASH_LUNS                 (2)
#define SYSFLASH_BLOCKS               (SYSFLASH_BLOCKS_PER_LUN * SYSFLASH_LUNS)
#define SYSFLASH_MAX_NO_PAGES         (SYSFLASH_BLOCKS * SYSFLASH_PAGES_PER_BLOCK)

#define SYSFLASH_FACTORY_BAD_BLOCK_MARKED  1
#define SYSFLASH_FACTORY_BAD_BLOCK_CLEARED 0

/* Chip status register flags/bit masks */
#define SYSFLASH_STATUS_MASK                (0xFF)
/* Write Protect, 0 => Protected */
#define SYSFLASH_STATUS_WP                  (0x80)
/* Ready/Busy, 0 => Busy */
#define SYSFLASH_STATUS_RDY                 (0x40)
/* Ready/busy for array operations, 0 => Busy */
#define SYSFLASH_STATUS_ARDY                (0x20)
/* Previous op (N-1) Pass/Fail, 0 => Pass */
#define SYSFLASH_STATUS_FAILC               (0x02)
/* Latest op (N) Pass/Fail, 0 => Pass */
#define SYSFLASH_STATUS_FAIL                (0x01)

/* Chip ID data structure */
#define SYSFLASH_CID_SIZE       (2)

typedef struct
{
  uint32_t chip0[SYSFLASH_CID_SIZE];
} sysflash_cid_t;
static const uint32_t sysflash_cid_MT29F32G08AFABA[SYSFLASH_CID_SIZE] = {
  0x2C480026, 0x89000000
};
static const uint32_t sysflash_cid_MT29F64G08AFAAA[SYSFLASH_CID_SIZE] = {
  0x2C680027, 0xA9000000
};


#endif /* _SYSTEM_FLASH_COMMON_H_ */
