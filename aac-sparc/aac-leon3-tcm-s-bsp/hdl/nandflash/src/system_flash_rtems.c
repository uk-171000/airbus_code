/*----------------------------------------------------------------------
 * Copyright (C) 2015 - 2017 ÅAC Microtec AB
 *
 * Filename:     system_flash_rtems.c
 * Module name:  system_flash
 *
 * Author(s):    OdMa, JoBe, MaWe, DaHe
 * Support:      support@aacmicrotec.com
 * Description:  System NAND flash controller driver implementation
 * Requirements: Adheres to the ÅAC System Flash Driver Requirement
 *               Specification Rev. A
 *--------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <rtems.h>
#include <rtems/libio.h>
#include <bsp.h>
#include <bsp/irq.h>
#include <rtems/seterr.h>

#include "system_flash.h"
#include "system_flash_rtems.h"

#include "toolchain_support.h"

/* Board definitions */
uint32_t _sysflash_base = AAC_BSP_SYSFLASH_BASE;
uint32_t _sysflash_IRQ = AAC_BSP_SYSFLASH_IRQ;

#define SYSFLASH_SEM_ATTR    (RTEMS_FIFO | RTEMS_SIMPLE_BINARY_SEMAPHORE | \
                             RTEMS_NO_MULTIPROCESSOR_RESOURCE_SHARING | RTEMS_LOCAL)

static rtems_id sysflash_ip_sem_id;
static rtems_id sysflash_int_sem_id;
static bool device_open = false;

void sysflash_prog_page_done_callback(void *arg);
void sysflash_read_status_done_callback(void *arg);
void sysflash_read_page_setup_done_callback(void *arg);
void sysflash_erase_block_done_callback(void *arg);
void sysflash_read_id_done_callback(void *arg);
void sysflash_reset_done_callback(void *arg);
void sysflash_set_feature_done_callback(void *arg);
int32_t sysflash_are_rw_args_valid(rtems_libio_rw_args_t *rw_args);
int32_t sysflash_write_spare_area(uint32_t page,
                                  const uint8_t *buffer,
                                  uint32_t size);

/*
 * SYSFLASH callback functions
 */
sysflash_callbacks_t sysflash_callbacks = {
  .sysflash_prog_page_done_irq_callback = sysflash_prog_page_done_callback,
  .sysflash_read_status_done_irq_callback = sysflash_read_status_done_callback,
  .sysflash_read_p_setup_done_irq_callback = sysflash_read_page_setup_done_callback,
  .sysflash_erase_block_done_irq_callback = sysflash_erase_block_done_callback,
  .sysflash_read_id_done_irq_callback = sysflash_read_id_done_callback,
  .sysflash_reset_done_irq_callback = sysflash_reset_done_callback,
  .sysflash_set_feature_done_irq_callback = sysflash_set_feature_done_callback
};

void sysflash_prog_page_done_callback(void *arg)
{
  rtems_semaphore_release(sysflash_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Sysflash program page done.");
}

void sysflash_read_status_done_callback(void *arg)
{
  rtems_semaphore_release(sysflash_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Sysflash read status done.");
}

void sysflash_read_page_setup_done_callback(void *arg)
{
  rtems_semaphore_release(sysflash_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Sysflash read page setup done.");
}

void sysflash_erase_block_done_callback(void *arg)
{
  rtems_semaphore_release(sysflash_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Sysflash erase block done.");
}

void sysflash_read_id_done_callback(void *arg)
{
  rtems_semaphore_release(sysflash_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Sysflash read id done.");
}

void sysflash_reset_done_callback(void *arg)
{
  rtems_semaphore_release(sysflash_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Sysflash reset done.");
}

void sysflash_set_feature_done_callback(void *arg)
{
  rtems_semaphore_release(sysflash_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Sysflash set feature done.");
}

int32_t sysflash_are_rw_args_valid(rtems_libio_rw_args_t *rw_args)
{
  uint32_t page;
  uint32_t number_of_pages;

  /* NOTE: To follow POSIX offset should be in bytes, but since off_t for now is
           limited to 32 bits signed use pages instead */
  page = rw_args->offset;

  /* Page within limits */
  if ((page < 0) || (page >= SYSFLASH_MAX_NO_PAGES)) {
    DBG(DBG_SEVERITY_ERR, "Invalid page reference. (offset = %d)", rw_args->offset);
    return -EINVAL;
  }

  /* Sanity check count */
  number_of_pages = rw_args->count / SYSFLASH_PAGE_SIZE;
  if ((rw_args->count % SYSFLASH_PAGE_SIZE) != 0) {
    number_of_pages ++;
  }

  if ((page + number_of_pages) > SYSFLASH_MAX_NO_PAGES) {
      DBG(DBG_SEVERITY_ERR,
          "Page operation out of boundaries.\n"
          "  Page offset (%d).\n"
          "  Pages to make operation (%d).\n"
          "  Max allowed pages (%d).",
          page,
          number_of_pages,
          SYSFLASH_MAX_NO_PAGES);

      return -EINVAL;
    }

  return ENOERR;
}

rtems_device_driver sysflash_initialize(rtems_device_major_number major,
                                       rtems_device_minor_number unused,
                                       void *args)
{
  int32_t ret;
  rtems_status_code status;

  status = rtems_semaphore_create(rtems_build_name('S', 'F', 'I', 'P'),
                                  0,
                                  SYSFLASH_SEM_ATTR,
                                  RTEMS_NO_PRIORITY_CEILING,
                                  &sysflash_ip_sem_id);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to create sysflash ip semaphore, error %d, %s",
        status, rtems_status_text(status));
    rtems_io_unregister_driver(major);
    return status;
  }

  status = rtems_semaphore_create(rtems_build_name('S', 'F', 'I', 'R'),
                                  0,
                                  SYSFLASH_SEM_ATTR,
                                  RTEMS_NO_PRIORITY_CEILING,
                                  &sysflash_int_sem_id);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to create sysflash interrupt semaphore, error %d, %s",
        status, rtems_status_text(status));
    rtems_semaphore_delete(sysflash_ip_sem_id);
    rtems_io_unregister_driver(major);
    return status;
  }

  ret = sysflash_init();
  if (ENOERR != ret) {
    DBG(DBG_SEVERITY_ERR, "Failed to init System Flash (%d)", ret);
    rtems_semaphore_delete(sysflash_ip_sem_id);
    rtems_semaphore_delete(sysflash_int_sem_id);
    rtems_io_unregister_driver(major);
    return RTEMS_INTERNAL_ERROR;
  }

  status = rtems_interrupt_handler_install(_sysflash_IRQ,
                                           SYSFLASH_DEVICE_NAME,
                                           RTEMS_INTERRUPT_UNIQUE,
                                           sysflash_isr,
                                           NULL);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to install RTEMS sysflash interrupt handler %d, %s",
        status, rtems_status_text(status));
    rtems_semaphore_delete(sysflash_ip_sem_id);
    rtems_semaphore_delete(sysflash_int_sem_id);
    rtems_io_unregister_driver(major);
    return status;
  }

  status = rtems_io_register_name(SYSFLASH_DEVICE_NAME, major, 0);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to register system flash driver (%d)", status);
    rtems_semaphore_delete(sysflash_ip_sem_id);
    rtems_semaphore_delete(sysflash_int_sem_id);
    rtems_interrupt_handler_remove(_sysflash_IRQ,
                                   sysflash_isr,
                                   NULL);
    rtems_io_unregister_driver(major);
    return status;
  }

  DBG(DBG_SEVERITY_INFO, "System flash driver [%d:0] registered.", major);

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver sysflash_open(rtems_device_major_number major,
                                 rtems_device_minor_number unused,
                                 void *args)
{
  rtems_libio_open_close_args_t *rw_args;
  rw_args = (rtems_libio_open_close_args_t *) args;

  DBG(DBG_SEVERITY_INFO, "Opening sysflash device [%d:%d].", major, unused);
  DBG(DBG_SEVERITY_INFO, "Flags 0x%X", rw_args->flags);

  if(device_open == true) {
    DBG(DBG_SEVERITY_WARN, "Sysflash driver [%d:%d] already opened. Aborting request.",
        major, unused);
    rtems_set_errno_and_return_minus_one(EEXIST);
  }

  device_open = true;

  rtems_semaphore_release(sysflash_ip_sem_id);
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver sysflash_close(rtems_device_major_number major,
                                  rtems_device_minor_number unused,
                                  void *args)
{
  rtems_libio_open_close_args_t *rw_args;
  rw_args = (rtems_libio_open_close_args_t *) args;

  DBG(DBG_SEVERITY_INFO, "Closing device [%d:%d].", major, unused);
  DBG(DBG_SEVERITY_INFO, "Flags 0x%X", rw_args->flags);

  if(device_open == false) {
    DBG(DBG_SEVERITY_WARN, "Sysflash driver [%d:%d] not opened. Aborting request.",
        major, unused);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  device_open = false;

  /* obtain ip semaphore to wait for any ongoing operations to finish */
  rtems_semaphore_obtain(sysflash_ip_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver sysflash_read(rtems_device_major_number major,
                                 rtems_device_minor_number unused,
                                 void *args)
{
  uint32_t page;
  uint32_t bytes_read;
  uint32_t bytes_to_read;
  int32_t  ret;
  uint8_t  *buffer;
  rtems_status_code status;
  rtems_libio_rw_args_t *rw_args;

  rw_args = (rtems_libio_rw_args_t *) args;

  if (ENOERR != sysflash_are_rw_args_valid(rw_args)) {
    return RTEMS_INVALID_NAME; /* Will translate to errno = EINVAL */
  }

  status = rtems_semaphore_obtain(sysflash_ip_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to grab sysflash ip semaphore, error: %s",
        rtems_status_text(status));
    return status;
  }

  DBG(DBG_SEVERITY_INFO, "Reading from system flash [%d:%d].", major, unused);
  DBG(DBG_SEVERITY_INFO, " rw_args->offset = %d.", rw_args->offset);
  DBG(DBG_SEVERITY_INFO, " rw_args->count = %d.", rw_args->count);

  sysflash_interrupt_enable(SYSFLASH_IE_RPAGE_DONE);
  bytes_read = 0;
  buffer = (uint8_t *) rw_args->buffer;

  while (bytes_read < rw_args->count) {
    /* NOTE: To follow POSIX offset should be in bytes, but since off_t for now is
             limited to 32 bits signed use pages instead */
    page = rw_args->offset + (bytes_read/SYSFLASH_PAGE_SIZE);
    bytes_to_read = rw_args->count - bytes_read;
    if (bytes_to_read >= SYSFLASH_PAGE_SIZE) {
      bytes_to_read = SYSFLASH_PAGE_SIZE;
    }
    DBG(DBG_SEVERITY_INFO, "Reading page:%d bytes:%d", page, bytes_to_read);

    ret = sysflash_read_page_setup(page);
    if (ENOERR != ret) {
      DBG(DBG_SEVERITY_ERR,
          "Read page setup failed. Bare metal driver returned: (%d)", ret);
      sysflash_interrupt_disable(SYSFLASH_IE_RPAGE_DONE);
      rtems_semaphore_release(sysflash_ip_sem_id);
      if(-EBUSY == ret) {
        return RTEMS_RESOURCE_IN_USE; /* Will translate to errno = EBUSY */
      } else if (-EINVAL == ret ) {
        return RTEMS_INVALID_NAME;  /* Will translate to errno = EINVAL */
      }
      return RTEMS_NOT_DEFINED; /* Will translate to errno = EIO */
    }

    /* Obtain interrupt semaphore to yield cpu and await interrupt for read setup done */
    status = rtems_semaphore_obtain(sysflash_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
    if (RTEMS_SUCCESSFUL != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to grab sysflash interrupt semaphore, error: %s",
          rtems_status_text(status));
      sysflash_interrupt_disable(SYSFLASH_IE_RPAGE_DONE);
      rtems_semaphore_release(sysflash_ip_sem_id);
      return status;
    }

    ret = sysflash_read_page(buffer, bytes_to_read);
    if (ENOERR != ret) {
      DBG(DBG_SEVERITY_ERR,
          "Read page failed. Bare metal driver returned: (%d)", ret);
      sysflash_interrupt_disable(SYSFLASH_IE_RPAGE_DONE);
      rtems_semaphore_release(sysflash_ip_sem_id);
      return RTEMS_IO_ERROR; /* Will translate to errno = EIO */
    }

    bytes_read += bytes_to_read;
    buffer = buffer + bytes_to_read;
  }
  rw_args->bytes_moved = bytes_read;

  sysflash_interrupt_disable(SYSFLASH_IE_RPAGE_DONE);
  rtems_semaphore_release(sysflash_ip_sem_id);

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver sysflash_write(rtems_device_major_number major,
                                  rtems_device_minor_number unused,
                                  void *args)
{
  uint32_t page;
  uint32_t bytes_written;
  uint32_t bytes_to_write;
  int32_t  ret;
  uint8_t  *buffer;
  rtems_status_code status;
  rtems_libio_rw_args_t *rw_args;

  rw_args = (rtems_libio_rw_args_t *) args;

  if (ENOERR != sysflash_are_rw_args_valid(rw_args)) {
    return RTEMS_INVALID_NAME; /* Will translate to errno = EINVAL */
  }

  status = rtems_semaphore_obtain(sysflash_ip_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to grab sysflash ip semaphore, error: %s",
        rtems_status_text(status));
    return status;
  }

  DBG(DBG_SEVERITY_INFO, "Writing to System flash [%d:%d].", major, unused);
  DBG(DBG_SEVERITY_INFO, " rw_args->offset = %d.", rw_args->offset);
  DBG(DBG_SEVERITY_INFO, " rw_args->count = %d.", rw_args->count);

  sysflash_interrupt_enable(SYSFLASH_IE_PROG_DONE);
  bytes_written = 0;
  buffer = (uint8_t *) rw_args->buffer;

  while (bytes_written < rw_args->count) {
    /* NOTE: To follow POSIX offset should be in bytes, but since off_t for now is
             limited to 32 bits signed use pages instead */
    page = rw_args->offset + (bytes_written/SYSFLASH_PAGE_SIZE);
    bytes_to_write = rw_args->count - bytes_written;
    if (bytes_to_write >= SYSFLASH_PAGE_SIZE) {
      bytes_to_write = SYSFLASH_PAGE_SIZE;
    }
    DBG(DBG_SEVERITY_INFO, "Writing page:%d bytes:%d", page, bytes_to_write);

    ret = sysflash_program_page(page, buffer, bytes_to_write);
    if (ENOERR != ret) {
      DBG(DBG_SEVERITY_ERR,
          "Failed to program page. Bare metal driver returned: (%d)", ret);
      sysflash_interrupt_disable(SYSFLASH_IE_PROG_DONE);
      rtems_semaphore_release(sysflash_ip_sem_id);
      if(-EBUSY == ret) {
        return RTEMS_RESOURCE_IN_USE; /* Will translate to errno = EBUSY */
      } else if (-EINVAL == ret ) {
        return RTEMS_INVALID_NAME;  /* Will translate to errno = EINVAL */
      }
      return RTEMS_NOT_DEFINED; /* Will translate to errno = EIO */
    }

    /* Obtain interrupt semaphore to yield cpu and await interrupt for program done */
    status = rtems_semaphore_obtain(sysflash_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
    if (RTEMS_SUCCESSFUL != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to grab sysflash interrupt semaphore, error: %s",
          rtems_status_text(status));
      sysflash_interrupt_disable(SYSFLASH_IE_PROG_DONE);
      rtems_semaphore_release(sysflash_ip_sem_id);
      return status;
    }

    ret = sysflash_verify_command();
    if (ENOERR != ret) {
      DBG(DBG_SEVERITY_ERR, "Failed to program page - block should be marked bad!\n");
      sysflash_interrupt_disable(SYSFLASH_IE_PROG_DONE);
      rtems_semaphore_release(sysflash_ip_sem_id);

      return RTEMS_IO_ERROR; /* Will translate to errno = EIO */
    }
    bytes_written = bytes_written + bytes_to_write;
    buffer =  buffer + bytes_to_write;
  }
  rw_args->bytes_moved = bytes_written;

  sysflash_interrupt_disable(SYSFLASH_IE_PROG_DONE);
  rtems_semaphore_release(sysflash_ip_sem_id);
  return RTEMS_SUCCESSFUL;
}


int32_t sysflash_write_spare_area(uint32_t page,
                                  const uint8_t *buffer,
                                  uint32_t size)
{
  int32_t ret;
  rtems_status_code status;

  /* Page within limits */
  if (page < 0 || page >= SYSFLASH_MAX_NO_PAGES) {
    DBG(DBG_SEVERITY_ERR, "Invalid page reference. (%u)", page);
    return -EINVAL;
  }

  /* Sanity check size */
  if (size > SYSFLASH_PAGE_SPARE_AREA_SIZE ||
      size == 0)  {
    DBG(DBG_SEVERITY_ERR, "Invalid size. (%u)", size);
    return -EINVAL;
  }

  DBG(DBG_SEVERITY_INFO, "Writing to system flash spare area.");

  sysflash_interrupt_enable(SYSFLASH_IE_PROG_DONE);
  ret = sysflash_program_page_spare_area(page, buffer, size);
  if (ENOERR != ret) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to program spare area. Bare metal driver returned: (%d)", ret);
    sysflash_interrupt_disable(SYSFLASH_IE_PROG_DONE);
    if(-EBUSY == ret || -EINVAL == ret) {
      return ret;
    }
    return -EIO;
  }

  /* Obtain interrupt semaphore to yield cpu and await interrupt for program done */
  status = rtems_semaphore_obtain(sysflash_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to grab sysflash interrupt semaphore, error: %s",
        rtems_status_text(status));
    sysflash_interrupt_disable(SYSFLASH_IE_PROG_DONE);
    rtems_semaphore_release(sysflash_ip_sem_id);
    return status;
  }

  ret = sysflash_verify_command();
  if (ENOERR != ret) {
    DBG(DBG_SEVERITY_ERR, "Failed to program page spare area - "
                          "block should be marked bad!\n");
    sysflash_interrupt_disable(SYSFLASH_IE_PROG_DONE);
    return -EIO;
  }

  sysflash_interrupt_disable(SYSFLASH_IE_PROG_DONE);
  return ENOERR;
}

rtems_device_driver sysflash_control(rtems_device_major_number major,
                                    rtems_device_minor_number unused,
                                    void *parg)
{
  rtems_libio_ioctl_args_t *ioctl_args;
  rtems_status_code        status;

  /* Variables needed by some of the ioctls */
  uint8_t                  chip_status;
  uint16_t                 ctrl_status;

  /* Variable used for handling spare area ioctls */
  sysflash_ioctl_spare_area_args_t *spare_args;

  ioctl_args = (rtems_libio_ioctl_args_t *) parg;

  /* Every branch in the switch statment below needs the semaphore taken, so
   * we can take it before we enter it. The default label really doesn't need
   * it, so it just has to release it before returning the error code.*/
  status = rtems_semaphore_obtain(sysflash_ip_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to grab sysflash ip semaphore, error: %s",
        rtems_status_text(status));

    return status;
  }

  switch (ioctl_args->command) {
    case SYSFLASH_IO_RESET:
      sysflash_interrupt_enable(SYSFLASH_IE_RESET_DONE);

      sysflash_reset_target(SYSFLASH_EXT_ADDR_TARGET_CE);

      /* Wait for reset on chip CE is complete */
      status = rtems_semaphore_obtain(sysflash_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
      if (RTEMS_SUCCESSFUL != status) {
        DBG(DBG_SEVERITY_ERR, "Failed to grab sysflash interrupt semaphore, error: %s",
            rtems_status_text(status));

        sysflash_interrupt_disable(SYSFLASH_IE_RESET_DONE);
        rtems_semaphore_release(sysflash_ip_sem_id);

        return status;
      }

      sysflash_reset_target(SYSFLASH_EXT_ADDR_TARGET_CE2);

      /* Wait for reset on chip CE2 is complete */
      status = rtems_semaphore_obtain(sysflash_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
      if (RTEMS_SUCCESSFUL != status) {
        DBG(DBG_SEVERITY_ERR, "Failed to grab sysflash interrupt semaphore, error: %s",
            rtems_status_text(status));

        sysflash_interrupt_disable(SYSFLASH_IE_RESET_DONE);
        rtems_semaphore_release(sysflash_ip_sem_id);

        return status;
      }

      sysflash_interrupt_disable(SYSFLASH_IE_RESET_DONE);

      ioctl_args->ioctl_return = ENOERR;
      break;

    case SYSFLASH_IO_READ_CHIP_STATUS:
      ioctl_args->ioctl_return =
        sysflash_read_status((uint8_t*)ioctl_args->buffer, &ctrl_status);
      break;

    case SYSFLASH_IO_READ_CTRL_STATUS:
      ioctl_args->ioctl_return =
        sysflash_read_status(&chip_status, (uint16_t*)ioctl_args->buffer);
      break;

    case SYSFLASH_IO_READ_ID:
      ioctl_args->ioctl_return = sysflash_read_id((sysflash_cid_t*)ioctl_args->buffer);
      break;

    case SYSFLASH_IO_ERASE_BLOCK:
      sysflash_interrupt_enable(SYSFLASH_IE_ERASE_DONE);
      ioctl_args->ioctl_return = sysflash_erase_block((uint32_t)ioctl_args->buffer);
      if (ioctl_args->ioctl_return < 0) {
        sysflash_interrupt_disable(SYSFLASH_IE_ERASE_DONE);
        rtems_semaphore_release(sysflash_ip_sem_id);

        return RTEMS_SUCCESSFUL; /* Failed to erase block */
      }

      /* Obtain interrupt semaphore to yield cpu and await interrupt for erase done */
      status = rtems_semaphore_obtain(sysflash_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
      if (RTEMS_SUCCESSFUL != status) {
        DBG(DBG_SEVERITY_ERR, "Failed to grab sysflash interrupt semaphore, error: %s",
            rtems_status_text(status));

        sysflash_interrupt_disable(SYSFLASH_IE_ERASE_DONE);
        rtems_semaphore_release(sysflash_ip_sem_id);
        return status;
      }

      sysflash_interrupt_disable(SYSFLASH_IE_ERASE_DONE);

      /* Set erase block status */
      ioctl_args->ioctl_return = sysflash_verify_command();
      break;

    case SYSFLASH_IO_READ_SPARE_AREA:
      sysflash_interrupt_enable(SYSFLASH_IE_RPAGE_DONE);
      spare_args = (sysflash_ioctl_spare_area_args_t*) ioctl_args->buffer;
      ioctl_args->ioctl_return =
        sysflash_read_page_spare_area_setup(spare_args->page_num,
                                            spare_args->raw);

      if (ioctl_args->ioctl_return == ENOERR) {
        /* Obtain interrupt semaphore to yield cpu and await interrupt for read setup done */
        status = rtems_semaphore_obtain(sysflash_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
        if (RTEMS_SUCCESSFUL != status) {
          DBG(DBG_SEVERITY_ERR, "Failed to grab sysflash interrupt semaphore, error: %s",
              rtems_status_text(status));
          ioctl_args->ioctl_return = -EIO;
        }

        ioctl_args->ioctl_return =
          sysflash_read_page_spare_area(spare_args->data_buf,
                                        spare_args->size);
      }
      sysflash_interrupt_disable(SYSFLASH_IE_RPAGE_DONE);

      break;

  case SYSFLASH_IO_WRITE_SPARE_AREA:
      spare_args = (sysflash_ioctl_spare_area_args_t*) ioctl_args->buffer;
      ioctl_args->ioctl_return =
        sysflash_write_spare_area(spare_args->page_num,
                                  spare_args->data_buf,
                                  spare_args->size);
      break;

    case SYSFLASH_IO_BAD_BLOCK_CHECK:
      ioctl_args->ioctl_return =
        sysflash_factory_bad_block_check((uint32_t)ioctl_args->buffer);
      break;

    default:
      /* Release the semaphore before returning the error. */
      rtems_semaphore_release(sysflash_ip_sem_id);
      return RTEMS_NOT_DEFINED;
  }

  rtems_semaphore_release(sysflash_ip_sem_id);

  /* All non-RTEMS errors passed in ioctl_return variable at this point */
  return RTEMS_SUCCESSFUL;
}
