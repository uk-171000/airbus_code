/*-------------------------------------------------------------------------
 * Copyright (C) 2015 - 2017 ÅAC Microtec AB
 *
 * Filename:     system_flash.h
 * Module name:  system_flash
 *
 * Author(s):    Dahe, MaWe, InSi, OdMa, JoBe
 * Support:      support@aacmicrotec.com
 * Description:  System NAND flash controller driver declarations.
 * Requirements: Adheres to the ÅAC System Flash Driver Requirement
 *               Specification Rev. A
 *-----------------------------------------------------------------------*/

#ifndef _SYSTEM_FLASH_H_
#define _SYSTEM_FLASH_H_

#include <stdint.h>
#include <toolchain_support.h>

#include "system_flash_common.h"

extern uint32_t _sysflash_base;
extern uint32_t _sysflash_IRQ;

/* Use a timeout counter while waiting for flags to avoid getting stuck.
   0x800000 should give a maximum loop time on the order of a couple of
   seconds, plenty of time for the flash to respond but still limited. */
#define SYSFLASH_TIMEOUT                    (0x800000)

#define SYSFLASH_WORD_BITS                  (32)
#define SYSFLASH_WORD_SIZE                  (SYSFLASH_WORD_BITS / 8)

#define SYSFLASH_PAGES_PER_LUN              (SYSFLASH_BLOCKS_PER_LUN * \
                                             SYSFLASH_PAGES_PER_BLOCK)

/* Controller registers */
#define SYSFLASH_CMD                        (0x00*4)
#define SYSFLASH_SR                         (0x01*4)
#define SYSFLASH_IE                         (0x02*4)
#define SYSFLASH_EXT_ADDR                   (0x03*4)
#define SYSFLASH_ADDR                       (0x04*4)
#define SYSFLASH_DIO                        (0x05*4)
#define SYSFLASH_FEATURE                    (0x06*4)
#define SYSFLASH_MEMORY_SELECT              (0x07*4)
#define SYSFLASH_EDAC                       (0x08*4)

/* Register flags */
#define SYSFLASH_CMD_PROG_PAGE              (1<<0)
#define SYSFLASH_CMD_READ_STATUS            (1<<1)
#define SYSFLASH_CMD_READ_PAGE              (1<<2)
#define SYSFLASH_CMD_ERASE_BLOCK            (1<<3)
#define SYSFLASH_CMD_READ_ID                (1<<4)
#define SYSFLASH_CMD_RESET                  (1<<5)
#define SYSFLASH_CMD_STOP_RP                (1<<6)
#define SYSFLASH_CMD_SET_FEATURE            (1<<8)
#define SYSFLASH_CMD_EDAC_ILV               (1<<9)
#define SYSFLASH_CMD_ERR_INJECT             (1<<10)

#define SYSFLASH_SR_PROG_PAGE_DONE          (1<<0)
#define SYSFLASH_SR_READ_STATUS_DONE        (1<<1)
#define SYSFLASH_SR_READ_P_SETUP_DONE       (1<<2)
#define SYSFLASH_SR_ERASE_BLOCK_DONE        (1<<3)
#define SYSFLASH_SR_READ_ID_DONE            (1<<4)
#define SYSFLASH_SR_RESET_DONE              (1<<5)
#define SYSFLASH_SR_BUSY                    (1<<7)
#define SYSFLASH_SR_SET_FEATURE_DONE        (1<<8)
#define SYSFLASH_SR_EDAC_ILV                (1<<9)
#define SYSFLASH_SR_ERR_INJECT              (1<<10)

/* Status Register Interrupt Mask Definitions */
#define SYSFLASH_SR_IFLAGS_MASK             (SYSFLASH_SR_PROG_PAGE_DONE    | \
                                             SYSFLASH_SR_READ_STATUS_DONE  | \
                                             SYSFLASH_SR_READ_P_SETUP_DONE | \
                                             SYSFLASH_SR_ERASE_BLOCK_DONE  | \
                                             SYSFLASH_SR_READ_ID_DONE      | \
                                             SYSFLASH_SR_RESET_DONE        | \
                                             SYSFLASH_SR_BUSY              | \
                                             SYSFLASH_SR_SET_FEATURE_DONE)

#define SYSFLASH_SR_VALID_BITS_MASK         (SYSFLASH_SR_IFLAGS_MASK | \
                                             SYSFLASH_SR_EDAC_ILV    | \
                                             SYSFLASH_SR_ERR_INJECT)

#define SYSFLASH_IE_PROG_DONE               (1<<0)
#define SYSFLASH_IE_RSTAT_DONE              (1<<1)
#define SYSFLASH_IE_RPAGE_DONE              (1<<2)
#define SYSFLASH_IE_ERASE_DONE              (1<<3)
#define SYSFLASH_IE_RID_DONE                (1<<4)
#define SYSFLASH_IE_RESET_DONE              (1<<5)
#define SYSFLASH_IE_SFEATURE_DONE           (1<<8)

/* Interrupt Enable Register Mask Definitions */
#define SYSFLASH_INT_EN_REG_MASK            (SYSFLASH_IE_PROG_DONE  | \
                                             SYSFLASH_IE_RSTAT_DONE | \
                                             SYSFLASH_IE_RPAGE_DONE | \
                                             SYSFLASH_IE_ERASE_DONE | \
                                             SYSFLASH_IE_RID_DONE   | \
                                             SYSFLASH_IE_RESET_DONE | \
                                             SYSFLASH_IE_SFEATURE_DONE)

/* Feature Definitions */
#define SYSFLASH_FEATURE_TIMING_MODE        (0x0201)
#define SYSFLASH_FEATURE_OUT_DRIVE_STRENGTH (0x0210)
#define SYSFLASH_FEATURE_PULLDOWN_STRENGTH  (0x0081)

/* Extended address register (previously known as target select register) */
/* Target select sets which Logical Unit are being addressed */
#define SYSFLASH_EXT_ADDR_TARGET_SHIFT               (0)
#define SYSFLASH_EXT_ADDR_TARGET                     (1 << SYSFLASH_EXT_ADDR_TARGET_SHIFT)
#define SYSFLASH_EXT_ADDR_TARGET_CE                  (0)
#define SYSFLASH_EXT_ADDR_TARGET_CE2                 (1)
/* Extended address is is used for extra MSB bit 32 when addressing 8GB chips */
#define SYSFLASH_EXT_ADDR_8GB_EXTENDED_ADDRESS_SHIFT (8)
#define SYSFLASH_EXT_ADDR_8GB_EXTENDED_ADDRESS \
  (1 << SYSFLASH_TS_8GB_EXTENDED_ADDRESS_SHIFT)


/* Bit mask for the EDAC data */
#define SYSFLASH_EDAC_DATA_MASK             (0xFF)

/* Memory select adapts the RTL to the chip type hardware being used */
#define SYSFLASH_MEMORY_SELECT_MT29F32G08AFABA (0)
#define SYSFLASH_MEMORY_SELECT_MT29F64G08AFAAA (1)

#ifndef ENOERR
#define ENOERR                              (0)
#endif

typedef struct
{
  /*
   * Name:        sysflash_interrupt_callback
   * @brief       Interrupt callback function.
   * @param[in]   arg  Pointer to function arguments.
   */
  void (*sysflash_prog_page_done_irq_callback)(void *arg);
  void (*sysflash_read_status_done_irq_callback)(void *arg);
  void (*sysflash_read_p_setup_done_irq_callback)(void *arg);
  void (*sysflash_erase_block_done_irq_callback)(void *arg);
  void (*sysflash_read_id_done_irq_callback)(void *arg);
  void (*sysflash_reset_done_irq_callback)(void *arg);
  void (*sysflash_set_feature_done_irq_callback)(void *arg);
} sysflash_callbacks_t;

/*
 * Name:        sysflash_init
 * @brief:      Initialize sysflash. Performs reset of targets and sets default features.
 *
 * @param[in]:  -
 * @return:     ENOERR if OK
 *              -EIO   if timeout.
 */
int32_t sysflash_init(void);

/*
 * Name:        sysflash_set_features_default
 * @brief:      Set sub features to default values.
 * @param[in]:  -
 * @return:     ENOERR if OK.
 *              -EIO   if timeout.
 */
int32_t sysflash_set_features_default(void);

/* Name:        sysflash_get_interrupt_status
 * @brief       Gets status for interrupts parts of sysflash status register.
 * @param[in]:  -
 * @return      Content of the status register (interrupt status part).
 */
uint32_t sysflash_get_interrupt_status(void);

/* Name:        sysflash_interrupt_enable
 * @brief       Enables sysflash interrupt.
 * @param[in]   int_source Interrupt source(s) to enable.
 * @return      ENOERR  Enabling sysflash interrupts was successful.
 *              -EINVAL Invalid parameter.
 */
int32_t sysflash_interrupt_enable(const int32_t int_source);

/* Name:        sysflash_interrupt_disable
 * @brief       Disables sysflash interrupt.
 * @param[in]   int_source Interrupt source(s) to disable.
 * @return      ENOERR  Disabling sysflash interrupts was successful.
 *              -EINVAL Invalid parameter.
 */
int32_t sysflash_interrupt_disable(const int32_t int_source);

/*
 * Name:        sysflash_isr
 * @brief:      Interrupt service routine for mass memory interrupts.
 *              The function decodes active interrupts, activates a callback
 *              routine (if it is registered) and acknowledges the status
 *              register for active interrupts. It is a bit mask of the
 *              corresponding interrupt sources in the interrupt enable
 *              register where
 *                '0' indicates that the corresponding source does not have an
 *                    active interrupt
 *                '1' indicates that the corresponding source does have an
 *                    active interrupt
 * @param[in]:  data_arg Defined due to RTEMS prototype (not used)
 * @return:     -
 */
AAC_INTERRUPT_HANDLER_SIGNATURE(sysflash_isr, data_arg);

/*
 * Name:        sysflash_reset_target
 * @brief:      Sends the reset command to the selected target chip
 *              Intended to be used when corresponding interrupt is enabled.
 * @param[in]:  target Target to be reset, either SYSFLASH_TS_TARGET_CE or
 *                     SYSFLASH_TS_TARGET_CE2
 * @return:     ENOERR  if OK
 *              -EINVAL if target is neither low or high target
 */
int32_t sysflash_reset_target(int32_t target);

/*
 * Name:        sysflash_stop_read_program
 * @brief:      Sends the stop read/program command to the flash controller
 * @param[in]:  -
 * @return:     -
 */
void sysflash_stop_read_program(void);

/*
 * Name:        sysflash_set_page_spare_area_addr
 * @brief:      Set the address to the page spare area
 * @param[in]:  page_num Page number
 * @return:     ENOERR  if OK
 *              -EINVAL if page number is out-of-range
 */
int32_t sysflash_set_page_spare_area_addr(uint32_t page_num);

/*
 * Name:        sysflash_set_page_addr
 * @brief:      Sets the target and starting address for the page. Pages are
 *              counted continuously over the whole device, low numbers select
 *              the low target, high numbers select the high target.
 * @param[in]:  page_num  Page number
 * @return:     ENOERR  if OK
 *              -EINVAL if page number is out-of-range
 */
int32_t sysflash_set_page_addr(uint32_t page_num);

/*
 * Name:        sysflash_read_status
 * @brief:      Reads the flash chip status and the controller status register.
 * @param[in]:  chip_status  Data chip status buffer pointer.
 *              cntrl_status Controller status buffer pointer.
 * @return:     ENOERR if OK.
 *              -EBUSY if controller is busy.
 */
int32_t sysflash_read_status(uint8_t *chip_status,
                             uint16_t *ctrl_status);

/*
 * Name:        sysflash_read_id
 * @brief:      Reads the ID data from the flash chips
 *              Intended to be used when corresponding interrupt is disabled.
 * @param[in]:  cid Pointer to Chip ID data structure
 * @return:     ENOERR if OK.
 *              -EBUSY if controller busy.
 */
int32_t sysflash_read_id(sysflash_cid_t *cid);

/*
 * Name:        sysflash_verify_command
 * @brief:      Check Pass/Fail flag in the status register of the flash chip.
 *              Shall be called after a Page Program or Block Erase has
 *              finished to make sure the operation succeded on the chip.
 * @param[in]:  -
 * @return:     ENOERR if last Page Program or Block Erase passed.
 *              -EBUSY if the status from the chip is not available.
 *              -EIO   if last Page Program or Block Erase failed.
 */
int32_t sysflash_verify_command(void);

/*
 * Name:        sysflash_erase_block
 * @brief:      Start erasing a block of the flash without bad block check
 *              Intended to be used when corresponding interrupt is enabled.
 *              CAUTION: Only use on blocks that have already been checked,
 *                       bad blocks should never be erased or programmed!
 * @param[in]:  block_num Block number to erase
 * @return:     ENOERR  if Ok.
 *              -EINVAL if block number is out-of-range.
 *              -EBUSY  if controller busy.
 */
int32_t sysflash_erase_block(uint32_t block_num);

/*
 * Name:        sysflash_read_page_setup
 * @brief:      Setup to read a page from the flash
 *              After this function has run, wait for "Read page setup done"
 *              interrupt (if enabled) or poll the corresponding flag in the
 *              status register. Then run sysflash_read_page() to get the data
 *              from the flash.
 * @param[in]:  page_num Page number to read
 * @return:     ENOERR  if operation was successful
 *              -EBUSY  if controller busy
 *              -EINVAL if page number is out-of-range.
 */
int32_t sysflash_read_page_setup(uint32_t page_num);

/*
 * Name:        sysflash_read_page
 * @brief:      Read a page from the flash
 *              This function should run after sysflash_read_page_setup() to get
 *              the actual data from the flash. The function is most efficient
 *              when the data buffer has 4-byte alignment and the length is a
 *              multiple of 4 bytes.
 * @param[in]:  data_buf Buffer to store the data
 *              size     Number of bytes to read
 * @return:     ENOERR  if operation was successful
 *              -EINVAL if data_buf pointer is null pointer, if size is 0, or if
 *                      size is larger than one page.
 */
int32_t sysflash_read_page(uint8_t *data_buf,
                           uint32_t size);

/*
 * Name:        sysflash_factory_bad_block_check
 * Description: Checks if a block was marked as bad from factory (in spare area)
 * Arguments:   block_num Block number to check (blocks are counted continuously
 *                        over the whole device)
 * Return:      SYSFLASH_FACTORY_BAD_BLOCK_CLEARED if block is OK
 *              SYSFLASH_FACTORY_BAD_BLOCK_MARKED  if block was marked as bad
 *              -EIO                               if timeout
 *              -EBUSY                             if controller busy
 *              -EINVAL                            if block number is
 *                                                 out-of-range
 */
int32_t sysflash_factory_bad_block_check(uint32_t block_num);

/*
 * Name:        sysflash_read_page_spare_area_poll
 * @brief:      Reads spare area for a page from the flash
 *              Intended to be used when corresponding interrupt is disabled.
 *              The function is most efficient when the data buffer has 4-byte
 *              alignment and the length is a multiple of 4 bytes.
 * @param[in]:  page_num Page number to read
 *              raw      Read raw bytes (no interleaving and EDAC)
 *              data_buf Buffer to store the data
 *              size     Number of bytes to read
 * @return:     ENOERR  if OK.
 *              -EBUSY  if controller busy
 *              -EIO    if timeout
 *              -EINVAL if page number, raw, or size is out-of-range
 */
int32_t sysflash_read_page_spare_area_poll(uint32_t page_num,
                                           uint32_t raw,
                                           uint8_t *data_buf,
                                           uint32_t size);

/*
 * Name:        sysflash_read_page_spare_area_setup
 * @brief:      Reads spare area for a page from the flash
 *              Intended to be used when corresponding interrupt is enabled.
 * @param[in]:  page_num Page number to read
 *              raw      Read raw bytes (no interleaving and EDAC)
 * @return:     ENOERR  if OK.
 *              -EBUSY  if controller busy
 *              -EIO    if timeout
 *              -EINVAL if page number is out-of-range or raw is not a
                        boolean value (0 or 1)
 */
int32_t sysflash_read_page_spare_area_setup(uint32_t page_num,
                                            uint32_t raw);

/*
 * Name:        sysflash_read_page_spare_area
 * @brief:      Reads spare area for a page from the flash
 *              This function should run after sysflash_read_page_setup() to get
 *              the actual data from the flash. The function is most efficient
 *              when the data buffer has 4-byte alignment and the length is a
 *              multiple of 4 bytes.
 * @param[in]:  data_buf Buffer to store the data
 *              size     Number of bytes to read
 * @return:     ENOERR  if OK.
 *              -EIO    if timeout
 *              -EINVAL if data_buf is NULL or size is larger than spare area
 */
int32_t sysflash_read_page_spare_area(uint8_t *data_buf,
                                      uint32_t size);

/*
 * Name:        sysflash_program_page
 * @brief:      Program a page to the flash
 *              When function has finished, wait for the "Program page done"
 *              interrupt (if enabled) or poll the corresponding flag in the
 *              status register, then check the chip status using
 *              sysflash_verify_command(). The function is most efficient when
 *              the data buffer has 4-byte alignment and the length is a
 *              multiple of 4 bytes.
 * @param[in]:  page_num Page number to program
 *              data_buf Buffer containing the data
 *              size     Number of bytes to program
 * @return:     ENOERR  if OK.
 *              -EBUSY  if controller busy.
 *              -EINVAL if page number is out-of-range, data buffer is NULL, or
 *                      size is larger than one page.
 */
int32_t sysflash_program_page(uint32_t page_num,
                              const uint8_t *data_buf,
                              uint32_t size);

/*
 * Name:        sysflash_program_page_spare_area
 * @brief:      Program the spare area of a page to the flash.
 *              When function has finished, wait for the "Program page done"
 *              interrupt (if enabled) or poll the corresponding flag in the
 *              status register, then check the chip status using
 *              sysflash_verify_command(). The function is most efficient when
 *              the data buffer has 4-byte alignment and the length is a
 *              multiple of 4 bytes.
 *              Note that the first byte of the spare area of the first page of
 *              a block is reserved for the factory bad block marking, so
 *              changeing that may cause blocks to be falsely classified as bad.
 * @param[in]:  page_num Page number to program
 *              data_buf Buffer containing the data
 *              size     Number of bytes to program
 * @return:     ENOERR  if OK.
 *              -EBUSY  if controller busy
 *              -EIO    if timeout
 *              -EINVAL if page number or number of bytes was out-of-range.
 *                      If size is larger than spare area.
 */
int32_t sysflash_program_page_spare_area(uint32_t page_num,
                                         const uint8_t *data_buf,
                                         uint32_t size);
#endif
