/*----------------------------------------------------------------------
 * Copyright (C) 2016 - 2017 ÅAC Microtec AB
 *
 * Filename:     system_flash.c
 * Module name:  system_flash
 *
 * Author(s):    Dahe, MaWe, InSi, OdMa, JoBe
 * Support:      support@aacmicrotec.com
 * Description:  System NAND flash controller driver definitions
 * Requirements: Adheres to the ÅAC System Flash Driver Requirement
 *               Specification Rev. A
 *--------------------------------------------------------------------*/

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include "system_flash.h"
#include "toolchain_support.h"

/* Addressing is not continuous between pages, page address step size is four times the size
 * of a page, since a page is half of a "raw page" (due to EDAC), and the addres step
 * is two raw pages, where the latter half of the raw page contains the spare area and
 * unaddressable space.
 */
#define SYSFLASH_4GB_RAW_PAGE_SIZE     (2 * SYSFLASH_4GB_PAGE_SIZE)
#define SYSFLASH_4GB_PAGE_ADDRESS_STEP (2 * SYSFLASH_4GB_RAW_PAGE_SIZE)
#define SYSFLASH_8GB_RAW_PAGE_SIZE     (2 * SYSFLASH_8GB_PAGE_SIZE)
#define SYSFLASH_8GB_PAGE_ADDRESS_STEP (2 * SYSFLASH_8GB_RAW_PAGE_SIZE)

extern sysflash_callbacks_t sysflash_callbacks;

typedef enum {
  MT29F32G08AFABA,
  MT29F64G08AFAAA,
  INVALID_TYPE
} sysflash_chip_type_t;
static sysflash_chip_type_t global_chip_type = INVALID_TYPE;

void sysflash_clear_interrupt(int32_t interrupt);

int32_t sysflash_set_feature(uint32_t feature,
                             uint32_t target);

static sysflash_chip_type_t sysflash_determine_chip_type(sysflash_cid_t chip_ids);

int32_t sysflash_init(void)
{
  uint32_t timeout;
  sysflash_cid_t chip_id;

  timeout = 0;

  /* Set all registers in a known state */
  REG32(_sysflash_base + SYSFLASH_CMD)      = 0x0;
  REG32(_sysflash_base + SYSFLASH_SR)       = SYSFLASH_SR_IFLAGS_MASK;
  REG32(_sysflash_base + SYSFLASH_IE)       = 0x0;
  REG32(_sysflash_base + SYSFLASH_EXT_ADDR) = 0x0;
  REG32(_sysflash_base + SYSFLASH_ADDR)     = 0x0;
  REG32(_sysflash_base + SYSFLASH_DIO)      = 0x0;
  REG32(_sysflash_base + SYSFLASH_EDAC)     = 0x0;

  sysflash_reset_target(SYSFLASH_EXT_ADDR_TARGET_CE);
  /* Wait for reset of low target to finish */
  timeout = 0;
  while ((REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_RESET_DONE) == 0 &&
         timeout < SYSFLASH_TIMEOUT) {
    timeout++;
  }
  if (timeout >= SYSFLASH_TIMEOUT) {
    return -EIO;
  }
  /* Clear status flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;

  sysflash_reset_target(SYSFLASH_EXT_ADDR_TARGET_CE2);
  /* Wait for reset of high target to finish */
  timeout = 0;
  while ((REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_RESET_DONE) == 0 &&
         timeout < SYSFLASH_TIMEOUT) {
    timeout++;
  }
  if (timeout >= SYSFLASH_TIMEOUT) {
    return -EIO;
  }
  /* Clear status flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;

  if (sysflash_set_features_default() != ENOERR) {
    return -EIO;
  }

  if (sysflash_read_id(&chip_id) != ENOERR) {
    return -EIO;
  }
  global_chip_type = sysflash_determine_chip_type(chip_id);
  switch (global_chip_type) {
    case MT29F32G08AFABA:
      REG32(_sysflash_base + SYSFLASH_MEMORY_SELECT) = SYSFLASH_MEMORY_SELECT_MT29F32G08AFABA;
      break;

    case MT29F64G08AFAAA:
      REG32(_sysflash_base + SYSFLASH_MEMORY_SELECT) = SYSFLASH_MEMORY_SELECT_MT29F64G08AFAAA;
      break;

    case INVALID_TYPE:
      return -EIO;
  }

  return ENOERR;
}

int32_t sysflash_set_feature(uint32_t feature,
                             uint32_t target)
{
  uint32_t timeout;

  REG32(_sysflash_base + SYSFLASH_EXT_ADDR) = SYSFLASH_EXT_ADDR_TARGET & target;

  /* Clear status register by writing to all flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;

  REG32(_sysflash_base + SYSFLASH_FEATURE) = feature;

  /* Issue set feature command */
  REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_SET_FEATURE;

  /* Wait for set feature done */
  timeout = 0;
  while ((REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_SET_FEATURE_DONE) == 0 &&
         timeout < SYSFLASH_TIMEOUT) {
    timeout++;
  }

  if (timeout >= SYSFLASH_TIMEOUT) {
    return -EIO;
  }

  return ENOERR;
}

int32_t sysflash_set_features_default(void)
{
  int32_t ret;

  ret = sysflash_set_feature(SYSFLASH_FEATURE_TIMING_MODE,
                             SYSFLASH_EXT_ADDR_TARGET_CE);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;
    return ret;
  }

  ret = sysflash_set_feature(SYSFLASH_FEATURE_TIMING_MODE,
                             SYSFLASH_EXT_ADDR_TARGET_CE2);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;
    return ret;
  }

  ret = sysflash_set_feature(SYSFLASH_FEATURE_OUT_DRIVE_STRENGTH,
                             SYSFLASH_EXT_ADDR_TARGET_CE);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;
    return ret;
  }

  ret = sysflash_set_feature(SYSFLASH_FEATURE_OUT_DRIVE_STRENGTH,
                             SYSFLASH_EXT_ADDR_TARGET_CE2);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;
    return ret;
  }

  ret = sysflash_set_feature(SYSFLASH_FEATURE_PULLDOWN_STRENGTH,
                             SYSFLASH_EXT_ADDR_TARGET_CE);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;
    return ret;
  }

  ret = sysflash_set_feature(SYSFLASH_FEATURE_PULLDOWN_STRENGTH,
                             SYSFLASH_EXT_ADDR_TARGET_CE2);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;
    return ret;
  }

  /* Clear status register by writing to all flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;
  return ENOERR;
}

uint32_t sysflash_get_interrupt_status(void)
{
  return (REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_IFLAGS_MASK);
}

int32_t sysflash_interrupt_enable(const int32_t int_source)
{
  /* Check parameters */
  if (((SYSFLASH_INT_EN_REG_MASK | int_source) != SYSFLASH_INT_EN_REG_MASK) ||
      0 == int_source) {
    return -EINVAL;
  }
  REG32(_sysflash_base + SYSFLASH_IE) |= int_source;

  return ENOERR;
}

int32_t sysflash_interrupt_disable(const int32_t int_source)
{
  /* Check parameters */
  if (((SYSFLASH_INT_EN_REG_MASK | int_source) != SYSFLASH_INT_EN_REG_MASK) ||
      (0 == int_source)) {
    return -EINVAL;
  }
  REG32(_sysflash_base + SYSFLASH_IE) &= ~int_source;

  return ENOERR;
}

void sysflash_clear_interrupt(const int32_t interrupt)
{
  REG32(_sysflash_base + SYSFLASH_SR) |= interrupt;
}

AAC_INTERRUPT_HANDLER_SIGNATURE(sysflash_isr, data_arg)
{
  uint32_t int_status;
  uint32_t enabled_ints;

  /* Get interrupt status */
  int_status = REG32(_sysflash_base + SYSFLASH_SR);
  enabled_ints = REG32(_sysflash_base + SYSFLASH_IE);

  /* Mask status to only consider enabled interrupts */
  int_status &= enabled_ints;

  /* Clear interrupts before handling them. If any new interrupts of the same
  source occurs in between reading the status register and handling it in the
  callback, the newest state will simply be reflected and handled by the ISR.
  If the interrupt occurs before clearing the status register, the extra
  interrupt will not be generated. If occurring after clearing the status
  register but before handling it in the callback, a new interrupt reflecting
  the exact same state will be generated. This has to be handled in the ISR
  callback. If the new interrupt occurs after the callback, the ISR will be
  executed again directly.

  If another source generates an interrupt, then a new interrupt will be
  generated on clear of the status register and will be seen as the ISR will
  be executed again directly. */
  REG32(_sysflash_base + SYSFLASH_SR) = int_status;

  /* Ahoy the callback function */
  if ((int_status & SYSFLASH_SR_PROG_PAGE_DONE) &&
      (NULL != sysflash_callbacks.sysflash_prog_page_done_irq_callback)) {
    sysflash_callbacks.sysflash_prog_page_done_irq_callback(data_arg);
  }
  if ((int_status & SYSFLASH_SR_READ_P_SETUP_DONE) &&
      (NULL != sysflash_callbacks.sysflash_read_p_setup_done_irq_callback)) {
    sysflash_callbacks.sysflash_read_p_setup_done_irq_callback(data_arg);
  }
  if ((int_status & SYSFLASH_SR_ERASE_BLOCK_DONE) &&
      (NULL != sysflash_callbacks.sysflash_erase_block_done_irq_callback)) {
    sysflash_callbacks.sysflash_erase_block_done_irq_callback(data_arg);
  }
  if ((int_status & SYSFLASH_SR_RESET_DONE) &&
      (NULL != sysflash_callbacks.sysflash_reset_done_irq_callback)) {
    sysflash_callbacks.sysflash_reset_done_irq_callback(data_arg);
  }
  if ((int_status & SYSFLASH_SR_READ_STATUS_DONE) &&
      (NULL != sysflash_callbacks.sysflash_read_status_done_irq_callback)) {
    sysflash_callbacks.sysflash_read_status_done_irq_callback(data_arg);
  }
  if ((int_status & SYSFLASH_SR_SET_FEATURE_DONE) &&
      (NULL != sysflash_callbacks.sysflash_set_feature_done_irq_callback)) {
    sysflash_callbacks.sysflash_set_feature_done_irq_callback(data_arg);
  }
}

int32_t sysflash_reset_target(int32_t target)
{

  /* Sanity check target */
  if ((SYSFLASH_EXT_ADDR_TARGET_CE != target) &&
      (SYSFLASH_EXT_ADDR_TARGET_CE2 != target)) {
    return -EINVAL;
  }

  /* Abort ongoing read/program */
  sysflash_stop_read_program();

  /* Select target */
  REG32(_sysflash_base + SYSFLASH_EXT_ADDR) = SYSFLASH_EXT_ADDR_TARGET & target;

  /* Clear status register by writing to all flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;

  /* Issue reset command */
  REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_RESET;

  return ENOERR;
}

inline void sysflash_stop_read_program(void)
{
  REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_STOP_RP;
}

inline int32_t sysflash_set_page_spare_area_addr(uint32_t page_num)
{
  int32_t ret;

  ret = sysflash_set_page_addr(page_num);

  if (ENOERR != ret) {
    return ret;
  }

  /* Increment address to the raw page end (spare area start). */
  if (global_chip_type == MT29F64G08AFAAA) {
    REG32(_sysflash_base + SYSFLASH_ADDR) += SYSFLASH_8GB_RAW_PAGE_SIZE;
  } else {
    REG32(_sysflash_base + SYSFLASH_ADDR) += SYSFLASH_4GB_RAW_PAGE_SIZE;
  }

  return ENOERR;
}

/* Page numbers are counted over a whole chip, i.e. both LUNs together */
int32_t sysflash_set_page_addr(uint32_t page_num)
{
  uint32_t target;

  target = SYSFLASH_EXT_ADDR_TARGET_CE;

  /* Sanity check page number */
  if (page_num > (SYSFLASH_MAX_NO_PAGES - 1)) {
    return -EINVAL;
  }

  if (page_num >= SYSFLASH_PAGES_PER_LUN) {
    target = SYSFLASH_EXT_ADDR_TARGET_CE2;
    page_num -= SYSFLASH_PAGES_PER_LUN;
  }

  /* Set target */
  REG32(_sysflash_base + SYSFLASH_EXT_ADDR) = SYSFLASH_EXT_ADDR_TARGET & target;

  /* Set address */
  if (global_chip_type == MT29F64G08AFAAA) {
    /* 33 bits in address, LSB bits 0-31 in address register, MSB bit 32 in extended
     * address register.
     */
    const uint64_t address = (uint64_t)page_num * SYSFLASH_8GB_PAGE_ADDRESS_STEP;
    REG32(_sysflash_base + SYSFLASH_ADDR) = (uint32_t)address;
    REG32(_sysflash_base + SYSFLASH_EXT_ADDR) |=
          ((address >> 32) & 1) << SYSFLASH_EXT_ADDR_8GB_EXTENDED_ADDRESS_SHIFT;
  } else {
    REG32(_sysflash_base + SYSFLASH_ADDR) = page_num * SYSFLASH_4GB_PAGE_ADDRESS_STEP;
  }

  return ENOERR;
}

int32_t sysflash_read_status(uint8_t  *chip_status,
                             uint16_t *ctrl_status)
{
  /* Make sure the controller is not busy */
  if (0 != (REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_BUSY)) {
    return -EBUSY;
  }

  REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_READ_STATUS;

  *chip_status = REG32(_sysflash_base + SYSFLASH_DIO) & SYSFLASH_STATUS_MASK;
  /* Read controller status register, mask out reserved bits */
  *ctrl_status = REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_VALID_BITS_MASK;

  /* Clear read status flag */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_READ_STATUS_DONE;

  return ENOERR;
}

int32_t sysflash_read_id(sysflash_cid_t *cid)
{
  uint32_t  i;

  /* Make sure the controller is not busy */
  if (0 != (REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_BUSY)) {
    return -EBUSY;
  }

  REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_READ_ID;

  for (i = 0; i < SYSFLASH_CID_SIZE; i++) {
    cid->chip0[i] = REG32(_sysflash_base + SYSFLASH_DIO);
  }

  /* Clear status flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;

  return ENOERR;
}

static sysflash_chip_type_t sysflash_determine_chip_type(sysflash_cid_t chip_id)
{
  if (chip_id.chip0[0] == sysflash_cid_MT29F32G08AFABA[0] &&
      chip_id.chip0[1] == sysflash_cid_MT29F32G08AFABA[1]) {
    return MT29F32G08AFABA;
  }

  if (chip_id.chip0[0] == sysflash_cid_MT29F64G08AFAAA[0] &&
      chip_id.chip0[1] == sysflash_cid_MT29F64G08AFAAA[1]) {
    return MT29F64G08AFAAA;
  }

  return INVALID_TYPE;
}

inline int32_t sysflash_verify_command(void)
{
  uint8_t  chip_status;
  uint16_t ctrl_status;

  chip_status = 0;
  ctrl_status = 0;

  if (sysflash_read_status(&chip_status, &ctrl_status) < 0) {
    return -EBUSY;
  }

  if (chip_status & SYSFLASH_STATUS_FAIL) {
    return -EIO;
  }

  return ENOERR;
}

int32_t sysflash_erase_block(uint32_t block_num)
{
  uint32_t ret;

  /* Make sure the controller is not busy */
  if (0 != (REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_BUSY)) {
    return -EBUSY;
  }

  /* Clear status flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;

  /* Set address (includes sanity check of block_num) */
  ret = sysflash_set_page_addr(block_num * SYSFLASH_PAGES_PER_BLOCK);
  if (ENOERR != ret) {
    return ret;
  }

  /* Issue erase command */
  REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_ERASE_BLOCK;

  return ENOERR;
}

int32_t sysflash_read_page_setup(uint32_t page_num)
{
  int32_t ret;

  /* Make sure the controller is not busy */
  if (0 != (REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_BUSY)) {
    return -EBUSY;
  }

  /* Clear status flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;

  /* Store start address to read in the address register */
  ret = sysflash_set_page_addr(page_num);
  if (ENOERR != ret) {
    return ret;
  }

  /* Issue the Read Page command */
  REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_EDAC_ILV | SYSFLASH_CMD_READ_PAGE;

  return ENOERR;
}

int32_t sysflash_read_page(uint8_t *data_buf,
                           uint32_t size)
{
  uint32_t i;
  uint32_t *data32;
  uint32_t data_word;

  /* Sanity check data_buf */
  if (NULL == data_buf) {
    return -EINVAL;
  }

  /* Sanity check data size */
  if ((size > SYSFLASH_PAGE_SIZE) || (0 == size)) {
    return -EINVAL;
  }

  /* If the data buffer is 4-byte aligned and the size is an integer multiple of
     4 we get the most efficient 32-bit operation */
  if (0 == ((uintptr_t)(const void *)data_buf % SYSFLASH_WORD_SIZE) &&
      0 == size%SYSFLASH_WORD_SIZE) {
    data32 = (uint32_t *) data_buf;
    i = 0;
    while (i < size/SYSFLASH_WORD_SIZE) {
      data32[i] = REG32(_sysflash_base + SYSFLASH_DIO);
      i++;
    }
  }
  else {
    /* Data buffer not aligned/size not even -> more complex handling is needed */
    i = 0;
    while (size - i >= SYSFLASH_WORD_SIZE) {
      data_word = REG32(_sysflash_base + SYSFLASH_DIO);

      data_buf[i+0] = 0xFF & (data_word>>24);
      data_buf[i+1] = 0xFF & (data_word>>16);
      data_buf[i+2] = 0xFF & (data_word>>8);
      data_buf[i+3] = 0xFF & (data_word>>0);

      i += SYSFLASH_WORD_SIZE;
    }

    if (size - i == 3) {
      data_word = REG32(_sysflash_base + SYSFLASH_DIO);

      data_buf[i+0] = 0xFF & (data_word>>24);
      data_buf[i+1] = 0xFF & (data_word>>16);
      data_buf[i+2] = 0xFF & (data_word>>8);
    }
    else if (size - i == 2) {
      data_word = REG32(_sysflash_base + SYSFLASH_DIO);

      data_buf[i+0] = 0xFF & (data_word>>24);
      data_buf[i+1] = 0xFF & (data_word>>16);
    }
    else if (size - i == 1) {
      data_word = REG32(_sysflash_base + SYSFLASH_DIO);

      data_buf[i+0] = 0xFF & (data_word>>24);
    }
  }

  sysflash_stop_read_program();

  return ENOERR;
}

int32_t sysflash_factory_bad_block_check(uint32_t block_num)
{
  /* The bad block marker is the first byte of the spare area of the
   * first page of every block. If that is 0x00 the block is bad.
   */

  /* Only the first byte is strictly needed, but it is more efficient to read
     full 32-bit words. */
  uint8_t data_buf[SYSFLASH_WORD_SIZE] __attribute__ ((aligned (4)));

  int32_t ret;

  /* Sanity check block_num */
  if (block_num > (SYSFLASH_BLOCKS - 1)) {
    return -EINVAL;
  }

  /* Read first byte of first-page-of-block spare area */
  ret = sysflash_read_page_spare_area_poll(block_num * SYSFLASH_PAGES_PER_BLOCK,
                                           1, /* use raw reading */
                                           &data_buf[0],
                                           SYSFLASH_WORD_SIZE);
  if (ENOERR != ret) {
    return ret;
  }
  /* Check values */
  if (data_buf[0] == 0x00) {
    return SYSFLASH_FACTORY_BAD_BLOCK_MARKED;
  }

  return SYSFLASH_FACTORY_BAD_BLOCK_CLEARED;
}

int32_t sysflash_read_page_spare_area_poll(uint32_t page_num,
                                           uint32_t raw,
                                           uint8_t *data_buf,
                                           uint32_t size)
{
  int32_t ret;
  uint32_t timeout;

  ret = sysflash_read_page_spare_area_setup(page_num, raw);
    if (ENOERR != ret) {
    return ret;
  }

  /* Wait for Read Page Setup Done */
  timeout = 0;
  while ((REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_READ_P_SETUP_DONE) == 0 &&
         timeout < SYSFLASH_TIMEOUT) {
    timeout++;
  }

  if (timeout >= SYSFLASH_TIMEOUT) {
    /* Give the Stop read/program command to avoid the controller *
     * state machine getting stuck *
     */
    sysflash_stop_read_program();
    return -EIO;
  }

  /* Clear flag */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_READ_P_SETUP_DONE;

  return sysflash_read_page_spare_area(data_buf, size);
}

int32_t sysflash_read_page_spare_area_setup(uint32_t page_num,
                                            uint32_t raw)
{
  int32_t ret;

  /* raw is a boolean value */
  if (raw != 0 && raw != 1) {
    return -EINVAL;
  }

  /* Make sure the controller is not busy */
  if (0 != (REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_BUSY)) {
    return -EBUSY;
  }

  /* Clear status flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;

  /* Set address */
  ret = sysflash_set_page_spare_area_addr(page_num);
  if (ENOERR != ret) {
    return ret;
  }

  /* Issue read page command. For raw bytes disable edac and interleaving. */
  if (raw == 1) {
    REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_READ_PAGE;
  }
  else {
    REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_EDAC_ILV | SYSFLASH_CMD_READ_PAGE;
  }

  return ENOERR;
}

int32_t sysflash_read_page_spare_area(uint8_t *data_buf,
                                      uint32_t size)
{
  uint32_t i;
  uint32_t data_word;
  uint32_t *data32;

  /* Sanity check data_buf pointer */
  if (NULL == data_buf) {
    /* Signal to the controller that read is finished */
    sysflash_stop_read_program();
    return -EINVAL;
  }

  /* Sanity check size */
  if (size > SYSFLASH_PAGE_SPARE_AREA_SIZE ||
      0 == size)  {
    /* Signal to the controller that read is finished */
    sysflash_stop_read_program();
    return -EINVAL;
  }

  /* Loop to read data */
  /* If the data buffer is 4-byte aligned and the size is an integer multiple of
     4 we get the most efficient 32-bit operation */
  if (0 == ((uintptr_t)(const void *)data_buf % SYSFLASH_WORD_SIZE) &&
      0 == size%SYSFLASH_WORD_SIZE) {
    data32 = (uint32_t *) data_buf;
    i = 0;
    while (i < size/SYSFLASH_WORD_SIZE) {
      data32[i] = REG32(_sysflash_base + SYSFLASH_DIO);
      i++;
    }
  }
  else {
    /* Data buffer not aligned/size not even -> more complex handling is needed */
    i = 0;
    while (size - i >= SYSFLASH_WORD_SIZE) {
      data_word = REG32(_sysflash_base + SYSFLASH_DIO);

      data_buf[i+0] = 0xFF & (data_word>>24);
      data_buf[i+1] = 0xFF & (data_word>>16);
      data_buf[i+2] = 0xFF & (data_word>>8);
      data_buf[i+3] = 0xFF & (data_word>>0);

      i += SYSFLASH_WORD_SIZE;
    }

    if (size - i == 3) {
      data_word = REG32(_sysflash_base + SYSFLASH_DIO);

      data_buf[i+0] = 0xFF & (data_word>>24);
      data_buf[i+1] = 0xFF & (data_word>>16);
      data_buf[i+2] = 0xFF & (data_word>>8);
    }
    else if (size - i == 2) {
      data_word = REG32(_sysflash_base + SYSFLASH_DIO);

      data_buf[i+0] = 0xFF & (data_word>>24);
      data_buf[i+1] = 0xFF & (data_word>>16);
    }
    else if (size - i == 1) {
      data_word = REG32(_sysflash_base + SYSFLASH_DIO);

      data_buf[i+0] = 0xFF & (data_word>>24);
    }
  }

  /* Signal to the controller that read is finished */
  sysflash_stop_read_program();

  return ENOERR;
}


int32_t sysflash_program_page(uint32_t page_num,
                              const uint8_t *data_buf,
                              uint32_t size)
{
  int32_t ret;
  uint32_t i;
  uint32_t data_word;
  uint32_t *data32;

  /* Sanity check data size */
  if ((size > SYSFLASH_PAGE_SIZE) ||
      (0 == size)) {
    return -EINVAL;
  }

  /* Sanity check data_buf */
  if (NULL == data_buf) {
    return -EINVAL;
  }

  /* Make sure the controller is not busy */
  if (0 != (REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_BUSY)) {
    return -EBUSY;
  }

  /* Clear status flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;

  /* Set address */
  ret = sysflash_set_page_addr(page_num);
  if (ENOERR != ret) {
    return ret;
  }

  /* Issue Program Page command */
  REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_PROG_PAGE;

  /* Push data to the flash controller */
  /* If the data buffer is 4-byte aligned and the size is an integer multiple of
     4 we get the most efficient 32-bit operation */
  if (0 == ((uintptr_t)(const void *)data_buf % SYSFLASH_WORD_SIZE) &&
      0 == size%SYSFLASH_WORD_SIZE) {
    data32 = (uint32_t *) data_buf;
    i = 0;
    while (i < size/SYSFLASH_WORD_SIZE) {
      REG32(_sysflash_base + SYSFLASH_DIO) = data32[i];
      i++;
    }
  }
  else {
    /* Data buffer not aligned/size not even -> more complex handling is needed */
    i = 0;
    while (size - i >= SYSFLASH_WORD_SIZE) {
      data_word  = data_buf[i+0]<<24;
      data_word |= data_buf[i+1]<<16;
      data_word |= data_buf[i+2]<<8;
      data_word |= data_buf[i+3]<<0;

      REG32(_sysflash_base + SYSFLASH_DIO) = data_word;
      i += SYSFLASH_WORD_SIZE;
    }

    if (size - i == 3) {
      data_word  = 0;
      data_word |= data_buf[i+0]<<24;
      data_word |= data_buf[i+1]<<16;
      data_word |= data_buf[i+2]<<8;

      REG32(_sysflash_base + SYSFLASH_DIO) = data_word;
    }
    else if (size - i == 2) {
      data_word  = 0;
      data_word |= data_buf[i+0]<<24;
      data_word |= data_buf[i+1]<<16;

      REG32(_sysflash_base + SYSFLASH_DIO) = data_word;
    }
    else if (size - i == 1) {
      data_word  = 0;
      data_word |= data_buf[i+0]<<24;

      REG32(_sysflash_base + SYSFLASH_DIO) = data_word;
    }
  }

  sysflash_stop_read_program();

  return ENOERR;
}

int32_t sysflash_program_page_spare_area(uint32_t page_num,
                                         const uint8_t *data_buf,
                                         uint32_t size)
{
  int32_t  ret;
  uint32_t i;
  uint32_t data_word;
  uint32_t *data32;

  /* Sanity check data_buf pointer */
  if (NULL == data_buf) {
    return -EINVAL;
  }

  /* Sanity check size */
  if (size > SYSFLASH_PAGE_SPARE_AREA_SIZE ||
      size == 0) {
    return -EINVAL;
  }

  /* Make sure the controller is not busy */
  if (0 != (REG32(_sysflash_base + SYSFLASH_SR) & SYSFLASH_SR_BUSY)) {
    return -EBUSY;
  }

  /* Clear status flags */
  REG32(_sysflash_base + SYSFLASH_SR) = SYSFLASH_SR_IFLAGS_MASK;

  /* Set address */
  ret = sysflash_set_page_spare_area_addr(page_num);
  if (ENOERR != ret) {
    return ret;
  }

  /* Issue Program Page command */
  REG32(_sysflash_base + SYSFLASH_CMD) = SYSFLASH_CMD_PROG_PAGE;

  /* Push data to the flash controller */
  /* If the data buffer is 4-byte aligned and the size is an integer multiple of
     4 we get the most efficient 32-bit operation */
  if (0 == ((uintptr_t)(const void *)data_buf % SYSFLASH_WORD_SIZE) &&
      0 == size%SYSFLASH_WORD_SIZE) {
    data32 = (uint32_t *) data_buf;
    i = 0;
    while (i < size/SYSFLASH_WORD_SIZE) {
      REG32(_sysflash_base + SYSFLASH_DIO) = data32[i];
      i++;
    }
  }
  else {
    /* Data buffer not aligned/size not even -> more complex handling is needed */
    i = 0;
    while (size - i >= SYSFLASH_WORD_SIZE) {
      data_word  = data_buf[i+0]<<24;
      data_word |= data_buf[i+1]<<16;
      data_word |= data_buf[i+2]<<8;
      data_word |= data_buf[i+3]<<0;

      REG32(_sysflash_base + SYSFLASH_DIO) = data_word;
      i += SYSFLASH_WORD_SIZE;
    }

    if (size - i == 3) {
      data_word  = 0;
      data_word |= data_buf[i+0]<<24;
      data_word |= data_buf[i+1]<<16;
      data_word |= data_buf[i+2]<<8;

      REG32(_sysflash_base + SYSFLASH_DIO) = data_word;
    }
    else if (size - i == 2) {
      data_word  = 0;
      data_word |= data_buf[i+0]<<24;
      data_word |= data_buf[i+1]<<16;

      REG32(_sysflash_base + SYSFLASH_DIO) = data_word;
    }
    else if (size - i == 1) {
      data_word  = 0;
      data_word |= data_buf[i+0]<<24;

      REG32(_sysflash_base + SYSFLASH_DIO) = data_word;
    }
  }

  sysflash_stop_read_program();

  return ENOERR;
}
