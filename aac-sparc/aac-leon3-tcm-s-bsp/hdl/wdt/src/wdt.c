/*
 * Copyright (C) 2015 ÅAC Microtec AB
 *
 * Filename:    wdt.c
 * Module name: wdt
 *
 * Author(s):    Erik Zachrisson <erik.zachrisson@aacmicrotec.com>
 * Description:  Simple functions implementing on/off, reset and kick functionality.
 * Requirements: WDT IP Detailed Design Document
 */

#include "toolchain_support.h"
#include "wdt.h"

extern uint32_t _wd_base;
#define WDT_BASE _wd_base

void wdt_enable(void)
{
  REG32(WDT_BASE + WDT_CTRL_REG_ADR) = WDT_CTRL_ENABLE;
}

void wdt_disable(void)
{
  REG32(WDT_BASE + WDT_CTRL_REG_ADR) = WDT_CTRL_DISABLE;
}

void wdt_kick(void)
{
  REG32(WDT_BASE + WDT_KICK_REG_ADR) = WDT_KICK_COMMAND;
}

void wdt_set(uint32_t seconds)
{
  REG32(WDT_BASE + WDT_SECONDS_REG_ADR) = seconds;
}
