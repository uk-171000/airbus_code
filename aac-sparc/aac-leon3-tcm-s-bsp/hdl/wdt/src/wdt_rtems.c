/******************************************************************************
 * Copyright (C) 2015 ÅAC Microtec AB
 *
 *
 * Filename: watchdog_rtems.c
 *
 * Author(s): Erik Zachrisson <erik.zachrisson@aacmicrotec.com>
 * Description: AAC watchdog RTEMS driver implementation
 * Requirements: Bare metal watchdog driver
 *****************************************************************************/

#include <inttypes.h>
#include <rtems/libio.h>
#include <rtems/seterr.h>
#include <bsp.h>

#include "wdt.h"
#include "wdt_rtems.h"
#include "toolchain_support.h"

const uint32_t _wd_base = AAC_BSP_WDT_BASE;

/*
 * Access functions
 */
rtems_device_driver rtems_watchdog_initialize(rtems_device_major_number major,
                                              rtems_device_minor_number unused,
                                              void *args)
{
  rtems_status_code rtems_result;

  DBG(DBG_SEVERITY_INFO, "Registering watchdog driver (M%d:0).", major, 0);

  rtems_result = rtems_io_register_name(RTEMS_WATCHDOG_DEVICE_NAME, major, 0);
  if(rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register watchdog driver (%d)", rtems_result);
    rtems_set_errno_and_return_minus_one(ENODEV);
  }

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_watchdog_open(rtems_device_major_number major,
                                        rtems_device_minor_number unused,
                                        void *args)
{
  DBG(DBG_SEVERITY_INFO, "Opening watchdog device (M%d:m%d).", major, unused);
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_watchdog_close(rtems_device_major_number major,
                                         rtems_device_minor_number unused,
                                         void *args)
{
  DBG(DBG_SEVERITY_INFO, "Closing watchdog device (M%d:m%d).", major, unused);
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_watchdog_read(rtems_device_major_number major,
                                        rtems_device_minor_number unused,
                                        void *args)
{
  DBG(DBG_SEVERITY_INFO, "Reading from watchdog device (M%d:m%d). This is not supported",
      major, unused);
  rtems_set_errno_and_return_minus_one(EPERM);

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_watchdog_write(rtems_device_major_number major,
                                         rtems_device_minor_number unused,
                                         void *args)
{
  rtems_libio_rw_args_t *rw_args;
  rw_args = (rtems_libio_rw_args_t *) args;

  DBG(DBG_SEVERITY_INFO, "Write to watchdog (M%d:m%d).", major, unused);
  DBG(DBG_SEVERITY_INFO, "flag %d\r\n", rw_args->flags);

  if((rw_args->flags & LIBIO_FLAGS_OPEN)  == false) {
    DBG(DBG_SEVERITY_WARN, "Watchdog driver [%d,%d] not opened. Aborting request.",
        major, unused);
    rtems_set_errno_and_return_minus_one(EPERM);
  }

  wdt_kick();
  DBG(DBG_SEVERITY_INFO, "Successfully kicked watchdog (M%d:m%d).", major, unused);

  rw_args->bytes_moved = rw_args->count;

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_watchdog_control(rtems_device_major_number major,
                                           rtems_device_minor_number unused,
                                           void *args)
{
  DBG(DBG_SEVERITY_INFO, "Setting control options to device (M%d:m%d).", major, unused);
  rtems_libio_ioctl_args_t *rw_args = (rtems_libio_ioctl_args_t *) args;
  uint32_t val = (uint32_t) rw_args->buffer;

  switch (rw_args->command) {
  case WATCHDOG_ENABLE_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: WATCHDOG_ENABLE_IOCTL %u", val);
    rw_args->ioctl_return = RTEMS_SUCCESSFUL;

    switch (val) {
    case 0:
      wdt_disable();
      break;

    case 1:
      wdt_enable();
      break;

    default:
      rtems_set_errno_and_return_minus_one(EINVAL);
      break;
    }
    break;

  case WATCHDOG_SET_TIMEOUT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: WATCHDOG_SET_TIMEOUT_IOCTL %u", val);
    if (val > WDT_MAX_TIMEOUT_S) {
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    wdt_set(val);
    rw_args->ioctl_return = RTEMS_SUCCESSFUL;
    break;

  default:
    rw_args->ioctl_return = 0;
    return RTEMS_NOT_DEFINED;
    break;
  }
  return RTEMS_SUCCESSFUL;
}
