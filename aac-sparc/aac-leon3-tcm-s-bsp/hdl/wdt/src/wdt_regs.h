/*
 * Copyright (C) 2015 ÅAC Microtec AB
 *
 * Filename:    wdt_regs.h
 * Module name: wdt
 *
 * Author(s):    Erik Zachrisson <erik.zachrisson@aacmicrotec.com>
 * Description:  Describes the watchdog registers
 * Requirements: WDT IP Detailed Design Document
 */

#ifndef _WDT_REGS_H_
#define _WDT_REGS_H_

// Addresses
#define WDT_CTRL_REG_ADR                 0x00
#define WDT_KICK_REG_ADR                 0x0C
#define WDT_SECONDS_REG_ADR              0x10

// Commands
#define WDT_CTRL_ENABLE                  (1 << 0)
#define WDT_CTRL_DISABLE                 (0 << 0)

#define WDT_KICK_COMMAND                 (1 << 0)

#define WDT_MAX_TIMEOUT_S                255

#endif
