/*
 * Copyright (C) 2015 ÅAC Microtec AB
 *
 * Filename:    wdt.h
 * Module name: wdt
 *
 * Author(s):    Erik Zachrisson <erik.zachrisson@aacmicrotec.com>
 * Description:  Describes the external functions
 * Requirements: WDT IP Detailed Design Document
 */

#ifndef _WDT_H_
#define _WDT_H_

#include "wdt_regs.h"
#include <stdint.h>

/*
 * Name:        wdt_enable
 * @brief       Enables the watchdog
 */
void wdt_enable(void);

/* Name:        wdt_disable
 * @brief       Disables the watchdog
 */
void wdt_disable(void);

/* Name:        wdt_kick
 * @brief       Kicks the watchdog
 */
void wdt_kick(void);

/* Name:        wdt_set
 * @brief       Kicks the watchdog
 * @param[in]   seconds [in] The number of seconds until the watchdog times out
 */
void wdt_set(uint32_t seconds);

#endif
