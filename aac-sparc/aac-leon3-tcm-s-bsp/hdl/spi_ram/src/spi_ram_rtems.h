/*------------------------------------------------------------------------------
 * Copyright (C) 2018 ÅAC Microtec AB
 *
 * Filename:     spi_ram_rtems.h
 * Module name:  spi_ram
 *
 * Author(s):    CoSj, JoZe, DaHe
 * Support:      support@aacmicrotec.com
 * Description:  SPI RAM RTEMS driver implementation
 * Requirements: Adheres to the ÅAC SPI RAM Driver Detailed Design Description Rev. A
 *----------------------------------------------------------------------------*/

#ifndef _RTEMS_SPI_RAM_H_
#define _RTEMS_SPI_RAM_H_

/* SPI RAM IOCTL EDAC */
typedef enum {
  SPI_RAM_IOCTL_EDAC_ENABLE,
  SPI_RAM_IOCTL_EDAC_DISABLE
} spi_ram_ioctl_edac_e;

/* EDAC Status */
#define SPI_RAM_EDAC_STATUS_MULT_ERROR         (0x04U)
#define SPI_RAM_EDAC_STATUS_DOUBLE_ERROR       (0x02U)
#define SPI_RAM_EDAC_STATUS_SINGLE_ERROR       (0x01U)

/* Debug detect Status */
#define SPI_RAM_DEBUG_DETECT_TRUE              (0x01U)
#define SPI_RAM_DEBUG_DETECT_FALSE             (0x00U)

#define SPI_RAM_DEVICE_NAME      "/dev/spi_ram"

#define SPI_RAM_SET_EDAC_IOCTL         _IOW('G', 1, uint32_t)
#define SPI_RAM_SET_DIVISOR_IOCTL      _IOW('G', 2, uint32_t)
#define SPI_RAM_GET_EDAC_STATUS_IOCTL  _IOR('G', 3, uint32_t)
#define SPI_RAM_GET_DEBUG_DETECT_IOCTL _IOR('G', 4, uint32_t)

rtems_device_driver rtems_spi_ram_initialize(rtems_device_major_number major,
                                             rtems_device_minor_number minor,
                                             void *args);

/* As the driver registers as an IMFS generic node, the driver table only needs
   to handle the initialization */
#define AAC_SPI_RAM_DRIVER_TABLE_ENTRY { \
      rtems_spi_ram_initialize,          \
      NULL,                              \
      NULL,                              \
      NULL,                              \
      NULL,                              \
      NULL }

#endif /* _RTEMS_SPI_RAM_H_ */
