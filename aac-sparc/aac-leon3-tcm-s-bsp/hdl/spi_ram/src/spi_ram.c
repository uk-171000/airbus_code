/*-----------------------------------------------------------------------------
 * Copyright (C) 2018 ÅAC Microtec AB
 *
 * Filename:     spi_ram.c
 * Module name:  spi_ram
 *
 * Author(s):    CoSj, JoZe, DaHe
 * Support:      support@aacmicrotec.com
 * Description:  SPI RAM bare metal driver implementation
 * Requirements: Adheres to the ÅAC SPI RAM Driver Detailed Design Description Rev. A
 *----------------------------------------------------------------------------*/

#include "toolchain_support.h"
#include <errno.h>
#include <string.h>
#include "spi_ram.h"
#include "spi_ram_internal.h"

int32_t spi_ram_init(uint32_t base_addr)
{
  int32_t status;
  spi_ram_config_t config;

  status = 0;

  /* Set MRAM config reg to zero */
  spi_ram_write_protection(base_addr, SPI_RAM_WRITE_PROTECTION_DISABLE);
  spi_ram_status_reg_write(base_addr, 0x00);
  spi_ram_write_protection(base_addr, SPI_RAM_WRITE_PROTECTION_ENABLE);

  /* Set default config values */
  config.divisor      = 4;
  config.edac         = SPI_RAM_EDAC_ENABLE;
  status = spi_ram_config(base_addr, &config);

  return status;
}

int32_t spi_ram_config(uint32_t base_addr, const spi_ram_config_t *config)
{
  int32_t status;
  uint32_t spi_control_reg;

  status = 0;
  spi_control_reg = 0;

  /* Check parameter */
  if (NULL == config) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Null pointer in arguments!");
  }

  /* Read SPI control register */
  if (0 == status) {
    spi_control_reg = REG32(base_addr + SPI_RAM_SPI_CONTROL_REG);
  }

  /* Check divisor */
  if (0 == status) {
    if ( (config->divisor < 2) || (config->divisor > 50) ){
      status = -EINVAL;
      DBG(DBG_SEVERITY_ERR, "Divisor out of range (0x%X)", config->divisor);
    }
    else {
      spi_control_reg &= ~SPI_RAM_SPI_CONTROL_DIVISOR_MASK;
      spi_control_reg |= (config->divisor << SPI_RAM_SPI_CONTROL_DIVISOR_POS);
    }
  }

  /* Check EDAC */
  if (0 == status) {
    switch (config->edac) {
      case SPI_RAM_EDAC_ENABLE:
        spi_control_reg &= ~SPI_RAM_SPI_CONTROL_EDAC_MASK;
        spi_control_reg |= SPI_RAM_SPI_CONTROL_EDAC_ENABLE;
        break;
      case SPI_RAM_EDAC_DISABLE:
        spi_control_reg &= ~SPI_RAM_SPI_CONTROL_EDAC_MASK;
        spi_control_reg |= SPI_RAM_SPI_CONTROL_EDAC_DISABLE;
        break;
      default:
        status = -EINVAL;
        DBG(DBG_SEVERITY_ERR, "Unknown EDAC setting (0x%X)", config->edac);
        break;
    }
  }

  /* Write SPI control register */
  if (0 == status) {
    REG32(base_addr + SPI_RAM_SPI_CONTROL_REG) = spi_control_reg;
  }

  return status;
}

int32_t spi_ram_write_protection(uint32_t base_addr, uint8_t write_protection)
{
  int32_t status = 0;

  switch (write_protection) {
    case SPI_RAM_WRITE_PROTECTION_DISABLE:
      REG32(base_addr + SPI_RAM_SPI_COMMAND_REG) = SPI_RAM_CMD_WRITE_ENABLE;
      break;
    case SPI_RAM_WRITE_PROTECTION_ENABLE:
      REG32(base_addr + SPI_RAM_SPI_COMMAND_REG) = SPI_RAM_CMD_WRITE_DISABLE;
      break;
    default:
      status = -EINVAL;
      DBG(DBG_SEVERITY_ERR,
          "Invalid write protection setting (0x%X)",
          write_protection);
      break;
  }

  return status;
}

int32_t spi_ram_status_reg_read(uint32_t base_addr, uint8_t *value)
{
  int32_t status;
  uint32_t local_data;

  status = 0;

  /* Check parameter */
  if (NULL == value) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Null pointer in arguments!");
  }

  /* Read chip status */
  if (0 == status) {
    /* Write command register */
    REG32(base_addr + SPI_RAM_SPI_COMMAND_REG) = SPI_RAM_CMD_READ_STATUS_REG;
    /* Read command register */
    local_data = REG32(base_addr + SPI_RAM_SPI_COMMAND_REG);
    /* Mask out status byte */
    *value = (local_data & SPI_RAM_SPI_COMMAND_STATUS_MASK);
  }

  return status;
}

void spi_ram_status_reg_write(uint32_t base_addr, uint8_t value)
{
  uint32_t local_data;

  /* Assemble write status reg command */
  local_data = ((value << SPI_RAM_SPI_COMMAND_DATA_POS) & SPI_RAM_SPI_COMMAND_DATA_MASK);
  local_data |= SPI_RAM_CMD_WRITE_STATUS_REG;

  /* Set write enable latch and write status reg */
  REG32(base_addr + SPI_RAM_SPI_COMMAND_REG) = local_data;

}

int32_t spi_ram_data_read(uint32_t base_addr,
                          uint8_t *dest,
                          uint16_t src,
                          uint16_t size)
{
  int32_t status;
  uint32_t local_data;
  uint16_t cnt;
  uint32_t spi_cntrl;

  status = 0;

  spi_cntrl = REG32(base_addr + SPI_RAM_SPI_CONTROL_REG);

  /* Check parameter */
  if (NULL == dest) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Destination buffer is NULL");
  } else if (0 == size) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Size to read is 0");
  } else if (((spi_cntrl & SPI_RAM_SPI_CONTROL_EDAC_MASK) != 0) &&
             ((src + size) > SPI_RAM_EDAC_OFF_MAX_RANGE)) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR,
        "Invalid read access off the end of the NVRAM while EDAC is off. "
        "Max is 0x%08x-1, read ends on 0x%08x",
        SPI_RAM_EDAC_OFF_MAX_RANGE,
        src + size);
  } else if (((spi_cntrl & SPI_RAM_SPI_CONTROL_EDAC_MASK) == 0) &&
             ((src + size) > SPI_RAM_EDAC_ON_MAX_RANGE)) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR,
        "Invalid read access off the end of the NVRAM while EDAC is on. "
        "Max is 0x%08x-1, read ends on 0x%08x",
        SPI_RAM_EDAC_ON_MAX_RANGE,
        src + size);
  } else if (0 != (size % sizeof(uint32_t))) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Read access size is non-aligned");
  } else if (0 != (src % sizeof(uint32_t))) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Read access src address is non-aligned");
  }

  if (0 == status) {
    cnt = 0;
    do {
      local_data = REG32(base_addr + src + cnt);
      memcpy(&dest[cnt], &local_data, sizeof(uint32_t));
      cnt += sizeof(uint32_t);
    } while (cnt != size);
  }

  return status;
}

int32_t spi_ram_data_write(uint32_t base_addr,
                           uint16_t dest,
                           const uint8_t *src,
                           uint16_t size)
{
  int32_t status;
  uint32_t local_data;
  uint16_t cnt;
  uint32_t spi_cntrl;

  status = 0;

  spi_cntrl = REG32(base_addr + SPI_RAM_SPI_CONTROL_REG);

  /* Check parameter */
  if (NULL == src) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Source buffer is NULL");
  } else if (0 == size) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Size to write is 0");
  } else if (((spi_cntrl & SPI_RAM_SPI_CONTROL_EDAC_MASK) != 0) &&
             ((dest + size) > SPI_RAM_EDAC_OFF_MAX_RANGE)) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR,
        "Invalid write access off the end of the NVRAM while EDAC is off. "
        "Max is 0x%08x-1, write ends on 0x%08x",
        SPI_RAM_EDAC_OFF_MAX_RANGE,
        dest + size);
  } else if (((spi_cntrl & SPI_RAM_SPI_CONTROL_EDAC_MASK) == 0) &&
             ((dest + size) > SPI_RAM_EDAC_ON_MAX_RANGE)) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR,
        "Invalid write access off the end of the NVRAM while EDAC is on. "
        "Max is 0x%08x-1, write ends on 0x%08x",
        SPI_RAM_EDAC_ON_MAX_RANGE,
        dest + size);
  } else if (0 != (size % sizeof(uint32_t))) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Write access size is non-aligned");
  } else if (0 != (dest % sizeof(uint32_t))) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Write access dest address is non-aligned");
  }

  if (0 == status) {
    cnt = 0;
    do {
      memcpy(&local_data, &src[cnt], sizeof(uint32_t));
      REG32(base_addr + dest + cnt) = local_data;
      cnt += sizeof(uint32_t);
    } while (cnt != size);
  }

  return status;
}

int32_t spi_ram_edac_status_read(uint32_t base_addr, uint8_t *value)
{
  int32_t status;
  uint32_t edac_reg;

  status = 0;

  /* Check parameter */
  if (NULL == value) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Null pointer in arguments!");
  }

  /* Read EDAC status register */
  if (0 == status) {
    edac_reg = REG32(base_addr + SPI_RAM_EDAC_REG);
    *value = (edac_reg & 0xFF);
  }

  return status;
}

int32_t spi_ram_debug_detect_read(uint32_t base_addr, bool *value)
{
  int32_t status;
  uint32_t status_reg;

  status = 0;

  /* Check parameter */
  if (NULL == value) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Null pointer in arguments!");
  }

  /* Read debug detect */
  if (0 == status) {
    status_reg = REG32(base_addr + SPI_RAM_STATUS_REG);
    if ((status_reg & SPI_RAM_STATUS_DEBUG_DETECT_MASK) != 0) {
      *value =  true;
    }
    else {
      *value =  false;
    }
  }

  return status;
}

int32_t spi_ram_busy_read(uint32_t base_addr, bool *value)
{
  int32_t status;
  uint32_t status_reg;

  status = 0;

  /* Check parameter */
  if (NULL == value) {
    status = -EINVAL;
    DBG(DBG_SEVERITY_ERR, "Null pointer in arguments!");
  }

  if (0 == status) {
    status_reg = REG32(base_addr + SPI_RAM_STATUS_REG);

    /* Read busy status */
    if ((status_reg & SPI_RAM_STATUS_BUSY_MASK) != 0) {
      *value = true;
    }
    else {
      *value = false;
    }
  }

  return status;
}
