/*------------------------------------------------------------------------------
 * Copyright (C) 2016-2018 ÅAC Microtec AB
 *
 * Filename:     spi_ram_rtems.c
 * Module name:  spi_ram
 *
 * Author(s):    CoSj, JoZe, MaWe, DaHe
 * Support:      support@aacmicrotec.com
 * Description:  SPI RAM RTEMS driver implementation
 * Requirements: Adheres to the ÅAC SPI RAM Driver Detailed Design Description Rev. A
 *----------------------------------------------------------------------------*/

#include <rtems/imfs.h>
#include <rtems/libio.h>
#include <rtems/seterr.h>
#include <rtems/score/cpu.h>

#include <bsp.h>
#include <bsp/irq.h>
#include <inttypes.h>

#include "toolchain_support.h"
#include "spi_ram.h"
#include "spi_ram_rtems.h"

#define AAC_BSP_SPI_RAM_COUNT 1

typedef struct {
  const char *name;
  uint32_t base_addr;

  rtems_id rw_access_mutex;
  spi_ram_config_t config;

  uint8_t edac_read_status;
} ctx_t;

static ctx_t spi_ram_ctx[AAC_BSP_SPI_RAM_COUNT] = {
  {
    .name = SPI_RAM_DEVICE_NAME,
    .base_addr = AAC_BSP_NVRAM_BASE
  }
};

static int32_t lock_rw_access(ctx_t *ctx)
{
  rtems_status_code status;

  status = rtems_semaphore_obtain(ctx->rw_access_mutex,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to obtain mutex for SPI RAM driver (%s): %s)",
        ctx->name, rtems_status_text(status));
    return -1;
  }

  return 0;
}

static int32_t unlock_rw_access(ctx_t *ctx)
{
  rtems_status_code status;

  status = rtems_semaphore_release(ctx->rw_access_mutex);

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to release mutex for SPI RAM driver (%s): %s)",
        ctx->name, rtems_status_text(status));
    return -1;
  }

  return 0;
}

static int32_t data_write(ctx_t          *ctx,
                          const uint8_t  *buffer,
                          uint16_t        offset,
                          uint16_t        count)
{
  int32_t status;
  bool busy;

  if (spi_ram_write_protection(ctx->base_addr,
                               SPI_RAM_WRITE_PROTECTION_DISABLE) < 0) {
    DBG(DBG_SEVERITY_WARN,
        "SPI RAM driver (%s) failed disable write protection. Aborting request.",
        ctx->name);
    return -EINVAL;
  }

  /* Check busy before memory transfer */
  busy = true;
  while (busy){
    spi_ram_busy_read(ctx->base_addr, &busy);

  }
  status = spi_ram_data_write(ctx->base_addr,
                              offset,
                              buffer,
                              count);
  if (status < 0) {
    DBG(DBG_SEVERITY_WARN, "SPI RAM driver (%s) write data failed",
        ctx->name);
  }

  if (spi_ram_write_protection(ctx->base_addr,
                               SPI_RAM_WRITE_PROTECTION_ENABLE) < 0) {
    DBG(DBG_SEVERITY_WARN,
        "SPI RAM driver (%s) failed enable write protection.",
        ctx->name);
  }

  return status;
}

static int32_t configure(ctx_t *ctx,
                         const spi_ram_config_t *const config)
{
  if (spi_ram_config(ctx->base_addr, config) < 0) {
    DBG(DBG_SEVERITY_WARN,
        "SPI RAM driver (%s) failed to configure. Aborting request.",
        ctx->name);
    return -EINVAL;
  }

  ctx->config = *config;

  return 0;
}

static ssize_t rtems_spi_ram_read(rtems_libio_t *iop,
                                  void          *buffer,
                                  size_t         count)
{
  ctx_t *ctx;
  uint8_t edac_status;
  int32_t read_result;
  int32_t status_read_result;
  bool busy;

  ctx = (ctx_t *) IMFS_generic_get_context_by_iop(iop);

  DBG(DBG_SEVERITY_INFO, "Reading from SPI RAM device (%s), offset 0x%x.",
      ctx->name,
      iop->offset);

  if (lock_rw_access(ctx) < 0) {
      printf("spi_ram_read %d\n", __LINE__);
    rtems_set_errno_and_return_minus_one(ENODEV);
  }

  /* Check busy before memory transfer */
  busy = true;
  while (busy){
    spi_ram_busy_read(ctx->base_addr, &busy);

  }
  read_result = spi_ram_data_read(ctx->base_addr,
                                  buffer,
                                  iop->offset,
                                  count);

  status_read_result = spi_ram_edac_status_read(ctx->base_addr,
                                                &edac_status);
  ctx->edac_read_status = edac_status;
  if (unlock_rw_access(ctx) < 0) {
      printf("spi_ram_read %d\n", __LINE__);
    rtems_set_errno_and_return_minus_one(ENODEV);
  }
  if (read_result < 0) {
      printf("spi_ram_read %d\n", __LINE__);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }
  if (status_read_result < 0) {
      printf("spi_ram_read %d\n", __LINE__);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }
  switch (edac_status) {
  case 0:
    /* success */
    break;

  case SPI_RAM_EDAC_STATUS_SINGLE_ERROR:
  case SPI_RAM_EDAC_STATUS_DOUBLE_ERROR:
    /* reported as semi-successful with both length returned and errno set */
    errno = EIO;
    break;

  case SPI_RAM_EDAC_STATUS_MULT_ERROR:
      printf("spi_ram_read %d\n", __LINE__);
    rtems_set_errno_and_return_minus_one(EIO);
  }

  iop->offset += count;
  return count;
}

static ssize_t rtems_spi_ram_write(rtems_libio_t *iop,
                                   const void    *buffer,
                                   size_t         count)
{
  ctx_t *ctx;
  int32_t status;
  uint16_t size;

  ctx = (ctx_t *) IMFS_generic_get_context_by_iop(iop);

  if(((ctx->config.edac == SPI_RAM_EDAC_ENABLE) &&
      (iop->offset > SPI_RAM_EDAC_ON_MAX_RANGE)) ||
      (iop->offset > SPI_RAM_EDAC_OFF_MAX_RANGE)) {
    DBG(DBG_SEVERITY_ERR, "Offset 0x%X out of range.", iop->offset);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  if((ctx->config.edac == SPI_RAM_EDAC_ENABLE) &&
     (iop->offset + count > SPI_RAM_EDAC_ON_MAX_RANGE)) {
    size = SPI_RAM_EDAC_ON_MAX_RANGE - iop->offset;
    DBG(DBG_SEVERITY_INFO, "Adjusting write size to %d.", size);
  }
  else if (iop->offset + count > SPI_RAM_EDAC_OFF_MAX_RANGE) {
    size = SPI_RAM_EDAC_OFF_MAX_RANGE - iop->offset;
    DBG(DBG_SEVERITY_INFO, "Adjusting write size to %d.", size);
  }
  else {
    size = count;
  }

  DBG(DBG_SEVERITY_INFO,
      "Writing to SPI RAM device (%s), offset 0x%x, size %d.",
      ctx->name,
      iop->offset,
      size);

  if (lock_rw_access(ctx) < 0) {
    rtems_set_errno_and_return_minus_one(ENODEV);
}

  status = data_write(ctx, buffer, iop->offset, size);

  if (unlock_rw_access(ctx) < 0) {
    rtems_set_errno_and_return_minus_one(ENODEV);
  }
  if (status < 0) {
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  iop->offset += size;
  return size;
}

static int rtems_spi_ram_ioctl(rtems_libio_t   *iop,
                               ioctl_command_t  command,
                               void            *buffer)
{
  ctx_t *ctx;
  uint32_t value;
  spi_ram_config_t config;
  int32_t status;
  bool debug_detect;
  uint8_t edac_status;

  ctx = (ctx_t *) IMFS_generic_get_context_by_iop(iop);

  value = (uint32_t) buffer;

  DBG(DBG_SEVERITY_INFO, "Setting SPI RAM control options for device (%s).",
      ctx->name);

  switch(command) {
  case SPI_RAM_SET_EDAC_IOCTL:
  {
    switch(value) {
    case SPI_RAM_IOCTL_EDAC_ENABLE:
      config.edac = SPI_RAM_EDAC_ENABLE;
      break;

    case SPI_RAM_IOCTL_EDAC_DISABLE:
      config.edac = SPI_RAM_EDAC_DISABLE;
      break;

    default:
      DBG(DBG_SEVERITY_WARN, "Unknown EDAC setting 0x%08X. Ignoring request",
          value);
      rtems_set_errno_and_return_minus_one(EINVAL);
      break;
    }

    if (lock_rw_access(ctx)) {
      rtems_set_errno_and_return_minus_one(ENODEV);
    }
    config.divisor = ctx->config.divisor;
    status = configure(ctx, &config);
    if (unlock_rw_access(ctx)) {
      rtems_set_errno_and_return_minus_one(ENODEV);
    }
    if (status < 0) {
      rtems_set_errno_and_return_minus_one(EINVAL);
    }

    break;
  }

  case SPI_RAM_SET_DIVISOR_IOCTL:
  {
    if ( (value < 2) || (value > 50) ){
      DBG(DBG_SEVERITY_WARN, "Divisor value %" PRIu32 " out of range. "
          "Ignoring request", value);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }

    config.divisor = value;

    if (lock_rw_access(ctx)) {
      rtems_set_errno_and_return_minus_one(ENODEV);
    }
    config.edac = ctx->config.edac;
    status = configure(ctx, &config);
    if (unlock_rw_access(ctx)) {
      rtems_set_errno_and_return_minus_one(ENODEV);
    }
    if (status < 0) {
      rtems_set_errno_and_return_minus_one(EINVAL);
    }

    break;
  }

  case SPI_RAM_GET_EDAC_STATUS_IOCTL:
  {
    if (lock_rw_access(ctx)) {
      rtems_set_errno_and_return_minus_one(ENODEV);
    }

    edac_status = ctx->edac_read_status;

    ctx->edac_read_status = 0;

    if (unlock_rw_access(ctx)) {
      rtems_set_errno_and_return_minus_one(ENODEV);
    }

    *(uint32_t *) buffer = edac_status;
    break;
  }

  /* Read debug detect */
  case SPI_RAM_GET_DEBUG_DETECT_IOCTL:
  {
    if (spi_ram_debug_detect_read(ctx->base_addr, &debug_detect) < 0) {
      DBG(DBG_SEVERITY_WARN,
          "SPI RAM driver (%s) failed to read debug detect. Aborting request.",
          ctx->name);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }

    *(uint32_t *) buffer = debug_detect;
    break;
  }

  default:
    DBG(DBG_SEVERITY_WARN, "Unknown ioctl command 0x%08X. Ignoring request.",
        command);
    rtems_set_errno_and_return_minus_one(EINVAL);
    break;
  }

  return 0;
}

static const rtems_filesystem_file_handlers_r rtems_spi_ram_handlers = {
  .open_h = rtems_filesystem_default_open,
  .close_h = rtems_filesystem_default_close,
  .read_h = rtems_spi_ram_read,
  .write_h = rtems_spi_ram_write,
  .ioctl_h = rtems_spi_ram_ioctl,
  .lseek_h = rtems_filesystem_default_lseek_file,
  .fstat_h = IMFS_stat,
  .ftruncate_h = rtems_filesystem_default_ftruncate,
  .fsync_h = rtems_filesystem_default_fsync_or_fdatasync,
  .fdatasync_h = rtems_filesystem_default_fsync_or_fdatasync,
  .fcntl_h = rtems_filesystem_default_fcntl,
  .readv_h = rtems_filesystem_default_readv,
  .writev_h = rtems_filesystem_default_writev,
};

static const IMFS_node_control
rtems_spi_ram_node_control = IMFS_GENERIC_INITIALIZER(
  &rtems_spi_ram_handlers,
  IMFS_node_initialize_generic,
  IMFS_node_destroy_default
);

rtems_device_driver rtems_spi_ram_initialize(rtems_device_major_number major,
                                             rtems_device_minor_number minor,
                                             void *args)
{
  int rv;
  rtems_status_code status;
  spi_ram_config_t config;

  config.edac = SPI_RAM_EDAC_ENABLE;
  config.divisor = 4;

  DBG(DBG_SEVERITY_INFO, "Initializing SPI RAM");

  for (minor = 0; minor < AAC_BSP_SPI_RAM_COUNT; minor++) {
    DBG(DBG_SEVERITY_INFO, "Registering SPI RAM driver for %s (M%lu:m%lu)",
        spi_ram_ctx[minor].name,
        major,
        minor);

    rv = IMFS_make_generic_node(spi_ram_ctx[minor].name,
                                S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO,
                                &rtems_spi_ram_node_control,
                                &spi_ram_ctx[minor]);
    if(rv < 0) {
      DBG(DBG_SEVERITY_ERR, "Failed to register SPI RAM driver (M%lu:m%lu): %s)",
          major, minor, strerror(errno));
      return RTEMS_UNSATISFIED /* ENODEV */;
    }

    status = rtems_semaphore_create(rtems_build_name('S', 'R', 'S', '0' + minor),
                                    1,
                                    RTEMS_DEFAULT_ATTRIBUTES | RTEMS_SIMPLE_BINARY_SEMAPHORE,
                                    0,
                                    &spi_ram_ctx[minor].rw_access_mutex);
    if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR,
          "Failed to create mutex for SPI RAM driver (M%lu:m%lu), error %d, %s)",
          major, minor, status, rtems_status_text(status));
      return RTEMS_UNSATISFIED; /* ENODEV */
    }

    if (spi_ram_init(spi_ram_ctx[minor].base_addr) < 0) {
      DBG(DBG_SEVERITY_WARN,
          "SPI RAM driver (M%lu:m%lu) failed to initialize driver. Aborting request.",
          major, minor);
    return RTEMS_INVALID_NODE; /* EINVAL */
    }

    if (configure(&spi_ram_ctx[minor], &config) < 0) {
      return RTEMS_INVALID_NODE; /* EINVAL */
    }
  }

  return RTEMS_SUCCESSFUL;
}
