/*-----------------------------------------------------------------------------
 * Copyright (C) 2018 ÅAC Microtec AB
 *
 * Filename:     spi_ram_internal.h
 * Module name:  spi_ram
 *
 * Author(s):    CoSj, JoZe, DaHe
 * Support:      support@aacmicrotec.com
 * Description:  SPI RAM bare metal driver definitions
 * Requirements: Adheres to the ÅAC SPI RAM Driver Detailed Design Description Rev. A
 *----------------------------------------------------------------------------*/

#ifndef _SPI_RAM_INTERNAL_H_
#define _SPI_RAM_INTERNAL_H_

/* SPI RAM register map */
#define SPI_RAM_REGISTER_OFFSET               (0x8000)
#define SPI_RAM_SPI_CONTROL_REG               (SPI_RAM_REGISTER_OFFSET+0x00)
#define SPI_RAM_SPI_COMMAND_REG               (SPI_RAM_REGISTER_OFFSET+0x04)
#define SPI_RAM_STATUS_REG                    (SPI_RAM_REGISTER_OFFSET+0x08)
#define SPI_RAM_EDAC_REG                      (SPI_RAM_REGISTER_OFFSET+0x10)

/* SPI control register bit masks/mappings */
#define SPI_RAM_SPI_CONTROL_DIVISOR_POS       (4)
#define SPI_RAM_SPI_CONTROL_DIVISOR_MASK      (0xFFF << SPI_RAM_SPI_CONTROL_DIVISOR_POS)
#define SPI_RAM_SPI_CONTROL_EDAC_POS          (3)
#define SPI_RAM_SPI_CONTROL_EDAC_MASK         (0x1 << SPI_RAM_SPI_CONTROL_EDAC_POS)

#define SPI_RAM_SPI_CONTROL_EDAC_ENABLE       (0x0 << SPI_RAM_SPI_CONTROL_EDAC_POS)
#define SPI_RAM_SPI_CONTROL_EDAC_DISABLE      (0x1 << SPI_RAM_SPI_CONTROL_EDAC_POS)

/* SPI command register bit masks/mappings */
#define SPI_RAM_SPI_COMMAND_CMD_POS           (24)
#define SPI_RAM_SPI_COMMAND_CMD_MASK          (0xFF << SPI_RAM_SPI_COMMAND_CMD_POS)
#define SPI_RAM_SPI_COMMAND_DATA_POS          (16)
#define SPI_RAM_SPI_COMMAND_DATA_MASK         (0xFF << SPI_RAM_SPI_COMMAND_DATA_POS)
#define SPI_RAM_SPI_COMMAND_STATUS_POS        (0)
#define SPI_RAM_SPI_COMMAND_STATUS_MASK       (0xFF << SPI_RAM_SPI_COMMAND_STATUS_POS)

/* SPI RAM commands */
#define SPI_RAM_CMD_WRITE_ENABLE              (0x06 << SPI_RAM_SPI_COMMAND_CMD_POS)
#define SPI_RAM_CMD_WRITE_DISABLE             (0x04 << SPI_RAM_SPI_COMMAND_CMD_POS)
#define SPI_RAM_CMD_READ_STATUS_REG           (0x05 << SPI_RAM_SPI_COMMAND_CMD_POS)
#define SPI_RAM_CMD_WRITE_STATUS_REG          (0x01 << SPI_RAM_SPI_COMMAND_CMD_POS)

/* Status register bit masks/mappings */
#define SPI_RAM_STATUS_DEBUG_DETECT_POS       (1)
#define SPI_RAM_STATUS_DEBUG_DETECT_MASK      (0x1 << SPI_RAM_STATUS_DEBUG_DETECT_POS)
#define SPI_RAM_STATUS_BUSY_POS               (0)
#define SPI_RAM_STATUS_BUSY_MASK              (0x1 << SPI_RAM_STATUS_BUSY_POS)

/* EDAC status register bit masks/mappings */
#define SPI_RAM_EDAC_MULT_ERR_POS             (2)
#define SPI_RAM_EDAC_MULT_ERR_MASK            (0x1 << SPI_RAM_EDAC_MULT_ERR_POS)
#define SPI_RAM_EDAC_DOUBLE_ERR_POS           (1)
#define SPI_RAM_EDAC_DOUBLE_ERR_MASK          (0x1 << SPI_RAM_EDAC_DOUBLE_ERR_POS)
#define SPI_RAM_EDAC_SINGLE_ERR_POS           (0)
#define SPI_RAM_EDAC_SINGLE_ERR_MASK          (0x1 << SPI_RAM_EDAC_SINGLE_ERR_POS)

#endif /* _SPI_RAM_INTERNAL_H_ */
