/*----------------------------------------------------------------------
 * Copyright (C) 2016 - 2017 ÅAC Microtec AB
 *
 * Filename:     massmem_flash.c
 * Module name:  massmem_flash
 *
 * Author(s):    Dahe, MaWe, InSi, OdMa, JoBe
 * Support:      support@aacmicrotec.com
 * Description:  NAND mass memory flash controller driver definitions
 * Requirements: Adheres to the ÅAC Mass Memory Flash Driver Requirement
 *               Specification Rev. A
 *--------------------------------------------------------------------*/

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include "massmem_flash.h"
#include "toolchain_support.h"

/* Addressing is done in terms of "page group" words, or sub-page bytes, depending on which
 * way you look at it.
 *
 * Addressing is not continuous between pages, page address step size is twice the size of
 * the page size, the latter half contains the spare area and unaddressable space.
 */
#define MASSMEM_16GB_PAGE_ADDRESS_STEP (2 * MASSMEM_16GB_PAGE_SIZE)
#define MASSMEM_32GB_PAGE_ADDRESS_STEP (2 * MASSMEM_32GB_PAGE_SIZE)

extern massmem_callbacks_t massmem_callbacks;

typedef enum {
  MT29F32G08AFABA,
  MT29F64G08AFAAA,
  INVALID_TYPE
} massmem_chip_type_t;
static massmem_chip_type_t global_chip_type = INVALID_TYPE;

/* Actual capacity of the current chip type, ignoring artificial size limiting for api, set
 * by massmem_init().
 */
static size_t massmem_page_bytes_actual;
static size_t massmem_spare_area_bytes_actual;

/* Available sizes in bytes for current chip type and configuration for use at runtime, set
 * by massmem_init(), exposed read-only via getters. Can be artificially limited.
 */
static size_t massmem_page_bytes;
static size_t massmem_spare_area_bytes;

void massmem_handle_error_injection(const uint8_t *data_buf,
                                    massmem_error_injection_t *error_injection,
                                    uint32_t size);

void massmem_clear_interrupt(int32_t interrupt);

int32_t massmem_set_feature(uint32_t feature,
                            uint32_t target);

static massmem_chip_type_t massmem_determine_chip_type(massmem_cid_t chip_ids);

int32_t massmem_init(void)
{
  uint32_t timeout;
  massmem_cid_t chip_ids;

  timeout = 0;

  /* Set all registers in a known state */
  REG32(_massmem_base + MASSMEM_CMD)     = 0x0;
  REG32(_massmem_base + MASSMEM_SR)      = MASSMEM_SR_VALID_BITS_MASK;
  REG32(_massmem_base + MASSMEM_IE)      = 0x0;
  REG32(_massmem_base + MASSMEM_TS)      = 0x0;
  REG32(_massmem_base + MASSMEM_ADDR)    = 0x0;
  REG32(_massmem_base + MASSMEM_DIO)     = 0x0;
  REG32(_massmem_base + MASSMEM_EDAC)    = 0x0;
  REG32(_massmem_base + MASSMEM_ERR_CNT) = 0x0;

  massmem_reset_target(MASSMEM_TS_TARGET_CE);
  /* Wait for reset of low target to finish */
  timeout = 0;
  while ((REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_RESET_DONE) == 0 &&
         timeout < MASSMEM_TIMEOUT) {
    timeout++;
  }
  if (timeout >= MASSMEM_TIMEOUT) {
    return -EIO;
  }
  /* Clear status flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  massmem_reset_target(MASSMEM_TS_TARGET_CE2);
  /* Wait for reset of high target to finish */
  timeout = 0;
  while ((REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_RESET_DONE) == 0 &&
         timeout < MASSMEM_TIMEOUT) {
    timeout++;
  }
  if (timeout >= MASSMEM_TIMEOUT) {
    return -EIO;
  }
  /* Clear status flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  if (massmem_set_features_default() != ENOERR) {
    return -EIO;
  }

  if (massmem_read_id(&chip_ids) != ENOERR) {
    return -EIO;
  }
  global_chip_type = massmem_determine_chip_type(chip_ids);
  switch (global_chip_type) {
    case MT29F32G08AFABA:
      massmem_page_bytes_actual = MASSMEM_16GB_PAGE_BYTES;
      massmem_spare_area_bytes_actual = MASSMEM_16GB_SPARE_AREA_BYTES;
      massmem_page_bytes = MASSMEM_16GB_PAGE_BYTES;
      massmem_spare_area_bytes = MASSMEM_16GB_SPARE_AREA_BYTES;
      REG32(_massmem_base + MASSMEM_MEMORY_SELECT) = MASSMEM_MEMORY_SELECT_MT29F32G08AFABA;
      break;

    case MT29F64G08AFAAA:
      massmem_page_bytes_actual = MASSMEM_32GB_PAGE_BYTES;
      massmem_spare_area_bytes_actual = MASSMEM_32GB_SPARE_AREA_BYTES;
#ifdef MASSMEM_ENABLE_32GB
      massmem_page_bytes = MASSMEM_32GB_PAGE_BYTES;
      massmem_spare_area_bytes = MASSMEM_32GB_SPARE_AREA_BYTES;
#else
      massmem_page_bytes = MASSMEM_16GB_PAGE_BYTES;
      massmem_spare_area_bytes = MASSMEM_16GB_SPARE_AREA_BYTES;
#endif
      REG32(_massmem_base + MASSMEM_MEMORY_SELECT) = MASSMEM_MEMORY_SELECT_MT29F64G08AFAAA;
      break;

    case INVALID_TYPE:
      return -EIO;
  }

  return ENOERR;
}

int32_t massmem_set_feature(uint32_t feature,
                            uint32_t target)
{
  uint32_t timeout;

  REG32(_massmem_base + MASSMEM_TS) = MASSMEM_TS_TARGET & target;

  /* Clear status register by writing to all flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  REG32(_massmem_base + MASSMEM_FEATURE) = feature;

  /* Issue set feature command */
  REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_SET_FEATURE;

  /* Wait for set feature done */
  timeout = 0;
  while ((REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_SET_FEATURE_DONE) == 0 &&
         timeout < MASSMEM_TIMEOUT) {
    timeout++;
  }

  if (timeout >= MASSMEM_TIMEOUT) {
    return -EIO;
  }

  return ENOERR;
}

int32_t massmem_set_features_default(void)
{
  int32_t ret;

  ret = massmem_set_feature(MASSMEM_FEATURE_TIMING_MODE,
                            MASSMEM_TS_TARGET_CE);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;
    return ret;
  }

  ret = massmem_set_feature(MASSMEM_FEATURE_TIMING_MODE,
                            MASSMEM_TS_TARGET_CE2);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;
    return ret;
  }

  ret = massmem_set_feature(MASSMEM_FEATURE_OUT_DRIVE_STRENGTH,
                            MASSMEM_TS_TARGET_CE);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;
    return ret;
  }

  ret = massmem_set_feature(MASSMEM_FEATURE_OUT_DRIVE_STRENGTH,
                            MASSMEM_TS_TARGET_CE2);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;
    return ret;
  }

  ret = massmem_set_feature(MASSMEM_FEATURE_PULLDOWN_STRENGTH,
                            MASSMEM_TS_TARGET_CE);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;
    return ret;
  }

  ret = massmem_set_feature(MASSMEM_FEATURE_PULLDOWN_STRENGTH,
                            MASSMEM_TS_TARGET_CE2);
  if (ENOERR != ret) {
    /* Clear status register by writing to all flags */
    REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;
    return ret;
  }

  /* Clear status register by writing to all flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;
  return ENOERR;
}

void massmem_set_status(uint32_t status)
{
  REG32(_massmem_base + MASSMEM_SR) = status;
}

uint32_t massmem_get_status(void)
{
  return REG32(_massmem_base + MASSMEM_SR);
}

uint32_t massmem_get_interrupt_status(void)
{
  return (REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_VALID_BITS_MASK);
}

void massmem_set_target(const uint32_t target)
{
  REG32(_massmem_base + MASSMEM_TS) = target;
}

uint32_t massmem_get_interrupt_enable(void)
{
  return REG32(_massmem_base + MASSMEM_IE);
}

int32_t massmem_interrupt_enable(const int32_t int_source)
{
  /* Check parameters */
  if (((MASSMEM_INT_EN_REG_MASK | int_source) != MASSMEM_INT_EN_REG_MASK) ||
      0 == int_source) {
    return -EINVAL;
  }
  REG32(_massmem_base + MASSMEM_IE) |= int_source;

  return ENOERR;
}

int32_t massmem_interrupt_disable(const int32_t int_source)
{
  /* Check parameters */
  if (((MASSMEM_INT_EN_REG_MASK | int_source) != MASSMEM_INT_EN_REG_MASK) ||
      (0 == int_source)) {
    return -EINVAL;
  }
  REG32(_massmem_base + MASSMEM_IE) &= ~int_source;

  return ENOERR;
}

void massmem_clear_interrupt(const int32_t interrupt)
{
  REG32(_massmem_base + MASSMEM_SR) |= interrupt;
}

AAC_INTERRUPT_HANDLER_SIGNATURE(massmem_isr, data_arg)
{
  uint32_t int_status;
  uint32_t enabled_ints;

  /* Get interrupt status */
  int_status = REG32(_massmem_base + MASSMEM_SR);
  enabled_ints = REG32(_massmem_base + MASSMEM_IE);

  /* Mask status to only consider enabled interrupts */
  int_status &= enabled_ints;

  /* Clear interrupts before handling them. If any new interrupts of the same
  source occurs in between reading the status register and handling it in the
  callback, the newest state will simply be reflected and handled by the ISR.
  If the interrupt occurs before clearing the status register, the extra
  interrupt will not be generated. If occurring after clearing the status
  register but before handling it in the callback, a new interrupt reflecting
  the exact same state will be generated. This has to be handled in the ISR
  callback. If the new interrupt occurs after the callback, the ISR will be
  executed again directly.

  If another source generates an interrupt, then a new interrupt will be
  generated on clear of the status register and will be seen as the ISR will
  be executed again directly. */
  REG32(_massmem_base + MASSMEM_SR) = int_status;

  /* Ahoy the callback function */
  if ((int_status & MASSMEM_SR_PROG_PAGE_DONE) &&
      (NULL != massmem_callbacks.massmem_prog_page_done_irq_callback)) {
    massmem_callbacks.massmem_prog_page_done_irq_callback(data_arg);
  }
  if ((int_status & MASSMEM_SR_READ_P_SETUP_DONE) &&
      (NULL != massmem_callbacks.massmem_read_p_setup_done_irq_callback)) {
    massmem_callbacks.massmem_read_p_setup_done_irq_callback(data_arg);
  }
  if ((int_status & MASSMEM_SR_ERASE_BLOCK_DONE) &&
      (NULL != massmem_callbacks.massmem_erase_block_done_irq_callback)) {
    massmem_callbacks.massmem_erase_block_done_irq_callback(data_arg);
  }
  if ((int_status & MASSMEM_SR_RESET_DONE) &&
      (NULL != massmem_callbacks.massmem_reset_done_irq_callback)) {
    massmem_callbacks.massmem_reset_done_irq_callback(data_arg);
  }
  if ((int_status & MASSMEM_SR_READ_STATUS_DONE) &&
      (NULL != massmem_callbacks.massmem_read_status_done_irq_callback)) {
    massmem_callbacks.massmem_read_status_done_irq_callback(data_arg);
  }
  if ((int_status & MASSMEM_SR_SET_FEATURE_DONE) &&
      (NULL != massmem_callbacks.massmem_set_feature_done_irq_callback)) {
    massmem_callbacks.massmem_set_feature_done_irq_callback(data_arg);
  }
}

int32_t massmem_reset_target(int32_t target)
{

  /* Sanity check target */
  if ((MASSMEM_TS_TARGET_CE != target) &&
      (MASSMEM_TS_TARGET_CE2 != target)) {
    return -EINVAL;
  }

  /* Abort ongoing read/program */
  massmem_stop_read_program();

  /* Select target */
  REG32(_massmem_base + MASSMEM_TS) = MASSMEM_TS_TARGET & target;

  /* Clear status register by writing to all flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  /* Issue reset command */
  REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_RESET;

  return ENOERR;
}

inline void massmem_stop_read_program(void)
{
  REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_STOP_RP;
}

inline int32_t massmem_set_page_spare_area_addr(uint32_t page_num, uint32_t offset)
{
  int32_t ret;

  ret = massmem_set_page_addr(page_num);

  if (ENOERR != ret) {
    return ret;
  }

  /* Increment address to the page end (spare area start) plus offset. The address is based on
   * the "page group" word, or the sub-page byte, depending on which way you look at it.
   *
   * For MT29F64G08AFAAA the spare area addressing will only affect lower inter-page address
   * bits, hence the extended address register can be ignored here.
   */
  REG32(_massmem_base + MASSMEM_ADDR) +=
    (massmem_page_bytes_actual + offset) / MASSMEM_WORD_SIZE;

  return ENOERR;
}

/* Page numbers are counted over a whole chip, i.e. both LUNs together */
int32_t massmem_set_page_addr(uint32_t page_num)
{
  uint32_t target;

  target = MASSMEM_TS_TARGET_CE;

  /* Sanity check page number */
  if (page_num > (MASSMEM_MAX_NR_PAGES - 1)) {
    return -EINVAL;
  }

  if (page_num >= MASSMEM_PAGES_PER_LUN) {
    target = MASSMEM_TS_TARGET_CE2;
    page_num -= MASSMEM_PAGES_PER_LUN;
  }

  /* Set target */
  REG32(_massmem_base + MASSMEM_TS) = MASSMEM_TS_TARGET & target;

  /* Set address */
  if (global_chip_type == MT29F64G08AFAAA) {
    /* 33 bits in address, LSB bits 0-31 in address register, MSB bit 32 in extended
     * address ("target select") register.
     */
    const uint64_t address = (uint64_t)page_num * MASSMEM_32GB_PAGE_ADDRESS_STEP;
    REG32(_massmem_base + MASSMEM_ADDR) = (uint32_t)address;
    REG32(_massmem_base + MASSMEM_TS) |=
        ((address >> 32) & 1) << MASSMEM_TS_32GB_EXTENDED_ADDRESS_SHIFT;
  } else {
    /* 32 bits in address, all in address register. */
    REG32(_massmem_base + MASSMEM_ADDR) = page_num * MASSMEM_16GB_PAGE_ADDRESS_STEP;
  }

  return ENOERR;
}

int32_t massmem_read_status(uint32_t *data_status,
                            uint8_t  *edac_status,
                            uint16_t *ctrl_status)
{
  /* Make sure the controller is not busy */
  if (0 != (REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_BUSY)) {
    return -EBUSY;
  }

  REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_READ_STATUS;

  *edac_status = REG32(_massmem_base + MASSMEM_EDAC) & MASSMEM_EDAC_DATA_MASK;
  *data_status = REG32(_massmem_base + MASSMEM_DIO);
  /* Read controller status register, mask out reserved bits */
  *ctrl_status = REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_VALID_BITS_MASK;

  /* Clear status flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  return ENOERR;
}

int32_t massmem_read_id(massmem_cid_t *cid)
{
  uint32_t data;
  uint32_t  i;

  /* Make sure the controller is not busy */
  if (0 != (REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_BUSY)) {
    return -EBUSY;
  }

  REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_READ_ID;

  /* After issuing the Read ID command the bytes are read from the flash chips
   * when the Data I/O register has been read. this means the EDAC ID bytes
   * have to be read first, otherwise they are discarded.
   */
  for (i = 0; i < MASSMEM_CID_SIZE; i++) {
    cid->edac[i] = REG32(_massmem_base + MASSMEM_EDAC) & 0xFF;
    data = REG32(_massmem_base + MASSMEM_DIO);
    cid->chip0[i] = (data >> 0) & 0xFF;
    cid->chip1[i] = (data >> 8) & 0xFF;
    cid->chip2[i] = (data >> 16) & 0xFF;
    cid->chip3[i] = (data >> 24) & 0xFF;
  }

  /* Clear status flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  return ENOERR;
}

static massmem_chip_type_t massmem_determine_chip_type(massmem_cid_t chip_ids)
{
  massmem_chip_type_t chip_type;

  /* check that edac chip is a valid type */
  if (memcmp(chip_ids.edac, massmem_cid_MT29F32G08AFABA, sizeof(chip_ids.edac)) == 0) {
    chip_type = MT29F32G08AFABA;
  } else if (memcmp(chip_ids.edac, massmem_cid_MT29F64G08AFAAA, sizeof(chip_ids.edac)) ==
             0) {
    chip_type = MT29F64G08AFAAA;
  } else {
    return INVALID_TYPE;
  }

  /* check that all chips are of the same type */
  if (memcmp(chip_ids.chip0, chip_ids.edac, sizeof(chip_ids.chip0)) != 0) {
    return INVALID_TYPE;
  }
  if (memcmp(chip_ids.chip1, chip_ids.edac, sizeof(chip_ids.chip1)) != 0) {
    return INVALID_TYPE;
  }
  if (memcmp(chip_ids.chip2, chip_ids.edac, sizeof(chip_ids.chip2)) != 0) {
    return INVALID_TYPE;
  }
  if (memcmp(chip_ids.chip3, chip_ids.edac, sizeof(chip_ids.chip3)) != 0) {
    return INVALID_TYPE;
  }

  return chip_type;
}

inline int32_t massmem_verify_command(void)
{
  uint32_t data_status;
  uint8_t  edac_status;
  uint16_t ctrl_status;

  data_status = 0;
  edac_status = 0;
  ctrl_status = 0;

  massmem_read_status(&data_status, &edac_status, &ctrl_status);

  if ((data_status & MASSMEM_STATUS_FAIL_32) ||
      (edac_status & MASSMEM_STATUS_FAIL)) {
    return -EIO;
  }

  return ENOERR;
}

int32_t massmem_erase_block(uint32_t block_num)
{
  uint32_t ret;

  /* Make sure the controller is not busy */
  if (0 != (REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_BUSY)) {
    return -EBUSY;
  }

  /* Set address (includes sanity check of block_num) */
  ret = massmem_set_page_addr(block_num * MASSMEM_PAGES_PER_BLOCK);
  if (ENOERR != ret) {
    return ret;
  }

  /* Issue erase command */
  REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_ERASE_BLOCK;

  return ENOERR;
}

int32_t massmem_read_page_dma(uint32_t page_num,
                              uint8_t *data_buf,
                              uint32_t size)
{
  int32_t ret;

  /* Sanity check data size */
  if (size > massmem_page_bytes ||
      0 == size ||
      0 != (size % MASSMEM_WORD_SIZE)) {
    return -EINVAL;
  }

  /* Sanity check data_buf, must be 4 byte aligned address! */
  if (NULL == data_buf || (0 != ((uintptr_t)(const void *)data_buf % MASSMEM_WORD_SIZE))) {
    return -EINVAL;
  }

  /* Make sure the controller is not busy */
  if (0 != (REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_BUSY)) {
    return -EBUSY;
  }

  /* Clear status flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  /* Store start address to read in the address register */
  ret = massmem_set_page_addr(page_num);
  if (ENOERR != ret) {
    return ret;
  }

  /* Setup SDRAM address to be used for storing during DMA read */
  REG32(_massmem_base + MASSMEM_DMA_BASE_ADDR) = (uint32_t)data_buf;
  /* Setup the number of bytes to read during DMA read */
  REG32(_massmem_base + MASSMEM_DMA_LENGTH) = size;
  /* Issue the Read Page command with DMA */
  REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_DMA_ON | MASSMEM_CMD_READ_PAGE;
  return ENOERR;
}

int32_t massmem_factory_bad_block_check(uint32_t block_num)
{
  /* The bad block marker is the first byte of the spare area of the
   * first page of every block. If that is 0x00 the block is bad.
   */

  uint8_t data_bad_block[MASSMEM_WORD_SIZE] __attribute__ ((aligned (4)));
  uint8_t edac_bad_block __attribute__ ((aligned (4)));

  int32_t ret;
  uint8_t i;

  /* Sanity check block_num */
  if (block_num > (MASSMEM_BLOCKS - 1)) {
    return -EINVAL;
  }

  /* Read first byte of first-page-of-block spare area */
  ret = massmem_read_spare_area_poll(block_num * MASSMEM_PAGES_PER_BLOCK,
                                     0, /* read from byte 0 */
                                     MASSMEM_RAW,
                                     &data_bad_block[0],
                                     &edac_bad_block,
                                     MASSMEM_WORD_SIZE);
  if (ENOERR != ret) {
    return ret;
  }
  /* Check values */
  if (edac_bad_block != 0xFF) {
    return MASSMEM_FACTORY_BAD_BLOCK_MARKED;
  }

  for (i = 0; i < MASSMEM_WORD_SIZE; i++) {
    if (data_bad_block[i] != 0xFF) {
      return MASSMEM_FACTORY_BAD_BLOCK_MARKED;
    }
  }

  return MASSMEM_FACTORY_BAD_BLOCK_CLEARED;
}

int32_t massmem_read_spare_area_poll(uint32_t page_num,
                                     uint32_t offset,
                                     uint32_t mode,
                                     uint8_t *data_buf,
                                     uint8_t *edac_buf,
                                     uint32_t size)
{
  int32_t ret;
  uint32_t timeout;
  uint32_t i;

  /* Sanity check mode */
  switch (mode) {
    case MASSMEM_EDAC_INTERLEAVING:
    case MASSMEM_RAW:
      break;

    default:
      return -EINVAL;
  }

  /* Sanity check size and offset */
  if ((offset + size > massmem_spare_area_bytes) ||
      (size == 0) ||
      (0 != (size % MASSMEM_WORD_SIZE)) ||
      (0 != (offset % MASSMEM_WORD_SIZE)))  {
    return -EINVAL;
  }

  /* Sanity check data_buf, must be 4 byte aligned address! */
  if (0 != ((uintptr_t)(const void *)data_buf % MASSMEM_WORD_SIZE)) {
    return -EINVAL;
  }
  /* Sanity check edac_buf, must be 4 byte aligned address! */
  if (0 != ((uintptr_t)(const void *)edac_buf % MASSMEM_WORD_SIZE)) {
    return -EINVAL;
  }
  /* Sanity check data_buf pointer. (edac_buf is allowed to be NULL) */
  if (NULL == data_buf) {
    return -EINVAL;
  }
  /* Reading EDAC in non-raw mode when EDAC and interleaving is enabled is not possible. */
  if (mode == MASSMEM_EDAC_INTERLEAVING && edac_buf) {
    return -EINVAL;
  }

  /* Clear status flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  /* Make sure the controller is not busy */
  if (0 != (REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_BUSY)) {
    return -EBUSY;
  }

  /* Set address */
  ret = massmem_set_page_spare_area_addr(page_num, offset);
  if (ENOERR != ret) {
    return ret;
  }

  if (mode == MASSMEM_RAW) {
    REG32(_massmem_base + MASSMEM_CMD) =  MASSMEM_CMD_READ_PAGE;
  } else {
    /* Use DMA in order to enable EDAC and interleaving, no interrupts are enabled and
     * polling is still used to block until finished.
     */

    /* Setup SDRAM address to be used for storing during DMA read */
    REG32(_massmem_base + MASSMEM_DMA_BASE_ADDR) = (uint32_t)data_buf;
    /* Setup the number of bytes to read during DMA read */
    REG32(_massmem_base + MASSMEM_DMA_LENGTH) = size;
    /* Issue the Read Page command with DMA */
    REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_DMA_ON | MASSMEM_CMD_READ_PAGE;
  }

  /* Wait for Read Page Setup Done */
  timeout = 0;
  while ((REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_READ_P_SETUP_DONE) == 0 &&
         timeout < MASSMEM_TIMEOUT) {
    timeout++;
  }

  if (timeout >= MASSMEM_TIMEOUT) {
    /* Give the Stop read/program command to avoid the controller *
     * state machine getting stuck *
     */
    massmem_stop_read_program();
    return -EIO;
  }

  /* Clear flag */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_READ_P_SETUP_DONE;

  if (mode == MASSMEM_RAW) {
    /* Loop to read data */
    for (i = 0; i < (size / MASSMEM_WORD_SIZE); i++) {
      /* First check EDAC (if wanted) */
      if (NULL != edac_buf) {
        *(edac_buf + i) = REG32(_massmem_base + MASSMEM_EDAC) & 0xFF;
      }

      /* Read data word */
      *(((uint32_t *) data_buf) + i) = REG32(_massmem_base + MASSMEM_DIO);
    }

    /* Signal to the NFC that read is finished */
    massmem_stop_read_program();
  }

  return 0;
}

int32_t massmem_program_page_dma(uint32_t page_num,
                                 const uint8_t *data_buf,
                                 uint32_t size)
{
  int32_t ret;

  /* Sanity check data size */
  if (size > massmem_page_bytes ||
      (0 == size) ||
      (0 != (size % MASSMEM_WORD_SIZE))) {
    return -EINVAL;
  }

  /* Sanity check data_buf */
  if (!data_buf) {
    return -EINVAL;
  }

  /* Make sure the controller is not busy */
  if (0 != (REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_BUSY)) {
    return -EBUSY;
  }

  /* Clear status flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  /* Set address */
  ret = massmem_set_page_addr(page_num);
  if (ENOERR != ret) {
    return ret;
  }

  /* Setup SDRAM address to be used for storing during DMA program page */
  REG32(_massmem_base + MASSMEM_DMA_BASE_ADDR) = (uint32_t) data_buf;

  /* Setup the number of bytes to read during DMA program page */
  REG32(_massmem_base + MASSMEM_DMA_LENGTH) = size;

  /* Issue Program Page command */
  REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_DMA_ON | MASSMEM_CMD_PROG_PAGE;
  return ENOERR;
}

void massmem_handle_error_injection(const uint8_t *data_buf,
                                    massmem_error_injection_t *error_injection,
                                    uint32_t size)
{
  uint32_t reps;
  uint32_t remaining_words;
  uint32_t padding_words;
  uint32_t i;

  /* Issue Program page command */
  REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_PROG_PAGE;

  /* Push 8 words of data to the flash controller */
  i = 0;
  while (i < size) {
    if ((size - i) >= 8) {
      for (reps = 0; reps < 8; reps++) {
        REG32(_massmem_base + MASSMEM_DIO) = *(((uint32_t *) data_buf) + i);
        i++;
      }
    } else {
      /* Fill out remaining words with 0x00000000 to reach the
       * required 8 words according to the MASSMEM IP DDD
       */
      remaining_words = size - i;
      padding_words = 8 - remaining_words;

      for (reps = 0; reps < remaining_words; reps++) {
        REG32(_massmem_base + MASSMEM_DIO) = *(((uint32_t *) data_buf) + i);
        i++;
      }

      for (reps = 0; reps < padding_words; reps++) {
        REG32(_massmem_base + MASSMEM_DIO) = 0x00;
      }
    }

    /* Push 8 error injections to the flash controller,
     * first injection with error and 7 pad injections
     */
    REG32(_massmem_base + MASSMEM_EDAC) = error_injection->edac_error_injection;
    REG32(_massmem_base + MASSMEM_DIO) = error_injection->data_error_injection;

    for (reps = 0; reps < 7; reps++) {
      REG32(_massmem_base + MASSMEM_EDAC) = 0x00;
      REG32(_massmem_base + MASSMEM_DIO) = 0x00;
    }
  }
}

int32_t massmem_program_page_poll(uint32_t page_num,
                                  const uint8_t *data_buf,
                                  massmem_error_injection_t *error_injection,
                                  uint32_t size)
{
  int32_t ret;
  uint32_t timeout;

  /* Sanity check data_buf */
  if (NULL == data_buf) {
    return -EINVAL;
  }
  /* Sanity check data_buf, must be 4 byte aligned address! */
  if (0 != ((uintptr_t)(const void *)data_buf % MASSMEM_WORD_SIZE)) {
    return -EINVAL;
  }
  /* Sanity check data size */
  if (size > massmem_page_bytes ||
      (size == 0) ||
      (0 != (size % MASSMEM_WORD_SIZE))) {
    return -EINVAL;
  }

  /* Make sure the controller is not busy */
  if (0 != (REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_BUSY)) {
    return -EBUSY;
  }

  /* Clear status flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  /* Set address */
  ret = massmem_set_page_addr(page_num);
  if (ENOERR != ret) {
    return ret;
  }

  if (NULL == error_injection) {
    /* Use DMA in order to enable EDAC and interleaving, no interrupts are enabled and
     * polling is still used to block until finished.
     */

    /* Setup SDRAM address to be used for storing during DMA program page */
    REG32(_massmem_base + MASSMEM_DMA_BASE_ADDR) = (uint32_t) data_buf;
    /* Setup the number of bytes to write during DMA program page */
    REG32(_massmem_base + MASSMEM_DMA_LENGTH) = size;
    /* Issue Program Page command */
    REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_DMA_ON | MASSMEM_CMD_PROG_PAGE;
  } else {
    massmem_handle_error_injection(data_buf, error_injection, (size / MASSMEM_WORD_SIZE));
    massmem_stop_read_program();
  }

  /* Wait for flag (or timeout) */
  timeout = 0;
  while ((REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_PROG_PAGE_DONE) == 0 &&
         timeout < MASSMEM_TIMEOUT) {
    timeout++;
  }
  if (timeout >= MASSMEM_TIMEOUT) {
    return -EIO;
  }

  /* Check chip status after program */
  return massmem_verify_command();
}

int32_t massmem_program_spare_area_dma(uint32_t page_num,
                                       uint32_t offset,
                                       const uint8_t *data_buf,
                                       uint32_t size)
{
  int32_t  ret;

  /* Sanity check data_buf */
  if (NULL == data_buf) {
    return -EINVAL;
  }

  /* Sanity check data_buf, must be 4 byte aligned address! */
  if (0 != ((uintptr_t)(const void *)data_buf % MASSMEM_WORD_SIZE)) {
    return -EINVAL;
  }

  /* Sanity check data size */
  if (offset + size > massmem_spare_area_bytes ||
      (size == 0) ||
      (0 != (size % (32 * MASSMEM_WORD_SIZE))) ||
      (0 != (offset % (32 * MASSMEM_WORD_SIZE)))) {
    return -EINVAL;
  }

  /* Make sure the controller is not busy */
  if (0 != (REG32(_massmem_base + MASSMEM_SR) & MASSMEM_SR_BUSY)) {
    return -EBUSY;
  }

  /* Clear status flags */
  REG32(_massmem_base + MASSMEM_SR) = MASSMEM_SR_VALID_BITS_MASK;

  /* Set address */
  ret = massmem_set_page_spare_area_addr(page_num, offset);
  if (ENOERR != ret) {
    return ret;
  }

  /* Setup SDRAM address to be used for storing during DMA program page */
  REG32(_massmem_base + MASSMEM_DMA_BASE_ADDR) = (uint32_t) data_buf;

  /* Setup the number of bytes to write during DMA program page */
  REG32(_massmem_base + MASSMEM_DMA_LENGTH) = size;

  /* Issue Program Page command */
  REG32(_massmem_base + MASSMEM_CMD) = MASSMEM_CMD_DMA_ON | MASSMEM_CMD_PROG_PAGE;

  return ENOERR;
}

size_t massmem_get_page_bytes(void)
{
  return massmem_page_bytes;
}

size_t massmem_get_spare_area_bytes(void)
{
  return massmem_spare_area_bytes;
}
