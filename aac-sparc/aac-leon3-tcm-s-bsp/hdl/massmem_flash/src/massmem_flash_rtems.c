/*----------------------------------------------------------------------
 * Copyright (C) 2015 - 2017 ÅAC Microtec AB
 *
 * Filename:     massmem_flash_rtems.c
 * Module name:  massmem_flash
 *
 * Author(s):    OdMa, JoBe, MaWe
 * Support:      support@aacmicrotec.com
 * Description:  NAND mass memory flash controller driver implementation
 * Requirements: Adheres to the ÅAC Mass Memory Flash Driver Requirement
 *               Specification Rev. A
 *--------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <rtems.h>
#include <rtems/libio.h>
#include <bsp.h>
#include <bsp/irq.h>
#include <rtems/seterr.h>

#include "massmem_flash.h"
#include "massmem_flash_rtems.h"
#include "toolchain_support.h"

/* Board definitions */
uint32_t _massmem_base = AAC_BSP_MASSMEM_BASE;
uint32_t _massmem_IRQ = AAC_BSP_MASSMEM_IRQ;

#define MASSMEM_SEM_ATTR    (RTEMS_FIFO | RTEMS_SIMPLE_BINARY_SEMAPHORE | \
                             RTEMS_NO_MULTIPROCESSOR_RESOURCE_SHARING | RTEMS_LOCAL)

static rtems_id massmem_ip_sem_id;
static rtems_id massmem_int_sem_id;

/* cached */
static size_t massmem_page_bytes;
static size_t massmem_spare_area_bytes;

void massmem_prog_page_done_callback(void *arg);
void massmem_read_status_done_callback(void *arg);
void massmem_read_page_done_callback(void *arg);
void massmem_erase_block_done_callback(void *arg);
void massmem_read_id_done_callback(void *arg);
void massmem_reset_done_callback(void *arg);
void massmem_set_feature_done_callback(void *arg);
int32_t massmem_are_rw_args_valid(rtems_libio_rw_args_t *rw_args);
int32_t massmem_write_spare_area(const uint32_t page,
                                 const uint32_t offset,
                                 const uint8_t *buffer,
                                 const uint32_t size);

/*
 * MASSMEM callback functions
 */
massmem_callbacks_t massmem_callbacks = {
  .massmem_prog_page_done_irq_callback = massmem_prog_page_done_callback,
  .massmem_read_status_done_irq_callback = massmem_read_status_done_callback,
  .massmem_read_p_setup_done_irq_callback = massmem_read_page_done_callback,
  .massmem_erase_block_done_irq_callback = massmem_erase_block_done_callback,
  .massmem_read_id_done_irq_callback = massmem_read_id_done_callback,
  .massmem_reset_done_irq_callback = massmem_reset_done_callback,
  .massmem_set_feature_done_irq_callback = massmem_set_feature_done_callback
};

void massmem_prog_page_done_callback(void *arg)
{
  rtems_semaphore_release(massmem_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Massmem program page done.");
}

void massmem_read_status_done_callback(void *arg)
{
  rtems_semaphore_release(massmem_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Massmem read status done.");
}

void massmem_read_page_done_callback(void *arg)
{
  rtems_semaphore_release(massmem_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Massmem read page done.");
}

void massmem_erase_block_done_callback(void *arg)
{
  rtems_semaphore_release(massmem_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Massmem erase block done.");
}

void massmem_read_id_done_callback(void *arg)
{
  rtems_semaphore_release(massmem_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Massmem read id done.");
}

void massmem_reset_done_callback(void *arg)
{
  rtems_semaphore_release(massmem_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Massmem reset done.");
}

void massmem_set_feature_done_callback(void *arg)
{
  rtems_semaphore_release(massmem_int_sem_id);
  DBG(DBG_SEVERITY_INFO, "Massmem set feature done.");
}

int32_t massmem_are_rw_args_valid(rtems_libio_rw_args_t *rw_args)
{
  uint32_t number_of_pages;

  /* Page within limits */
  if (rw_args->offset < 0 || rw_args->offset >= MASSMEM_MAX_NR_PAGES) {
    DBG(DBG_SEVERITY_ERR, "Invalid page reference. (%d)", rw_args->offset);
    return -EINVAL;
  }

  /* Sanity check count */
  number_of_pages = rw_args->count / massmem_page_bytes;
  if ((rw_args->count % massmem_page_bytes) != 0) {
    number_of_pages ++;
  }

  if ((rw_args->offset + number_of_pages) > MASSMEM_MAX_NR_PAGES)
    {
      DBG(DBG_SEVERITY_ERR,
          "Page operation out of boundaries. Offset(%d). " \
          "Pages to make operation (%d). "                 \
          "Max allowed pages (%d)",
          rw_args->offset,
          number_of_pages,
          MASSMEM_MAX_NR_PAGES);

      return -EINVAL;
    }

  return ENOERR;
}

rtems_device_driver massmem_initialize(rtems_device_major_number major,
                                       rtems_device_minor_number unused,
                                       void *args)
{
  int32_t ret;
  rtems_status_code status;

  status = rtems_semaphore_create(rtems_build_name('M', 'M', 'I', 'P'),
                                  0,
                                  MASSMEM_SEM_ATTR,
                                  RTEMS_NO_PRIORITY_CEILING,
                                  &massmem_ip_sem_id);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to create massmem ip semaphore, error %d, %s",
        status, rtems_status_text(status));
    rtems_io_unregister_driver(major);
    return status;
  }

  status = rtems_semaphore_create(rtems_build_name('M', 'M', 'I', 'R'),
                                  0,
                                  MASSMEM_SEM_ATTR,
                                  RTEMS_NO_PRIORITY_CEILING,
                                  &massmem_int_sem_id);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to create massmem interrupt semaphore, error %d, %s",
        status, rtems_status_text(status));
    rtems_semaphore_delete(massmem_ip_sem_id);
    rtems_io_unregister_driver(major);
    return status;
  }

  ret = massmem_init();
  if (ENOERR != ret) {
    DBG(DBG_SEVERITY_ERR, "Failed to init massmem (%d)", ret);
    rtems_semaphore_delete(massmem_ip_sem_id);
    rtems_semaphore_delete(massmem_int_sem_id);
    rtems_io_unregister_driver(major);
    return RTEMS_INTERNAL_ERROR;
  }

  /* cached in rtems layer to avoid repeated function calls */
  massmem_page_bytes = massmem_get_page_bytes();
  massmem_spare_area_bytes = massmem_get_spare_area_bytes();

  status = rtems_interrupt_handler_install(_massmem_IRQ,
                                           MASSMEM_DEVICE_NAME,
                                           RTEMS_INTERRUPT_UNIQUE,
                                           massmem_isr,
                                           NULL);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to install RTEMS mass memory interrupt handler %d, %s",
        status, rtems_status_text(status));
    rtems_semaphore_delete(massmem_ip_sem_id);
    rtems_semaphore_delete(massmem_int_sem_id);
    rtems_io_unregister_driver(major);
    return status;
  }

  status = rtems_io_register_name(MASSMEM_DEVICE_NAME, major, 0);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to register mass memory flash driver (%d)", status);
    rtems_semaphore_delete(massmem_ip_sem_id);
    rtems_semaphore_delete(massmem_int_sem_id);
    rtems_interrupt_handler_remove(_massmem_IRQ,
                                   massmem_isr,
                                   NULL);
    rtems_io_unregister_driver(major);
    return status;
  }

  DBG(DBG_SEVERITY_INFO, "Mass memory flash driver (M%d:0) registered.", major);

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver massmem_open(rtems_device_major_number major,
                                 rtems_device_minor_number unused,
                                 void *args)
{
  rtems_semaphore_release(massmem_ip_sem_id);
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver massmem_close(rtems_device_major_number major,
                                  rtems_device_minor_number unused,
                                  void *args)
{
  /* obtain ip semaphore to wait for any ongoing operations to finish */
  rtems_semaphore_obtain(massmem_ip_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver massmem_read(rtems_device_major_number major,
                                 rtems_device_minor_number unused,
                                 void *args)
{
  uint32_t page;
  uint32_t bytes_read;
  uint32_t bytes_to_read;
  int32_t  ret;
  uint8_t  *buffer;
  rtems_status_code status;
  rtems_libio_rw_args_t *rw_args;

  rw_args = (rtems_libio_rw_args_t *) args;

  if (ENOERR != massmem_are_rw_args_valid(rw_args)) {
    return RTEMS_INVALID_NAME; /* Will translate to errno = EINVAL */
  }

  status = rtems_semaphore_obtain(massmem_ip_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to grab massmem ip semaphore, error: %s",
        rtems_status_text(status));
    return status;
  }

  DBG(DBG_SEVERITY_INFO, "Reading from mass memory (M%d:m%d).", major, unused);

  massmem_interrupt_enable(MASSMEM_IE_RPAGE_DONE);
  bytes_read = 0;
  buffer = (uint8_t *) rw_args->buffer;

  while (bytes_read < rw_args->count) {
    page = rw_args->offset + (bytes_read / massmem_page_bytes);
    bytes_to_read = rw_args->count - bytes_read;
    if (bytes_to_read >= massmem_page_bytes) {
      bytes_to_read = massmem_page_bytes;
    }
    DBG(DBG_SEVERITY_INFO, "Reading page:%d bytes:%d\n", page, bytes_to_read);

    ret = massmem_read_page_dma(page, buffer, bytes_to_read);
    if (ENOERR != ret) {
      DBG(DBG_SEVERITY_ERR,
          "Failed to read page. Bare metal driver returned: (%d)", ret);
      massmem_interrupt_disable(MASSMEM_IE_RPAGE_DONE);
      rtems_semaphore_release(massmem_ip_sem_id);
      if(-EBUSY == ret) {
        return RTEMS_RESOURCE_IN_USE; /* Will translate to errno = EBUSY */
      } else if (-EINVAL == ret ) {
        return RTEMS_INVALID_NAME;  /* Will translate to errno = EINVAL */
      }

      return RTEMS_NOT_DEFINED; /* Will translate to errno = EIO */
    }
    /* Obtain interrupt semaphore to yield cpu and await interrupt for read done */
    status = rtems_semaphore_obtain(massmem_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
    if (RTEMS_SUCCESSFUL != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to grab massmem interrupt semaphore, error: %s",
          rtems_status_text(status));
      massmem_interrupt_disable(MASSMEM_IE_RPAGE_DONE);
      rtems_semaphore_release(massmem_ip_sem_id);
      return status;
    }

    bytes_read += bytes_to_read;
    buffer = buffer + bytes_to_read;
  }
  rw_args->bytes_moved = bytes_read;

  massmem_interrupt_disable(MASSMEM_IE_RPAGE_DONE);
  rtems_semaphore_release(massmem_ip_sem_id);

  /* Invalidate cache after DMA transfer to avoid stale data in read buffer.
   *
   * Current write-through configuration saves us from the possibility of
   * invalidating dirty data.
   */
  rtems_cache_invalidate_multiple_data_lines(rw_args->buffer, rw_args->bytes_moved);

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver massmem_write(rtems_device_major_number major,
                                  rtems_device_minor_number unused,
                                  void *args)
{
  uint32_t page;
  uint32_t bytes_written;
  uint32_t bytes_to_write;
  int32_t  ret;
  uint8_t  *buffer;
  rtems_status_code status;
  rtems_libio_rw_args_t *rw_args;

  rw_args = (rtems_libio_rw_args_t *) args;

  if (ENOERR != massmem_are_rw_args_valid(rw_args)) {
    return RTEMS_INVALID_NAME; /* Will translate to errno = EINVAL */
  }

  status = rtems_semaphore_obtain(massmem_ip_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to grab massmem ip semaphore, error: %s",
        rtems_status_text(status));
    return status;
  }

  DBG(DBG_SEVERITY_INFO, "Writing to mass memory (M%d:m%d).", major, unused);

  massmem_interrupt_enable(MASSMEM_IE_PROG_DONE);
  bytes_written = 0;
  buffer = (uint8_t *) rw_args->buffer;

  while (bytes_written < rw_args->count) {
    page = rw_args->offset + (bytes_written / massmem_page_bytes);
    bytes_to_write = rw_args->count - bytes_written;
    if (bytes_to_write >= massmem_page_bytes) {
      bytes_to_write = massmem_page_bytes;
    }
    DBG(DBG_SEVERITY_INFO, "Writing page:%d bytes:%d", page, bytes_to_write);

    ret = massmem_program_page_dma(page, buffer, bytes_to_write);
    if (ENOERR != ret) {
      DBG(DBG_SEVERITY_ERR,
          "Failed to program page. Bare metal driver returned: (%d)", ret);
      massmem_interrupt_disable(MASSMEM_IE_PROG_DONE);
      rtems_semaphore_release(massmem_ip_sem_id);
      if(-EBUSY == ret) {
        return RTEMS_RESOURCE_IN_USE; /* Will translate to errno = EBUSY */
      } else if (-EINVAL == ret ) {
        return RTEMS_INVALID_NAME;  /* Will translate to errno = EINVAL */
      }
      return RTEMS_NOT_DEFINED; /* Will translate to errno = EIO */
    }
    /* Obtain interrupt semaphore to yield cpu and await interrupt for program done */
    status = rtems_semaphore_obtain(massmem_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
    if (RTEMS_SUCCESSFUL != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to grab massmem interrupt semaphore, error: %s",
          rtems_status_text(status));
      massmem_interrupt_disable(MASSMEM_IE_PROG_DONE);
      rtems_semaphore_release(massmem_ip_sem_id);
      return status;
    }

    ret = massmem_verify_command();
    if (ENOERR != ret) {
      DBG(DBG_SEVERITY_ERR, "Failed to write data. Data might be corrupted.\n");
      massmem_interrupt_disable(MASSMEM_IE_PROG_DONE);
      rtems_semaphore_release(massmem_ip_sem_id);

      return RTEMS_IO_ERROR; /* Will translate to errno = EIO */
    }
    bytes_written = bytes_written + bytes_to_write;
    buffer =  buffer + bytes_to_write;
  }
  rw_args->bytes_moved = bytes_written;

  massmem_interrupt_disable(MASSMEM_IE_PROG_DONE);
  rtems_semaphore_release(massmem_ip_sem_id);
  return RTEMS_SUCCESSFUL;
}


int32_t massmem_write_spare_area(const uint32_t page,
                                 const uint32_t offset,
                                 const uint8_t *buffer,
                                 const uint32_t size)
{
  int32_t  ret;
  rtems_status_code status;

  /* Page within limits */
  if (page < 0 || page >= MASSMEM_MAX_NR_PAGES) {
    DBG(DBG_SEVERITY_ERR, "Invalid page reference. (%d)", page);
    return -EINVAL;
  }

  /* Sanity check size and offset */
  if ((offset + size > massmem_spare_area_bytes) ||
      (size == 0) ||
      (0 != (size % 4)) ||
      (0 != (offset % 4)))  {
    DBG(DBG_SEVERITY_ERR, "Invalid offset or size reference. (%u, %u)", offset, size);
    return -EINVAL;
  }

  DBG(DBG_SEVERITY_INFO, "Writing to mass memory spare area.");

  massmem_interrupt_enable(MASSMEM_IE_PROG_DONE);

  ret = massmem_program_spare_area_dma(page, offset, buffer, size);
  if (ENOERR != ret) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to program spare area. Bare metal driver returned: (%d)", ret);
    massmem_interrupt_disable(MASSMEM_IE_PROG_DONE);
    if(-EBUSY == ret || -EINVAL == ret) {
      return ret;
    }
    return -EIO;
  }

  /* Obtain interrupt semaphore to yield cpu and await interrupt for program done.
     (No error handling since the semaphore is never deleted.) */
  rtems_semaphore_obtain(massmem_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);

  ret = massmem_verify_command();
  if (ENOERR != ret) {
    DBG(DBG_SEVERITY_ERR, "Failed to write data. Data might be corrupted.\n");
    massmem_interrupt_disable(MASSMEM_IE_PROG_DONE);
    return -EIO;
  }

  massmem_interrupt_disable(MASSMEM_IE_PROG_DONE);
  return ENOERR;
}

rtems_device_driver massmem_control(rtems_device_major_number major,
                                    rtems_device_minor_number unused,
                                    void *parg)
{
  rtems_libio_ioctl_args_t *ioctl_args;
  rtems_status_code        status;

  /* Variables needed by some of the ioctls */
  uint32_t                 data_status;
  uint8_t                  edac_status;
  uint16_t                 ctrl_status;

  /* Variable used for handling spare area ioctls */
  massmem_ioctl_spare_area_args_t *spare_args;

  /* Variable used for handling program page with error injection */
  massmem_ioctl_error_injection_args_t *error_injection_args;

  ioctl_args = (rtems_libio_ioctl_args_t *) parg;

  /* Every branch in the switch statment below needs the semaphore taken, so
   * we can take it before we enter it. The default label really doesn't need
   * it, so it just has to release it before returning the error code.*/
  status = rtems_semaphore_obtain(massmem_ip_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
  if (RTEMS_SUCCESSFUL != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to grab massmem ip semaphore, error: %s",
        rtems_status_text(status));

    return status;
  }

  switch (ioctl_args->command) {
    case MASSMEM_IO_RESET:
      massmem_interrupt_enable(MASSMEM_IE_RESET_DONE);

      massmem_reset_target(MASSMEM_TS_TARGET_CE);

      /* Wait for reset on chip CE is complete */
      status = rtems_semaphore_obtain(massmem_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
      if (RTEMS_SUCCESSFUL != status) {
        DBG(DBG_SEVERITY_ERR, "Failed to grab massmem interrupt semaphore, error: %s",
            rtems_status_text(status));

        massmem_interrupt_disable(MASSMEM_IE_RESET_DONE);
        rtems_semaphore_release(massmem_ip_sem_id);

        return status;
      }

      massmem_reset_target(MASSMEM_TS_TARGET_CE2);

      /* Wait for reset on chip CE2 is complete */
      status = rtems_semaphore_obtain(massmem_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
      if (RTEMS_SUCCESSFUL != status) {
        DBG(DBG_SEVERITY_ERR, "Failed to grab massmem interrupt semaphore, error: %s",
            rtems_status_text(status));

        massmem_interrupt_disable(MASSMEM_IE_RESET_DONE);
        rtems_semaphore_release(massmem_ip_sem_id);

        return status;
      }

      massmem_interrupt_disable(MASSMEM_IE_RESET_DONE);

      ioctl_args->ioctl_return = ENOERR;
      break;

    case MASSMEM_IO_READ_DATA_STATUS:
      ioctl_args->ioctl_return =
        massmem_read_status((uint32_t*)ioctl_args->buffer, &edac_status, &ctrl_status);
      break;

    case MASSMEM_IO_READ_EDAC_STATUS:
      ioctl_args->ioctl_return =
        massmem_read_status(&data_status, (uint8_t*)ioctl_args->buffer, &ctrl_status);
      break;

    case MASSMEM_IO_READ_CTRL_STATUS:
      ioctl_args->ioctl_return =
        massmem_read_status(&data_status,
                            &edac_status,
                            (uint16_t*)ioctl_args->buffer);
      break;

    case MASSMEM_IO_READ_ID:
      ioctl_args->ioctl_return = massmem_read_id((massmem_cid_t*)ioctl_args->buffer);
      break;

    case MASSMEM_IO_ERASE_BLOCK:
      massmem_interrupt_enable(MASSMEM_IE_ERASE_DONE);
      ioctl_args->ioctl_return = massmem_erase_block((uint32_t)ioctl_args->buffer);
      if (ioctl_args->ioctl_return < 0) {
        massmem_interrupt_disable(MASSMEM_IE_ERASE_DONE);
        rtems_semaphore_release(massmem_ip_sem_id);

        return RTEMS_SUCCESSFUL; /* Failed to erase block */
      }

      /* Obtain interrupt semaphore to yield cpu and await interrupt for erase done */
      status = rtems_semaphore_obtain(massmem_int_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
      if (RTEMS_SUCCESSFUL != status) {
        DBG(DBG_SEVERITY_ERR, "Failed to grab massmem interrupt semaphore, error: %s",
            rtems_status_text(status));

        massmem_interrupt_disable(MASSMEM_IE_ERASE_DONE);
        rtems_semaphore_release(massmem_ip_sem_id);
        return status;
      }

      massmem_interrupt_disable(MASSMEM_IE_ERASE_DONE);
      /* Set erase block status */
      ioctl_args->ioctl_return = massmem_verify_command();
      break;

    case MASSMEM_IO_READ_SPARE_AREA:
      spare_args = (massmem_ioctl_spare_area_args_t*) ioctl_args->buffer;
      /* Always ignore EDAC, since not available in non-raw mode (which is what current users
       * of the driver likely expects).
       */
      ioctl_args->ioctl_return =
        massmem_read_spare_area_poll(spare_args->page_num,
                                     spare_args->offset,
                                     MASSMEM_EDAC_INTERLEAVING,
                                     spare_args->data_buf,
                                     NULL,
                                     spare_args->size);
      rtems_cache_invalidate_multiple_data_lines(spare_args->data_buf, spare_args->size);
      break;

    case MASSMEM_IO_WRITE_SPARE_AREA:
      spare_args = (massmem_ioctl_spare_area_args_t*) ioctl_args->buffer;
      ioctl_args->ioctl_return =
        /* Call helper function, in this unit */
        massmem_write_spare_area(spare_args->page_num,
                                 spare_args->offset,
                                 spare_args->data_buf,
                                 spare_args->size);
      break;

    case MASSMEM_IO_BAD_BLOCK_CHECK:
      ioctl_args->ioctl_return =
        massmem_factory_bad_block_check((uint32_t)ioctl_args->buffer);
      break;

    case MASSMEM_IO_ERROR_INJECTION:
      error_injection_args = (massmem_ioctl_error_injection_args_t*) ioctl_args->buffer;
      ioctl_args->ioctl_return =
        massmem_program_page_poll(error_injection_args->page_num,
                                  error_injection_args->data_buf,
                                  error_injection_args->error_injection,
                                  error_injection_args->size);
      break;

    case MASSMEM_IO_GET_PAGE_BYTES:
      memcpy(ioctl_args->buffer, &massmem_page_bytes, sizeof(massmem_page_bytes));
      ioctl_args->ioctl_return = RTEMS_SUCCESSFUL;
      break;

    case MASSMEM_IO_GET_SPARE_AREA_BYTES:
      memcpy(ioctl_args->buffer, &massmem_spare_area_bytes, sizeof(massmem_spare_area_bytes));
      ioctl_args->ioctl_return = RTEMS_SUCCESSFUL;
      break;

    default:
      /* Release the semaphore before returning the error. */
      rtems_semaphore_release(massmem_ip_sem_id);
      return RTEMS_NOT_DEFINED;
  }

  rtems_semaphore_release(massmem_ip_sem_id);

  /* All non-RTEMS errors passed in ioctl_return variable at this point */
  return RTEMS_SUCCESSFUL;
}
