/*-------------------------------------------------------------------------
 * Copyright (C) 2016 - 2017 ÅAC Microtec AB
 *
 * Filename:     massmem_flash_common.h
 * Module name:  massmem_flash
 *
 * Author(s):    MaWe
 * Support:      support@aacmicrotec.com
 * Description:  Commmon declarations and defines for bare metal and RTEMS.
 *
 *               These are common items which needs to be available to both the
 *               bare metal and RTEMS driver, and additionally exposed to the
 *               application layer utilising the driver, thus cannot be in
 *               either of the bare metal or RTEMS headers since those are not
 *               allowed to include one another (in order to avoid either
 *               exposing bare metal to application layer, or making bare metal
 *               dependent on RTEMS, or duplicating defines).
 *
 * Requirements: Adheres to the ÅAC Mass Memory Flash Driver Requirement
 *               Specification Rev. A
 *-----------------------------------------------------------------------*/

#ifndef _MASSMEM_FLASH_COMMON_H_
#define _MASSMEM_FLASH_COMMON_H_

#include <stdint.h>

/* MASSMEM_ENABLE_32GB can be set via the BSP_AAC_MASSMEM_ENABLE_32GB when compiling the
 * RTEMS BSP.
 *
 * When using bare-metal, the MASSMEM_ENABLE_32GB define needs to be injected at compile-time
 * for the driver and the application.
 */
#ifdef __rtems__
  #include <bspopts.h>
  #ifdef BSP_AAC_MASSMEM_ENABLE_32GB
    #define MASSMEM_ENABLE_32GB
  #endif
#endif

/* The mass memory consists of 5 Micron MT29F32G08AFABA or 5 Micron MT29F64G08AFAAA chips,
 * each with 4GB (8GB if using MT29F64G08AFAAA and the new api) capacity. 4 of these chips
 * are used for data, 1 for EDAC.
 *
 * Access is done in parallel, a 32bit read/program is split as 1 byte to 4 sub-pages in 4
 * chips, plus 1 byte EDAC. Similarly an erase is always sent to 4 + 1 parallel sub-blocks.
 *
 * For the sake of abstraction, this group of 4 + 1 parallel sub-pages is referred to as a
 * single page, with a data capacity of 4 * 4096 bytes (4 * 8192 bytes if using
 * MT29F64G08AFAAA and the new api). Similarly the group of 4 + 1 sub-blocks is referred to
 * as a single block.
 */

#define MASSMEM_WORD_BITS 32
#define MASSMEM_WORD_SIZE (MASSMEM_WORD_BITS / 8)

#define MASSMEM_16GB_PAGE_SIZE        4096
#define MASSMEM_16GB_PAGE_BYTES       (MASSMEM_16GB_PAGE_SIZE * MASSMEM_WORD_SIZE)
#define MASSMEM_16GB_SPARE_AREA_SIZE  224
#define MASSMEM_16GB_SPARE_AREA_BYTES (MASSMEM_16GB_SPARE_AREA_SIZE * MASSMEM_WORD_SIZE)

#define MASSMEM_32GB_PAGE_SIZE        8192
#define MASSMEM_32GB_PAGE_BYTES       (MASSMEM_32GB_PAGE_SIZE * MASSMEM_WORD_SIZE)
#define MASSMEM_32GB_SPARE_AREA_SIZE  448
#define MASSMEM_32GB_SPARE_AREA_BYTES (MASSMEM_32GB_SPARE_AREA_SIZE * MASSMEM_WORD_SIZE)

#ifndef MASSMEM_ENABLE_32GB
  /* Current api with size determined at compile time, exposes only 16GB of capacity, to allow
   * same api for both chips.
   */
  #define MASSMEM_PAGE_SIZE        MASSMEM_16GB_PAGE_SIZE
  #define MASSMEM_PAGE_BYTES       MASSMEM_16GB_PAGE_BYTES
  #define MASSMEM_SPARE_AREA_SIZE  MASSMEM_16GB_SPARE_AREA_SIZE
  #define MASSMEM_SPARE_AREA_BYTES MASSMEM_16GB_SPARE_AREA_BYTES
#endif

/* New api with size determined at runtime. Static maximums provided for setting sufficient
 * buffer sizes at compile time, without needing to know the target chip type.
 */
#define MASSMEM_PAGE_SIZE_MAX             MASSMEM_32GB_PAGE_SIZE
#define MASSMEM_PAGE_BYTES_MAX            MASSMEM_32GB_PAGE_BYTES
#define MASSMEM_SPARE_AREA_SIZE_MAX       MASSMEM_32GB_SPARE_AREA_SIZE
#define MASSMEM_SPARE_AREA_SIZE_BYTES_MAX MASSMEM_32GB_SPARE_AREA_SIZE_BYTES

#define MASSMEM_PAGES_PER_BLOCK 128

/* There are two logical units, LUNs, in each chip, and the block are interleaved odd/even
 * between them.
 */
#define MASSMEM_BLOCKS_PER_LUN  4096
#define MASSMEM_NUM_LUNS        2

#define MASSMEM_BLOCKS          (MASSMEM_NUM_LUNS * MASSMEM_BLOCKS_PER_LUN)
#define MASSMEM_MAX_NR_PAGES    (MASSMEM_BLOCKS * MASSMEM_PAGES_PER_BLOCK)

#define MASSMEM_FACTORY_BAD_BLOCK_MARKED  1
#define MASSMEM_FACTORY_BAD_BLOCK_CLEARED 0

/* Chip ID data structure */
#define MASSMEM_CID_SIZE 5
typedef struct
{
  uint8_t edac[MASSMEM_CID_SIZE];
  uint8_t chip0[MASSMEM_CID_SIZE];
  uint8_t chip1[MASSMEM_CID_SIZE];
  uint8_t chip2[MASSMEM_CID_SIZE];
  uint8_t chip3[MASSMEM_CID_SIZE];
} massmem_cid_t;
static const uint8_t massmem_cid_MT29F32G08AFABA[] = { 0x2C, 0x48, 0x00, 0x26, 0x89 };
static const uint8_t massmem_cid_MT29F64G08AFAAA[] = { 0x2C, 0x68, 0x00, 0x27, 0xA9 };

typedef struct
{
  uint8_t  edac_error_injection;
  uint32_t data_error_injection;
} massmem_error_injection_t;

#endif /* _MASSMEM_FLASH_COMMON_H_ */
