/*----------------------------------------------------------------------
 * Copyright (C) 2015 - 2017 ÅAC Microtec AB
 *
 * Filename:     massmem_flash_rtems.h
 * Module name:  massmem_flash
 *
 * Author(s):    OdMa, JoBe, MaWe
 * Support:      support@aacmicrotec.com
 * Description:  NAND mass memory flash controller driver declarations
 * Requirements: Adheres to the ÅAC Mass Memory Flash Driver Requirement
 *               Specification Rev. A
 *--------------------------------------------------------------------*/

#ifndef _RTEMS_MASSMEM_FLASH_H_
#define _RTEMS_MASSMEM_FLASH_H_

#include <rtems.h>

#include "massmem_flash_common.h"

#define MASSMEM_DEVICE_NAME                   "/dev/massmem"
#define MASSMEM_OP_SUCCESS                    0xE0E0E0E0

#define MASSMEM_IOCTL_GROUP                   'G'
#define MASSMEM_IO_ERROR_INJECTION            _IOW(MASSMEM_IOCTL_GROUP, 21, void*)
#define MASSMEM_IO_RESET                      _IO (MASSMEM_IOCTL_GROUP, 22)
#define MASSMEM_IO_READ_DATA_STATUS           _IOR(MASSMEM_IOCTL_GROUP, 23, uint32_t*)
#define MASSMEM_IO_READ_CTRL_STATUS           _IOR(MASSMEM_IOCTL_GROUP, 24, uint8_t*)
#define MASSMEM_IO_READ_EDAC_STATUS           _IOR(MASSMEM_IOCTL_GROUP, 25, uint8_t*)
#define MASSMEM_IO_READ_ID                    _IOR(MASSMEM_IOCTL_GROUP, 26, uint8_t*)
#define MASSMEM_IO_ERASE_BLOCK                _IOW(MASSMEM_IOCTL_GROUP, 27, uint32_t)
#define MASSMEM_IO_READ_SPARE_AREA            _IOR(MASSMEM_IOCTL_GROUP, 28, void*)
#define MASSMEM_IO_WRITE_SPARE_AREA           _IOR(MASSMEM_IOCTL_GROUP, 29, void*)
#define MASSMEM_IO_BAD_BLOCK_CHECK            _IOR(MASSMEM_IOCTL_GROUP, 30, uint32_t)
#define MASSMEM_IO_GET_PAGE_BYTES             _IOR(MASSMEM_IOCTL_GROUP, 31, size_t*)
#define MASSMEM_IO_GET_SPARE_AREA_BYTES       _IOR(MASSMEM_IOCTL_GROUP, 32, size_t*)

/* Supported IO-controls */
typedef struct
{
  uint32_t page_num;
  uint32_t offset;
  uint8_t *data_buf;
  uint8_t *edac_buf;
  uint32_t size;
} massmem_ioctl_spare_area_args_t;

typedef struct
{
  uint32_t page_num;
  uint8_t *data_buf;
  uint32_t size;
  massmem_error_injection_t *error_injection;
} massmem_ioctl_error_injection_args_t;


rtems_device_driver massmem_initialize(rtems_device_major_number major,
                                       rtems_device_minor_number unused ,
                                       void *args);

rtems_device_driver massmem_open(rtems_device_major_number major,
                                 rtems_device_minor_number unused,
                                 void *args);

rtems_device_driver massmem_close(rtems_device_major_number major,
                                  rtems_device_minor_number unused,
                                  void *args);

rtems_device_driver massmem_read(rtems_device_major_number major,
                                 rtems_device_minor_number unused,
                                 void *args);

rtems_device_driver massmem_write(rtems_device_major_number major,
                                  rtems_device_minor_number unused,
                                  void *args);

rtems_device_driver massmem_control(rtems_device_major_number major,
                                    rtems_device_minor_number unused,
                                    void *args);

#define AAC_MASSMEM_DRIVER_TABLE_ENTRY {        \
    massmem_initialize,                         \
    massmem_open,                               \
    massmem_close,                              \
    massmem_read,                               \
    massmem_write,                              \
    massmem_control }


#endif /* _RTEMS_MASSMEM_FLASH_H_ */
