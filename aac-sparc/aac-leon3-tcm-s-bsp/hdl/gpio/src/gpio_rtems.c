/*-----------------------------------------------------------------------------
 * Copyright (C) 2005-2016 ÅAC Microtec AB
 *
 * Filename:     gpio_rtems.c
 * Module name:  gpio
 *
 * Author(s):    OdMa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  GPIO RTEMS Driver implementation
 * Requirements: Adheres to the ÅAC GPIO Driver Requirement Specification Rev. A
 *-----------------------------------------------------------------------------------------*/

#include <rtems/libio.h>
#include <rtems/seterr.h>
#include <rtems/score/cpu.h>

#include <bsp.h>
#include <bsp/irq.h>
#include <rtems/rtems/tasks.h>

#include "gpio.h"
#include "gpio_rtems.h"
#include "toolchain_support.h"

/* Define the bare-metal constants from an RTEMS context */
uint32_t _gpio_base = AAC_BSP_GPIO_BASE;
uint32_t _gpio_IRQ = AAC_BSP_GPIO_IRQ;
uint32_t _gpio_count = AAC_BSP_GPIO_COUNT;

static gpio_devices_t gpio_devices;

void gpio_isr_callback(uint32_t int_active, uint32_t data)
{
  uint32_t i;

  DBG(DBG_SEVERITY_INFO, "GPIO device callback int_active=0x%x data=0x%x", int_active,
      data);

  i = ffs(int_active);
  while (i) {
    /* Signal any active reads that the GPIO state changed */
    rtems_semaphore_release(gpio_devices.semaphore_id[i - 1]);

    DBG(DBG_SEVERITY_INFO, "GPIO device sending interrupt msg for %u", i - 1);

    int_active &= ~GPIO(i - 1);
    i = ffs(int_active);
  }
}

rtems_device_driver rtems_gpio_initialize(rtems_device_major_number major,
                                          rtems_device_minor_number minor,
                                          void *args)
{
  rtems_status_code     status;
  uint32_t              i;

  DBG(DBG_SEVERITY_INFO, "GPIO driver (M%lu:m:%lu) initialize.", major, minor);

  gpio_init();
  gpio_devices.gpio_diff_mode = 0;

  /* Loop through all the defined GPIOs and register a device name for it */
  for (i = 0; i < AAC_BSP_GPIO_COUNT; i++) {
    status = rtems_io_register_name(gpio_device_names[i], major, i);
    if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR, "Failed to register GPIO driver (%d)", status);
      rtems_io_unregister_driver(major);
      rtems_set_errno_and_return_minus_one(status);
    }

    /* Create semaphores for interrupt signalling */
    status = rtems_semaphore_create(rtems_build_name('G', 'P', '-', 'A' + i),
                                    0,
                                    RTEMS_FIFO | RTEMS_SIMPLE_BINARY_SEMAPHORE |
                                    RTEMS_NO_MULTIPROCESSOR_RESOURCE_SHARING |
                                    RTEMS_LOCAL,
                                    RTEMS_NO_PRIORITY_CEILING,
                                    &gpio_devices.semaphore_id[i]);
    if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR,
          "Failed to create semaphore for GPIO pin %d, got status: %d",
          index, status);
      rtems_io_unregister_driver(major);
      rtems_set_errno_and_return_minus_one(status);
    }
    /* Set default value of timeout to be indefinite */
    gpio_devices.timeout[i] = 0;
  }

  DBG(DBG_SEVERITY_INFO, "GPIO driver %s registered.", gpio_device_names[i]);

  /* Install the ISR */
  status = rtems_interrupt_handler_install(AAC_BSP_GPIO_IRQ,
                                           GPIO_DEVICE_NAME,
                                           RTEMS_INTERRUPT_UNIQUE,
                                           gpio_isr,
                                           NULL);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to install RTEMS gpio interrupt handler %d, %s",
        status, rtems_status_text(status));
    rtems_io_unregister_driver(major);
    rtems_set_errno_and_return_minus_one(status);
  }

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_gpio_open(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *args)
{
  rtems_libio_open_close_args_t *rw_args;

  rw_args = (rtems_libio_open_close_args_t *) args;

  DBG(DBG_SEVERITY_INFO, "Opening GPIO device %s.", gpio_device_names[minor]);

  if (minor >= AAC_BSP_GPIO_COUNT) {
    DBG(DBG_SEVERITY_ERR, "GPIO device does not exist. Invalid GPIO minor %d", minor);
    rtems_set_errno_and_return_minus_one(ENODEV);
  }

  if ((rw_args->flags & LIBIO_FLAGS_OPEN) == true) {
    DBG(DBG_SEVERITY_WARN, "GPIO device [%lu,%lu] already opened. Aborting request.",
        major, minor);
    rtems_set_errno_and_return_minus_one(EALREADY);
  }

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_gpio_close(rtems_device_major_number major,
                                     rtems_device_minor_number minor,
                                     void *args)
{
  DBG(DBG_SEVERITY_INFO, "Closing GPIO device %s.", gpio_device_names[minor]);

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_gpio_read(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *args)
{
  rtems_libio_rw_args_t *       rw_args;
  uint32_t                      gpio_data;
  uint32_t                      int_enable;
  uint32_t                      fall_edge_config;
  uint32_t                      rise_edge_config;
  rtems_status_code             sem_result;

  rw_args = (rtems_libio_rw_args_t *) args;

  DBG(DBG_SEVERITY_INFO, "Reading from GPIO device %s.", gpio_device_names[minor]);

  if (rw_args == (rtems_libio_rw_args_t *) NULL) {
    DBG(DBG_SEVERITY_ERR, "Unable to read %s. Bad data argument.", gpio_device_names[minor]);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  rw_args->bytes_moved = 0;

  /* Now, check if there is an interrupt enabled (with a fall or rise detection set) for
     this pin. In that case we should block and wait for an change on the pin before
     returning. */
  int_enable = gpio_get_interrupt_enable();
  fall_edge_config = gpio_get_fall_edge_detection();
  rise_edge_config = gpio_get_rise_edge_detection();

  if ((int_enable & GPIO(minor)) &&
      ((fall_edge_config & GPIO(minor)) || (rise_edge_config & GPIO(minor)))) {
    /* Ok, we need to block */
    sem_result = rtems_semaphore_obtain(gpio_devices.semaphore_id[minor], RTEMS_WAIT,
                                        gpio_devices.timeout[minor]);
    if (sem_result != RTEMS_SUCCESSFUL) {
      rtems_set_errno_and_return_minus_one(rtems_status_code_to_errno(sem_result));
    }
  }

  gpio_data = gpio_get_datain();

  DBG(DBG_SEVERITY_INFO, "Read data=%u for %s.", gpio_data,
      gpio_device_names[minor]);

  /* Convert the GPIO array data down to just the bit we're interested in */
  rw_args->buffer[0] = (uint8_t) ((gpio_data & GPIO(minor)) >> minor);

  /* We always read only one byte */
  rw_args->bytes_moved = 1;

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_gpio_write(rtems_device_major_number major,
                                     rtems_device_minor_number minor,
                                     void *args)
{
  rtems_libio_rw_args_t *       rw_args;
  uint32_t                      direction;
  uint32_t                      dataout;
  uint32_t                      master_pin;

  rw_args = (rtems_libio_rw_args_t *) args;

  DBG(DBG_SEVERITY_INFO, "Writing to GPIO device %s.", gpio_device_names[minor]);

  if (rw_args == (rtems_libio_rw_args_t *) NULL) {
    DBG(DBG_SEVERITY_ERR, "Unable to write %s. Bad data argument.",
        gpio_device_names[minor]);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  rw_args->bytes_moved = 0;

  direction = gpio_get_direction();

  /* Get the current output setting */
  dataout = gpio_get_dataout();
  if (((gpio_devices.gpio_diff_mode & GPIO(minor)) >> minor) == GPIO_DIFF_MODE_ENABLE) {
    /* We're working in diff mode, then this needs to be done on a pair */

    /* Get the lower numbered pin */
    master_pin = (minor / 2)*2;

    /* Clear the bits in question */
    dataout &= ~((uint32_t) (3 << minor));

    /* Now, set them to the value the user wanted */
    if ((rw_args->buffer[0] & 0x1) == 0) {
      /* Logic '0' on lower pin */
      dataout |= 2 << master_pin;
    } else {
      /* Logic '1' on lower pin */
      dataout |= 1 << master_pin;
    }
  } else {
    /* Normal access */
    /* Clear the bit in question */
    dataout &= ~GPIO(minor);

    /* Now, set it to the value the user wanted */
    dataout |= (((uint32_t) rw_args->buffer[0]) & 0x1) << minor;
  }

  /* Finally, write this back to the IP */
  gpio_set_dataout(dataout);

  DBG(DBG_SEVERITY_INFO, "Write data=%u for %s.",
      (uint32_t) rw_args->buffer[0],
      gpio_device_names[minor]);

  /* We always allow only one byte */
  rw_args->bytes_moved = 1;

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_gpio_control(rtems_device_major_number major,
                                       rtems_device_minor_number minor,
                                       void *args)
{
  rtems_libio_ioctl_args_t *    ioctl_args;
  uint32_t *                    config;
  uint32_t                      data;
  uint32_t                      other_edge;
  uint32_t                      master_pin;
  uint32_t                      slave_pin;

  ioctl_args = (rtems_libio_ioctl_args_t *) args;
  config = (uint32_t *) ioctl_args->buffer;

  DBG(DBG_SEVERITY_INFO, "Control command for GPIO device %s.", gpio_device_names[minor]);

  /* Make sure the argument is sound */
  if (ioctl_args == (rtems_libio_ioctl_args_t *) NULL) {
    DBG(DBG_SEVERITY_ERR, "Unable to configure %s. Bad data argument.",
        gpio_device_names[minor]);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  /* Default error code - no error */
  ioctl_args->ioctl_return = ENOERR;

  switch (ioctl_args->command) {

    case GPIO_IOCTL_GET_DIRECTION:
      /* Get the current output setting */
      data = gpio_get_direction();

      *config = ((data & GPIO(minor)) >> minor) & 0x1;

      DBG(DBG_SEVERITY_INFO, "Get direction=%u for %s.", *config,
          gpio_device_names[minor]);

      break;

    case GPIO_IOCTL_SET_DIRECTION:
      /* Get the current output setting */
      data = gpio_get_direction();
      /* Clear the bit in question */
      data &= ~(GPIO(minor));
      /* Now, set it to the value the user wanted */
      data |= (*config & 0x1) << minor;
      /* Finally, write this back to the IP */
      gpio_set_direction(data);

      DBG(DBG_SEVERITY_INFO, "Set direction=%u for %s.", *config,
          gpio_device_names[minor]);

      break;

    case GPIO_IOCTL_GET_FALL_EDGE_DETECTION:
      /* Get the current falling edge detection setting */
      data = gpio_get_fall_edge_detection();
      *config = ((data & GPIO(minor)) >> minor) & 0x1;

      DBG(DBG_SEVERITY_INFO, "Get falling edge detection %u for %s.", *config,
          gpio_device_names[minor]);

      break;

    case GPIO_IOCTL_SET_FALL_EDGE_DETECTION:
      /* Get the current falling edge detection setting */
      data = gpio_get_fall_edge_detection();
      /* Clear the bit in question */
      data &= ~(GPIO(minor));
      /* Now, set it to the value the user wanted */
      data |= (*config & 0x1) << minor;
      /* Finally, write this back to the IP */
      gpio_set_fall_edge_detection(data);

      /* Check to see if we need to turn on/off the interrupt enable to match the
         new behaviour */
      data = gpio_get_interrupt_enable();
      if (*config & 0x1) {
        /* Enable interrupt */
        data |= GPIO(minor);
      } else {
        /* Possibly disable interrupt */
        other_edge = gpio_get_rise_edge_detection();
        if ((other_edge & GPIO(minor)) == 0) {
          /* The other edge isn't enabled either, disable the interrupt */
          data &= ~GPIO(minor);
        }
      }
      /* Write back calculated interrupt setting */
      gpio_set_interrupt_enable(data);

      DBG(DBG_SEVERITY_INFO, "Set falling edge detection %u for %s.", *config,
          gpio_device_names[minor]);

      break;

    case GPIO_IOCTL_GET_RISE_EDGE_DETECTION:
      /* Get the current rising edge detection setting */
      data = gpio_get_rise_edge_detection();
      *config = ((data & GPIO(minor)) >> minor) & 0x1;

      DBG(DBG_SEVERITY_INFO, "Get rising edge detection %u for %s.", *config,
          gpio_device_names[minor]);

      break;

    case GPIO_IOCTL_SET_RISE_EDGE_DETECTION:
      /* Get the current rising edge detection setting */
      data = gpio_get_rise_edge_detection();
      /* Clear the bit in question */
      data &= ~(GPIO(minor));
      /* Now, set it to the value the user wanted */
      data |= (*config & 0x1) << minor;
      /* Finally, write this back to the IP */
      gpio_set_rise_edge_detection(data);

      /* Check to see if we need to turn on/off the interrupt enable to match the
         new behaviour */
      data = gpio_get_interrupt_enable();
      if (*config & 0x1) {
        /* Enable interrupt */
        data |= GPIO(minor);
      } else {
        /* Possibly disable interrupt */
        other_edge = gpio_get_fall_edge_detection();
        if ((other_edge & GPIO(minor)) == 0) {
          /* The other edge isn't enabled either, disable the interrupt */
          data &= ~GPIO(minor);
        }
      }
      /* Write back calculated interrupt setting */
      gpio_set_interrupt_enable(data);

      DBG(DBG_SEVERITY_INFO, "Set rising edge detection %u for %s.", *config,
          gpio_device_names[minor]);

      break;

    case GPIO_IOCTL_GET_TIMESTAMP_ENABLE:
      /* Get the current timestamp setting */
      data = gpio_get_timestamp_enable();
      *config = ((data & GPIO(minor)) >> minor) & 0x1;

      DBG(DBG_SEVERITY_INFO, "Get timestamp enable=%u for %s.", *config,
          gpio_device_names[minor]);

      break;

    case GPIO_IOCTL_SET_TIMESTAMP_ENABLE:
      /* Get the current timestamp setting */
      data = gpio_get_timestamp_enable();
      /* Clear the bit in question */
      data &= ~(GPIO(minor));
      /* Now, set it to the value the user wanted */
      data |= (*config & 0x1) << minor;
      /* Finally, write this back to the IP */
      gpio_set_timestamp_enable(data);

      DBG(DBG_SEVERITY_INFO, "Set timestamp enable=%u for %s.", *config,
          gpio_device_names[minor]);

      break;

    case GPIO_IOCTL_GET_DIFF_MODE:
      /* Get the current differential mode setting */
      *config = (gpio_devices.gpio_diff_mode & GPIO(minor)) >> minor;

      DBG(DBG_SEVERITY_INFO, "Get differential mode=%u for %s.", *config,
          gpio_device_names[minor]);
      break;

    case GPIO_IOCTL_SET_DIFF_MODE:
      /* For differential mode, it's vital that any mode change is simultaneous on both
         pins in the pair. The lower numbered pin will be the master in all things
         regarding changes of the differentialness. */
      master_pin = (minor / 2) * 2;
      slave_pin = master_pin + 1;

      if (*config == GPIO_DIFF_MODE_DISABLE) {
        DBG(DBG_SEVERITY_INFO, "Set differential mode=%u for %s.", 0,
            gpio_device_names[minor]);
        /* Mark as disabled for both master and slave pins */
        gpio_devices.gpio_diff_mode &= ~((1 << master_pin) | (1 << slave_pin));
      } else {
        DBG(DBG_SEVERITY_INFO, "Set differential mode=%u for %s.", 1,
            gpio_device_names[minor]);
        /* Mark as enabled for both master and slave pins */
        gpio_devices.gpio_diff_mode |= (1 << master_pin) | (1 << slave_pin);

        /* Set the slave pin value to the inverse of the master pin */
        data = gpio_get_dataout();
        if (data & (1 << master_pin)) {
          data &= ~(1 << slave_pin);
        } else {
          data |= 1 << slave_pin;
        }
        gpio_set_dataout(data);

        /* Set the slave pin direction to the same direction as the master pin */
        data = gpio_get_direction();
        if (data & (1 << master_pin)) {
          data |= 1 << slave_pin;
        } else {
          data &= ~(1 << slave_pin);
        }
        gpio_set_direction(data);
      }
      break;

    case GPIO_IOCTL_GET_EDGE_TIMEOUT:
      /* Get the edge change timeout in ticks */
      *config = (uint32_t) gpio_devices.timeout[minor];
      break;

    case GPIO_IOCTL_SET_EDGE_TIMEOUT:
      /* Set the edge change timeout in ticks */
      gpio_devices.timeout[minor] = (rtems_interval) *config;
      break;

    default:
      DBG(DBG_SEVERITY_WARN, "Unknown ioctl command 0x%08X - ignoring request.",
          ioctl_args->command);
      break;
  }

  return RTEMS_SUCCESSFUL;
}
