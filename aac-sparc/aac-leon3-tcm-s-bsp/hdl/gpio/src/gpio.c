/*-----------------------------------------------------------------------------
 * Copyright (C) 2005-2016 ÅAC Microtec AB
 *
 * Filename:     gpio.c
 * Module name:  gpio
 *
 * Author(s):    OdMa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  GPIO bare metal driver implementation
 * Requirements: ÅAC GPIO Driver Requirement Specification, Rev. A
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <errno.h>
#include "toolchain_support.h"
#include "gpio.h"

/* Board definitions */
#ifdef __rtems__
extern uint32_t _gpio_base;
extern uint32_t _gpio_IRQ;
extern uint32_t _gpio_count;
#else
uint32_t _gpio_base =  AAC_BSP_GPIO_BASE;
uint32_t _gpio_IRQ =   AAC_BSP_GPIO_IRQ;
uint32_t _gpio_count = AAC_BSP_GPIO_COUNT;
#endif /*__rtems__*/

extern void gpio_isr_callback(uint32_t int_active, uint32_t data);

#if (_gpio_count > 32)
#error "Maximum supported number of GPIO pins is 32!"
#endif

void gpio_init(void)
{
  /* Set all registers in a known state */
  REG32(_gpio_base + GPIO_TRISTATE_REG) = 0xFFFFFFFF;
  REG32(_gpio_base + GPIO_DATAOUT_REG) = 0x0;
  REG32(_gpio_base + GPIO_INT_STATUS_REG) = 0xFFFFFFFF;
  REG32(_gpio_base + GPIO_INT_ENABLE_REG) = 0x0;
  REG32(_gpio_base + GPIO_TIMESTAMP_REG) = 0x0;
  REG32(_gpio_base + GPIO_RISING_EDGE_REG) = 0x0;
  REG32(_gpio_base + GPIO_FALLING_EDGE_REG) = 0x0;
}

inline uint32_t gpio_get_direction(void)
{
  return REG32(_gpio_base + GPIO_TRISTATE_REG);
}

inline void gpio_set_direction(uint32_t direction)
{
  REG32(_gpio_base + GPIO_TRISTATE_REG) = direction;
}

inline uint32_t gpio_get_interrupt_status(void)
{
  return REG32(_gpio_base + GPIO_INT_STATUS_REG);
}

inline void gpio_clear_interrupt(uint32_t clear)
{
  REG32(_gpio_base + GPIO_INT_STATUS_REG) = clear;
}

inline uint32_t gpio_get_fall_edge_detection(void)
{
  return REG32(_gpio_base + GPIO_FALLING_EDGE_REG);
}

inline void gpio_set_fall_edge_detection(uint32_t fall_edge_config)
{
  REG32(_gpio_base + GPIO_FALLING_EDGE_REG) = fall_edge_config;
}

inline uint32_t gpio_get_rise_edge_detection(void)
{
  return REG32(_gpio_base + GPIO_RISING_EDGE_REG);
}

inline void gpio_set_rise_edge_detection(uint32_t rise_edge_config)
{
  REG32(_gpio_base + GPIO_RISING_EDGE_REG) = rise_edge_config;
}

inline uint32_t gpio_get_interrupt_enable(void)
{
  return REG32(_gpio_base + GPIO_INT_ENABLE_REG);
}

inline void gpio_set_interrupt_enable(uint32_t int_enable)
{
  REG32(_gpio_base + GPIO_INT_ENABLE_REG) = int_enable;
}

inline uint32_t gpio_get_timestamp_enable(void)
{
  return REG32(_gpio_base + GPIO_TIMESTAMP_REG);
}

inline void gpio_set_timestamp_enable(uint32_t timestamp_enable)
{
  REG32(_gpio_base + GPIO_TIMESTAMP_REG) = timestamp_enable;
}

inline uint32_t gpio_get_datain(void)
{
  return REG32(_gpio_base + GPIO_DATAIN_REG);
}

inline uint32_t gpio_get_dataout(void)
{
  return REG32(_gpio_base + GPIO_DATAOUT_REG);
}

inline void gpio_set_dataout(uint32_t data)
{
  REG32(_gpio_base + GPIO_DATAOUT_REG) = data;
}

AAC_INTERRUPT_HANDLER_SIGNATURE(gpio_isr, data_ptr)
{
  uint32_t int_enabled;
  uint32_t int_status;
  uint32_t data;
  uint32_t int_active;

  /* Get interrupt status */
  int_status = REG32(_gpio_base + GPIO_INT_STATUS_REG);
  int_enabled = REG32(_gpio_base + GPIO_INT_ENABLE_REG);

  /* Filter out disabled interrupts */
  int_active = int_enabled & int_status;

  /* Clear interrupts before handling them. If any new interrupts occur on the
  same GPIOs in between reading the status register and the data register, the
  newest state will simply be reflected and handled by the ISR. If the interrupt
  occurs before clearing the status register, the extra interrupt will not be
  generated. If occurring after clearing the status register but before handling
  it in the callback, a new interrupt reflecting the exact same state will
  be generated. This has to be handled in the ISR callback. If the new interrupt
  occurs after the callback, the ISR will be executed again directly.

  If a new interrupt occurs on another GPIO, then a new interrupt will be
  generated on clear of the status register and will be seen as the ISR will
  be executed again directly. */
  REG32(_gpio_base + GPIO_INT_STATUS_REG) = int_active;

  /* Read current data for callback function */
  data = REG32(_gpio_base + GPIO_DATAIN_REG);

  /* Ahoy the callback function */
  gpio_isr_callback(int_active, data);
}
