/*-----------------------------------------------------------------------------
 * Copyright (C) 2005-2016 ÅAC Microtec AB
 *
 * Filename:     gpio.h
 * Module name:  gpio
 *
 * Author(s):    OdMa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  GPIO bare metal driver definitions
 * Requirements: ÅAC GPIO Driver Detailed Design Description
 *----------------------------------------------------------------------------*/

#ifndef _GPIO_H_
#define _GPIO_H_

#include <toolchain_support.h>
#include <stdint.h>
#include <stdbool.h>

#define GPIO_TRISTATE_REG       (0x00) /* In/Out: Tristate Register */
#define GPIO_DATAOUT_REG        (0x04) /* In/Out: Data Out Register */
#define GPIO_DATAIN_REG         (0x08) /* In:     Data In Register */
#define GPIO_INT_STATUS_REG     (0x0C) /* In/Out: Interrupt Status Register */
#define GPIO_INT_ENABLE_REG     (0x10) /* In/Out: Interrupt Enable Register */
#define GPIO_TIMESTAMP_REG      (0x14) /* In/Out: Time Stamp Register */
#define GPIO_RISING_EDGE_REG    (0x18) /* In/Out: Rising Edge Detection Register */
#define GPIO_FALLING_EDGE_REG   (0x1C) /* In/Out: Falling Edge Detection Register  */

#define GPIO(x)                 (1 << (x))

#define GPIO_DIRECTION_OUT      (0)
#define GPIO_DIRECTION_IN       (1)

#ifndef ENOERR
#define ENOERR                  (0)
#endif

/* Name:        gpio_init
 * @brief       Initializes the GPIO IP
 */
void gpio_init(void);

/* Name:        gpio_get_direction
 * @brief       Gets the direction of each supported I/O pin
 * @return      Configuration pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' indicates that the corresponding pin is in write mode
 *              '1' indicates that the corresponding pin is in read mode
 * @note        Bits > _gpio_count are undefined
 */
uint32_t gpio_get_direction(void);

/* Name:        gpio_set_direction
 * @brief       Sets the direction of each supported I/O pin
 * @param[in]   Configuration pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' indicates that the corresponding pin is in write mode
 *              '1' indicates that the corresponding pin is in read mode
 * @note        Bits > _gpio_count are undefined
 */
void gpio_set_direction(uint32_t direction);

/* Name:        gpio_get_interrupt_status
 * @brief       Gets the interrupt status of each supported I/O pin
 * @return      Configuration pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' indicates that the corresponding pin has not detected change
 *              '1' indicates that the corresponding pin has detected change
 *              Bits > _gpio_count are undefined
 */
uint32_t gpio_get_interrupt_status(void);

/* Name:        gpio_clear_interrupt
 * @brief       Clear interrupt of each supported I/O pin
 * @param[in]   Configuration pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' indicates that the corresponding pin is not affected
 *              '1' indicates that the corresponding pin shall be cleared
 * @note        Bits > _gpio_count are undefined
 */
void gpio_clear_interrupt(uint32_t clear);

/* Name:        gpio_get_fall_edge_detection
 * @brief       Gets the falling edge detection of each supported I/O pin
 * @return      Falling edge configuration pattern [_gpio_count - 1 .. 0] representing the
 *              I/O pins where
 *              '0' indicates that the corresponding pin does not detect falling edge
 *              '1' indicates that the corresponding pin does detect falling edge
 * @note        Bits > _gpio_count are undefined
 */
uint32_t gpio_get_fall_edge_detection(void);

/* Name:        gpio_set_fall_edge_detection
 * @brief       Sets the falling detection of each supported I/O pin
 * @param[in]   Falling edge configuration pattern [_gpio_count - 1 .. 0] representing the
 *              I/O pins where
 *              '0' indicates that the corresponding pin does not detect falling edge
 *              '1' indicates that the corresponding pin does detect falling edge
 * @note        Bits > _gpio_count are undefined
 */
void gpio_set_fall_edge_detection(uint32_t fall_edge_config);

/* Name:        gpio_get_rise_edge_detection
 * @brief       Gets the rising edge detection of each supported I/O pin
 * @return      Rising edge configuration pattern [_gpio_count - 1 .. 0] representing the
 *              I/O pins where
 *              '0' indicates that the corresponding pin does not detect rising edge
 *              '1' indicates that the corresponding pin does detect rising edge
 * @note        Bits > _gpio_count are undefined
 */
uint32_t gpio_get_rise_edge_detection(void);

/* Name:        gpio_set_rise_edge_detection
 * @brief       Sets the rising edge detection of each supported I/O pin
 * @param[in]   Rising edge configuration pattern [_gpio_count - 1 .. 0] representing the
 *              I/O pins where
 *              '0' indicates that the corresponding pin does not detect rising edge
 *              '1' indicates that the corresponding pin does detect rising edge
 * @note        Bits > _gpio_count are undefined
 */
void gpio_set_rise_edge_detection(uint32_t rise_edge_config);

/* Name:        gpio_get_interrupt_enable
 * @brief       Gets the interrupt enable of each supported I/O pin
 * @return      Configuration pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' indicates that the corresponding pin does not generate an interrupt on
 *                  change
 *              '1' indicates that the corresponding pin does generate an interrupt on change
 * @note        Bits > _gpio_count are undefined
 */
uint32_t gpio_get_interrupt_enable(void);

/* Name:        gpio_set_interrupt_enable
 * @brief       Sets the interrupt enable of each supported I/O pin
 * @param[in]   Configuration pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' indicates that the corresponding pin does not generate an interrupt on
 *                  change
 *              '1' indicates that the corresponding pin does generate an interrupt on change
 * @note        Bits > _gpio_count are undefined
 */
void gpio_set_interrupt_enable(uint32_t int_enable);

/* Name:        gpio_get_timestamp_enable
 * @brief       Gets the timestamp enable of each supported I/O pin
 * @return      Configuration pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' indicates that the corresponding pin does not generate a timestamp on
 *                  change
 *              '1' indicates that the corresponding pin does generate a timestamp on change
 * @note        Bits > _gpio_count are undefined
 */
uint32_t gpio_get_timestamp_enable(void);

/* Name:        gpio_set_timestamp_enable
 * @brief       Sets the timestamp enable of each supported I/O pin
 * @param[in]   Configuration pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' indicates that the corresponding pin does not generate a timestamp on
 *                  change
 *              '1' indicates that the corresponding pin does generate a timestamp on change
 * @note        Bits > _gpio_count are undefined
 */
void gpio_set_timestamp_enable(uint32_t timestamp_enable);

/* Name:        gpio_get_datain
 * @brief       Reads value of each supported I/O pin
 * @return      Data pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' or '1' indicates the value of the corresponding pin
 * @note        Bits > _gpio_count are undefined
 */
uint32_t gpio_get_datain(void);

/* Name:        gpio_get_dataout
 * @brief       Reads the designated output value of each supported I/O pin
 * @param[in]   Data pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' or '1' indicates the intended value of the corresponding pin
 * @note        Bits > _gpio_count are undefined
 */
uint32_t gpio_get_dataout(void);

/* Name:        gpio_set_dataout
 * @brief       Writes value of each supported I/O pin
 * @param[in]   Data pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' or '1' indicates the value of the corresponding pin
 * @note        Bits > _gpio_count are undefined
 */
void gpio_set_dataout(uint32_t data);

/* Name:        gpio_isr
 * @brief       Interrupt service routine associated with the GPIO interrupt and which shall
 *              be initialized by the application. The ISR decodes active interrupts
 *              and calls the linked callback function.
 * @param[in]   General data pointer (not needed, but required for function prototype match)
 */
AAC_INTERRUPT_HANDLER_SIGNATURE(gpio_isr, data_ptr);

/* Name:        gpio_isr_callback
 * @brief       Callback from ISR for application to act on event. Function declaration
 *              must be inside application code.
 * @param[in]   Data pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' indicates no event on this pin
 *              '1' indicates event on this pin
 * @param[in]   Data pattern [_gpio_count - 1 .. 0] representing the I/O pins where
 *              '0' or '1' indicates the [new] value of the corresponding pin
 */
void gpio_isr_callback(uint32_t int_active, uint32_t data);

#endif // __GPIO_H_
