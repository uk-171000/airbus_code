/*-----------------------------------------------------------------------------
 * Copyright (C) 2005-2015 ÅAC Microtec AB
 *
 * Filename:     gpio_rtems.h
 * Module name:  gpio
 *
 * Author(s):    OdMa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  ÅAC GPIO RTEMS driver declarations
 * Requirements: RTEMS gpio driver
 *----------------------------------------------------------------------------*/

#ifndef _RTEMS_GPIO_H_
#define _RTEMS_GPIO_H_
#include "toolchain_support.h"

#define GPIO_GPIO_IOCTL_GROUP 'G'
#define GPIO_DEVICE_NAME "/dev/gpio"

/* IOCTL command arguments */
#define GPIO_IOCTL_GET_DIRECTION                _IOR('R',  1, uint32_t)
#define GPIO_IOCTL_SET_DIRECTION                _IOW('W',  2, uint32_t)
#define GPIO_IOCTL_GET_FALL_EDGE_DETECTION      _IOR('R',  3, uint32_t)
#define GPIO_IOCTL_SET_FALL_EDGE_DETECTION      _IOW('W',  4, uint32_t)
#define GPIO_IOCTL_GET_RISE_EDGE_DETECTION      _IOR('R',  5, uint32_t)
#define GPIO_IOCTL_SET_RISE_EDGE_DETECTION      _IOW('W',  6, uint32_t)
#define GPIO_IOCTL_GET_TIMESTAMP_ENABLE         _IOR('R',  7, uint32_t)
#define GPIO_IOCTL_SET_TIMESTAMP_ENABLE         _IOW('W',  8, uint32_t)
#define GPIO_IOCTL_GET_DIFF_MODE                _IOR('R',  9, uint32_t)
#define GPIO_IOCTL_SET_DIFF_MODE                _IOW('W', 10, uint32_t)
#define GPIO_IOCTL_GET_EDGE_TIMEOUT             _IOR('R', 11, uint32_t)
#define GPIO_IOCTL_SET_EDGE_TIMEOUT             _IOW('W', 12, uint32_t)

#define GPIO_IOCTL_DIRECTION_OUTPUT             (0)
#define GPIO_IOCTL_DIRECTION_INPUT              (1)

/* Kept for backwards compatibility */
#ifndef GPIO_DIRECTION_OUT
#define GPIO_DIRECTION_OUT                      (0)
#endif
#ifndef GPIO_DIRECTION_IN
#define GPIO_DIRECTION_IN                       (1)
#endif
/* End of backwards compatibility */

#define GPIO_IOCTL_EDGE_DETECTION_DISABLE       (0)
#define GPIO_IOCTL_EDGE_DETECTION_ENABLE        (1)

#define GPIO_IOCTL_TIMESTAMP_DISABLE            (0)
#define GPIO_IOCTL_TIMESTAMP_ENABLE             (1)

#define GPIO_DIFF_MODE_DISABLE                  (0)
#define GPIO_DIFF_MODE_ENABLE                   (1)

static char * gpio_device_names[] = {"/dev/gpio0",
                                     "/dev/gpio1",
                                     "/dev/gpio2",
                                     "/dev/gpio3",
                                     "/dev/gpio4",
                                     "/dev/gpio5",
                                     "/dev/gpio6",
                                     "/dev/gpio7",
                                     "/dev/gpio8",
                                     "/dev/gpio9",
                                     "/dev/gpio10",
                                     "/dev/gpio11",
                                     "/dev/gpio12",
                                     "/dev/gpio13",
                                     "/dev/gpio14",
                                     "/dev/gpio15",
                                     "/dev/gpio16",
                                     "/dev/gpio17",
                                     "/dev/gpio18",
                                     "/dev/gpio19",
                                     "/dev/gpio20",
                                     "/dev/gpio21",
                                     "/dev/gpio22",
                                     "/dev/gpio23",
                                     "/dev/gpio24",
                                     "/dev/gpio25",
                                     "/dev/gpio26",
                                     "/dev/gpio27",
                                     "/dev/gpio28",
                                     "/dev/gpio29",
                                     "/dev/gpio30",
                                     "/dev/gpio31"};

typedef struct {
  rtems_id              semaphore_id[AAC_BSP_GPIO_COUNT];
  uint32_t              gpio_diff_mode;
  rtems_interval        timeout[AAC_BSP_GPIO_COUNT];
} gpio_devices_t;

rtems_device_driver rtems_gpio_initialize(rtems_device_major_number major,
                                          rtems_device_minor_number minor,
                                          void *args);

rtems_device_driver rtems_gpio_open(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *args);

rtems_device_driver rtems_gpio_close(rtems_device_major_number major,
                                     rtems_device_minor_number minor,
                                     void *args);

rtems_device_driver rtems_gpio_read(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *args);

rtems_device_driver rtems_gpio_write(rtems_device_major_number major,
                                     rtems_device_minor_number minor,
                                     void *args);

rtems_device_driver rtems_gpio_control(rtems_device_major_number major,
                                       rtems_device_minor_number minor,
                                       void *args);

#define AAC_GPIO_DRIVER_TABLE_ENTRY { \
      rtems_gpio_initialize,          \
      rtems_gpio_open,                \
      rtems_gpio_close,               \
      rtems_gpio_read,                \
      rtems_gpio_write,               \
      rtems_gpio_control }

#endif /* _RTEMS_GPIO_H_ */
