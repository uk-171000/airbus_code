/*
 * Copyright (C) 2015 ÅAC Microtec AB
 *
 * Filename:    error_manager_regs.h
 * Module name: error_manager
 *
 * Author(s):    Erik Zachrisson <erik.zachrisson@aacmicrotec.com>
 * Description:  Describes the error manager registers
 * Requirements:
 */

#ifndef _ERROR_MANAGER_REGS_H_
#define _ERROR_MANAGER_REGS_H_

#define ERRMAN_CTR                                   (0x00)
#define ERRMAN_MENO_IEN                              (1 << 11)
#define ERRMAN_PLEN                                  (1 << 10)
#define ERRMAN_PLEN_POS                              (10)

#define ERRMAN_RESET_ENABLE_POS                      (9)
#define ERRMAN_RESET_ENABLE                          (1 << ERRMAN_RESET_ENABLE_POS)
#define ERRMAN_RESET_DISABLE                         (0 << ERRMAN_RESET_ENABLE_POS)

#define ERRMAN_SCRUB_POS                             (8)
#define ERRMAN_SCRUB_ENABLE                          (1 << ERRMAN_SCRUB_POS)
#define ERRMAN_SCRUB_DISABLE                         (0 << ERRMAN_SCRUB_POS)

#define ERRMAN_SYSRST                                (1 << 0)

#define ERRMAN_SR                                    (0x08)
#define ERRMAN_PULSEFLG                              (1 << 11)
#define ERRMAN_POWFLG                                (1 << 10)
#define ERRMAN_MEMCLR                                (1 << 9)
#define ERRMAN_PARFLG                                (1 << 7)
#define ERRMAN_MEOTHFLG                              (1 << 6)
#define ERRMAN_SEOTHFLG                              (1 << 5)
#define ERRMAN_MECRIFLG                              (1 << 4)
#define ERRMAN_SECRIFLG                              (1 << 3)
#define ERRMAN_WDTFLAG                               (1 << 2)
#define ERRMAN_RFLG                                  (1 << 1)

#define ERRMAN_WDTCNT                                (0x10)
#define ERRMAN_SECNT                                 (0x14)
#define ERRMAN_MECNT                                 (0x18)
#define ERRMAN_PARCNT                                (0x1C)
#define ERRMAN_SYSFSCNT                              (0x20)
#define ERRMAN_SYSFMCNT                              (0x24)
#define ERRMAN_MASSFSCNT                             (0x28)
#define ERRMAN_MASSFMCNT                             (0x2C)

#define ERRMAN_CF                                    (0x30)
#define ERRMAN_PARCFLG                               (1 << 7)
#define ERRMAN_MEOFLG                                (1 << 6)
#define ERRMAN_SEOFLG                                (1 << 5)
#define ERRMAN_MECFLG                                (1 << 4)
#define ERRMAN_SECFLG                                (1 << 3)
#define ERRMAN_WDTCFLG                               (1 << 2)
#define ERRMAN_RFCFLG                                (1 << 1)

#define ERRMAN_SELFW                                 (0x40)
#define ERRMAN_SELFW_WIDTH                           (0x03)
#define ERRMAN_SELFW_PROGRAM_FW_FROM_POWER_ON        (0x00)
#define ERRMAN_SELFW_PROGRAM_FW                      (0x01)
#define ERRMAN_SELFW_PROGRAM_FW_BACKUP_COPY          (0x02)
#define ERRMAN_SELFW_SAFE_FW                         (0x03)
#define ERRMAN_SELFW_SAFE_FW_BACKUP_COPY             (0x04)
#define ERRMAN_SELFW_EXTRA_FW                        (0x05)
#define ERRMAN_SELFW_EXTRA_FW_BACKUP_COPY            (0x06)
#define ERRMAN_RAM_FW_OR_SAFE_FW_COPY_WITH_CRC_ERROR (0x07)

#define ERRMAN_RUNFW                                 (0x44)
#define ERRMAN_RUNFW_WIDTH                           (0x03)
#define ERRMAN_RUNFW_PROGRAM_FW_FROM_POWER_ON        (0x00)
#define ERRMAN_RUNFW_PROGRAM_FW                      (0x01)
#define ERRMAN_RUNFW_PROGRAM_FW_BACKUP_COPY          (0x02)
#define ERRMAN_RUNFW_SAFE_FW                         (0x03)
#define ERRMAN_RUNFW_SAFE_FW_BACKUP_COPY             (0x04)
#define ERRMAN_RUNFW_EXTRA_FW                        (0x05)
#define ERRMAN_RUNFW_EXTRA_FW_BACKUP_COPY            (0x06)
#define ERRMAN_RAM_FW_OR_SAFE_FW_COPY_WITH_CRC_ERROR (0x07)

#define ERRMAN_NVRAMSCNT                             (0x48)
#define ERRMAN_NVRAMDCNT                             (0x4C)
#define ERRMAN_NVRAMMCNT                             (0x50)

#endif
