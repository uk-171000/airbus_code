/*
 * Copyright (C) 2016-2018 ÅAC Microtec AB
 *
 * Filename:    error_manager_rtems.c
 * Module name: error_manager
 *
 * Author(s):    ErZa, LiJo
 * Support:      support@aacmicrotec.com
 * Description:  AAC error manager RTEMS driver implementation
 * Requirements: Bare metal error manager driver
 *
 */

#include <bsp.h>
#include <bsp/irq.h>
#include <fcntl.h>
#include <inttypes.h>
#include <rtems/imfs.h>
#include <rtems/libio.h>
#include <rtems/seterr.h>
#include <sys/stat.h>

#include "error_manager.h"
#include "error_manager_rtems.h"
#include "toolchain_support.h"

/* Board definitions */
uint32_t _errmgr_base = AAC_BSP_ERRMAN_BASE;
uint32_t _errmgr_IRQ = AAC_BSP_ERRMAN_IRQ;

static errman_int_cbs_t int_cbs;
static rtems_id msg_q;

/* The driver permits multiple readers but only one writer at a time */
#define INVALID_FD (-1)
static int writer_fd = INVALID_FD;
static rtems_id writer_mutex;

void errman_pulse_cmd_triggered(void);
void errman_mult_err_other_triggered(void);
void errman_pwr_loss_triggered(void);

void errman_pulse_cmd_triggered()
{
  uint32_t buffer;
  uint32_t count;

  buffer = ERRMAN_IRQ_PULSE_COMMAND;
  rtems_message_queue_broadcast(msg_q, &buffer, sizeof(uint32_t), &count);
}

void errman_mult_err_other_triggered()
{
  uint32_t buffer;
  uint32_t count;

  buffer = ERRMAN_IRQ_EDAC_MULTIPLE_ERR_OTHER;
  rtems_message_queue_broadcast(msg_q, &buffer, sizeof(uint32_t), &count);
}

void errman_pwr_loss_triggered()
{
  uint32_t count;
  uint32_t buffer;

  buffer = ERRMAN_IRQ_POWER_LOSS;
  rtems_message_queue_broadcast(msg_q, &buffer, sizeof(uint32_t), &count);
}

/*
 * Access functions
 */

static int rtems_error_manager_open(rtems_libio_t *iop, const char *path, int oflag,
                                    mode_t mode)
{
  int fd;
  int err;
  rtems_status_code status;

  fd = rtems_libio_iop_to_descriptor(iop);
  err = 0;

  DBG(DBG_SEVERITY_INFO, "Opening Error Manager device file descriptor %d.", fd);

  /* Use mutex to protect against simultaneous open from different tasks */
  status = rtems_semaphore_obtain(writer_mutex, RTEMS_WAIT, RTEMS_NO_TIMEOUT);

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to obtain mutex for Error Manager driver: %s",
        rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  /* Multiple readers, one writer allowed */
  if ((iop->flags & LIBIO_FLAGS_WRITE) == LIBIO_FLAGS_WRITE) {
    if (writer_fd != INVALID_FD) {
      DBG(DBG_SEVERITY_INFO,
          "Error manager decive already opened for writing. Aborting request.");
      err = EALREADY;
    } else {
      writer_fd = fd;
    }
  }

  status = rtems_semaphore_release(writer_mutex);

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to relsease mutex for Error manager driver: %s)",
        rtems_status_text(status));
  }

  if (err != 0) {
    rtems_set_errno_and_return_minus_one(err);
  }

  return 0;
}

static int rtems_error_manager_close(rtems_libio_t *iop)
{
  int fd;

  fd = rtems_libio_iop_to_descriptor(iop);

  DBG(DBG_SEVERITY_INFO, "Closing Error manager device file descriptor %d", fd);

  /* Multiple readers, one writer allowed. */
  if (fd == writer_fd) {
    DBG(DBG_SEVERITY_INFO, "Clear writer_fd.");
    writer_fd = INVALID_FD;
  }

  return 0;
}

static ssize_t rtems_error_manager_read(rtems_libio_t *iop, void *buffer, size_t count)
{
  DBG(DBG_SEVERITY_INFO, "Reading from error_manager device is not supported.\n");

  rtems_set_errno_and_return_minus_one(EPERM);
}

static ssize_t rtems_error_manager_write(rtems_libio_t *iop, const void *buffer,
                                         size_t count)
{
  DBG(DBG_SEVERITY_INFO, "Writing to error_manager device is not supported.\n");

  rtems_set_errno_and_return_minus_one(EPERM);
}

static int rtems_error_manager_control(rtems_libio_t *iop, ioctl_command_t command,
                                       void *buffer)
{
  int fd;
  int rv;
  uint32_t val;

  rv = ENOSYS;

  fd = rtems_libio_iop_to_descriptor(iop);

  DBG(DBG_SEVERITY_INFO, "Error mananger IOCTL");

  val = (uint32_t)buffer;

  switch (command) {
  case ERRMAN_GET_SR_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_SR_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_SR);
    rv = 0;
    break;

  case ERRMAN_GET_CF_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_CF_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_CF);
    rv = 0;
    break;

  case ERRMAN_GET_SELFW_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_SELFW_IOCTL");
    *((uint32_t *)buffer) = errman_get_next_boot_firmware();
    rv = 0;
    break;

  case ERRMAN_GET_RUNFW_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_RUNFW_IOCTL");
    *((uint32_t *)buffer) = errman_get_running_firmware();
    rv = 0;
    break;

  case ERRMAN_GET_SCRUBBER_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_SCRUBBER_IOCTL");
    *((uint32_t *)buffer) = errman_get_scrubber();
    rv = 0;
    break;

  case ERRMAN_GET_RESET_ENABLE_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_RESET_ENABLE_IOCTL");
    *((uint32_t *)buffer) = errman_get_reset_enable();
    rv = 0;
    break;

  case ERRMAN_GET_WDT_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_WDT_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_WDTCNT);
    rv = 0;
    break;

  case ERRMAN_GET_EDAC_SINGLE_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_EDAC_SINGLE_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_SECNT);
    rv = 0;
    break;

  case ERRMAN_GET_EDAC_MULTI_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_EDAC_MULTI_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_MECNT);
    rv = 0;
    break;

  case ERRMAN_GET_CPU_PARITY_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_CPU_PARITY_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_PARCNT);
    rv = 0;
    break;

  case ERRMAN_GET_SYS_SINGLE_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_SYS_SINGLE_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_SYSFSCNT);
    rv = 0;
    break;

  case ERRMAN_GET_SYS_MULTI_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_SYS_MULTI_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_SYSFMCNT);
    rv = 0;
    break;

  case ERRMAN_GET_MMU_SINGLE_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_MMU_SINGLE_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_MASSFSCNT);
    rv = 0;
    break;

  case ERRMAN_GET_MMU_MULTI_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_MMU_MULTI_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_MASSFMCNT);
    rv = 0;
    break;

  case ERRMAN_GET_NVRAM_SINGLE_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_NVRAM_SINGLE_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_NVRAMSCNT);
    rv = 0;
    break;

  case ERRMAN_GET_NVRAM_DOUBLE_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_NVRAM_DOUBLE_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_NVRAMDCNT);
    rv = 0;
    break;

  case ERRMAN_GET_NVRAM_MULTI_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_NVRAM_MULTI_ERRCNT_IOCTL");
    *((uint32_t *)buffer) = errman_read_reg(ERRMAN_NVRAMMCNT);
    rv = 0;
    break;

  case ERRMAN_GET_POWER_LOSS_ENABLE_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_GET_POWER_LOSS_ENABLE_IOCTL");
    *((uint32_t *)buffer) = errman_get_power_loss_enable();
    rv = 0;
    break;

  case ERRMAN_SET_SR_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_SR_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_SR, val);
      rv = 0;
    }

    break;

  case ERRMAN_SET_CF_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_CF_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_CF, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_SELFW_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_SELFW_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      rv = errman_set_next_boot_firmware(val);
    }
    break;

  case ERRMAN_SET_RUNFW_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_RUNFW_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      rv = errman_set_running_firmware(val);
    }
    break;

  case ERRMAN_SET_SCRUBBER_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_SCRUBBER_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      rv = errman_set_scrubber(val);
    }
    break;

  case ERRMAN_SET_RESET_ENABLE_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_RESET_ENABLE_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      rv = errman_set_reset_enable(val);
    }
    break;

  case ERRMAN_SET_WDT_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_WDT_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_WDTCNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_EDAC_SINGLE_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_EDAC_SINGLE_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_SECNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_EDAC_MULTI_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_EDAC_MULTI_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_MECNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_CPU_PARITY_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_CPU_PARITY_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_PARCNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_SYS_SINGLE_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_SYS_SINGLE_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_SYSFSCNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_SYS_MULTI_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_SYS_MULTI_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_SYSFMCNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_MMU_SINGLE_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_MMU_SINGLE_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_MASSFSCNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_MMU_MULTI_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_MMU_MULTI_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_MASSFMCNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_NVRAM_SINGLE_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_NVRAM_SINGLE_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_NVRAMSCNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_NVRAM_DOUBLE_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_NVRAM_DOUBLE_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_NVRAMDCNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_NVRAM_MULTI_ERRCNT_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_NVRAM_MULTI_ERRCNT_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      errman_write_reg(ERRMAN_NVRAMMCNT, val);
      rv = 0;
    }
    break;

  case ERRMAN_SET_POWER_LOSS_ENABLE_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_SET_POWER_LOSS_ENABLE_IOCTL %" PRIu32 "", val);
    if (fd != writer_fd) {
      DBG(DBG_SEVERITY_WARN, "File descriptor not opened for writing. Aborting request.");
      rv = -EBADF;
    } else {
      rv = errman_set_power_loss_enable(val);
    }
    break;

  case ERRMAN_RESET_SYSTEM_IOCTL:
    DBG(DBG_SEVERITY_INFO, "IOCTL: ERRMAN_RESET_SYSTEM_IOCTL %" PRIu32 "", val);
    errman_reset_system();
    rv = 0;
    break;

  default:
    DBG(DBG_SEVERITY_WARN, "IOCTL: unknown IOCTL");
    rv = -EINVAL;
  }

  if (rv < 0) {
    rtems_set_errno_and_return_minus_one(-rv);
  } else {
    return rv;
  }
}

static const rtems_filesystem_file_handlers_r rtems_errman_handlers = {
    .open_h = rtems_error_manager_open,
    .close_h = rtems_error_manager_close,
    .read_h = rtems_error_manager_read,
    .write_h = rtems_error_manager_write,
    .ioctl_h = rtems_error_manager_control,
    .lseek_h = rtems_filesystem_default_lseek_file,
    .fstat_h = IMFS_stat,
    .ftruncate_h = rtems_filesystem_default_ftruncate,
    .fsync_h = rtems_filesystem_default_fsync_or_fdatasync,
    .fdatasync_h = rtems_filesystem_default_fsync_or_fdatasync,
    .fcntl_h = rtems_filesystem_default_fcntl,
    .readv_h = rtems_filesystem_default_readv,
    .writev_h = rtems_filesystem_default_writev,
};

static const IMFS_node_control
rtems_errman_node_control = IMFS_GENERIC_INITIALIZER(
    &rtems_errman_handlers,
    IMFS_node_initialize_generic,
    IMFS_node_destroy_default);

rtems_device_driver rtems_error_manager_initialize(rtems_device_major_number major,
                                                   rtems_device_minor_number minor,
                                                   void *args)
{
  int rv;
  rtems_status_code rtems_result;

  DBG(DBG_SEVERITY_INFO, "Create Error manager device node (M%" PRIu32 ":m%" PRIu32 ").",
      major, minor);
  /* Note: Since we are using generic IMFS nodes, major and minor numbers do not
     matter. */

  rv =
      IMFS_make_generic_node(RTEMS_ERRMAN_DEVICE_NAME, S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO,
                             &rtems_errman_node_control, NULL);

  if (rv < 0) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to create Error manager device node (M%" PRIu32 ":m0): %s)", major,
        strerror(errno));
    return RTEMS_INTERNAL_ERROR;
  }

  rtems_result = rtems_semaphore_create(
      rtems_build_name('S', 'W', 'R', 'T'), 1,
      RTEMS_DEFAULT_ATTRIBUTES | RTEMS_SIMPLE_BINARY_SEMAPHORE, 0, &writer_mutex);
  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to create mutex for Error manager driver, error %d, %s)",
        rtems_result, rtems_status_text(rtems_result));
    return RTEMS_INTERNAL_ERROR;
  }

  rtems_result = rtems_interrupt_handler_install(
      _errmgr_IRQ, "errman", RTEMS_INTERRUPT_UNIQUE, errman_interrupt_handler, NULL);
  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to install interrupt handler %d, %s", rtems_result,
        rtems_status_text(rtems_result));
    return RTEMS_INTERNAL_ERROR;
  }

  rtems_result =
      rtems_message_queue_create(rtems_build_name('E', 'M', 'G', 'R'), 1, sizeof(uint32_t),
                                 RTEMS_DEFAULT_ATTRIBUTES, &msg_q);

  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register message queue. Did you enable "
                          "CONFIGURE_MAXIMUM_MESSAGE_QUEUES? Errcode: %d, errstring: %s",
        rtems_result, rtems_status_text(rtems_result));
    return RTEMS_INTERNAL_ERROR;
  }

  int_cbs.power_loss = errman_pwr_loss_triggered;
  int_cbs.pulse_command = errman_pulse_cmd_triggered;
  int_cbs.mult_err_other = errman_mult_err_other_triggered;

  errman_set_interrupt_callback(int_cbs);

  errman_write_reg(ERRMAN_SR, (ERRMAN_PULSEFLG | ERRMAN_MEOTHFLG));

  /* Enable interrupts, disable MEOTH for now */
  errman_write_reg(ERRMAN_CTR, (ERRMAN_PLEN | ERRMAN_MENO_IEN | ERRMAN_RESET_ENABLE |
                                ERRMAN_SCRUB_ENABLE));

  return RTEMS_SUCCESSFUL;
}
