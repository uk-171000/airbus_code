/*
 * Copyright (C) 2015 ÅAC Microtec AB
 *
 * Filename:    error_manager.h
 * Module name: error_manager
 *
 * Author(s):    Erik Zachrisson <erik.zachrisson@aacmicrotec.com>
 * Description:  Describes the error manager reg access functions
 * Requirements:
 */

#ifndef _ERROR_MANAGER_H_
#define _ERROR_MANAGER_H_

#include "error_manager_regs.h"
#include <toolchain_support.h>
#include <stdint.h>

/* Name:        errman_cb_t
 * @brief       Callback function decleration for on all asynchronous calls.
 */
typedef void (*errman_cb_t)(void);

/* Name:        scet_int_cbs_t
 * @brief       Struct containing the callback pointers for all interrupt sources
 */
typedef struct {
  errman_cb_t power_loss;
  errman_cb_t pulse_command;
  errman_cb_t mult_err_other;
} errman_int_cbs_t;

/* Name:        errman_write_reg
 * @brief       Writes an error manager register
 * @param[in]   addr Register to write
 * @param[in]   data Data to write
 */
void errman_write_reg(uint32_t addr, uint32_t data);

/* Name:        errman_read_reg
 * @brief       Reads an error manager register
 * @param[in]   addr Address to read
 * @return      Read data
 */
uint32_t errman_read_reg(uint32_t addr);

/* Name:        errman_set_interrupt_callback
 * @brief       Registers a callback function triggered upon interrupt
 * @param[in]   cb_func The callback function for all asynchronous calls.
 */
void errman_set_interrupt_callback(errman_int_cbs_t);

/*
 * Name:        errman_interrupt_handler
 * @brief       Interrupt handler for error manager
 * @param[in]   args Args are ignored
 */
AAC_INTERRUPT_HANDLER_SIGNATURE(errman_interrupt_handler, args);

/*
 * Name:        errman_set_scrubber
 * @brief       Controls the scrubber functionality
 * @param[in]   new_scrubber_val New scrubber value. Valid is 0 = Off, 1 = On
 * @return      SUCCESS All OK
 * @return      EINVAL Invalid value
 */
int32_t errman_set_scrubber(uint32_t new_scrubber_val);

/* Name:        errman_get_scrubber
 * @brief       Gets the currently configured scrubber functionality
 * @return      0 Scrubber is disabled
 * @return      1 Scrubber is enabled
 */
uint32_t errman_get_scrubber(void);

/* Name:        errman_get_power_loss_enable
 * @brief       Gets the power loss enable state
 * @return      0 Power loss is enabled
 * @return      1 Power loss is disabled
 */
uint32_t errman_get_power_loss_enable(void);

/* Name:        errman_set_next_boot_firmware
 * @brief       Controls which firmware that is to be used upon next boot
 * @param[in]   next_boot_firmware Next firmware value. Valid is 0 to 7
 * @return      SUCCESS All OK
 * @return      EINVAL Invalid value
 */
int32_t errman_set_next_boot_firmware(uint32_t next_boot_firmware);

/* Name:        errman_get_next_boot_firmware
 * @brief       Returns currently set firmware to boot next boot
 * @return      0-7 pointing at different firmwares
 */
uint32_t errman_get_next_boot_firmware(void);

/* Name:        errman_set_running_firmware
 * @brief       Sets which firmware that is currently running
 * @param[in]   new_running_firmware New firmware value. Valid is 0 to 7
 * @return      SUCCESS All OK
 * @return      EINVAL Invalid value
 */
int32_t errman_set_running_firmware(uint32_t new_running_firmware);

/* Name:        errman_get_running_firmware
 * @brief       Returns currently running firmware
 * @return      0-7 pointing at different firmwares
 */
uint32_t errman_get_running_firmware(void);

/* Name:        errman_set_reset_enable
 * @brief       Controls whether the reset enable shall be on or off
 * @param[in]   new_reset_enable New reset enable value. Valid is 0 = Off, 1 = On
 * @return      SUCCESS All OK
 * @return      EINVAL Invalid value
 */
int32_t errman_set_reset_enable(uint32_t new_reset_enable);

/* Name:        errman_set_power_loss_enable
 * @brief       Controls whether the power loss enable shall be on or off
 * @param[in]   new_reset_enable new power loss enable value. Valid is 0 = Off, 1 = On
 * @return      SUCCESS All OK
 * @return      EINVAL Invalid value
 */
int32_t errman_set_power_loss_enable(uint32_t new_power_loss_enable);

/* Name:        errman_get_reset_enable
 * @brief       Returns whether reset is enabled or not
 * @return      0 Reset is disabled
 * @return      1 Reset is enabled
 */
uint32_t errman_get_reset_enable(void);

/* Name:        errman_reset_system
 * @brief       Performs a software reset of the system if the reset enable bit is enabled.
 */
void errman_reset_system(void);

#endif
