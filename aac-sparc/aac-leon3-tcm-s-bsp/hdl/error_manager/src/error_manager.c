/*
 * Copyright (C) 2016 ÅAC Microtec AB
 *
 * Filename:    error_manager.c
 * Module name: error_manager
 *
 * Author(s):    Erik Zachrisson <erik.zachrisson@aacmicrotec.com>
 * Description:  Implements the error manager ctrl functions
 * Requirements:
 */

#include <stdint.h>
#include <math.h>
#include <errno.h>

#include "toolchain_support.h"
#include "error_manager.h"

extern uint32_t _errmgr_base;
#define ERRMAN_BASE _errmgr_base

extern uint32_t _errmgr_IRQ;
#define ERRMAN_IRQ _errmgr_IRQ

volatile errman_int_cbs_t interrupt_callbacks;

void errman_write_reg(uint32_t addr, uint32_t data)
{
  REG32(ERRMAN_BASE + addr) = data;
}

uint32_t errman_read_reg(uint32_t addr)
{
  return REG32(ERRMAN_BASE + addr);
}

AAC_INTERRUPT_HANDLER_SIGNATURE(errman_interrupt_handler, args)
{
  /* Suppress unused warning */
  (void) args;

  uint32_t status_reg = errman_read_reg(ERRMAN_SR);
  uint32_t ctrl_reg = errman_read_reg(ERRMAN_CTR);

  /* Filter in current and enabled interrupts */
  uint32_t int_active = 0;
  if (ctrl_reg & ERRMAN_PLEN) {
    int_active |= (status_reg & ERRMAN_POWFLG);
  }
  if (ctrl_reg & ERRMAN_MENO_IEN) {
    int_active |= (status_reg & ERRMAN_MEOTHFLG);
  }

  /* Clear interrupts before handling them. If any new interrupts of the same
  source occurs in between reading the status register and handling it in the
  callback, the newest state will simply be reflected and handled by the ISR.
  If the interrupt occurs before clearing the status register, the extra
  interrupt will not be generated. If occurring after clearing the status
  register but before handling it in the callback, a new interrupt reflecting
  the exact same state will be generated. This has to be handled in the ISR
  callback. If the new interrupt occurs after the callback, the ISR will be
  executed again directly.

  If another source generates an interrupt, then a new interrupt will be
  generated on clear of the status register and will be seen as the ISR will
  be executed again directly. */
  errman_write_reg(ERRMAN_SR, int_active);

  if (int_active & ERRMAN_POWFLG) {
    uint32_t rmw;

    /* Clear Power loss interrupt enable to prevent stall of cpu */
    rmw = errman_read_reg(ERRMAN_CTR);
    rmw ^= (-0 ^ rmw) & ERRMAN_PLEN;
    errman_write_reg(ERRMAN_CTR,rmw);

    if (interrupt_callbacks.power_loss != NULL) {
      interrupt_callbacks.power_loss();
    }
  }

  /* NOTE: A pulse command does NOT trigger an interrupt. We can of course end
  up here anyway in case another interrupt is triggered and a pulse command has
  occured (status bit set) */
  if (status_reg & ERRMAN_PULSEFLG) {
    if (interrupt_callbacks.pulse_command != NULL) {
      interrupt_callbacks.pulse_command();
    }
  }

  if (int_active & ERRMAN_MEOTHFLG) {
    if (interrupt_callbacks.mult_err_other != NULL) {
      interrupt_callbacks.mult_err_other();
    }
  }
}

void errman_set_interrupt_callback(errman_int_cbs_t cb_funcs)
{
  interrupt_callbacks = cb_funcs;
}

int32_t errman_set_scrubber(uint32_t new_scrubber_val)
{
  uint32_t rmw;

  if (new_scrubber_val > 1) {
    return -EINVAL;
  }

  rmw = REG32(ERRMAN_BASE + ERRMAN_CTR);
  rmw ^= (-new_scrubber_val ^ rmw) & ERRMAN_SCRUB_ENABLE;

  REG32(ERRMAN_BASE + ERRMAN_CTR) = rmw;

  return 0;
}

uint32_t errman_get_scrubber()
{
  return (REG32(ERRMAN_BASE + ERRMAN_CTR) >> ERRMAN_SCRUB_POS) & 1;
}

uint32_t errman_get_power_loss_enable()
{
  return (REG32(ERRMAN_BASE + ERRMAN_CTR) >> ERRMAN_PLEN_POS) & 1;
}

int32_t errman_set_next_boot_firmware(uint32_t next_boot_firmware)
{
  if (next_boot_firmware >= (uint32_t) pow(2, ERRMAN_SELFW_WIDTH)) {
    return -EINVAL;
  }

  REG32(ERRMAN_BASE + ERRMAN_SELFW) = next_boot_firmware;
  return 0;
}

uint32_t errman_get_next_boot_firmware()
{
  return REG32(ERRMAN_BASE + ERRMAN_SELFW);
}

int32_t errman_set_running_firmware(uint32_t new_running_firmware)
{
  if (new_running_firmware >= (uint32_t) pow(2, ERRMAN_RUNFW_WIDTH)) {
    return -EINVAL;
  }
  REG32(ERRMAN_BASE + ERRMAN_RUNFW) = new_running_firmware;
  return 0;
}

uint32_t errman_get_running_firmware()
{
  return REG32(ERRMAN_BASE + ERRMAN_RUNFW);
}

int32_t errman_set_reset_enable(uint32_t new_reset_val)
{
  uint32_t rmw;

  if (new_reset_val > 1) {
    return -EINVAL;
  }

  rmw = REG32(ERRMAN_BASE + ERRMAN_CTR);
  rmw ^= (-new_reset_val ^ rmw) & ERRMAN_RESET_ENABLE;

  REG32(ERRMAN_BASE + ERRMAN_CTR) = rmw;

  return 0;
}

int32_t errman_set_power_loss_enable(uint32_t new_power_loss_val)
{
  uint32_t rmw;

  if (new_power_loss_val > 1) {
    return -EINVAL;
  }

  rmw = REG32(ERRMAN_BASE + ERRMAN_CTR);
  rmw ^= (-new_power_loss_val ^ rmw) & ERRMAN_PLEN;

  REG32(ERRMAN_BASE + ERRMAN_CTR) = rmw;

  return 0;
}

uint32_t errman_get_reset_enable()
{
  return (REG32(ERRMAN_BASE + ERRMAN_CTR) >> ERRMAN_RESET_ENABLE_POS) & 1;
}

void errman_reset_system()
{
  uint32_t rmw;
  rmw = REG32(ERRMAN_BASE + ERRMAN_CTR);

  /* Perform RMW as it is possible that the reset_enable is
     disabled and the system won't reboot by this */
  REG32(ERRMAN_BASE + ERRMAN_CTR) = rmw | ERRMAN_SYSRST;
}
