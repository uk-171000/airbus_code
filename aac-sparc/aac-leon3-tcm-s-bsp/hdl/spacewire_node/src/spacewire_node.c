/*-------------------------------------------------------------------------------------------
 * Copyright (C) 2015 - 2018 ÅAC Microtec AB
 *
 * Filename:     spacewire_node.c
 * Module name:  SpaceWire
 *
 * Author(s):    PeSt, InSi, ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  Spacewire node bare-metal driver definitions
 * Requirements: Adheres to the ÅAC Spacewire Node Driver Requirement Specification Rev. A
 *-----------------------------------------------------------------------------------------*/

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>

#include "toolchain_support.h"
#include "spacewire_node.h"

/*-----------------------------------------------------------------------------
 * Constant definitions
 *-----------------------------------------------------------------------------*/
#define SPW_CLK_RATE_MAX        50000000 /* 50 Mhz */

/*-----------------------------------------------------------------------------
 * Variable declarations
 *-----------------------------------------------------------------------------*/

/* Allocate a correctly aligned minimal scratch buffer used to initialize the memory */
/* descriptor pointers. This is a little safer than initializing to 0 (or NULL).     */
static volatile uint8_t __attribute__ ((aligned (4))) scratch_buffer[4];

/*-----------------------------------------------------------------------------
 * Function declarations
 *-----------------------------------------------------------------------------*/
static void clear_receive_memory_descriptor(const uint32_t base_address);
static void clear_transmit_memory_descriptor(const uint32_t base_address);
static void clear_memory_descriptors(const uint32_t base_address);

/*-----------------------------------------------------------------------------
 * Interrupt handler
 *-----------------------------------------------------------------------------*/
AAC_INTERRUPT_HANDLER_SIGNATURE(spacewire_isr, arg)
{
  spacewire_device_info_t* device;
  uint32_t sr;
  uint32_t en;
  uint32_t act;
  uint32_t pkt_status;
  uint32_t base_addr;

  device = (spacewire_device_info_t*)arg;
  base_addr = device->base_address;

  /*  Get status */
  sr = REG32(base_addr + SPW_SR);
  en = REG32(base_addr + SPW_IER);

  /* Filter out disabled interrupts */
  act = en & sr;

  /* Clear interrupts before handling them. If any new interrupts of the same
  source occurs in between reading the status register and handling it in the
  callback, the newest state will simply be reflected and handled by the ISR.
  If the interrupt occurs before clearing the status register, the extra
  interrupt will not be generated. If occurring after clearing the status
  register but before handling it in the callback, a new interrupt reflecting
  the exact same state will be generated. This has to be handled in the ISR
  callback. If the new interrupt occurs after the callback, the ISR will be
  executed again directly.

  If another source generates an interrupt, then a new interrupt will be
  generated on clear of the status register and will be seen as the ISR will
  be executed again directly.

  NOTE: The Rx and Tx interrupts will not really be cleared here (the bits might
  be set, but they do not bite), because they are generated based on yet another
  hierarchy of interrupts in the RTL. They are cleared in the expressions below.
  The important thing is that they are cleared BEFORE handling the interrupt */
  REG32(base_addr + SPW_SR) = act;

  /* Link error occured ? */
  if (act & SPW_SR_LE) {
    spacewire_link_error(base_addr);
  }

  /* Link established? */
  if (act & SPW_SR_LUP) {
    spacewire_link_established(base_addr);
  }

  /* Link was reset? */
  if (act &  SPW_SR_LDWN) {
    spacewire_link_cancelled(base_addr);
  }

  /* Packet reception completed? */
  if (act & SPW_SR_RX)
  {
    pkt_status = REG32(base_addr + SPW_MD_OFFSET_RX_STAT);

    /* Clear the interrupt by signalling that the packet has been handled */
    REG32(base_addr + SPW_MD_OFFSET_RX_STAT) = (pkt_status &
                                                ~(SPW_MD_STAT_RX_IE_EOP |
                                                  SPW_MD_STAT_RX_IE_EEP |
                                                  SPW_MD_STAT_RX_IE_OVF |
                                                  SPW_MD_STAT_RX_EOP |
                                                  SPW_MD_STAT_RX_EEP |
                                                  SPW_MD_STAT_RX_OVF));

    spacewire_data_received(base_addr,
                            ((pkt_status & SPW_MD_STAT_MASK_LEN) >> 16),
                            (pkt_status & SPW_MD_STAT_MASK_FLAGS));
  }

  /* Pilot byte received? */
  if (act & SPW_SR_PB_RX)
  {
    /* Notify client */
    spacewire_data_incoming(base_addr, ((sr & SPW_SR_PB_MASK) >> 8));
  }

  /* Packet transmitted? */
  if (act & SPW_SR_TX)
  {
    pkt_status = REG32(base_addr + SPW_MD_OFFSET_TX_STAT);

    /* TX signal handled - Clear interrupt */
    REG32(base_addr + SPW_MD_OFFSET_TX_STAT) = (pkt_status &
                                                ~(SPW_MD_STAT_TX_TO |
                                                  SPW_MD_STAT_TX_SENT |
                                                  SPW_MD_STAT_TX_IE_SENT |
                                                  SPW_MD_STAT_TX_IE_TO));

    /* Notify client */
    spacewire_transmit_complete(base_addr,
                                ((pkt_status & SPW_MD_STAT_MASK_LEN) >> 16),
                                (pkt_status & SPW_MD_STAT_MASK_FLAGS));
  }
}

/*-----------------------------------------------------------------------------
 * Exported functions
 *-----------------------------------------------------------------------------*/

void spacewire_initialize(const uint32_t base_address)
{
  /* Set node in normal mode (electric drivers on) to allow daisy-chaining */
  REG32(base_address + SPW_MR) = SPW_MODE_NORMAL;

  /* Turn all interrupt sources off initially */
  REG32(base_address + SPW_IER) = 0;

  /* Reset IE register and set default values */
  REG32(base_address + SPW_SR) = (SPW_SR_PB_RX |
                                  SPW_SR_LE |
                                  SPW_SR_LDWN |
                                  SPW_SR_LUP);

  /* Set max size to receive all packets */
  REG32(base_address + SPW_MSR_RX) = SPW_PKT_SIZE_MAX;

  /* Reset the transmission spped to maximum (divisor value to 0) */
  REG32(base_address + SPW_TRR) = 0;

  /* Reset all memory descriptors */
  clear_memory_descriptors(base_address);
}

int32_t spacewire_mode_set(const uint32_t base_address, const uint32_t mode)
{
  /* Sanity check */
  if (mode > SPW_MODE_LOOPBACK) {
    return -EINVAL;
  }

  REG32(base_address + SPW_MR) = mode;

  return 0;
}

int32_t spacewire_transmit(const uint32_t base_address,
                           const uint8_t* buffer,
                           const uint16_t size)
{
  uint32_t pkt_status;

  /* Check for illegal buffer size */
  if ((size == 0) || (size > SPW_PKT_SIZE_MAX))
  {
    return -EINVAL;
  }

  /* Get transmission status */
  pkt_status = REG32(base_address + SPW_MD_OFFSET_TX_STAT);

  /* Transmission in progress ? */
  if (pkt_status & SPW_MD_STAT_ENA)
  {
    return -EBUSY;
  }

  /* Configure for packet transmission */
  REG32(base_address + SPW_MD_OFFSET_TX_ADDR) = (uint32_t)buffer;
  REG32(base_address + SPW_MD_OFFSET_TX_STAT) = ((size << 16) |
                                                 SPW_MD_STAT_WRAP |
                                                 SPW_MD_STAT_ENA |
                                                 SPW_MD_STAT_TX_IE_SENT |
                                                 SPW_MD_STAT_TX_IE_TO);

  return 0;
}

int32_t spacewire_register_receive_buffer(const uint32_t base_address,
                                          uint8_t* buffer,
                                          const uint16_t size,
                                          const uint32_t b_keep_pilot_byte)
{
  uint32_t status;

  /* Check for illegal buffer size */
  if ((buffer == NULL) || (size == 0) || (size > SPW_PKT_SIZE_MAX))
  {
    return -EINVAL;
  }

  /* Get reception status */
  status = REG32(base_address + SPW_MD_OFFSET_RX_STAT);

  /* Descriptor available? */
  if (status & SPW_MD_STAT_ENA)
  {
    return -EBUSY;
  }

  /* Set reception buffer size */
  REG32(base_address + SPW_MSR_RX) = size;

  /* Configure for packet reception */
  REG32(base_address + SPW_MD_OFFSET_RX_ADDR) = (uint32_t)buffer;
  REG32(base_address + SPW_MD_OFFSET_RX_STAT) = ((size << 16) |
                                                 b_keep_pilot_byte |
                                                 SPW_MD_STAT_RX_STP |
                                                 SPW_MD_STAT_WRAP |
                                                 SPW_MD_STAT_ENA |
                                                 SPW_MD_STAT_RX_IE_OVF |
                                                 SPW_MD_STAT_RX_IE_EEP |
                                                 SPW_MD_STAT_RX_IE_EOP);

  return 0;
}

int32_t spacewire_drop_packet(const uint32_t base_address)
{
  uint32_t status;

  /* Get reception status */
  status = REG32(base_address + SPW_MD_OFFSET_RX_STAT);

  /* Descriptor available? */
  if (status & SPW_MD_STAT_ENA)
  {
    return -EBUSY;
  }

  /* Do not get any data */
  REG32(base_address + SPW_MSR_RX) = 0;

  /* Configure for packet reception */
  REG32(base_address + SPW_MD_OFFSET_RX_ADDR) = (uint32_t)scratch_buffer;
  REG32(base_address + SPW_MD_OFFSET_RX_STAT) = (SPW_MD_STAT_WRAP | SPW_MD_STAT_ENA);

  return 0;
}

void spacewire_link_auto_start(const uint32_t base_address)
{
  REG32(base_address + SPW_CR) = SPW_CR_LAS;
}

void spacewire_link_start(const uint32_t base_address)
{
  REG32(base_address + SPW_CR) = SPW_CR_LST;
}

void spacewire_link_disable(const uint32_t base_address)
{
  REG32(base_address + SPW_CR) = SPW_CR_LDIS;
}

int32_t spacewire_set_transmit_speed(const uint32_t base_address,
                                     const uint32_t transmit_speed)
{
  uint32_t trr;

  trr = REG32(base_address + SPW_TRR);

  if (transmit_speed == 0)
  {
    /* 0 for maximum speed. */
    REG32(base_address + SPW_TRR) = (trr & ~SPW_TRR_TX_CLD_MASK);
  }
  else
  {
    /* Check for legal rate. */
    /* Note: The SpaceWire standard requires the bit rate to be above 2 Mbps. */
    /*         However, this is not checked here to allow testing with lower speeds. */
    if (transmit_speed <= SPW_CLK_RATE_MAX)
    {
      uint32_t divisor_work;

      divisor_work = (SPW_CLK_RATE_MAX / transmit_speed) - 1;
      if (divisor_work > SPW_CLK_RATE_MAX)
      {
        return -EINVAL;
      }
      else
      {
        trr = trr & ~SPW_TRR_TX_CLD_MASK;
        REG32(base_address + SPW_TRR) = trr | (divisor_work & SPW_TRR_TX_CLD_MASK);
      }
    }
    else
    {
      return -EINVAL;
    }
  }

  return 0;
}

/* Helper function to clear a receive memory descriptor. */
static void clear_receive_memory_descriptor(const uint32_t base_address)
{
  REG32(base_address + SPW_MD_OFFSET_RX_ADDR) = (uint32_t)&scratch_buffer;
  REG32(base_address + SPW_MD_OFFSET_RX_STAT) = SPW_MD_STAT_WRAP;
}

/* Helper function to clear a transmit memory descriptor. */
static void clear_transmit_memory_descriptor(const uint32_t base_address)
{
  REG32(base_address + SPW_MD_OFFSET_TX_ADDR) = (uint32_t)&scratch_buffer;
  REG32(base_address + SPW_MD_OFFSET_TX_STAT) = SPW_MD_STAT_WRAP;
}

/* Helper function to clear all memory descriptors. */
static void clear_memory_descriptors(const uint32_t base_address)
{
  clear_receive_memory_descriptor(base_address);
  clear_transmit_memory_descriptor(base_address);
}
