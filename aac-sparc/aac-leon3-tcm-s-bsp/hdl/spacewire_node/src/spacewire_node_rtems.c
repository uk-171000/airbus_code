/*-----------------------------------------------------------------------------
 * Copyright (C) 2015 - 2018 ÅAC Microtec AB
 *
 * Filename:     spacewire_node_rtems.c
 * Module name:  SpaceWire
 *
 * Author(s):    InSi, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  Spacewire node RTEMS driver definitions
 * Requirements: Adheres to the ÅAC Spacewire Node Driver Requirement Specification Rev. A
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <inttypes.h>

#include <rtems/imfs.h>
#include <rtems/libio.h>
#include <rtems/seterr.h>
#include <rtems/rtems/tasks.h>
#include <errno.h>

#include <bsp.h>
#include <bsp/irq.h>

#include "spacewire_node.h"
#include "spacewire_node_rtems.h"
#include "spacewire_internal_rtems.h"
#include "toolchain_support.h"

#define INVALID_FD (-1)

#define SPWN_TASK_SEM_ATTR_BINARY (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_SIMPLE_BINARY_SEMAPHORE)

static volatile spacewire_device_info_t spwn_device_info;


static rtems_id open_mutex;

/* Driver context variables */
static volatile spwn_state_link_enum    spwn_link;
static spwn_queue_t                     rx_q;
static spwn_queue_t                     tx_q;
static volatile spwn_dev_ctx_t          spwn_dev_ctxs[SPWN_NR_OF_LOG_ADDRESSES];

/* Transmit semaphores */
static rtems_id tx_send_mutex;
static rtems_id tx_complete_semaphore;

bool spwn_q_put(spwn_queue_t* q, volatile spwn_info_t *info_item)
{
  if (q->items < SPWN_ADDR_Q_SIZE) {
    q->infos[q->front] = info_item;
    q->front = (q->front + 1) % SPWN_ADDR_Q_SIZE ;
    q->items++;
    return true;
  }

  return false;
}

bool spwn_q_get(spwn_queue_t* q, volatile spwn_info_t **info_item)
{
  if (q->items > 0) {
    *info_item = q->infos[q->rear];
    q->rear = (q->rear + 1) % SPWN_ADDR_Q_SIZE;
    q->items--;
    return true;
  }
  return false;
}

void spacewire_link_established(const uint32_t base_address)
{
  DBG(DBG_SEVERITY_INFO, "Link established: [BA:%08X]", base_address);

  spwn_link = SPWN_STATE_LINK_UP;
}

void spacewire_link_cancelled(const uint32_t base_address)
{
  DBG(DBG_SEVERITY_WARN, "Link cancelled: [BA:%08X]", base_address);

  spwn_link = SPWN_STATE_LINK_DOWN;
}

void spacewire_link_error(const uint32_t base_address)
{
  DBG(DBG_SEVERITY_WARN, "Link error: [BA:%08X]", base_address);

  spwn_link = SPWN_STATE_LINK_DOWN;
}

void spacewire_transmit_complete(const uint32_t base_address,
                                 const uint16_t size,
                                 const uint16_t status)
{
  volatile spwn_info_t  *tx_info;
  rtems_status_code     rtems_status;

  DBG(DBG_SEVERITY_INFO, "SpaceWire transmit completed");

  if (spwn_q_get(&tx_q, &tx_info) == true) {
    tx_info->size = size;
    tx_info->status = status;
  } else {
    DBG(DBG_SEVERITY_ERR, "No tx_info available in TX queue to get, SHOULD NOT HAPPEN!");
  }

  rtems_status = rtems_semaphore_release(tx_complete_semaphore);
  if (rtems_status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to release tx_complete_semaphore (%d:%s).",
        rtems_status, rtems_status_text(rtems_status));
  }
}

void spacewire_data_incoming(const uint32_t base_address, const uint8_t pilot_byte)
{
  uint8_t               index;
  rtems_status_code     status;

  DBG(DBG_SEVERITY_INFO, "Packet incoming for log addr %d.", pilot_byte);

  if (pilot_byte < SPWN_RESERVED_LOG_ADDRESSES) {
    spacewire_drop_packet(base_address);

    DBG(DBG_SEVERITY_WARN,"First byte %d not valid, Packet dropped.", pilot_byte);

  } else {
    /* Get reader array index based on pilot byte */
    index = (pilot_byte - SPWN_RESERVED_LOG_ADDRESSES) & 0xFF;

    /* Listener available for this packet? */
    if (spwn_dev_ctxs[index].reader_fd != INVALID_FD) {
      spwn_q_put(&rx_q, &spwn_dev_ctxs[index].rx);

      DBG(DBG_SEVERITY_INFO, "Notifying user %d for a new packet.", pilot_byte);

      /* Notify user that a packet has arrived for reception */
      status = rtems_semaphore_release(spwn_dev_ctxs[index].rx.incoming_semaphore);
      if (status != RTEMS_SUCCESSFUL) {
        DBG(DBG_SEVERITY_ERR,
            "Failed to release semaphore rx[%d].incoming_semaphore (%d:%s).",
            pilot_byte, status, rtems_status_text(status));
      }
    } else {
      /* Drop packet as no one is interested */
      spacewire_drop_packet(base_address);

      DBG(DBG_SEVERITY_WARN,
          "No listener available for logic address %d. Packet dropped", pilot_byte);
    }
  }
}

void spacewire_data_received(const uint32_t base_address,
                             const uint16_t size,
                             const uint16_t status)
{
  volatile spwn_info_t  *rx_info;
  rtems_status_code     rtems_status;

  DBG(DBG_SEVERITY_INFO, "Packet received from IP. BA: %08X Size:%d status:0x%04X.",
      base_address, size, status);

  if (spwn_q_get(&rx_q, &rx_info) == true) {
    rx_info->size = size;
    rx_info->status = status;

    DBG(DBG_SEVERITY_INFO, "Notifying reader that packet reception is completed.");

    rtems_status = rtems_semaphore_release(rx_info->complete_semaphore);
    if (rtems_status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR,
          "Failed to release semaphore rx.complete_semaphore (%d:%s).",
          rtems_status, rtems_status_text(rtems_status));
    }
  }
}

static int spwn_open(rtems_libio_t *iop,
                     const char    *path,
                     int            oflag,
                     mode_t         mode)
{
  int                   fd;
  spwn_dev_ctx_t        *dev_ctx;
  rtems_status_code     status;

  fd = rtems_libio_iop_to_descriptor(iop);
  dev_ctx = (spwn_dev_ctx_t *) IMFS_generic_get_context_by_iop(iop);

  DBG(DBG_SEVERITY_INFO, "Opening SpaceWire device file descriptor %d.", fd);

  /* Use mutex to protect against simultaneous open from different tasks */
  status = rtems_semaphore_obtain(open_mutex,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to obtain mutex for SpaceWire driver: %s)",
        rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  /* Single writer allowed per SpaceWire logical address */
  if ((iop->flags & LIBIO_FLAGS_WRITE) == LIBIO_FLAGS_WRITE) {
    if (dev_ctx->writer_fd != INVALID_FD) {
      DBG(DBG_SEVERITY_WARN, "%s already opened for writing. Aborting request.", path);
      rtems_semaphore_release(open_mutex);
      rtems_set_errno_and_return_minus_one(EALREADY);
    } else {
      dev_ctx->writer_fd = fd;
    }
  }

  /* Single reader allowed per SpaceWire logical address */
  if ((iop->flags & LIBIO_FLAGS_READ) == LIBIO_FLAGS_READ) {
    if (dev_ctx->reader_fd != INVALID_FD) {
      DBG(DBG_SEVERITY_WARN, "%s already opened for reading. Aborting request.", path);
      rtems_semaphore_release(open_mutex);
      rtems_set_errno_and_return_minus_one(EALREADY);
    } else {
      dev_ctx->reader_fd = fd;
    }
  }

  rtems_semaphore_release(open_mutex);

  /* Create a semaphore for RX messages */
  status = rtems_semaphore_create(rtems_build_name('S', 'R', dev_ctx->index, 'R'),
                                  0,
                                  SPWN_TASK_SEM_ATTR_BINARY,
                                  RTEMS_NO_PRIORITY_CEILING, /* Accessed from ISR */
                                  (rtems_id *) &dev_ctx->rx.incoming_semaphore);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to create semaphore for RX messages, status: %d", status);
    rtems_set_errno_and_return_minus_one(EIO);
  }

  /* Create a semaphore for RX completed */
  status = rtems_semaphore_create(rtems_build_name('S', 'R', dev_ctx->index, 'D'),
                                  0,
                                  SPWN_TASK_SEM_ATTR_BINARY,
                                  RTEMS_NO_PRIORITY_CEILING,
                                  (rtems_id *) &dev_ctx->rx.complete_semaphore);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to create semaphore for RX completion, status: %d", status);
    rtems_semaphore_delete(dev_ctx->rx.incoming_semaphore);
    rtems_set_errno_and_return_minus_one(EIO);
  }

  /* There is no need to turn on/off electrical drivers here since these must always
     be on to allow for routing signals to cross a board without the SpaceWire
     device to be opened */

  return 0;
}

static int spwn_close(rtems_libio_t *iop)
{
  int                   fd;
  spwn_dev_ctx_t        *dev_ctx;

  fd = rtems_libio_iop_to_descriptor(iop);
  dev_ctx = (spwn_dev_ctx_t *) IMFS_generic_get_context_by_iop(iop);

  DBG(DBG_SEVERITY_INFO, "Closing SpaceWire file descriptor %d.", fd);

  rtems_semaphore_delete(dev_ctx->rx.incoming_semaphore);
  rtems_semaphore_delete(dev_ctx->rx.complete_semaphore);

  /* Only one reader per device */
  if (fd == dev_ctx->reader_fd) {
    DBG(DBG_SEVERITY_INFO, "Clear reader_fd.");
    dev_ctx->reader_fd = INVALID_FD;
  }

  /* Only one writer per device */
  if (fd == dev_ctx->writer_fd) {
    DBG(DBG_SEVERITY_INFO, "Clear writer_fd.");
    dev_ctx->writer_fd = INVALID_FD;
  }


  /* There is no need to turn on/off electrical drivers here since these must always
     be on to allow for routing signals to cross a board without the SpaceWire
     device to be opened */

  return 0;
}

static ssize_t spwn_read(rtems_libio_t *iop,
                         void          *buffer,
                         size_t         count)
{
  int                   fd;
  spwn_dev_ctx_t        *dev_ctx;
  volatile spwn_info_t  *rx_info; 
  rtems_status_code     status;
  int32_t               rv;

  fd = rtems_libio_iop_to_descriptor(iop);
  dev_ctx = (spwn_dev_ctx_t *) IMFS_generic_get_context_by_iop(iop);

  DBG(DBG_SEVERITY_INFO, "Reading from SpaceWire file descriptor %d.", fd);

  /* The calling function in libcsupport checks permissions and buffer. */

  if (count > SPWN_MAX_PACKET_SIZE) {
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  if (spwn_link != SPWN_STATE_LINK_UP) {
    spacewire_link_auto_start(AAC_BSP_SPW0_BASE);
  }

  /* One receiver per logical address at a time */
  DBG(DBG_SEVERITY_INFO, "Awaiting a packet for SpaceWire file descriptor %d.", fd);
  status = rtems_semaphore_obtain(dev_ctx->rx.incoming_semaphore,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to obtain rx.incoming_semaphore (%d:%s).",
        status, rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  /* Prepare to receive packet */
  rv = spacewire_register_receive_buffer(AAC_BSP_SPW0_BASE,
                                         (uint8_t *) buffer,
                                         count,
                                         SPW_MD_STAT_RX_KPB); /* keep pilot byte in packet */
  if (rv < 0) {
    DBG(DBG_SEVERITY_ERR, "Failed to set up receive buffer. Error %d.", (int) rv);

    /* Drop the packet so as not blocking the traffic */
    spacewire_drop_packet(AAC_BSP_SPW0_BASE);

    /* Remove from local information */
    spwn_q_get(&rx_q, &rx_info);

    /* Pass on error code as errno */
    rtems_set_errno_and_return_minus_one(-rv);
  }

  DBG(DBG_SEVERITY_INFO, "Awaiting packet reception for SpaceWire file descriptor %d.", fd);

  /* Wait until packet has been completely received */
  status = rtems_semaphore_obtain(dev_ctx->rx.complete_semaphore,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to obtain rx.complete_semaphore (%d:%s).",
        status, rtems_status_text(status));
  }

  /* Map errno (if any) to the packet status */
  if (dev_ctx->rx.status & SPW_MD_STAT_RX_EOP) {
    if (dev_ctx->rx.status & SPW_MD_STAT_RX_OVF) {
      errno = EOVERFLOW;
      return 0;
    } else {
      /* Invalidate cache after DMA receive to avoid stale data in read buffer.
       *
       * Current write-through configuration saves us from the possibility of
       * invalidating dirty data.
       */
      rtems_cache_invalidate_multiple_data_lines(buffer, dev_ctx->rx.size);
    }
  } else if (dev_ctx->rx.status & SPW_MD_STAT_RX_EEP) {
    rtems_set_errno_and_return_minus_one(ETIMEDOUT);
  }

  DBG(DBG_SEVERITY_INFO, "Packet reception complete for file descriptor %d.", fd);

  return dev_ctx->rx.size;
}

static ssize_t spwn_write(rtems_libio_t *iop,
                          const void    *buffer,
                          size_t         count)
{
  int                   fd;
  spwn_dev_ctx_t        *dev_ctx;
  rtems_status_code     status;
  int32_t               rv;

  fd = rtems_libio_iop_to_descriptor(iop);
  dev_ctx = (spwn_dev_ctx_t *) IMFS_generic_get_context_by_iop(iop);

  /* The calling function in libcsupport checks permissions and buffer. */

  DBG(DBG_SEVERITY_INFO, "Writing to SpaceWire file descriptor %d.", fd);

  if (count > SPWN_MAX_PACKET_SIZE) {
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  status = rtems_semaphore_obtain(tx_send_mutex,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to obtain tx_send_mutex (%d:%s)",
        status, rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  dev_ctx->tx.buffer = (uint8_t *) buffer;
  dev_ctx->tx.size = count;

  /* Set up for feedback after transmit */
  spwn_q_put(&tx_q, &dev_ctx->tx);

  if (spwn_link != SPWN_STATE_LINK_UP) {
    spacewire_link_start(AAC_BSP_SPW0_BASE);
  }

  /* Transmit packet */
  rv = spacewire_transmit(AAC_BSP_SPW0_BASE, dev_ctx->tx.buffer, dev_ctx->tx.size);
  if (rv < 0) {
    DBG(DBG_SEVERITY_ERR, "Failed to send packet, error %d", (int) rv);
    rtems_set_errno_and_return_minus_one(-rv);
  }

  /* Wait for IP to finish transmitting */
  status = rtems_semaphore_obtain(tx_complete_semaphore,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to obtain tx_complete_semaphore (%d:%s).",
        status, rtems_status_text(status));
  }

  rv = 0;
  if ((dev_ctx->tx.status & SPW_MD_STAT_TX_SENT) &&
      !(dev_ctx->tx.status & SPW_MD_STAT_TX_TO)) {
    rv = dev_ctx->tx.size;
    errno = 0;
  } else if (dev_ctx->tx.status & SPW_MD_STAT_TX_TO) {
    errno = ETIMEDOUT;
  } else {
    errno = EIO;
  }

  /* Allow the next user to transmit */
  status = rtems_semaphore_release(tx_send_mutex);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to release tx_send_mutex (%d:%s)", status, rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  if (errno != 0) {
    rtems_set_errno_and_return_minus_one(errno);
  }

  return rv;
}

static int spwn_control(rtems_libio_t   *iop,
                        ioctl_command_t  command,
                        void            *buffer)
{
  int                   fd;
  uint32_t              val;

  fd = rtems_libio_iop_to_descriptor(iop);

  DBG(DBG_SEVERITY_INFO, "Setting control options on file descriptor %d.", fd);

  val = (uint32_t) buffer;

  switch (command) {
    case SPWN_IOCTL_MODE_SET:
      if (val > SPWN_IOCTL_MODE_LOOPBACK) {
        rtems_set_errno_and_return_minus_one(EINVAL);
      } else {
        DBG(DBG_SEVERITY_INFO, "Setting SpaceWire mode to %02X.", val);
        spacewire_mode_set(AAC_BSP_SPW0_BASE, val);
      }
      break;

    default:
      DBG(DBG_SEVERITY_WARN, "Unknown ioctl command 0x%08X. Request ignored.", command);
      rtems_set_errno_and_return_minus_one(EINVAL);
      break;
  }

  return 0;
}

static const rtems_filesystem_file_handlers_r rtems_spwn_handlers = {
  .open_h       = spwn_open,
  .close_h      = spwn_close,
  .read_h       = spwn_read,
  .write_h      = spwn_write,
  .ioctl_h      = spwn_control,
  .lseek_h      = rtems_filesystem_default_lseek_file,
  .fstat_h      = IMFS_stat,
  .ftruncate_h  = rtems_filesystem_default_ftruncate,
  .fsync_h      = rtems_filesystem_default_fsync_or_fdatasync,
  .fdatasync_h  = rtems_filesystem_default_fsync_or_fdatasync,
  .fcntl_h      = rtems_filesystem_default_fcntl,
  .readv_h      = rtems_filesystem_default_readv,
  .writev_h     = rtems_filesystem_default_writev,
};

static const IMFS_node_control rtems_spwn_node_control =
  IMFS_GENERIC_INITIALIZER(
    &rtems_spwn_handlers,
    IMFS_node_initialize_generic,
    IMFS_node_destroy_default);

rtems_device_driver spwn_initialize(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *args)
{
  int                   rv;
  rtems_status_code     status;
  /* Allow for an additional 3 identifier digits and a '\0' in the device name */
  char                  dev_name[SPWN_DEVICE_NAME_LENGTH + 4];

  DBG(DBG_SEVERITY_INFO, "Initializing SpaceWire driver.");

  status = rtems_semaphore_create(rtems_build_name('S', 'P', 'R', 'W'),
                                  1,
                                  RTEMS_DEFAULT_ATTRIBUTES |
                                  RTEMS_SIMPLE_BINARY_SEMAPHORE,
                                  0,
                                  &open_mutex);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to create mutex for SpaceWire driver, error %d, %s)",
        status, rtems_status_text(status));
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  spwn_device_info.base_address = AAC_BSP_SPW0_BASE;

  for (minor = 0; minor < SPWN_NR_OF_LOG_ADDRESSES; minor++) {
    /* Create the name for this device */
    sprintf(dev_name, "%s%d", SPWN_DEVICE_0_NAME_PREFIX,
            (int) minor + SPWN_RESERVED_LOG_ADDRESSES);

    /* Set index for later identification */
    spwn_dev_ctxs[minor].index = minor;

    rv = IMFS_make_generic_node(dev_name,
                                S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO,
                                &rtems_spwn_node_control,
                                (void *) &spwn_dev_ctxs[minor]);
    if (rv < 0) {
      DBG(DBG_SEVERITY_ERR, "Failed to register SpaceWire driver (M%lu:m%lu): %s)",
          major, minor, strerror(errno));
      return RTEMS_UNSATISFIED /* ENODEV */;
    }

    /* Set default reader/writer threads */
    spwn_dev_ctxs[minor].reader_fd = INVALID_FD;
    spwn_dev_ctxs[minor].writer_fd = INVALID_FD;
  }

  DBG(DBG_SEVERITY_INFO, "All SPWN devices registered.");

  /* Initial link state */
  spwn_link = SPWN_STATE_LINK_UNKNOWN;

  /* Initialize mutex as TX lock for user applications */
  status = rtems_semaphore_create(rtems_build_name('S', 'T', 'T', 'X'),
                                  1,
                                  SPWN_TASK_SEM_ATTR_BINARY,
                                  RTEMS_NO_PRIORITY_CEILING,
                                  &tx_send_mutex);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to create mutex for TX. Status: %d", status);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  /* Initialize semaphore for TX messages from BM driver */
  status = rtems_semaphore_create(rtems_build_name('S', 'S', 'T', 'X'),
                                  0,
                                  SPWN_TASK_SEM_ATTR_BINARY,
                                  RTEMS_NO_PRIORITY_CEILING,
                                  &tx_complete_semaphore);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to create semaphore for TX. Status: %d", status);
    rtems_semaphore_delete(tx_send_mutex);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  spacewire_initialize(AAC_BSP_SPW0_BASE);

  /* Install interrupt handler */
  status = rtems_interrupt_handler_install(AAC_BSP_SPW0_IRQ,
                                           "SpaceWire",
                                           RTEMS_INTERRUPT_UNIQUE,
                                           spacewire_isr,
                                           (void *) &spwn_device_info);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to install interrupt handler! SpaceWire driver cannot be used.");
    rtems_semaphore_delete(tx_send_mutex);
    rtems_semaphore_delete(tx_complete_semaphore);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  /* Enable all interrupts right away so we can drop any incoming packets without readers */
  REG32(AAC_BSP_SPW0_BASE + SPW_IER) = (SPW_IER_LINK_UP |
                                             SPW_IER_LINK_DOWN |
                                             SPW_IER_RX |
                                             SPW_IER_TX |
                                             SPW_IER_LINK_ERR |
                                             SPW_IER_PB);

  return RTEMS_SUCCESSFUL;
}
