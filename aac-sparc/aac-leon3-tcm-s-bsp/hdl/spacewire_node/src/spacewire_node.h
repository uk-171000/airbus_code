/*-------------------------------------------------------------------------------------------
 * Copyright (C) 2015 - 2016 ÅAC Microtec AB
 *
 * Filename:     spacewire_node.h
 * Module name:  SpaceWire
 *
 * Author(s):    PeSt, InSi
 * Support:      support@aacmicrotec.com
 * Description:  SpaceWire Node driver header file
 * Requirements: Adheres to the ÅAC Spacewire Node Driver Requirement Specification Rev. A
 *-----------------------------------------------------------------------------------------*/

#ifndef SPACEWIRE_NODE_H
#define SPACEWIRE_NODE_H

#include <stdint.h>
#include <toolchain_support.h>

/* SpaceWire Node register offsets */
#define SPW_CR                  0x00      /* Command Register */
#define SPW_SR                  0x04      /* Status Register */
#define SPW_IER                 0x08      /* Interrupt Enable Register */
#define SPW_LSR                 0x0C      /* Link Status Register */
#define SPW_ESR                 0x10      /* Error Status Registe r*/
#define SPW_TRR                 0x14      /* Transmit Rate Register */
#define SPW_MSR_RX              0x18      /* Maximum Size Register Rx */
#define SPW_TOR_TX              0x1C      /* Time Out Register Tx */
#define SPW_DNR_RX              0x20      /* Descriptor Notification Register Rx */
#define SPW_DNR_TX              0x24      /* Descriptor Notification Register Tx */
#define SPW_MR                  0x28      /* Misc Register */

/* Command Register */
#define SPW_CR_LAS              (1 << 0)  /* Link Auto Start */
#define SPW_CR_LST              (1 << 1)  /* Link Start */
#define SPW_CR_LDIS             (1 << 2)  /* Link Disable */

/* Status Register */
#define SPW_SR_LUP              (1 << 0)  /* Link is Up */
#define SPW_SR_LDWN             (1 << 1)  /* Link is Down */
#define SPW_SR_RX               (1 << 2)  /* RX notification */
#define SPW_SR_TX               (1 << 3)  /* TX notification */
#define SPW_SR_LE               (1 << 4)  /* Link Error */
#define SPW_SR_LIRS             (1 << 5)  /* Link In Run State */
#define SPW_SR_PB_RX            (1 << 6)  /* Pilot Byte Received */
#define SPW_SR_PB_MASK          0xFF00

/* Interrupt Enable Register */
#define SPW_IER_LINK_UP         (1 << 0)  /* Link Up */
#define SPW_IER_LINK_DOWN       (1 << 1)  /* Link Down */
#define SPW_IER_RX              (1 << 2)  /* RX notification */
#define SPW_IER_TX              (1 << 3)  /* TX notification */
#define SPW_IER_LINK_ERR        (1 << 4)  /* Link Error */
#define SPW_IER_LINK_RS         (1 << 5)  /* Link in run state */
#define SPW_IER_PB              (1 << 6)  /* Pilot Byte received */

/* Timer Rate Register */
#define SPW_TRR_TX_CLD_MASK     0x3F      /* Clock Divisor */

/* Mode settings */
#define SPW_MODE_OFF            0x00      /* Line drive receive OFF, normal */
#define SPW_MODE_NORMAL         0x01      /* Line drive receive ON, normal */
#define SPW_MODE_LOOPBACK       0x02      /* Line drive receive OFF, loopback */

/* Memory Descriptors */
#define SPW_MD_OFFSET_TX_ADDR   0x40     /* TX Buffer Address */
#define SPW_MD_OFFSET_TX_STAT   0x44     /* TX Status */
#define SPW_MD_OFFSET_RX_ADDR   0x80     /* RX Buffer Address */
#define SPW_MD_OFFSET_RX_STAT   0x84     /* RX Status */

/* Status masks */
#define SPW_MD_STAT_MASK_LEN    0xFFFF0000
#define SPW_MD_STAT_MASK_FLAGS  0x0000FFFF

/* Status */
#define SPW_MD_STAT_ENA         (1 << 0)  /* Enable */
#define SPW_MD_STAT_WRAP        (1 << 1)  /* Wrap  */
/* TX */
#define SPW_MD_STAT_TX_SENT     (1 << 4)  /* Sent */
#define SPW_MD_STAT_TX_TO       (1 << 5)  /* Time Out  */
#define SPW_MD_STAT_TX_IE_SENT  (1 << 8)  /* Interrupt Exception Sent */
#define SPW_MD_STAT_TX_IE_TO    (1 << 9)  /* Interrupt Exception Time Out */
/* RX */
#define SPW_MD_STAT_RX_EOP      (1 << 4)  /* End Of Packet */
#define SPW_MD_STAT_RX_EEP      (1 << 5)  /* Error End of Packet  */
#define SPW_MD_STAT_RX_OVF      (1 << 6)  /* Overflow  */
#define SPW_MD_STAT_RX_IE_EOP   (1 << 8)  /* Interrupt Exception End Of Packet */
#define SPW_MD_STAT_RX_IE_EEP   (1 << 9)  /* Interrupt Exception Error End Of Packet */
#define SPW_MD_STAT_RX_IE_OVF   (1 << 10) /* Interrupt Exception Overflow */
#define SPW_MD_STAT_RX_STP      (1 << 12) /* Store Packet  */
#define SPW_MD_STAT_RX_KPB      (1 << 13) /* Keep Pilot Byte  */

/* Due to DMA transfer of 4 bytes for each write operation, 
   size is limited for  no to exceed the reception buffer */
#define SPW_PKT_SIZE_MAX        0xFFFC

/*-----------------------------------------------------------------------------
 * Data types used in the API.
 *-----------------------------------------------------------------------------*/
typedef struct
{
  uint32_t base_address;
  void* info;
} spacewire_device_info_t;


/*-----------------------------------------------------------------------------
 * Callback functions.
 * Note: All callbacks MUST be implemented by the client.
 *-----------------------------------------------------------------------------*/

/*
 * Name         spacewire_link_established
 * @brief       Callback on when a link has been established.
 * @param[in]   base_address  Base address of the spacewire node where the link 
 *                            was established.
 */
void spacewire_link_established(const uint32_t base_address);

/*
 * Name         spacewire_link_cancelled
 * @brief       Callback on when a link is cancelled.
 * @param[in]   base_address  Base address of the spacewire node where the link 
 *                            was cancelled.
 */
void spacewire_link_cancelled(const uint32_t base_address);

/*
 * Name         spacewire_link_error
 * @brief       Callback on when a link error is has occured.
 * @param[in]   base_address  Base address of the spacewire node where the link 
 *                            error occured.
 */
void spacewire_link_error(const uint32_t base_address);

/*
 * Name         spacewire_transmit_complete
 * @brief       Callback on when a packet has been transmitted.
 * @param[in]   base_address  Base address of the spacewire node
 * @param[in]   size          Size of data that was transmitted.
 * @param[in]   status        Status of the transmitted packet
 */
void spacewire_transmit_complete(const uint32_t base_address,
                                 const uint16_t size,
                                 const uint16_t status);

/*
 * Name         spacewire_data_incoming
 * @brief       Callback on when a packet has arrived on the datalink.
 * @param[in]   base_address  Base address of the spacewire node
 * @param[in]   pilot_byte    Pilot byte (the first byte) of packet
 */
void spacewire_data_incoming(const uint32_t base_address, const uint8_t pilot_byte);

/*
 * Name         spacewire_data_received
 * @brief       Callback on when a packet has been completely retrieved.
 * @param[in]   base_address  Base address of the spacewire node
 * @param[in]   size          Size of the packet
 * @param[in]   status        Status of the retrieved packet
 */
void spacewire_data_received(const uint32_t base_address,
                             const uint16_t size,
                             const uint16_t status);

/*-----------------------------------------------------------------------------
 * Exported functions
 *-----------------------------------------------------------------------------*/

/*
 * Name         spacewire_initialize
 * @brief       Initialises the driver. MUST be called first
 * @param[in]   base_address  Base address of the targeted spacewire node
 sp */
void spacewire_initialize(const uint32_t base_address);

/*
 * Name         spacewire_mode_set
 * @brief       Sets the targeted spacewire node into the specified operational mode.
 * @param[in]   base_address  Base address of the spacewire node where interrupt
 *                            occured.
 * @param[in]   mode          Following modes are possible:
 *                              SPW_MISC_LOOPBACK         Loopback mode.
 *                              SPW_MISC_NORMAL           Normal mode i.e. Not loopback.
 * @return      0             Requested mode set.
 *              -EINVAL       Invalid mode requested. request ignored.
 */
int32_t spacewire_mode_set(const uint32_t base_address, const uint32_t mode);

/*
 * Name:        spacewire_isr
 * @brief       Interrupt service routine for handling all interrupt events.
 * @param[in]   arg Device info context for the spacewire node.
 */
AAC_INTERRUPT_HANDLER_SIGNATURE(spacewire_isr, arg);

/*
 * Name         spacewire_transmit
 * @brief       Initiates transmission of a packet
 *              Interrupt driven call. Once the packet is transmitted,
 *              spacewire_transmit_complete is called.
 * @param[in]   base_address  Base address of the targeted spacewire node
 * @param[in]   buffer        Buffer containing the packet
 * @param[in]   size          Size of the packet. Valid sizes are
 *                            0 > size <= SPW_NODE_MAXIMUM_DATA_SEQUENCE_LENGTH.
 * @return      0             Transmission initiated
 *              -EINVAL       size is invalid
 *              -EBUSY        Datalink is busy.
 */
int32_t spacewire_transmit(const uint32_t base_address,
                           const uint8_t* buffer,
                           const uint16_t size);
/*
 * Name         spacewire_register_receive_buffer
 * @brief       Initiated the reception of a packet.
 *              Interrupt driven call. Once the packet has been retrieved,
 *              spacewire_data_received is called with status.
 * @param[in]   base_address  Base address of the targeted spacewire node
 * @param[in]   buffer        Buffer to store the packet
 * @param[in]   size          Size of the packet. Valid sizes are
 *                            0 > size <= SPW_NODE_MAXIMUM_DATA_SEQUENCE_LENGTH.
 * @param[in]   b_keep_pilot_byte  0: Exclude the pilot byte in the received packet.
 *                                 1: Include the pilot byte in the received packet.
 * @return      0:            Reception initiated.
 *              -EINVAL       size is invalid
 *              -EBUSY        Datalink is busy.
 */
int32_t spacewire_register_receive_buffer(const uint32_t base_address,
                                          uint8_t* buffer,
                                          const uint16_t size,
                                          const uint32_t b_keep_pilot_byte);
/*
 * Name         spacewire_drop_packet
 * @brief       Drops the received packet. 
 *              Note! Only valid after data_incoming callback call. If the packet is in the 
 *              process of being received with spacewire_register_receive_buffer, this call
 *              will not have any effect.
 * @param[in]   base_address  Base address of the targeted spacewire node
 * @return      0:            Packet dropped.
 *              -EBUSY        Datalink is busy.
 */
int32_t spacewire_drop_packet(const uint32_t base_address);

/*
 * Name         spacewire_link_auto_start
 * @brief       Prepares the spaceWire node to accept a link from a node.
 */
void spacewire_link_auto_start(const uint32_t base_address);

/*
 * Name         spacewire_link_start
 * @brief       Establishes a link to a node.
 * @param[in]   base_address  Base address of the targeted spacewire node
 */
void spacewire_link_start(const uint32_t base_address);

/*
 * Name         spacewire_link_disable
 * @brief       Breaks an established link
 * @param[in]   base_address  Base address of the targeted spacewire node
 */
void spacewire_link_disable(const uint32_t base_address);

/*
 * Name         spacewire_set_transmit_speed
 * @brief       Sets the spacewire link speed.
 * @param[in]   base_address    Base address of the targeted spacewire node
 * @param[in]   transmit_speed  Transmit speed in bit/s. 
 *                              Valid values are 2 - 50 Mbit/s.
 * @return      0               Requested transmission speed set.
 *              -EINVAL         transmission_speed is invalid.
 */
int32_t spacewire_set_transmit_speed(const uint32_t base_address,
                                     const uint32_t transmit_speed);

#endif
