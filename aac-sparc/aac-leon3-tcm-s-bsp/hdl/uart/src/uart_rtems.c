/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2018 ÅAC Microtec AB
 *
 * Filename:     uart_rtems.c
 * Module name:  uart
 *
 * Author(s):    ErZa, JaVi, MaWe
 * Support:      support@aacmicrotec.com
 * Description:  UART RTEMS Driver implementation
 * Requirements: Adheres to the ÅAC UART Driver Requirement Specification Rev. A
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <inttypes.h>

#include <rtems/imfs.h>
#include <rtems/libio.h>
#include <rtems/seterr.h>
#include <rtems/score/cpu.h>

#include <bsp.h>
#include <bsp/irq.h>

#include "uart.h"
#include "uart_rtems.h"
#include "uart_internal_rtems.h"
#include "toolchain_support.h"

#define INVALID_FD (-1)

static rtems_id open_mutex;

uart_callbacks_t uart_callbacks = {
  .uart_rx_linestatus_callback = uart_rtems_rx_linestatus_callback,
  .uart_rx_data_callback = uart_rtems_rx_data_callback,
  .uart_timeout_callback = uart_rtems_rx_data_callback,
  .uart_tx_empty_callback = uart_rtems_tx_empty_callback,
  .uart_modem_callback = NULL
};

uint32_t _or1k_board_clk_freq = AAC_BSP_CLOCK_FREQ;

#define UART_MSG_SEM_ATTR    (RTEMS_FIFO | RTEMS_SIMPLE_BINARY_SEMAPHORE | \
                              RTEMS_NO_MULTIPROCESSOR_RESOURCE_SHARING | RTEMS_LOCAL)

/* Assign the static portion of the info struct */
static uart_info_t uarts[AAC_BSP_UART_COUNT] = {
  {.name = "/dev/uart0",
   .alias = "",
   .base = AAC_BSP_UART0_BASE,
   .irq = AAC_BSP_UART0_IRQ,
   .default_mode = UART_MODER_RS422},
  {.name = "/dev/uart1",
   .alias = "",
   .base = AAC_BSP_UART1_BASE,
   .irq = AAC_BSP_UART1_IRQ,
   .default_mode = UART_MODER_RS422},
  {.name = "/dev/uart2",
   .alias = "",
   .base = AAC_BSP_UART2_BASE,
   .irq = AAC_BSP_UART2_IRQ,
   .default_mode = UART_MODER_RS422},
  {.name = "/dev/uart3",
   .alias = "",
   .base = AAC_BSP_UART3_BASE,
   .irq = AAC_BSP_UART3_IRQ,
   .default_mode = UART_MODER_RS422},
  {.name = "/dev/uart4",
   .alias = "",
   .base = AAC_BSP_UART4_BASE,
   .irq = AAC_BSP_UART4_IRQ,
   .default_mode = UART_MODER_RS422},
  {.name = "/dev/uart5",
   .alias = "",
   .base = AAC_BSP_UART5_BASE,
   .irq = AAC_BSP_UART5_IRQ,
   .default_mode = UART_MODER_RS422},
  {.name = "/dev/uart6",
   .alias = "/dev/uart_psu_control",
   .base = AAC_BSP_UART6_BASE,
   .irq = AAC_BSP_UART6_IRQ,
   .default_mode = UART_MODER_RS485},
  {.name = "/dev/uart7",
   .alias = "/dev/uart_safe_bus",
   .base = AAC_BSP_UART7_BASE,
   .irq = AAC_BSP_UART7_IRQ,
   .default_mode = UART_MODER_RS485},
};

static const uart_args_t isr_args[AAC_BSP_UART_COUNT] = {
  {AAC_BSP_UART0_BASE, &uarts[0]},
  {AAC_BSP_UART1_BASE, &uarts[1]},
  {AAC_BSP_UART2_BASE, &uarts[2]},
  {AAC_BSP_UART3_BASE, &uarts[3]},
  {AAC_BSP_UART4_BASE, &uarts[4]},
  {AAC_BSP_UART5_BASE, &uarts[5]},
  {AAC_BSP_UART6_BASE, &uarts[6]},
  {AAC_BSP_UART7_BASE, &uarts[7]}
};

void uart_q_put(uart_queue_t *q, uint32_t *value)
{
  uint32_t level;

  if (q->items < UART_Q_SIZE) {
    *value = q->rear;
    q->rear = (q->rear + 1) % UART_Q_SIZE;

    rtems_interrupt_disable(level);
    q->items++;
    rtems_interrupt_enable(level);
  }
}

bool uart_q_get(uart_queue_t *q, uint32_t *value)
{
  uint32_t level;

  if (q->items > 0) {
    *value = q->front;
    q->front = (q->front + 1) % UART_Q_SIZE;

    rtems_interrupt_disable(level);
    q->items--;
    rtems_interrupt_enable(level);

    return true;
  }
  return false;
}

bool uart_q_peek(uart_queue_t *q, uint32_t *value)
{
  if (q->items > 0) {
    *value = q->front;
    return true;
  }
  return false;
}

bool uart_q_next_free(uart_queue_t *q, uint32_t *value)
{
  if (q->items <= UART_Q_SIZE) {
    *value = q->rear;
    return true;
  }
  return false;
}

bool uart_q_is_full(uart_queue_t *q)
{
  if (q->items == UART_Q_SIZE) {
    return true;
  }
  return false;
}

bool uart_q_is_empty(uart_queue_t *q)
{
  if (q->items == 0) {
    return true;
  }
  return false;
}

void uart_q_clear(uart_queue_t *q)
{
  uint32_t level;

  rtems_interrupt_disable(level);
  q->front = 0;
  q->rear = 0;
  q->items = 0;
  rtems_interrupt_enable(level);
}

inline void uart_empty_fifo(uart_info_t *uart) {
  uint32_t      index;
  uint8_t       rxfifo_count;
  uint32_t      i;

  index = 0;
  if (uart_q_is_full(&uart->queue) == false) {
    /* Get next index */
    uart_q_put(&uart->queue, &index);
    uart->rx_buf_len[index] = 0;

    /* Check available data */
    rxfifo_count = uart_check_for_char(uart->base);

    /* Empty FIFO */
    for (i = 0; i < rxfifo_count; i++) {
      uart->rx_buf[index][uart->rx_buf_len[index]] = uart_getc(uart->base);
      uart->rx_buf_len[index]++;
    }
  } else {
    DBG(DBG_SEVERITY_INFO, "%s, ran out of queue elements", uart->name);

    /* Spill UART data */
    uart_clear_rx_fifo(uart->base);
    uart->error = 1; // Check this
  }
}

void uart_rtems_rx_linestatus_callback(uart_args_t *args)
{
  uart_info_t           *uart;
  rtems_status_code     status;
  uint8_t               lsr;

  uart = (uart_info_t *) args->info;

  /* This should clear the interrupt at the UART */
  lsr = uart_get_lsr(uart->base);

  /* Empty any data received into the RX FIFO */
  uart_empty_fifo(uart);

  /* Check possible error conditions */
  if (lsr & (UART_LSR_FE | UART_LSR_PE | UART_LSR_OE)) {
    uart->error = 1;

    status = rtems_semaphore_release(uart->read_semaphore);
    if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR, "%s, Failed to release read semaphore", uart->name);
    }
  }

  DBG(DBG_SEVERITY_INFO, "%s, Received line status interrupt containing 0x%02x",
      uart->name, lsr);
}

void uart_rtems_rx_data_callback(uart_args_t *args)
{
  uart_info_t           *uart;
  rtems_status_code     status;

  uart = (uart_info_t *) args->info;

  uart_empty_fifo(uart);

  status = rtems_semaphore_release(uart->read_semaphore);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "%s, Failed to release read semaphore", uart->name);
  }
}

void uart_rtems_tx_empty_callback(uart_args_t *args)
{
  uart_info_t *uart;
  uint32_t max;
  uint32_t i;
  rtems_status_code status;

  uart = (uart_info_t *) args->info;

  status = rtems_semaphore_obtain(uart->write_setup_mutex, RTEMS_NO_WAIT, RTEMS_NO_TIMEOUT);
  switch (status) {
    case RTEMS_UNSATISFIED:
      /* Write setup is in progress, wait until next tx empty interrupt */
      return;

    case RTEMS_SUCCESSFUL:
      /* Exclusive access to write setup granted */
      break;

    default:
      DBG(DBG_SEVERITY_ERR, "%s, Failed to obtain write setup mutex semaphore: %s",
          uart->name, rtems_status_text(status));
      return;
  }

  if (uart->tx_ongoing) {
    max = uart->tx_count - uart->tx_bytes_moved;

    if (max > uart->tx_buffer_size) {
      max = uart->tx_buffer_size;
    } else if (max == 0) {
      /* We're done, disable interrupt and release semaphore,
         TX FIFO is now empty and ready for another transaction */
      uart_txint_disable(uart->base);

      uart->tx_ongoing = false;

      status = rtems_semaphore_release(uart->write_done_semaphore);
      if (status != RTEMS_SUCCESSFUL) {
        DBG(DBG_SEVERITY_ERR, "%s, Failed to release write done semaphore", uart->name);
      }
    }

    for (i = 0; i < max; i++) {
      uart_putc_nonblocking(uart->base,
          uart->tx_buffer[uart->tx_bytes_moved + i]);
    }

    uart->tx_bytes_moved += max;
  }

  status = rtems_semaphore_release(uart->write_setup_mutex);
  if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR, "%s, Failed to release write setup mutex semaphore: %s",
          uart->name, rtems_status_text(status));
  }
}

static int rtems_uart_open(rtems_libio_t *iop,
                           const char    *path,
                           int            oflag,
                           mode_t         mode)
{
  int                   fd;
  rtems_status_code     status;
  uart_info_t           *uart;

  fd = rtems_libio_iop_to_descriptor(iop);
  uart = (uart_info_t *) IMFS_generic_get_context_by_iop(iop);

  DBG(DBG_SEVERITY_INFO, "Opening UART device file descriptor %d.", fd);

  /* Use mutex to protect against simultaneous open from different tasks */
  status = rtems_semaphore_obtain(open_mutex,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to obtain mutex for UART driver: %s)",
        rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  /* Single writer allowed per UART */
  if ((iop->flags & LIBIO_FLAGS_WRITE) == LIBIO_FLAGS_WRITE) {
    if (uart->writer_fd != INVALID_FD) {
      DBG(DBG_SEVERITY_WARN,
          "%s already opened for writing. Aborting request.", path);
      rtems_semaphore_release(open_mutex);
      rtems_set_errno_and_return_minus_one(EALREADY);
    } else {
      uart->writer_fd = fd;
    }
  }

  /* Single reader allowed per UART */
  if ((iop->flags & LIBIO_FLAGS_READ) == LIBIO_FLAGS_READ) {
    if (uart->reader_fd != INVALID_FD) {
      DBG(DBG_SEVERITY_WARN,
          "%s already opened for reading. Aborting request.", path);
      rtems_semaphore_release(open_mutex);
      rtems_set_errno_and_return_minus_one(EALREADY);
    } else {
      uart->reader_fd = fd;
    }
  }

  status = rtems_semaphore_release(open_mutex);

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to release mutex for UART driver: %s)",
        rtems_status_text(status));
  }

  if (uart->users == 0) {
    uart_set_moder(uart->base, uart->default_mode);
    uart_init(uart->base, UART_DEFAULT_BITRATE);
  }

  if ((iop->flags & LIBIO_FLAGS_WRITE) == LIBIO_FLAGS_WRITE) {
    DBG(DBG_SEVERITY_INFO, "UART driver [%s]. Enabling the tx line driver.", path);
    uart_set_txr(uart->base, UART_TXR_ENABLE);
  }

  if ((iop->flags & LIBIO_FLAGS_READ) == LIBIO_FLAGS_READ) {
    DBG(DBG_SEVERITY_INFO, "UART driver [%s]. Enabling the rx line driver.", path);
    uart_set_rxr(uart->base, UART_RXR_ENABLE);
    /* Enable reader interrupts */
    uart_rxint_enable(uart->base);
    uart_lsint_enable(uart->base);
  }

  uart->users++;

  return 0;
}

static int rtems_uart_close(rtems_libio_t *iop)
{
  int           fd;
  uart_info_t   *uart;

  fd = rtems_libio_iop_to_descriptor(iop);
  uart = (uart_info_t *) IMFS_generic_get_context_by_iop(iop);

  DBG(DBG_SEVERITY_INFO, "Closing UART file descriptor %d.", fd);

  /* Only one reader per device */
  if (fd == uart->reader_fd) {
    DBG(DBG_SEVERITY_INFO, "Clear reader_fd.");
    uart->reader_fd = INVALID_FD;
  }

  /* Only one writer per device */
  if (fd == uart->writer_fd) {
    DBG(DBG_SEVERITY_INFO, "Clear writer_fd.");
    uart->writer_fd = INVALID_FD;
  }

  uart->users--;
  if (uart->users == 0) {
    DBG(DBG_SEVERITY_INFO, "UART driver [%s]. Disabling the tx and rx line drivers.",
        uart->name);
    /* Make sure all data in transmit FIFO are sent before disabling line drivers */
    uart_flush(uart->base);
    uart_set_rxr(uart->base, UART_RXR_DISABLE);
    uart_set_txr(uart->base, UART_TXR_DISABLE);
    /* Disable reader interrupts */
    uart_rxint_disable(uart->base);
    uart_lsint_disable(uart->base);

    uart_q_clear(&uart->queue);
  }

  return 0;
}

static ssize_t rtems_uart_read(rtems_libio_t *iop,
                               void          *buffer,
                               size_t         count)
{
  rtems_status_code     status;
  int                   fd;
  uart_info_t           *uart;
  uint32_t              index;
  uint32_t              bytes_moved;

  fd = rtems_libio_iop_to_descriptor(iop);
  uart = (uart_info_t *) IMFS_generic_get_context_by_iop(iop);

  bytes_moved = 0;

  DBG(DBG_SEVERITY_INFO, "Reading from UART device %s", uart->name);

  /* The calling function in libcsupport checks permissions and buffer. */

  /* In the last read call, we might have read out all the data that the semaphore was
     signalling about this time, so make sure there is data available in the queue
     (possibly re-obtain the semaphore) before moving on. */
  do {
    /* Wait and block until data is received */
    status = rtems_semaphore_obtain(uart->read_semaphore,
                                    RTEMS_WAIT,
                                    RTEMS_NO_TIMEOUT);
  } while (uart_q_is_empty(&uart->queue));

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to grab read semaphore for %s, error: %s",
        uart->name, rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  /* If error is detected, clear software rx queue and return 0 byte */
  if (uart->error > 0) {
    uart->error = 0;
    uart_q_clear(&uart->queue);
    errno = EIO;

    /* For I/O errors we don't signal through means of the system call return codes
       but always return zero and set errno. */
    return 0;
  }

  while (uart_q_peek(&uart->queue, &index)) {

    /* Check if next queue item doesn't fit in user submitted buffer */
    if ((uart->rx_buf_len[index] + bytes_moved) > count) {

      /* Copy what we can to the user buffer */
      memcpy(buffer + bytes_moved,
             uart->rx_buf[index], count - bytes_moved);

      /* Decrement the buffer length with what we've consumed */
      uart->rx_buf_len[index] -= count - bytes_moved;

      /* Copy the remaining part of the queue element buffer data to the start */
      memmove(uart->rx_buf[index],
              uart->rx_buf[index] + (count - bytes_moved),
              uart->rx_buf_len[index]);

      bytes_moved = count;

      /* Release the semaphore to make sure we get to handle these straggling bytes
         for the next read iteration */
      status = rtems_semaphore_release(uart->read_semaphore);
      if (status != RTEMS_SUCCESSFUL) {
        DBG(DBG_SEVERITY_ERR, "%s, Failed to release read semaphore", uart->name);
      }
      break;
    }

    /* Advance queue */
    uart_q_get(&uart->queue, &index);

    /* Copy the UART data to reader buffer */
    memcpy(buffer + bytes_moved, uart->rx_buf[index], uart->rx_buf_len[index]);

    bytes_moved += uart->rx_buf_len[index];
  }

  return bytes_moved;
}

static ssize_t rtems_uart_write(rtems_libio_t *iop,
                                const void    *buffer,
                                size_t         count)
{
  int                   fd;
  uart_info_t           *uart;
  rtems_status_code     status;

  fd = rtems_libio_iop_to_descriptor(iop);
  uart = (uart_info_t *) IMFS_generic_get_context_by_iop(iop);

  DBG(DBG_SEVERITY_INFO, "Writing %d bytes to UART.");

  /* The calling function in libcsupport checks permissions and buffer. */

  status = rtems_semaphore_obtain(uart->write_setup_mutex,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to grab write setup mutex semaphore, error: %s",
        rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  /* Set up for write operation */
  uart->tx_bytes_moved = 0;
  uart->tx_count = count;
  uart->tx_buffer = (uint8_t *) buffer;
  uart->tx_ongoing = true;

  status = rtems_semaphore_release(uart->write_setup_mutex);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to release write setup mutex semaphore, error: %s",
        rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  /* Enable tx interrupt. The interrupt routine will handle the actual transaction */
  uart_txint_enable(uart->base);

  status = rtems_semaphore_obtain(uart->write_done_semaphore,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to grab write done semaphore, error: %s",
        rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  return uart->tx_bytes_moved;
}

static int rtems_uart_control(rtems_libio_t   *iop,
                              ioctl_command_t  command,
                              void            *buffer)
{
  int                   fd;
  uart_info_t           *uart;
  uint32_t              bitrate;
  uint32_t              val;
  uint32_t              *val_ptr;
  uint8_t               reg;
  int32_t               status;

  fd = rtems_libio_iop_to_descriptor(iop);
  uart = (uart_info_t *) IMFS_generic_get_context_by_iop(iop);

  val = (uint32_t) buffer;
  val_ptr = (uint32_t *) buffer;

  /* The calling function in libcsupport checks permissions and buffer. */

  DBG(DBG_SEVERITY_INFO, "Setting UART control options to device (%s).", uart->name);

  switch (command) {
    case UART_IOCTL_SET_BITRATE:

      switch (val) {
        case UART_B1200:
          bitrate = 1200;
          break;

        case UART_B2400:
          bitrate = 2400;
          break;

        case UART_B4800:
          bitrate = 4800;
          break;

        case UART_B9600:
          bitrate = 9600;
          break;

        case UART_B19200:
          bitrate = 19200;
          break;

        case UART_B38400:
          bitrate = 38400;
          break;

        case UART_B57600:
          bitrate = 57600;
          break;

        case UART_B76800:
          bitrate = 76800;
          break;

        case UART_B115200:
          bitrate = 115200;
          break;

        case UART_B153600:
          bitrate = 153600;
          break;

        case UART_B375000:
          bitrate = 375000;
          break;

        default:
          DBG(DBG_SEVERITY_WARN, "UART_IOCTL_SET_BITRATE: Unknown bitrate %"PRIu32
              ". Ignoring request", val);
          rtems_set_errno_and_return_minus_one(EINVAL);
          break;
      }

      uart_set_bitrate(uart->base, bitrate);
      break;

    case UART_IOCTL_MODE_SELECT:
      if (val == UART_RTEMS_MODE_LOOPBACK) {
        /* Enable loopback */
        uart_set_mcr(uart->base, UART_MCR_LOOP);
      } else if (uart->default_mode == UART_MODER_RS485) {
        if (val == UART_RTEMS_MODE_RS485) {
          /* Disable loopback */
          uart_set_mcr(uart->base, UART_RTEMS_MODE_RS485);
        } else {
          DBG(DBG_SEVERITY_WARN,
              "UART_IOCTL_MODE_SELECT: RS485 only devices does not support this mode");
          rtems_set_errno_and_return_minus_one(EINVAL);
        }
      } else {
        if (val > 1) {
          DBG(DBG_SEVERITY_INFO, "UART_IOCTL_MODE_SELECT: Value too large %"PRIu32, val);
          rtems_set_errno_and_return_minus_one(EINVAL);
        } else {
          uart_set_moder(uart->base, val);
        }
      }
      break;

    case UART_IOCTL_RX_FLUSH:
      /* Check that this user actually has read permission */
      if (fd != uart->reader_fd) {
        DBG(DBG_SEVERITY_WARN, "File descriptor not opened for reading. Aborting request.");
        rtems_set_errno_and_return_minus_one(EBADF);
      }

      DBG(DBG_SEVERITY_INFO, "UART_IOCTL_RX_FLUSH: Flushing rx queue for %s",
          uart->name);
      uart_q_clear(&uart->queue);
      break;

    case UART_IOCTL_SET_PARITY:
      DBG(DBG_SEVERITY_INFO, "UART_IOCTL_SET_PARITY: Setting parity to %"PRIu32, val);

      reg = uart_get_lcr(uart->base);

      switch (val) {
        case UART_PARITY_NONE:
          /* Disable parity */
          reg &= ~(UART_LCR_PEN);
          break;

        case UART_PARITY_ODD:
          /* Enable parity and disable even parity */
          reg |= (UART_LCR_PEN);
          reg &= ~(UART_LCR_EPS);
          break;

        case UART_PARITY_EVEN:
          /* Enable parity and enable even parity */
          reg |= (UART_LCR_PEN);
          reg |= (UART_LCR_EPS);
          break;

        default:
          DBG(DBG_SEVERITY_ERR, "UART_IOCTL_SET_PARITY: Invalid parity configuration %u",
              val);
          rtems_set_errno_and_return_minus_one(EINVAL);
          break;
      }

      uart_set_lcr(uart->base, reg);
      break;

    case UART_IOCTL_SET_BUFFER_DEPTH:
      DBG(DBG_SEVERITY_INFO,
          "UART_IOCTL_SET_BUFFER_DEPTH: Setting buffer depth to %"PRIu32, val);

      /* The RTEMS and bare-metal variables have been made sure to match */
      status = uart_set_buffer_depth(uart->base, val);
      if (status < 0) {
        DBG(DBG_SEVERITY_INFO,
            "UART_IOCTL_SET_BUFFER_DEPTH: Value too large %"PRIu32, val);
        rtems_set_errno_and_return_minus_one(-status);
      } else {
        /* Assign a real value to the variable here to improve interrupt callback time */
        switch (val) {
          case UART_BUFFER_DEPTH_128:
            uart->tx_buffer_size = 128;
            break;

          case UART_BUFFER_DEPTH_64:
            uart->tx_buffer_size = 64;
            break;

          case UART_BUFFER_DEPTH_32:
            uart->tx_buffer_size = 32;
            break;

          case UART_BUFFER_DEPTH_16:
          default:
            uart->tx_buffer_size = 16;
            break;
        }
      }
      break;

    case UART_IOCTL_GET_BUFFER_DEPTH:
      DBG(DBG_SEVERITY_INFO, "UART_IOCTL_GET_BUFFER_DEPTH: Getting buffer depth");

      /* The RTEMS and bare-metal variables have been made sure to match */
      *val_ptr = (uint32_t) uart_get_buffer_depth(uart->base);
      break;

    case UART_IOCTL_SET_TRIGGER_LEVEL:
      /* Check that this user actually has read permission */
      if (fd != uart->reader_fd) {
        DBG(DBG_SEVERITY_WARN, "File descriptor not opened for reading. Aborting request.");
        rtems_set_errno_and_return_minus_one(EBADF);
      }

      DBG(DBG_SEVERITY_INFO,
          "UART_IOCTL_SET_TRIGGER_LEVEL: Setting trigger level to %"PRIu32, val);

      /* The RTEMS and bare-metal variables have been made sure to match */
      status = uart_set_trigger_level(uart->base, val);
      if (status < 0) {
        DBG(DBG_SEVERITY_INFO, "UART_IOCTL_SET_BUFFER_DEPTH: Value too large %"PRIu32, val);
        rtems_set_errno_and_return_minus_one(-status);
      }
      break;

    case UART_IOCTL_GET_TRIGGER_LEVEL:
      /* Check that this user actually has read permission */
      if (fd != uart->reader_fd) {
        DBG(DBG_SEVERITY_WARN, "File descriptor not opened for reading. Aborting request.");
        rtems_set_errno_and_return_minus_one(EBADF);
      }

      DBG(DBG_SEVERITY_INFO, "UART_IOCTL_GET_TRIGGER_LEVEL: Getting trigger level");

      /* The RTEMS and bare-metal variables have been made sure to match */
      *val_ptr = (uint32_t) uart_get_trigger_level(uart->base);
      break;

    default:
      DBG(DBG_SEVERITY_WARN, "Unknown IOCTL command 0x%08X. Ignoring request:",
          command);
      rtems_set_errno_and_return_minus_one(EINVAL);
      break;
  }

  return 0;
}

static const rtems_filesystem_file_handlers_r rtems_uart_handlers = {
  .open_h       = rtems_uart_open,
  .close_h      = rtems_uart_close,
  .read_h       = rtems_uart_read,
  .write_h      = rtems_uart_write,
  .ioctl_h      = rtems_uart_control,
  .lseek_h      = rtems_filesystem_default_lseek_file,
  .fstat_h      = IMFS_stat,
  .ftruncate_h  = rtems_filesystem_default_ftruncate,
  .fsync_h      = rtems_filesystem_default_fsync_or_fdatasync,
  .fdatasync_h  = rtems_filesystem_default_fsync_or_fdatasync,
  .fcntl_h      = rtems_filesystem_default_fcntl,
  .readv_h      = rtems_filesystem_default_readv,
  .writev_h     = rtems_filesystem_default_writev,
};

static const IMFS_node_control rtems_uart_node_control =
  IMFS_GENERIC_INITIALIZER(
    &rtems_uart_handlers,
    IMFS_node_initialize_generic,
    IMFS_node_destroy_default);

rtems_device_driver rtems_uart_initialize(rtems_device_major_number major,
                                          rtems_device_minor_number minor,
                                          void *args)
{
  int                   rv;
  rtems_status_code     status;

  DBG(DBG_SEVERITY_INFO, "Initializing UART device");

  status = rtems_semaphore_create(rtems_build_name('U', 'A', 'R', 'W'),
                                  1,
                                  RTEMS_DEFAULT_ATTRIBUTES |
                                  RTEMS_SIMPLE_BINARY_SEMAPHORE,
                                  0,
                                  &open_mutex);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to create mutex for UART driver, error %d, %s)",
        status, rtems_status_text(status));
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  for (minor = 0; minor < AAC_BSP_UART_COUNT; minor++) {
    DBG(DBG_SEVERITY_INFO, "Registering uart driver (%lu:%lu).", major, minor);

    rv = IMFS_make_generic_node(uarts[minor].name,
                                S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO,
                                &rtems_uart_node_control,
                                &uarts[minor]);
    if (rv < 0) {
      DBG(DBG_SEVERITY_ERR, "Failed to register UART driver (M%lu:m%lu): %s)",
          major, minor, strerror(errno));
      return RTEMS_UNSATISFIED /* ENODEV */;
    }

    /* Check if this has an alias for registration as well */
    if (uarts[minor].alias[0] != '\0') {

      rv = IMFS_make_generic_node(uarts[minor].alias,
                                  S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO,
                                  &rtems_uart_node_control,
                                  (void *) &uarts[minor]);
      if (rv < 0) {
        DBG(DBG_SEVERITY_ERR, "Failed to register UART driver alias (M%lu:m%lu): %s)",
            major, minor, strerror(errno));
        /* No need to error out on alias */
      }
    }

    status = rtems_interrupt_handler_install(uarts[minor].irq,
                                             uarts[minor].name,
                                             RTEMS_INTERRUPT_UNIQUE,
                                             uart_isr,
                                             (void *) &isr_args[minor]);

    if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR, "Failed to install interrupt handler %d, %s",
          status, rtems_status_text(status));
      return RTEMS_INTERNAL_ERROR; /* EIO */
    }

    status = rtems_semaphore_create(rtems_build_name('U', 'A', 'R', '0' + minor),
                                    0,
                                    UART_MSG_SEM_ATTR,
                                    RTEMS_NO_PRIORITY_CEILING,
                                    &uarts[minor].read_semaphore);
    if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR, "Failed to create rx semaphore, error %d, %s",
          status, rtems_status_text(status));
      return RTEMS_INTERNAL_ERROR; /* EIO */
    }

    status = rtems_semaphore_create(rtems_build_name('U', 'A', 'W', '0' + minor),
                                    0,
                                    UART_MSG_SEM_ATTR,
                                    RTEMS_NO_PRIORITY_CEILING,
                                     &uarts[minor].write_done_semaphore);
    if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR, "Failed to create tx done semaphore, error %d, %s",
          status, rtems_status_text(status));
      return RTEMS_INTERNAL_ERROR; /* EIO */
    }

    status = rtems_semaphore_create(rtems_build_name('U', 'A', 'M', '0' + minor),
                                    1,
                                    UART_MSG_SEM_ATTR,
                                    RTEMS_NO_PRIORITY_CEILING,
                                    &uarts[minor].write_setup_mutex);
    if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR, "Failed to create tx setup mutex semaphore, error %d, %s",
          status, rtems_status_text(status));
      return RTEMS_INTERNAL_ERROR; /* EIO */
    }

    /* Set up default TX FIFO size as used by TX empty interrupt callback */
    uarts[minor].tx_buffer_size = 16;

    /* Clear queue */
    uarts[minor].queue.front = 0;
    uarts[minor].queue.rear = 0;
    uarts[minor].queue.items = 0;

    uarts[minor].error = 0;
    uarts[minor].users = 0;
    uarts[minor].tx_ongoing = false;

    /* Set default reader/writer file descriptors (to allow separate threads) */
    uarts[minor].reader_fd = INVALID_FD;
    uarts[minor].writer_fd = INVALID_FD;
  }

  return RTEMS_SUCCESSFUL;
}
