/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2016 ÅAC Microtec AB
 *
 * Filename:     uart.c
 * Module name:  uart
 *
 * Author(s):    PeBr, ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  UART bare metal driver implementation
 * Requirements: ÅAC UART Requirement specification, Rev. A
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <errno.h>
#include "toolchain_support.h"
#include "uart.h"

extern uint32_t _or1k_board_clk_freq;
extern uart_callbacks_t uart_callbacks;

/* The IP is a bit stupid and won't allow read-back of the FCR register, so we need
   to keep track of these values for any changes we do to the register. */
static uint8_t trigger_level;
static uint8_t buffer_depth;

void uart_init(uint32_t uart_base, uint32_t bitrate)
{
  /* Disable all interrupts */
  REG8(uart_base + UART_IER) = 0x00;

  /* Set 8 bit char, 1 stop bit, no parity */
  REG8(uart_base + UART_LCR) = UART_LCR_WLEN8 | UART_LCR_STOP_1 | UART_LCR_PDIS;

  /* Set up default FCR values */
  trigger_level = UART_FCR_TRIGGER_14;
  buffer_depth = UART_FCR_BUFFER_16;

  /* This also cleares the following interrupts:
     Receiver Line Status,
     Received Character Timeout Indication,
     Received Data Available */
  uart_set_bitrate(uart_base, bitrate);

  /* Clear sending and receiving control registers */
  REG8(uart_base + UART_RXR) = UART_RXR_DISABLE;
  REG8(uart_base + UART_TXR) = UART_TXR_DISABLE;

  /* Clear Transmitter Holding Register Empty interrupt */
  uart_get_iir(uart_base);

  /* Clear Modem Status interrupt */
  uart_get_msr(uart_base);
}

void uart_set_bitrate(uint32_t uart_base, uint32_t bitrate)
{
  uint32_t      divisor;
  float         float_divisor;

  /* Reset receiver and transmitter FIFOs */
  REG8(uart_base + UART_FCR) = (UART_FCR_ENABLE_FIFO | UART_FCR_CLEAR_RCVR |
                                UART_FCR_CLEAR_XMIT | trigger_level | buffer_depth);

  /* Set bitrate */
  float_divisor = (float) _or1k_board_clk_freq / (16 * bitrate);

  /* Ensure round up */
  float_divisor += 0.50f;
  divisor = (uint32_t) float_divisor;

  REG8(uart_base + UART_LCR) |= UART_LCR_DLAB;
  REG8(uart_base + UART_DLL) = divisor & UART_DLL_MASK;
  REG8(uart_base + UART_DLM) = (divisor >> UART_DLM_SHAMT) & UART_DLM_MASK;
  REG8(uart_base + UART_LCR) &= ~(UART_LCR_DLAB);
}

inline void uart_putc(uint32_t uart_base, uint8_t c)
{
  /* Wait for transmit FIFO empty to make sure there's free space */
  while ((REG8(uart_base + UART_LSR) & UART_LSR_THRE) != UART_LSR_THRE);

  REG8(uart_base + UART_THR) = c;
}

/* Only used when we know transmit FIFO is empty, typically in interrupt */
inline void uart_putc_nonblocking(uint32_t uart_base, uint8_t c)
{
  REG8(uart_base + UART_THR) = c;
}

inline void uart_flush(uint32_t uart_base)
{
  while ((REG8(uart_base + UART_LSR) & (UART_LSR_TEMT | UART_LSR_THRE)) !=
         (UART_LSR_TEMT | UART_LSR_THRE));
}

inline uint8_t uart_getc(uint32_t uart_base)
{
  return REG8(uart_base + UART_RBR);
}

inline uint8_t uart_check_for_char(uint32_t uart_base)
{
  return REG8(uart_base + UART_BCR);
}

inline void uart_rxint_enable(uint32_t uart_base)
{
  REG8(uart_base + UART_IER) |= UART_IER_RDI;
}

inline void uart_rxint_disable(uint32_t uart_base)
{
  REG8(uart_base + UART_IER) &= ~(UART_IER_RDI);
}

inline void uart_txint_enable(uint32_t uart_base)
{
  REG8(uart_base + UART_IER) |= UART_IER_THRI;
}

inline void uart_txint_disable(uint32_t uart_base)
{
  REG8(uart_base + UART_IER) &= ~(UART_IER_THRI);
}

inline uint8_t uart_get_iir(uint32_t uart_base)
{
  return REG8(uart_base + UART_IIR);
}

inline uint8_t uart_get_lsr(uint32_t uart_base)
{
  return REG8(uart_base + UART_LSR);
}

inline uint8_t uart_get_msr(uint32_t uart_base)
{
  return REG8(uart_base + UART_MSR);
}

inline uint8_t uart_get_lcr(uint32_t uart_base)
{
  return REG8(uart_base + UART_LCR);
}

inline void uart_clear_rx_fifo(uint32_t uart_base)
{
  REG8(uart_base + UART_FCR) = (UART_FCR_ENABLE_FIFO | UART_FCR_CLEAR_RCVR |
                                trigger_level | buffer_depth);
}

inline void uart_clear_tx_fifo(uint32_t uart_base)
{
  REG8(uart_base + UART_FCR) = (UART_FCR_ENABLE_FIFO | UART_FCR_CLEAR_XMIT |
                                trigger_level | buffer_depth);
}

inline void uart_lsint_enable(uint32_t uart_base)
{
  REG8(uart_base + UART_IER) |= UART_IER_RLSI;
}

inline void uart_lsint_disable(uint32_t uart_base)
{
  REG8(uart_base + UART_IER) &= ~(UART_IER_RLSI);
}

inline void uart_set_txr(uint32_t uart_base, uint8_t ctrl)
{
  REG8(uart_base + UART_TXR) = ctrl;
}

inline void uart_set_rxr(uint32_t uart_base, uint8_t ctrl)
{
  REG8(uart_base + UART_RXR) = ctrl;
}

inline void uart_set_moder(uint32_t uart_base, uint8_t mode)
{
  REG8(uart_base + UART_MODER) = mode;
}

inline void uart_set_mcr(uint32_t uart_base, uint8_t val)
{
  REG8(uart_base + UART_MCR) = val;
}

inline void uart_set_lcr(uint32_t uart_base, uint8_t val)
{
  REG8(uart_base + UART_LCR) = val;
}

int32_t uart_set_trigger_level(uint32_t uart_base, uint8_t val)
{
  switch (val) {
    case UART_TRIGGER_14:
      trigger_level = UART_FCR_TRIGGER_14;
      break;

    case UART_TRIGGER_8:
      trigger_level = UART_FCR_TRIGGER_8;
      break;

    case UART_TRIGGER_4:
      trigger_level = UART_FCR_TRIGGER_4;
      break;

    case UART_TRIGGER_1:
      trigger_level = UART_FCR_TRIGGER_1;
      break;

    default:
      /* Invalid value */
      return -EINVAL;
      break;
  }

  REG8(uart_base + UART_FCR) = (UART_FCR_ENABLE_FIFO | trigger_level | buffer_depth);

  return 0;
}

uint8_t uart_get_trigger_level(uint32_t uart_base)
{
  switch (trigger_level) {
    case UART_FCR_TRIGGER_14:
      return UART_TRIGGER_14;
      break;

    case UART_FCR_TRIGGER_8:
      return UART_TRIGGER_8;
      break;

    case UART_FCR_TRIGGER_4:
      return UART_TRIGGER_4;
      break;

    case UART_FCR_TRIGGER_1:
      return UART_TRIGGER_1;
      break;

    default:
      /* Something is scarily wrong, reset to standard value */
      trigger_level = UART_FCR_TRIGGER_14;
      REG8(uart_base + UART_FCR) = (UART_FCR_ENABLE_FIFO | trigger_level | buffer_depth);
      return UART_TRIGGER_14;
  }
}

int32_t uart_set_buffer_depth(uint32_t uart_base, uint8_t val)
{
  switch (val) {
    case UART_BUFFER_128:
      buffer_depth = UART_FCR_BUFFER_128;
      break;

    case UART_BUFFER_64:
      buffer_depth = UART_FCR_BUFFER_64;
      break;

    case UART_BUFFER_32:
      buffer_depth = UART_FCR_BUFFER_32;
      break;

    case UART_BUFFER_16:
      buffer_depth = UART_FCR_BUFFER_16;
      break;

    default:
      /* Invalid value */
      return -EINVAL;
      break;
  }

  REG8(uart_base + UART_FCR) = (UART_FCR_ENABLE_FIFO | trigger_level | buffer_depth);

  return 0;
}

uint8_t uart_get_buffer_depth(uint32_t uart_base)
{
  switch (buffer_depth) {
    case UART_FCR_BUFFER_128:
      return UART_BUFFER_128;
      break;

    case UART_FCR_BUFFER_64:
      return UART_BUFFER_64;
      break;

    case UART_FCR_BUFFER_32:
      return UART_BUFFER_32;
      break;

    case UART_FCR_BUFFER_16:
      return UART_BUFFER_16;
      break;

    default:
      /* Something is scarily wrong, reset to standard value */
      buffer_depth = UART_FCR_BUFFER_16;
      REG8(uart_base + UART_FCR) = (UART_FCR_ENABLE_FIFO | trigger_level | buffer_depth);
      return UART_BUFFER_16;
  }
}

AAC_INTERRUPT_HANDLER_SIGNATURE(uart_isr, data_arg)
{
  uint8_t       iir;
  uart_args_t   *args;

  args = (uart_args_t*) data_arg;

  /* Get IIR, but don't clear any interrupts. The UART is slightly different
  from other IPs. The identification register is cleared when the corresponding
  interrupt source is not in interrupt state anymore. Each callback is
  responsible for making sure that this occurs. If it needs a separate clear
  register access, it should be done before handling the interrupt. If any new
  interrupts of the same source occurs in between reading the identification
  register and handling it in the callback, the newest state will simply be
  reflected and handled by the ISR. If the interrupt occurs before clearing
  the interrupt status, the extra interrupt will not be generated. If occurring
  after clearing the interrupt status but before handling it in the callback,
  a new interrupt reflecting the exact same state will be generated. This has
  to be handled in the ISR callback. If the new interrupt occurs after the
  callback, the ISR will be executed again directly.

  If another source generates an interrupt, then a new interrupt will be
  generated and seen as the ISR will be executed again directly. */

  /* This will clear Transmitter Holding Register Empty interrupt */
  iir = uart_get_iir(args->base);

  /* Receiver Line Status */
  if ((iir & UART_IIR_RLSI) == UART_IIR_RLSI) {
    if (uart_callbacks.uart_rx_linestatus_callback != NULL) {
      /* The callback must clear the interrupt by reading the LSR
         as is done by the RTEMS driver which also checks error
         conditions */
      uart_callbacks.uart_rx_linestatus_callback(args);
    }
  }

  /* Received Character Timeout Indication */
  if ((iir & UART_IIR_TOI) == UART_IIR_TOI) {
    if (uart_callbacks.uart_timeout_callback != NULL) {
      /* Cleared in the callback by reading from the receiver FIFO */
      uart_callbacks.uart_timeout_callback(args);
    }
  }

  /* Received Data Available */
  if ((iir & UART_IIR_RDI) == UART_IIR_RDI) {
    if (uart_callbacks.uart_rx_data_callback != NULL) {
      /* Cleared in the callback by emptying the receiver FIFO */
      uart_callbacks.uart_rx_data_callback(args);
    }
  }

  /* Transmitter Holding Register Empty */
  if ((iir & UART_IIR_THRI) == UART_IIR_THRI) {
    if (uart_callbacks.uart_tx_empty_callback != NULL) {
      /* Cleared by the read of the IIR above */
      uart_callbacks.uart_tx_empty_callback(args);
    }
  }

  /* MODEM Status */
  if (iir == UART_IIR_MSI) {
    if (uart_callbacks.uart_modem_callback != NULL) {
      /* Clear interrupt by reading the MSR. The current
         RTEMS driver does not implement the callback so make
         sure it is cleared here. */
      uart_get_msr(args->base);
      uart_callbacks.uart_modem_callback(args);
    }
  }
}
