/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2018 ÅAC Microtec AB
 *
 * Filename:     uart_internal_rtems.h
 * Module name:  uart
 *
 * Author(s):    ErZa, JaVi, MaWe
 * Support:      support@aacmicrotec.com
 * Description:  UART internal RTEMS driver definitions
 * Requirements: Adheres to the ÅAC UART Driver Requirement Specification Rev. A
 *----------------------------------------------------------------------------*/

#ifndef _RTEMS_UART_INTERNAL_H_
#define _RTEMS_UART_INTERNAL_H_

#define UART_Q_SIZE             (256)
#define UART_RXFIFO_MAX_SIZE    (128)

typedef volatile struct uart_queue
{
  uint32_t              front; /* Next slot to write */
  uint32_t              rear;  /* Next slot to read  */
  uint32_t              items; /* Number of slots */
} uart_queue_t;

typedef struct uart_info {
  const char            *name;
  const char            *alias;
  const uint32_t        base;
  const uint32_t        irq;
  const uint32_t        default_mode;
  volatile uint32_t     users;

  rtems_id              write_setup_mutex;
  rtems_id              read_semaphore;
  rtems_id              write_done_semaphore;

  uart_queue_t          queue;
  uint8_t               rx_buf[UART_Q_SIZE][UART_RXFIFO_MAX_SIZE];
  volatile uint32_t     rx_buf_len[UART_Q_SIZE];

  uint8_t               error;
  size_t                tx_count;
  uint8_t               *tx_buffer;
  uint32_t              tx_buffer_size;
  uint32_t              tx_bytes_moved;
  bool                  tx_ongoing;

  int                   reader_fd;
  int                   writer_fd;
} uart_info_t;

void uart_q_put(uart_queue_t *q, uint32_t *value);
bool uart_q_get(uart_queue_t *q, uint32_t *value);
bool uart_q_next_free(uart_queue_t *q, uint32_t *value);
bool uart_q_is_full(uart_queue_t *q);
bool uart_q_is_empty(uart_queue_t *q);
bool uart_q_peek(uart_queue_t *q, uint32_t *value);
void uart_q_clear(uart_queue_t *q);
void uart_empty_fifo(uart_info_t *uart);

/* Callback functions */
void uart_rtems_rx_linestatus_callback(uart_args_t *args);
void uart_rtems_rx_data_callback(uart_args_t *args);
void uart_rtems_timeout_callback(uart_args_t *args);
void uart_rtems_tx_empty_callback(uart_args_t *args);

#endif
