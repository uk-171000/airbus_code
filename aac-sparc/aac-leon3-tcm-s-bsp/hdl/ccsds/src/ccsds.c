/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2016 ÅAC Microtec AB
 *
 * Filename:     ccsds.c
 * Module name:  ccsds
 *
 * Author(s):    PeBr, AnSi
 * Support:      support@aacmicrotec.com
 * Description:  CCSDS bare metal driver implementation
 * Requirements: ÅAC CCSDS Requirement specification
 *----------------------------------------------------------------------------*/

#include <string.h>
#include <errno.h>
#include "toolchain_support.h"
#include "ccsds.h"

/* Board definitions */
extern uint32_t _ccsds_base;

/* Callbacks */
extern ccsds_callbacks_t ccsds_callbacks;

/* Controller registers */
#define CCSDS_GENERAL_SETUP         ( 0x00 )
#define CCSDS_DMA_CONTROL           ( 0x04 )
#define CCSDS_TIME_STAMP_CONTROL    ( 0x08 )
#define CCSDS_TM_STATUS             ( 0x10 )
#define CCSDS_IP_CONFIG_1           ( 0x14 )
#define CCSDS_IP_CONFIG_2           ( 0x18 )
#define CCSDS_INTERRUPT_STATUS      ( 0x20 )
#define CCSDS_INTERRUPT_ENABLE      ( 0x24 )
#define CCSDS_TC_FRAME_CONTROL      ( 0x30 )
#define CCSDS_TC_FRAME_STATUS       ( 0x34 )
#define CCSDS_RADIO_STATUS          ( 0x38 )
#define CCSDS_CLCW                  ( 0x40 )
#define CCSDS_TC_ERROR_COUNT        ( 0x50 )
#define CCSDS_TM_ERROR_COUNT        ( 0x54 )
#define CCSDS_CPDU_STATUS           ( 0x60 )
#define CCSDS_TM_DMA_DESCRIPTOR     ( 0x400 )
#define CCSDS_TM_DMA_DESC_ADR       ( 0x404 )
#define CCSDS_TC_BUFFER             ( 0x500 )

#define BYTES_PER_WORD              ( 4 )

/* To keep track of what descriptor to use */
uint8_t tm_write_desc_no[NUMBER_OF_TM_VC];

/* To save the cpdu status after clearing the status register */
static uint32_t cpdu_status = 0;

/* To save the cpdu status after clearing the status register */
static uint32_t tc_frame_status = 0;

/* Internal function for reading cpdu register value */
uint32_t ccsds_read_cpdu_status(void);

/* Internal function for reading TC frame status register value */
uint32_t ccsds_read_tc_frame_status(void);

void ccsds_init(void)
{
  uint32_t i;

  ccsds_reset_dma_descriptors();

  /* Set up General IP Config */
  ccsds_set_ip_config(CCSDS_DEF_TM_CLK_DIVISOR |
                      CCSDS_TM_EN |
                      CCSDS_OCF_CLCW_EN |
                      CCSDS_FECF_EN |
                      CCSDS_MC_CNT_ENABLE |
                      CCSDS_IDLE_FRAME_ENABLE |
                      CCSDS_TC_DERANDOMIZER_BYPASS |
                      CCSDS_TM_CONV_BYPASS |
                      CCSDS_TM_PSEUDO_BYPASS);

  /* Set up Interrupt handler, init interrupts etc.. */
  ccsds_set_ie_config(CCSDS_DMA_FINISHED_VC0  |
                      CCSDS_DMA_FINISHED_VC1  |
                      CCSDS_CPDU |
                      CCSDS_VC0_TS |
                      CCSDS_TM_ERR |
                      CCSDS_TC_REJECTED |
                      CCSDS_TC_BUFFER_PARERR |
                      CCSDS_TC_BUFFER_OVERFLOW |
                      CCSDS_TC_RECEIVED |
                      CCSDS_TM_FRAME);

  for(i = 0; i < NUMBER_OF_TM_VC; i++)
  {
    tm_write_desc_no[i] = 0;
  }

  /* Enable DMA */
  ccsds_enable_dma_transfer();
}

AAC_INTERRUPT_HANDLER_SIGNATURE(ccsds_interrupt_handler, args)
{
  uint32_t ccsds_int_status;
  uint32_t ccsds_int_enabled;
  uint32_t ccsds_int_active;
  uint32_t tc_err_cnt;
  uint32_t tm_err_cnt;

  ccsds_int_status = ccsds_get_interrupt_status();
  ccsds_int_enabled = ccsds_get_ie_config();

  /* Only handle enabled interrupts */
  ccsds_int_active = ccsds_int_status & ccsds_int_enabled;

  /* Clear interrupts before handling them. If any new interrupts of the same
  source occurs in between reading the status register and handling it in the
  callback, the newest state will simply be reflected and handled by the ISR.
  If the interrupt occurs before clearing the status register, the extra
  interrupt will not be generated. If occurring after clearing the status
  register but before handling it in the callback, a new interrupt reflecting
  the exact same state will be generated. This has to be handled in the ISR
  callback. If the new interrupt occurs after the callback, the ISR will be
  executed again directly.

  If another source generates an interrupt, then a new interrupt will be
  generated on clear of the status register and will be seen as the ISR will
  be executed again directly. */
  ccsds_ack_interrupt(ccsds_int_enabled);

  /* Check if any DMA transaction is finished */
  if ((((ccsds_int_enabled & CCSDS_DMA_FINISHED_VC0) == CCSDS_DMA_FINISHED_VC0) &&
   ((ccsds_int_status & CCSDS_DMA_FINISHED_VC0) == CCSDS_DMA_FINISHED_VC0)) || 
  (((ccsds_int_enabled & CCSDS_DMA_FINISHED_VC1) == CCSDS_DMA_FINISHED_VC1) &&
  ((ccsds_int_status & CCSDS_DMA_FINISHED_VC1) == CCSDS_DMA_FINISHED_VC1)))
  {
    /* Call callback function if registered */
    if(NULL != ccsds_callbacks.dma_fin_callback_function)
    {
      ccsds_callbacks.dma_fin_callback_function(ccsds_int_status);
    }
  }

  /* TM Error */
  if((ccsds_int_active & CCSDS_TM_ERR) == CCSDS_TM_ERR)
  {
    /* Get the error status of the TM Path */
    tm_err_cnt = ccsds_get_tm_error_cnt();

    if(NULL != ccsds_callbacks.tm_err_callback_function)
    {
      ccsds_callbacks.tm_err_callback_function(tm_err_cnt);
    }
  }

  /* TM Frame send */
  if((ccsds_int_active & CCSDS_TM_FRAME) == CCSDS_TM_FRAME)
  {
    /* Call callback function if registered */
    if(NULL != ccsds_callbacks.tm_frame_sent_callback_function)
    {
      ccsds_callbacks.tm_frame_sent_callback_function();
    }
  } 

  /* TC Received */
  if((ccsds_int_active & CCSDS_TC_RECEIVED) == CCSDS_TC_RECEIVED)
  {

    tc_frame_status = ccsds_read_tc_frame_status();
    /* Call callback function if registered */
    if(NULL != ccsds_callbacks.tc_rec_callback_function)
    {
      ccsds_callbacks.tc_rec_callback_function(tc_frame_status);
    }
  }

  /* TC Errors */
  if((ccsds_int_active & CCSDS_TC_BUFFER_OVERFLOW) == CCSDS_TC_BUFFER_OVERFLOW)
  {
    /* Get the error counter for TC's */
    tc_err_cnt = ccsds_get_tc_error_cnt();

    /* Call callback function if registered */
    if(NULL != ccsds_callbacks.tc_buf_overflow_callback_function)
    {
      ccsds_callbacks.tc_buf_overflow_callback_function(tc_err_cnt);
    }
  }

  if((ccsds_int_active & CCSDS_TC_BUFFER_PARERR) == CCSDS_TC_BUFFER_PARERR)
  {
    /* Get the error counter for TC's */
    tc_err_cnt = ccsds_get_tc_error_cnt();
    ccsds_set_tc_frame_control(CCSDS_TC_BUFFER_RD);

    /* Call callback function if registered */
    if(NULL != ccsds_callbacks.tc_buf_par_err_callback_function)
    {
      ccsds_callbacks.tc_buf_par_err_callback_function(tc_err_cnt);
    }
  }

  if((ccsds_int_active & CCSDS_TC_REJECTED) == CCSDS_TC_REJECTED)
  {
    /* Get the error counter for TC's */
    tc_err_cnt = ccsds_get_tc_error_cnt();

    /* Reset buffers */
    ccsds_set_tc_frame_control(CCSDS_TC_BUFFER_RST | CCSDS_TC_BUFFER_RD);

    /* Call callback function if registered */
    if(NULL != ccsds_callbacks.tc_rejected_callback_function)
    {
      ccsds_callbacks.tc_rejected_callback_function(tc_err_cnt);
    }
  }

  if((ccsds_int_active & CCSDS_VC0_TS) == CCSDS_VC0_TS)
  {
    /* Call callback function if registered */
    if(NULL != ccsds_callbacks.vco_ts_callback_function)
    {
      ccsds_callbacks.vco_ts_callback_function();
    }
  }

  if((ccsds_int_active & CCSDS_CPDU) == CCSDS_CPDU)
  {
    /* Get the CPDU Status */
    cpdu_status = ccsds_read_cpdu_status();

    /* Call callback function if registered */
    if(NULL != ccsds_callbacks.cpdu_callback_function)
    {
      ccsds_callbacks.cpdu_callback_function(cpdu_status);
      /* Write to reset */
      REG32(_ccsds_base + CCSDS_CPDU_STATUS) = 0x00;
    }
  }
}

inline uint32_t ccsds_get_cpdu_status(void)
{
  return cpdu_status;
}

inline uint32_t ccsds_get_tc_frame_status(void)
{
  return tc_frame_status;
}

inline uint32_t ccsds_read_cpdu_status(void)
{
  return REG32(_ccsds_base + CCSDS_CPDU_STATUS);
}

inline void ccsds_set_ip_config(const uint32_t configuration)
{
  REG32(_ccsds_base + CCSDS_GENERAL_SETUP) = configuration;
}

inline uint32_t ccsds_get_ip_config(void)
{
  return REG32(_ccsds_base + CCSDS_GENERAL_SETUP);
}

inline void ccsds_set_dma_control(const uint32_t control)
{
  REG32(_ccsds_base + CCSDS_DMA_CONTROL) = control;
}

inline uint32_t ccsds_get_dma_control(void)
{
  return REG32(_ccsds_base + CCSDS_DMA_CONTROL);
}

inline void ccsds_set_timestamp_control(const uint32_t timestamp_ctrl)
{
  REG32(_ccsds_base + CCSDS_TIME_STAMP_CONTROL) = timestamp_ctrl;
}

inline uint32_t ccsds_get_timestamp_control(void)
{
  return REG32(_ccsds_base + CCSDS_TIME_STAMP_CONTROL);
}

inline void ccsds_set_ie_config(const uint32_t configuration)
{
  REG32(_ccsds_base + CCSDS_INTERRUPT_ENABLE) = configuration;
}

inline uint32_t ccsds_get_ie_config(void)
{
	return REG32(_ccsds_base + CCSDS_INTERRUPT_ENABLE);
}

inline void ccsds_set_dma_desc(const uint32_t vc_no,
                              const uint32_t desc_no,
                              const uint32_t descriptor)
{
  REG32(_ccsds_base + CCSDS_TM_DMA_DESCRIPTOR + (TM_VC_DESC_OFFSET * vc_no) + \
        (TM_DESC_NO_OFFSET * desc_no)) = descriptor;
}

inline uint32_t ccsds_get_dma_desc(const uint32_t vc_no, const uint32_t desc_no)
{
  return REG32(_ccsds_base + CCSDS_TM_DMA_DESCRIPTOR + (TM_VC_DESC_OFFSET * vc_no) + \
              (TM_DESC_NO_OFFSET * desc_no));
}

void ccsds_set_dma_desc_adr(const uint32_t vc_no,
                            const uint32_t desc_no,
                            const uint32_t desc_address)
{
  REG32(_ccsds_base + CCSDS_TM_DMA_DESC_ADR + (TM_VC_DESC_OFFSET * vc_no) + \
        (TM_DESC_NO_OFFSET * desc_no)) = desc_address;
}

inline uint32_t ccsds_get_dma_desc_adr(const uint32_t vc_no,
                                      const uint32_t desc_no)
{
  return REG32(_ccsds_base + CCSDS_TM_DMA_DESC_ADR + (TM_VC_DESC_OFFSET * vc_no) + \
              (TM_DESC_NO_OFFSET * desc_no));
}

inline uint32_t ccsds_get_interrupt_status(void)
{
  return REG32(_ccsds_base + CCSDS_INTERRUPT_STATUS);
}

inline void ccsds_ack_interrupt(const uint32_t interrupts)
{
  REG32(_ccsds_base + CCSDS_INTERRUPT_STATUS) = interrupts;
}

inline uint32_t ccsds_get_tm_status(void)
{
  return REG32(_ccsds_base + CCSDS_TM_STATUS);
}

inline uint32_t ccsds_get_ip_configs1(void)
{
  return REG32(_ccsds_base + CCSDS_IP_CONFIG_1);
}

inline uint32_t ccsds_get_ip_configs2(void)
{
  return REG32(_ccsds_base + CCSDS_IP_CONFIG_2);
}

inline uint32_t ccsds_get_tm_error_cnt(void)
{
  return REG32(_ccsds_base + CCSDS_TM_ERROR_COUNT);
}

inline uint32_t ccsds_get_tc_error_cnt(void)
{
  return REG32(_ccsds_base + CCSDS_TC_ERROR_COUNT);
}

inline void ccsds_set_tc_frame_control(const uint32_t tc_frame_control)
{
  REG32(_ccsds_base + CCSDS_TC_FRAME_CONTROL) = tc_frame_control;
}

void ccsds_read_tc_data_buffer(uint32_t *buffer, const uint16_t length)
{
  uint16_t i;
  uint32_t no_of_reads;

  /* Since 32-bit accesses, divide by 4 to get no if bytes to read */
  no_of_reads = length / BYTES_PER_WORD;

  /* Extra read needed ?? */
  if(length % BYTES_PER_WORD != 0)
  {
    no_of_reads++;
  }

  /* Read from TC FIFO */
  for(i = 0; i < no_of_reads; i++)
  {
    *buffer = REG32(_ccsds_base + CCSDS_TC_BUFFER + BYTES_PER_WORD * i);
    buffer++;
  }

  /* Set TC_BUFFER_RD when data from FIFO has been read */
  REG32(_ccsds_base + CCSDS_TC_FRAME_CONTROL) = CCSDS_TC_BUFFER_RD;
}

inline void ccsds_set_clcw(const uint32_t clcw)
{
  REG32(_ccsds_base + CCSDS_CLCW) = clcw;
}

inline uint32_t ccsds_get_clcw(void)
{
  return REG32(_ccsds_base + CCSDS_CLCW);
}

void ccsds_reset_dma_descriptors(void)
{
  uint8_t i;
  uint8_t j;

  for(i = 0; i < NUMBER_OF_TM_VC; i++)
  {
    for(j = 0; j < NUMBER_OF_TM_DMA_DESC_PER_VC; j++)
    {
      ccsds_set_dma_desc_adr(i, j, 0x00000000);
      ccsds_set_dma_desc(i, j, 0x00000000);
    }
  }
}

inline void ccsds_enable_dma_transfer(void)
{
  /* Set NO RESET, TM_DMA_EN = 0x01 */
  REG32(_ccsds_base + CCSDS_DMA_CONTROL) |= CCSDS_TM_DMA_EN;
}

inline void ccsds_disable_dma_transfer(void)
{
  /* Set NO RESET, TM_DMA_EN = 0x00 */
  REG32(_ccsds_base + CCSDS_DMA_CONTROL) &= ~CCSDS_TM_DMA_EN;
}

inline void ccsds_enable_tm(void)
{
  uint32_t ip_configuration;

  ip_configuration = ccsds_get_ip_config();
  ip_configuration |= CCSDS_TM_EN;
  ccsds_set_ip_config(ip_configuration);
}

inline void ccsds_disable_tm(void)
{
  uint32_t ip_configuration;

  ip_configuration = ccsds_get_ip_config();
  /* Clear TM_EN bit */
  ip_configuration &= ~CCSDS_TM_EN;
  ccsds_set_ip_config(ip_configuration);
}

int32_t ccsds_write_tm(const uint32_t vc_no, uint8_t *tm_data, uint32_t count)
{
  uint32_t descriptor_conf;
  int32_t used_desc_no;

  /* Check that TM is enabled. If not, the TM will not be sent*/
  if ((ccsds_get_ip_config() & CCSDS_TM_EN) != CCSDS_TM_EN)
  {
    return -EACCES;
  }

  /* Check that descriptor is available */
  descriptor_conf = ccsds_get_dma_desc(vc_no, tm_write_desc_no[vc_no]);
  if(descriptor_conf & CCSDS_TM_PRESENT)
  {
    return -EAGAIN;
  }

  /* Check length argument */
  if((count == 0) || (count > TM_DESC_MAX_LENGTH))
  {
    return -EINVAL;
  }

  /* Set address of DMA-descriptor to frame to be sent */
  ccsds_set_dma_desc_adr(vc_no, tm_write_desc_no[vc_no], (uint32_t ) tm_data);


  /* Set TM_PRESENT */
  if(tm_write_desc_no[vc_no] == (NUMBER_OF_TM_DMA_DESC_PER_VC - 1))
  {
    descriptor_conf = CCSDS_TM_PRESENT | CCSDS_IRQ_EN | CCSDS_WRAP | count;
  }
  else
  {
    descriptor_conf = CCSDS_TM_PRESENT | CCSDS_IRQ_EN | count;
  }

  ccsds_set_dma_desc(vc_no, tm_write_desc_no[vc_no], descriptor_conf);

  used_desc_no = tm_write_desc_no[vc_no];

  if(tm_write_desc_no[vc_no] >= (NUMBER_OF_TM_DMA_DESC_PER_VC - 1))
  {
    tm_write_desc_no[vc_no] = 0;
  }
  else
  {
    tm_write_desc_no[vc_no] = tm_write_desc_no[vc_no] + 1;
  }

  return used_desc_no;
}

inline uint32_t ccsds_read_tc_frame_status(void)
{
  return REG32(_ccsds_base + CCSDS_TC_FRAME_STATUS);
}

inline uint32_t ccsds_get_next_tc_length(void)
{
  return ccsds_get_tc_frame_status() & CCSDS_TC_BUFFER_COUNT_MASK;
}

inline uint32_t ccsds_get_radio_status(void)
{
  return REG32(_ccsds_base + CCSDS_RADIO_STATUS);
}
