/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2016 ÅAC Microtec AB
 *
 * Filename:     ccsds_internal_rtems.h
 * Module name:  ccsds
 *
 * Author(s):    PeBr
 * Support:      support@aacmicrotec.com
 * Description:  CCSDS bare metal driver implementation
 * Requirements: ÅAC CCSDS Requirement specification
 *----------------------------------------------------------------------------*/


#ifndef _CCSDS_INTERNAL_RTEMS_H_
#define _CCSDS_INTERNAL_RTEMS_H_
#include "ccsds_defs.h"

#define BYTES_PER_WORD            ( 4 )
#define TC_FRAME_MAX_BYTES        ( 1024 )
#define TC_FRAME_MAX_SIZE_WORDS   ( TC_FRAME_MAX_BYTES / BYTES_PER_WORD )
#define MAX_NO_OF_TC_FRAMES       ( 10 )
#define TC_BUFFER_ITEM_HDR_SIZE   ( 1 )
#define TC_BUFFER_SIZE            (MAX_NO_OF_TC_FRAMES * (TC_FRAME_MAX_SIZE_WORDS + \
                                  TC_BUFFER_ITEM_HDR_SIZE))

/* Callback functions */
void cpdu_callback_function(const uint32_t cpdu_status);
void vco_ts_callback_function(void);
void dma_fin_callback_function(const uint32_t interrupt_status);
void tm_err_callback_function(const uint32_t tm_err_status);
void tc_rejected_callback_function(const uint32_t tc_err_status);
void tc_buf_par_err_callback_function(const uint32_t tc_err_status);
void tc_buf_overflow_callback_function(const uint32_t tc_err_status);
void tc_rec_callback_function(const uint32_t tc_length);
#endif
