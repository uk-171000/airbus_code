/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2016 ÅAC Microtec AB
 *
 * Filename:     ccsds_defs.h
 * Module name:  ccsds
 *
 * Author(s):    PeBr, AnSi
 * Support:      support@aacmicrotec.com
 * Description:  CCSDS definitions
 * Requirements: ÅAC CCSDS Requirement specification
 *----------------------------------------------------------------------------*/

#ifndef _CCSDS_DEFS_H_
#define _CCSDS_DEFS_H_

/* Descriptor defs */
#define NUMBER_OF_TM_DMA_DESC_PER_VC  ( 4 ) // support for max 4
#define NUMBER_OF_TM_VC               ( 2 ) // support for max 7 (0-6)
#define TM_DESC_MAX_LENGTH            ( 0x1FFFF )
#define TM_VC_DESC_OFFSET             ( 0x20 )
#define TM_DESC_NO_OFFSET             ( 0x08 )

/* VC definitions */
#define VC0                           ( 0 )
#define VC1                           ( 1 )
#define VC2                           ( 2 )
#define VC3                           ( 3 )
#define VC4                           ( 4 )
#define VC5                           ( 5 )
#define VC6                           ( 6 )
#define VC7                           ( 7 )

/* TC Frame Definitions */
#define TC_FRAME_MAX_SIZE             ( 1024 )
#define TC_HEADER_SIZE                ( 5 )
#define TC_CRC_SIZE                   ( 2 )
#define TC_DATA_FIELD_SIZE            ( TC_FRAME_MAX_SIZE - TC_HEADER_SIZE - \
                                      TC_CRC_SIZE )

/* TM Frame Definitions */
#define TM_FRAME_SIZE                 ( 1115 )
#define TM_PRIM_HDR_SIZE              ( 6 )
#define TM_CLCW_SIZE                  ( 4 )
#define TM_FECF_SIZE                  ( 2)
#define TM_DATA_FIELD_SIZE            ( TM_FRAME_SIZE - TM_PRIM_HDR_SIZE - \
                                      TM_CLCW_SIZE - TM_FECF_SIZE )

#endif
