/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2016 ÅAC Microtec AB
 *
 * Filename:     ccsds_rtems.c
 * Module name:  ccsds
 *
 * Author(s):    PeBr, AnSi
 * Support:      support@aacmicrotec.com
 * Description:  CCSDS RTEMS Driver Implementation
 * Requirements: ÅAC CCSDS Requirement specification
 *----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <rtems/libio.h>
#include <rtems/rtems/tasks.h>
#include <rtems/seterr.h>
#include <bsp/irq.h>
#include <bsp.h>
#include <errno.h>
#include "toolchain_support.h"
#include "ccsds_rtems.h"
#include "ccsds_internal_rtems.h"
#include "ccsds.h"

/* Board definitions */
uint32_t _ccsds_base = AAC_BSP_CCSDS_BASE;

#define CCSDS_TC_SEM_ATTR         RTEMS_DEFAULT_ATTRIBUTES
#define CCSDS_TM_SEM_ATTR         (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_SIMPLE_BINARY_SEMAPHORE)

#define CCSDS_DMA_MSG_Q_NAME      (rtems_build_name('C', 'C', 'S', 'Q'))
#define CCSDS_MSG_Q_COUNT         ( 16 )
#define CCSDS_MSG_Q_SIZE          sizeof( dma_transfer_cb_t )
#define CCSDS_MSG_Q_ATTR          RTEMS_DEFAULT_ATTRIBUTES

/* Device related defines */
#define NO_OF_MINOR_DEVS          ( 11 )
#define CCSDS_MINOR               ( 0 )
#define CCSDS_MINOR_TM            ( 1 )
#define CCSDS_MINOR_TC            ( 2 )
#define CCSDS_MINOR_TM_VC0        ( 3 )
#define CCSDS_MINOR_TM_VC1        ( 4 )
#define CCSDS_MINOR_TM_VC2        ( 5 )
#define CCSDS_MINOR_TM_VC3        ( 6 )
#define CCSDS_MINOR_TM_VC4        ( 7 )
#define CCSDS_MINOR_TM_VC5        ( 8 )
#define CCSDS_MINOR_TM_VC6        ( 9 )
#define CCSDS_MINOR_TC_VC0        ( 10 )

#define TM_DEVICE_NAME_LENGTH     ( 13 )
#define CCSDS_NAME_TM_PREFIX      "/dev/ccsds-tm"

/* Note: The quite long timeout (1.5 secs) is applicable
when the TM Bitrate is very low */
#define TIMEOUT_MICROSEC          ( 1500000 )
#define DEV_NOT_OPENED            ( 0 )
#define DEV_OPENED                ( 1 )

#define VC_OK                     ( 0 )
#define VC_OUT_OF_RANGE           ( -1 )

typedef struct
{
  uint16_t fd_mode;
  uint16_t dev_opened;
} dev_info_t;

/* Semaphore for TC received indication */
rtems_id ccsds_rx_tc_sem_id;

/* Semaphores for TM write in blocking mode, indicates DMA transfer done */
rtems_id ccsds_tx_tm_id[NUMBER_OF_TM_VC];

/* Tracks descriptor index of last started DMA transfer.
 *
 * This is not volatile since it is only updated in a critical section in the write() call
 * (ensuring atomicity and visibility) and only read in the interrupt context (hence
 * atomicity and visibility in between interrupts are not required).
 *
 * Proper synchronisation between multi-threaded write() calls are not provided, hence
 * currently the application layer must ensure that multi-threaded writes to the same virtual
 * channel are mutexed.
 */
static uint32_t tm_dma_desc_write_index[NUMBER_OF_TM_VC];

/* Indicates if TM DMA transfer is ongoing, in order to distinguish between all-ongoing and
 * all-finished when descriptor read and write indexes are equal
 *
 * This is volatile in order to ensure visibility between interrupts clearing and checking
 * the flag. Atomicity from the write() call is ensured by disabling interrupts, and in
 * interrupt context by nested interrupts not being used.
 *
 * Proper synchronisation between multi-threaded write() calls are not provided, hence
 * currently the application layer must ensure that multi-threaded writes to the same virtual
 * channel are mutexed.
 */
static volatile bool tm_dma_ongoing[NUMBER_OF_TM_VC];

/* Message queue for DMA transfers */
rtems_id ccsds_dma_rx_msg_q_id;

/* Holds info about device */
static dev_info_t device_info[NO_OF_MINOR_DEVS];

/* Holds info about length of last rec TC */
static uint32_t length_TC;

/*
 * CCSDS callback functions
 */
ccsds_callbacks_t ccsds_callbacks =
{
  .cpdu_callback_function = cpdu_callback_function,
  .vco_ts_callback_function = vco_ts_callback_function,
  .dma_fin_callback_function = dma_fin_callback_function,
  .tm_err_callback_function = tm_err_callback_function,
  .tc_rejected_callback_function = tc_rejected_callback_function,
  .tc_buf_par_err_callback_function = tc_buf_par_err_callback_function,
  .tc_buf_overflow_callback_function = tc_buf_overflow_callback_function,
  .tc_rec_callback_function = tc_rec_callback_function
};

/* Internal functions */
static void prepare_dma_finished_msg(uint32_t vc,
                                     uint32_t descriptor_index,
                                     uint32_t descriptor_register,
                                     dma_transfer_cb_t *dma_msg_finished)
{
  /* Populate the message with descriptor info */
  dma_msg_finished->vc = vc;
  dma_msg_finished->adress = ccsds_get_dma_desc_adr(vc, descriptor_index);
  dma_msg_finished->length = descriptor_register & CCSDS_DESC_LENGTH_MASK;

  /* TODO: Read status from IP when implemented in IP.... */
  dma_msg_finished->status = SEND_FINISHED;
}

void vco_ts_callback_function(void)
{
  DBG(DBG_SEVERITY_INFO, "vco_ts_callback_function called");
}

void dma_fin_callback_function(const uint32_t interrupt_status)
{
  uint32_t vc;
  int32_t i;
  dma_transfer_cb_t dma_finished_msg;
  uint32_t descriptor_register;
  uint32_t handled_descriptor_index;
  rtems_status_code rtems_status;

  /* Here volatile should be sufficient for ensuring sequential non-concurrent calls
   * (assuming no recursive interrupts) to this function fully synchronises the read index.
   */
  static volatile uint32_t dma_desc_read_index[NUMBER_OF_TM_VC];

  DBG(DBG_SEVERITY_INFO, "dma_fin_callback_function called");

  /* Both the interrupt status and the tm status are ignored, since they cannot provide
   * atomicity. Read and write index plus ongoing flag is used instead to determine which VC
   * and descriptor finished.
   */
  for(vc = 0; vc < NUMBER_OF_TM_VC; vc++)
  {
    if(!tm_dma_ongoing[vc])
    {
      continue;
    }

    for(i = 0; i < NUMBER_OF_TM_DMA_DESC_PER_VC; ++i)
    {
      descriptor_register = ccsds_get_dma_desc(vc, dma_desc_read_index[vc]);

      if(descriptor_register & CCSDS_TM_PRESENT)
      {
        /* DMA transfer still ongoing. Since descriptors cannot finish out of order, skip
         * checking any more descriptors and move to next VC.
         */
        break;
      }

      if(!(device_info[CCSDS_MINOR_TM_VC0 + vc].fd_mode & LIBIO_FLAGS_NO_DELAY))
      {
        /* blocking */
        rtems_semaphore_release(ccsds_tx_tm_id[vc]);
      }
      else
      {
        /* Non-blocking, send DMA finished message to application layer. */
        prepare_dma_finished_msg(vc,
                                 dma_desc_read_index[vc],
                                 descriptor_register,
                                 &dma_finished_msg);
        rtems_status = rtems_message_queue_send(ccsds_dma_rx_msg_q_id,
                                                &dma_finished_msg,
                                                sizeof(dma_finished_msg));
        if(rtems_status != RTEMS_SUCCESSFUL)
        {
          DBG(DBG_SEVERITY_ERR,
              "Failed to send message to DMA rec Queue: %s",
              rtems_status_text(rtems_status));
        }
      }

      handled_descriptor_index = dma_desc_read_index[vc];

      dma_desc_read_index[vc] =
        (dma_desc_read_index[vc] + 1) % NUMBER_OF_TM_DMA_DESC_PER_VC;

      if(handled_descriptor_index == tm_dma_desc_write_index[vc])
      {
        tm_dma_ongoing[vc] = false;
        break;
      }
    }
  }
}

void tm_err_callback_function(const uint32_t tm_err_status)
{
  DBG(DBG_SEVERITY_INFO, "tm_err_callback_function called");
}

void tc_rejected_callback_function(const uint32_t tc_err_cnt)
{
  DBG(DBG_SEVERITY_INFO, "tc_rejected_callback_function called");
}

void tc_buf_par_err_callback_function(const uint32_t tc_err_cnt)
{
  DBG(DBG_SEVERITY_INFO, "tc_buf_par_err_callback_function called");
}

void tc_buf_overflow_callback_function(const uint32_t tc_err_cnt)
{
  DBG(DBG_SEVERITY_INFO, "tc_buf_overflow_callback_function called");
}

void tc_rec_callback_function(const uint32_t tc_frame_status)
{
  DBG(DBG_SEVERITY_INFO, "tc_rec_callback_function called");

  DBG(DBG_SEVERITY_INFO, "TC Frame Status: %d", tc_frame_status);
  /* A TC has been received. Release the semaphore for the TC Rx task */
  rtems_semaphore_release(ccsds_rx_tc_sem_id);
}

void cpdu_callback_function(const uint32_t cpdu_status)
{
  uint8_t cpdu_counter;
  uint16_t cpdu_line;

  cpdu_counter = cpdu_status & 0x0F;
  cpdu_line = (cpdu_status & 0xFFF0) >> 4;

  DBG(DBG_SEVERITY_INFO, "cpdu_callback_function called");
  DBG(DBG_SEVERITY_INFO, "CPDU counter: %d", cpdu_counter);
  DBG(DBG_SEVERITY_INFO, "CPDU Line: %d", cpdu_line);
}

rtems_device_driver ccsds_initialize(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *pargp __attribute__((unused)))
{
  uint16_t i;
  rtems_device_driver status;
  /* Additional for 1 digit nr and '\0' */
  char dev_name[TM_DEVICE_NAME_LENGTH + 2];

  DBG(DBG_SEVERITY_INFO,"Registering CCSDS driver (M%lu:m%lu).", major, minor);

  /* Register device names */
  status = rtems_io_register_name(CCSDS_NAME, major, CCSDS_MINOR);
  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR,
        "Failed to register CCSDS driver for Major %d (%d)", major, status);
    rtems_io_unregister_driver(major);
    return RTEMS_UNSATISFIED;
  }

  status = rtems_io_register_name(CCSDS_NAME_TM, major, CCSDS_MINOR_TM);
  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR,
        "Failed to register CCSDS driver for Major %d (%d)", major, status);
    rtems_io_unregister_driver(major);
    return RTEMS_UNSATISFIED;
  }

  status = rtems_io_register_name(CCSDS_NAME_TC, major, CCSDS_MINOR_TC);
  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR,
        "Failed to register CCSDS driver for Major %d (%d)", major, status);
    rtems_io_unregister_driver(major);
    return RTEMS_UNSATISFIED;
  }

  for(i = 0; i < NUMBER_OF_TM_VC; i++)
  {
    sprintf(dev_name, "%s%d", CCSDS_NAME_TM_PREFIX, (int) i);
    DBG(DBG_SEVERITY_INFO,"Registering device: %s", dev_name);
    status = rtems_io_register_name(dev_name, major, CCSDS_MINOR_TM_VC0 + i);
    if(status != RTEMS_SUCCESSFUL)
    {
      DBG(DBG_SEVERITY_ERR,
          "Failed to register CCSDS driver for Major %d (%d)", major, status);
      rtems_io_unregister_driver(major);
      return RTEMS_UNSATISFIED;
    }
  }

  status = rtems_io_register_name(CCSDS_NAME_TC_VC0, major, CCSDS_MINOR_TC_VC0);
  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR,
        "Failed to register CCSDS driver for Major %d (%d)", major, status);
    rtems_io_unregister_driver(major);
    return RTEMS_UNSATISFIED;
  }

  /* Install interrupt handler */
  status = rtems_interrupt_handler_install(AAC_BSP_CCSDS_IRQ,
                                          "ccsds",
                                          RTEMS_INTERRUPT_UNIQUE,
                                          ccsds_interrupt_handler,
                                          NULL);
  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR,"Failed to install CCSDS interrupt handler");
  }

  /* Semaphore for signalling a new TC is received */
  status = rtems_semaphore_create(rtems_build_name('T', 'C', 'S', '1'),
                                  0,
                                  CCSDS_TC_SEM_ATTR,
                                  RTEMS_NO_PRIORITY_CEILING,
                                  &ccsds_rx_tc_sem_id);
  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to create semaphore TCS1. Status: %d", status);
    DBG(DBG_SEVERITY_INFO, "Unregistering CCSDS driver (M%lu:m%lu).", major, minor);
    rtems_io_unregister_driver(major);
    return RTEMS_UNSATISFIED;
  }

  for(i = 0; i < NUMBER_OF_TM_VC; i++)
  {
    /* Semaphore for synch of transfers between SDRAM and application*/
    status = rtems_semaphore_create(rtems_build_name('T', 'M', 'S', i),
                                    0,
                                    CCSDS_TM_SEM_ATTR,
                                    RTEMS_NO_PRIORITY_CEILING,
                                    &ccsds_tx_tm_id[i]);
    if(status != RTEMS_SUCCESSFUL)
    {
      DBG(DBG_SEVERITY_ERR, "Failed to create semaphore TMS%d. Status: %d",i ,status);
      DBG(DBG_SEVERITY_INFO, "Unregistering CCSDS driver (M%lu:m%lu).", major, minor);
      rtems_semaphore_delete(ccsds_rx_tc_sem_id);
      rtems_io_unregister_driver(major);
      return RTEMS_UNSATISFIED;
    }

  }

  /* Message Q for DMA transfer messages */
  status = rtems_message_queue_create(CCSDS_DMA_MSG_Q_NAME,
                                      CCSDS_MSG_Q_COUNT,
                                      CCSDS_MSG_Q_SIZE,
                                      CCSDS_MSG_Q_ATTR,
                                      &ccsds_dma_rx_msg_q_id);

  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to create DMA message RX Queue. (%d. %s)", status,
      rtems_status_text(status));
    rtems_semaphore_delete(ccsds_rx_tc_sem_id);

    for(i = 0; i < NUMBER_OF_TM_VC; i++)
    {
      rtems_semaphore_delete(ccsds_tx_tm_id[i]);
    }
    return RTEMS_UNSATISFIED;
  }

  /* Initialize device info */
  for(i = 0; i < NO_OF_MINOR_DEVS; i++)
  {
    device_info[i].fd_mode = 0;
    device_info[i].dev_opened = DEV_NOT_OPENED;
  }

  ccsds_init();
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver ccsds_open(rtems_device_major_number major,
                              rtems_device_minor_number minor,
                              void *pargp)
{
  rtems_libio_open_close_args_t *arg;

  arg = (rtems_libio_open_close_args_t *) pargp;

  if(device_info[minor].dev_opened == DEV_OPENED)
  {
    DBG(DBG_SEVERITY_WARN,
        "CCSDS-device driver (M%lu:m%lu) already opened. Aborting request.",
        major,
        minor);
    return RTEMS_RESOURCE_IN_USE;
  }

  if((arg->mode & LIBIO_FLAGS_NO_DELAY) == LIBIO_FLAGS_NO_DELAY)
  {
    DBG(DBG_SEVERITY_INFO, "CCSDS-device (M%lu:m%lu) opened in non-blocking mode",
      major, minor);
    device_info[minor].fd_mode = LIBIO_FLAGS_NO_DELAY;
    device_info[minor].dev_opened = DEV_OPENED;
  }
  else
  {
    DBG(DBG_SEVERITY_INFO, "CCSDS-device (M%lu:m%lu) opened in blocking mode",
      major, minor);
    device_info[minor].fd_mode = 0;
    device_info[minor].dev_opened = DEV_OPENED;
  }
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver ccsds_close(rtems_device_major_number major,
                                rtems_device_minor_number minor,
                                void *pargp __attribute__((unused)))
{
  device_info[minor].fd_mode = 0;
  device_info[minor].dev_opened = DEV_NOT_OPENED;
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver ccsds_read(rtems_device_major_number major,
                              rtems_device_minor_number minor,
                              void *pargp)
{
  rtems_libio_rw_args_t *arg;

  arg = (rtems_libio_rw_args_t *) pargp;
  DBG(DBG_SEVERITY_INFO,"\r\nRead called\r\n");

  if(minor == CCSDS_MINOR_TC_VC0)
  {
    /* Wait and block until a new TC is available */
    rtems_semaphore_obtain(ccsds_rx_tc_sem_id, RTEMS_WAIT, RTEMS_NO_TIMEOUT);

    arg->bytes_moved = ccsds_get_next_tc_length();

    /* Read TC from internal FIFO on CCSDS IP */
    ccsds_read_tc_data_buffer((uint32_t *) arg->buffer, arg->bytes_moved);
  }
  else
  {
    /* Read of device not supported */
    arg->bytes_moved = 0;
    return RTEMS_NOT_DEFINED;
  }

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver ccsds_write(rtems_device_major_number major,
                                rtems_device_minor_number minor,
                                void *pargp)
{
  rtems_status_code rtems_status;
  int32_t result;
  rtems_libio_rw_args_t *arg;
  rtems_interrupt_level level;

  arg = (rtems_libio_rw_args_t *) pargp;

  DBG(DBG_SEVERITY_INFO,"\r\nWrite called\r\n");
  if((minor >= CCSDS_MINOR_TM_VC0) && ((minor <= CCSDS_MINOR_TM_VC6)))
  {
    /* Interrupt disabled in order to ensure DMA descriptor write index is updated before
     * interrupt can trigger.
     */
    rtems_interrupt_disable(level);
    result = ccsds_write_tm((minor - CCSDS_MINOR_TM_VC0),
        (uint8_t *) arg->buffer,
        (uint32_t ) arg->count);
    switch(result)
    {
      case -EAGAIN:
        /* No available descriptor for transfer */
        rtems_interrupt_enable(level);
        arg->bytes_moved = 0;
        return RTEMS_IO_ERROR;

      case -EINVAL:
        /* Argument out of bound */
        rtems_interrupt_enable(level);
        arg->bytes_moved = 0;
        return RTEMS_INVALID_NAME;

      case -EACCES:
        /* TM not enabled */
        rtems_interrupt_enable(level);
        arg->bytes_moved = 0;
        return RTEMS_NOT_CONFIGURED;

      case 0:
      case 1:
      case 2:
      case 3:
        tm_dma_ongoing[minor - CCSDS_MINOR_TM_VC0] = true;
        tm_dma_desc_write_index[minor - CCSDS_MINOR_TM_VC0] = result;
        rtems_interrupt_enable(level);
        arg->bytes_moved = arg->count;
        break;

      default:
        /* Unexpected return value */
        rtems_interrupt_enable(level);
        arg->bytes_moved = 0;
        return RTEMS_INVALID_NAME;
    }

    /* Handle writes differently depending on blocking or non-blocking */
    if(!(device_info[minor].fd_mode & LIBIO_FLAGS_NO_DELAY))
    {
      /* Blocking mode, wait until TM has been sent */
      rtems_status = rtems_semaphore_obtain(ccsds_tx_tm_id[minor - CCSDS_MINOR_TM_VC0],
                                            RTEMS_WAIT,
                                            TIMEOUT_MICROSEC);
      if(rtems_status == RTEMS_TIMEOUT)
      {
        arg->bytes_moved = 0;
        return RTEMS_TIMEOUT;
      }

      if(result == RTEMS_SUCCESSFUL)
      {
        arg->bytes_moved = arg->count;
        return RTEMS_SUCCESSFUL;
      }
    }
  }
  else
  {
    /* Write of device not supported */
    arg->bytes_moved = 0;
    return RTEMS_NOT_DEFINED;
  }
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver ccsds_control(rtems_device_major_number major,
                                  rtems_device_minor_number minor,
                                  void *parg)
{
  uint32_t temp_val;
  uint32_t read_val;
  uint8_t tm_was_disabled;

  tm_config_t *tm_config;
  tc_config_t *tc_config;
  tc_status_t *tc_status;
  tm_status_t *tm_status;
  tc_error_cnt_t *tc_err_counter;
  tm_error_cnt_t *tm_err_counter;
  radio_status_t *radio_status;
  rtems_libio_ioctl_args_t *arg;

  arg = (rtems_libio_ioctl_args_t *) parg;

  /* Handle IOCTLS for general CCSDS device*/
  if(minor == CCSDS_MINOR)
  {
    switch(arg->command)
    {
      case CCSDS_INIT:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_INIT\r\n");
        ccsds_init();

        arg->ioctl_return = 0;
        break;

      case CCSDS_GET_RADIO_STATUS:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_GET_RADIO_STATUS\r\n");
        read_val = ccsds_get_radio_status();
        /* TODO: Implement when present in SoC */
        radio_status = (radio_status_t *) arg->buffer;
        radio_status->tc_sub_carrier = 0x00;
        radio_status->tc_carrier = 0x00;

        arg->ioctl_return = 0;
        break;

      default:
        arg->ioctl_return = -EIO;
        break;
    }
  }

  /* Handle IOCTLS for device CCSDS TM device */
  if(minor == CCSDS_MINOR_TM)
  {
    switch(arg->command)
    {
      case CCSDS_SET_TM_CONFIG:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_SET_TM_CONFIG\r\n");
        tm_config = (tm_config_t *) arg->buffer;
        temp_val = 0;

        /* Read and modify */
        read_val = ccsds_get_ip_config();
        temp_val = read_val & (~CCSDS_CLK_DIVISOR_MASK);
        temp_val |= ((tm_config->clk_divisor << CCSDS_CLK_DIV_BIT_POS)
          & CCSDS_CLK_DIVISOR_MASK);

        temp_val = temp_val & (~CCSDS_TM_EN);
        temp_val |= ((tm_config->tm_enabled << CCSDS_TM_EN_BIT_POS)
          & CCSDS_TM_EN);

        temp_val = temp_val & (~CCSDS_FECF_EN);
        temp_val |= ((tm_config->fecf_enabled << CCSDS_FECF_EN_BIT_POS )
          & CCSDS_FECF_EN);

        temp_val = temp_val & (~CCSDS_MC_CNT_ENABLE);
        temp_val |= ((tm_config->mc_cnt_enabled << CCSDS_MC_CNT_EN_BIT_POS)
          & CCSDS_MC_CNT_ENABLE);

        temp_val = temp_val & (~CCSDS_IDLE_FRAME_ENABLE);
        temp_val |= ((tm_config->idle_frame_enabled << CCSDS_IDLE_FRAME_EN_BIT_POS)
          & CCSDS_IDLE_FRAME_ENABLE);

        temp_val = temp_val & (~CCSDS_TM_CONV_BYPASS);
        temp_val |= ((tm_config->tm_conv_bypassed << CCSDS_TM_CONV_BYPASS_BIT_POS)
          & CCSDS_TM_CONV_BYPASS);

        temp_val = temp_val & (~CCSDS_TM_PSEUDO_BYPASS);
        temp_val |= ((tm_config->tm_pseudo_rand_bypassed << CCSDS_TM_PSEUDO_BYPASS_BIT_POS)
          & CCSDS_TM_PSEUDO_BYPASS);

        temp_val = temp_val & (~CCSDS_OCF_CLCW_EN);
        temp_val |= ((tm_config->ocf_clcw_enabled << CCSDS_OCF_CLCW_EN_BIT_POS)
          & CCSDS_OCF_CLCW_EN);

        temp_val = temp_val & (~CCSDS_TM_RS_BYPASS);
        temp_val |= ((tm_config->tm_rs_bypassed << CCSDS_TM_RS_BYPASS_BIT_POS)
          & CCSDS_TM_RS_BYPASS);

        ccsds_set_ip_config(temp_val);

        arg->ioctl_return = 0;
        break;

      case CCSDS_GET_TM_CONFIG:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_GET_TM_CONFIG\r\n");
        temp_val = ccsds_get_ip_config();
        tm_config = (tm_config_t *) arg->buffer;
        tm_config->clk_divisor = (temp_val & CCSDS_CLK_DIVISOR_MASK) >>
          CCSDS_CLK_DIV_BIT_POS;
        tm_config->tm_enabled = (temp_val & CCSDS_TM_EN) >>
          CCSDS_TM_EN_BIT_POS;
        tm_config->fecf_enabled = (temp_val & CCSDS_FECF_EN) >>
          CCSDS_FECF_EN_BIT_POS;
        tm_config->mc_cnt_enabled = (temp_val & CCSDS_MC_CNT_ENABLE) >>
          CCSDS_MC_CNT_EN_BIT_POS;
        tm_config->idle_frame_enabled = (temp_val & CCSDS_IDLE_FRAME_ENABLE) >>
          CCSDS_IDLE_FRAME_EN_BIT_POS;
        tm_config->ocf_clcw_enabled = (temp_val & CCSDS_OCF_CLCW_EN) >>
          CCSDS_OCF_CLCW_EN_BIT_POS;
        tm_config->tm_conv_bypassed = (temp_val & CCSDS_TM_CONV_BYPASS) >>
          CCSDS_TM_CONV_BYPASS_BIT_POS;
        tm_config->tm_pseudo_rand_bypassed = (temp_val & CCSDS_TM_PSEUDO_BYPASS) >>
          CCSDS_TM_PSEUDO_BYPASS_BIT_POS;
        tm_config->tm_rs_bypassed = (temp_val & CCSDS_TM_RS_BYPASS) >>
          CCSDS_TM_RS_BYPASS_BIT_POS;

        arg->ioctl_return = 0;
        break;

      case CCSDS_ENABLE_TM:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_ENABLE_TM\r\n");
        ccsds_enable_tm();
        arg->ioctl_return = 0;
        break;

      case CCSDS_DISABLE_TM:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_DISABLE_TM\r\n");
        ccsds_disable_tm();
        arg->ioctl_return = 0;
        break;

      case CCSDS_GET_TM_STATUS:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_GET_TM_STATUS\r\n");
        temp_val = ccsds_get_tm_status();

        tm_status = (tm_status_t *) arg->buffer;

        tm_status->dma_desc_addr = (temp_val & 0x0000FF00) >> 8;
        tm_status->tm_fifo_err = (temp_val & 0x00000002) >> 1;
        tm_status->tm_busy = (temp_val & 0x00000001);
        arg->ioctl_return = 0;
        break;

      case CCSDS_GET_TM_ERR_CNT:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_GET_TM_ERR_CNT\r\n");
        temp_val = ccsds_get_tm_error_cnt();

        tm_err_counter = (tm_error_cnt_t *) arg->buffer;
        tm_err_counter->tm_par_err_cnt = temp_val & 0x000000FF;
        arg->ioctl_return = 0;
        break;

      case CCSDS_SET_TM_TIMESTAMP:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_SET_TM_TIMESTAMP\r\n");
        ccsds_set_timestamp_control((uint32_t)(arg->buffer));
        arg->ioctl_return = 0;
        break;

      case CCSDS_GET_TM_TIMESTAMP:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_GET_TM_TIMESTAMP\r\n");
        (*(uint32_t *)arg->buffer) = ccsds_get_timestamp_control();
        arg->ioctl_return = 0;
        break;

      default:
        arg->ioctl_return = -EIO;
        break;
    }
  }

  /* Handle IOCTLS for device CCSDS TC device */
  if(minor == CCSDS_MINOR_TC)
  {
    switch (arg->command)
    {
      case CCSDS_SET_TC_CONFIG:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_SET_TC_CONFIG\r\n");
        tc_config = (tc_config_t *) arg->buffer;
        temp_val = 0;
        tm_was_disabled = false;

        /* If TM is enabled it must be disabled prior to writing a new configuration */
        if((ccsds_get_ip_config() & CCSDS_TM_EN) == CCSDS_TM_EN)
        {
          ccsds_disable_tm();
          tm_was_disabled = true;

        }
          
        /* Read and modify */
        read_val = ccsds_get_ip_config();
        temp_val = read_val & (~CCSDS_TC_DERANDOMIZER_BYPASS);
        temp_val |= ((tc_config->tc_derandomizer_bypassed << CCSDS_TC_DERAND_BYPASS_BIT_POS) &
                     CCSDS_TC_DERANDOMIZER_BYPASS);
        ccsds_set_ip_config(temp_val);
           
       
        if(tm_was_disabled)
        {
          ccsds_enable_tm();  
        }
    
        arg->ioctl_return = 0;
        break;

      case CCSDS_GET_TC_CONFIG:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_GET_TC_CONFIG\r\n");
        temp_val = ccsds_get_ip_config();

        tc_config = (tc_config_t *) arg->buffer;

        tc_config->tc_derandomizer_bypassed =
        (temp_val & CCSDS_TC_DERANDOMIZER_BYPASS) >> CCSDS_TC_DERAND_BYPASS_BIT_POS;

        arg->ioctl_return = 0;
        break;

      case CCSDS_GET_TC_ERR_CNT:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_GET_TC_ERR_CNT\r\n");
        temp_val = ccsds_get_tc_error_cnt();

        tc_err_counter = (tc_error_cnt_t *) arg->buffer;

        tc_err_counter->tc_overflow_cnt =
          (temp_val & CCSDS_TC_OVERFLOW_MASK) >> CCSDS_TC_OVERFLOW_BIT_POS;
        tc_err_counter->tc_cpdu_rej_cnt =
          (temp_val & CCSDS_TC_CPDU_REJECTED_MASK) >> CCSDS_TC_CPDU_REJ_BIT_POS;
        tc_err_counter->tc_buf_rej_cnt =
          (temp_val & CCSDS_TC_BUF_REJECTED_MASK) >> CCSDS_TC_BUF_REJ_BIT_POS;
        tc_err_counter->tc_par_err_cnt =
          (temp_val & CCSDS_TC_PARERR_MASK) >> CCSDS_TC_PARERR_BIT_POS;

        arg->ioctl_return = 0;
        break;

      case CCSDS_GET_TC_STATUS:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_GET_TC_STATUS\r\n");
        tc_status = (tc_status_t *) arg->buffer;

        temp_val = ccsds_get_tc_frame_status();
        tc_status->tc_frame_cnt =
          (temp_val & CCSDS_TC_FRAME_COUNT_MASK) >> CCSDS_TC_FRAME_COUNT_BIT_POS;
        tc_status->tc_buffer_cnt = (temp_val & CCSDS_TC_BUFFER_COUNT_MASK);

        temp_val = ccsds_get_cpdu_status();
        tc_status->cpdu_line_status =
          (temp_val & CCSDS_CPDU_LINE_STATUS_MASK) >> CCSDS_CPDU_LINE_STATUS_BIT_POS;
        tc_status->cpdu_bypass_cnt = (temp_val & CCSDS_CPDU_BYPASS_CNT_MASK);

        arg->ioctl_return = 0;
        break;

      case CCSDS_SET_TC_FRAME_CTRL:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_SET_TC_FRAME_CTRL\r\n");
        ccsds_set_tc_frame_control((uint32_t)(arg->buffer));

        arg->ioctl_return = 0;
        break;

      case CCSDS_SET_CLCW:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_SET_CLCW\r\n");
        ccsds_set_clcw((uint32_t)(arg->buffer));
        arg->ioctl_return = 0;
        break;

      case CCSDS_GET_CLCW:
        DBG(DBG_SEVERITY_INFO,"\r\nIOCTL: CCSDS_GET_CLCW\r\n");
        (*(uint32_t *)arg->buffer) = ccsds_get_clcw();
        arg->ioctl_return = 0;
        break;

      default:
        arg->ioctl_return = -EIO;
        break;;
    }
  }

  return RTEMS_SUCCESSFUL;
}
