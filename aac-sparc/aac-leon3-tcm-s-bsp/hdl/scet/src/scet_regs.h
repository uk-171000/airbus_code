/*-----------------------------------------------------------------------------
 * Copyright (C) 2005-2016 ÅAC Microtec AB
 *
 * Filename: scet_regs.h
 *
 * Author(s): Per Brolin <per.brolin@aacmicrotec.com>
 *            Erik Zachrisson <erik.zachrisson@aacmicrotec.com
 * Description: AAC SCET driver registers header file
 * Requirements: AAC SCET IP Detailed Design Document
 *----------------------------------------------------------------------------*/

#ifndef _SCET_REGS_H_
#define _SCET_REGS_H_

/* SCET registers */
#define SCET_CONTROL                              (0x00)

#define SCET_CONTROL_INTERNAL_PPS                 (1 << 0)
#define SCET_CONTROL_ADJUST_SUBSECONDS            (1 << 1)
#define SCET_CONTROL_ADJUST_SECONDS               (1 << 2)
#define SCET_CONTROL_PPS_O_EN                     (1 << 3)
#define SCET_CONTROL_LATCH_EN                     (1 << 4)
#define SCET_CONTROL_TRIGGER0_LEVEL               (1 << 24)
#define SCET_CONTROL_TRIGGER1_LEVEL               (1 << 25)
#define SCET_CONTROL_TRIGGER2_LEVEL               (1 << 26)
#define SCET_CONTROL_TRIGGER3_LEVEL               (1 << 27)
#define SCET_CONTROL_TRIGGER4_LEVEL               (1 << 28)
#define SCET_CONTROL_TRIGGER5_LEVEL               (1 << 29)
#define SCET_CONTROL_TRIGGER6_LEVEL               (1 << 30)
#define SCET_CONTROL_TRIGGER7_LEVEL               (1 << 31)

#define SCET_CONTROL_PPS_O_EN_POS                 (3)

#define SCET_INTERRUPT_ENABLE                     (0x04)

#define SCET_INTERRUPT_ENABLE_PPS_ARRIVED         (1 << 0)
#define SCET_INTERRUPT_ENABLE_PPS_LOST            (1 << 1)
#define SCET_INTERRUPT_ENABLE_PPS_FOUND           (1 << 2)
#define SCET_INTERRUPT_ENABLE_TRIGGER0            (1 << 24)
#define SCET_INTERRUPT_ENABLE_TRIGGER1            (1 << 25)
#define SCET_INTERRUPT_ENABLE_TRIGGER2            (1 << 26)
#define SCET_INTERRUPT_ENABLE_TRIGGER3            (1 << 27)
#define SCET_INTERRUPT_ENABLE_TRIGGER4            (1 << 28)
#define SCET_INTERRUPT_ENABLE_TRIGGER5            (1 << 29)
#define SCET_INTERRUPT_ENABLE_TRIGGER6            (1 << 30)
#define SCET_INTERRUPT_ENABLE_TRIGGER7            (1 << 31)

#define SCET_INTERRUPT_ENABLE_NONE                (0x00)

#define SCET_INTERRUPT_STATUS                     (0x08)

#define SCET_INTERRUPT_STATUS_PPS_ARRIVED         (1 << 0)
#define SCET_INTERRUPT_STATUS_PPS_LOST            (1 << 1)
#define SCET_INTERRUPT_STATUS_PPS_FOUND           (1 << 2)
#define SCET_INTERRUPT_STATUS_TRIGGER0            (1 << 24)
#define SCET_INTERRUPT_STATUS_TRIGGER1            (1 << 25)
#define SCET_INTERRUPT_STATUS_TRIGGER2            (1 << 26)
#define SCET_INTERRUPT_STATUS_TRIGGER3            (1 << 27)
#define SCET_INTERRUPT_STATUS_TRIGGER4            (1 << 28)
#define SCET_INTERRUPT_STATUS_TRIGGER5            (1 << 29)
#define SCET_INTERRUPT_STATUS_TRIGGER6            (1 << 30)
#define SCET_INTERRUPT_STATUS_TRIGGER7            (1 << 31)

#define SCET_SECONDS_COUNTER                      (0x0C)
#define SCET_SUBSECONDS_COUNTER                   (0x10)
#define SCET_PPS_ARRIVED_COUNTER                  (0x14)
#define SCET_TRIGGER_COUNTER0                     (0x18)
#define SCET_TRIGGER_COUNTER1                     (0x1C)
#define SCET_TRIGGER_COUNTER2                     (0x20)
#define SCET_TRIGGER_COUNTER3                     (0x24)
#define SCET_TRIGGER_COUNTER4                     (0x28)
#define SCET_TRIGGER_COUNTER5                     (0x2C)
#define SCET_TRIGGER_COUNTER6                     (0x30)
#define SCET_TRIGGER_COUNTER7                     (0x34)
#define SCET_SECONDS_ADJUSTMENT                   (0x38)
#define SCET_SUBSECONDS_ADJUSTMENT                (0x3C)
#define SCET_PPS_THRESHOLD                        (0x40)
#define SCET_SECONDS_LATCH_TIMESTAMP              (0x44)
#define SCET_SUBSECONDS_LATCH_TIMESTAMP           (0x48)

#define SCET_NO_TRIGGERS                          (8)
#define SCET_TRIGGERS_MASK                        (0xFF000000)
#define SCET_TRIGGERS_SHIFT                       (24)

#define SCET_SUBSECONDS_MASK                      (0x0000FFFF)
/* This only applies to latched at events values,
 * actual seconds count is stored as 32-bit value
 * in register SCET_SECONDS_COUNTER (@ offset 0xC)
 */
#define SCET_SECONDS_MASK                         (0x00FF0000)
#define SCET_SECONDS_SHIFT                        (16)

#endif
