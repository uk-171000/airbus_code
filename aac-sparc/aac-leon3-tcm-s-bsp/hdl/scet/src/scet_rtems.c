/*-----------------------------------------------------------------------------
 * Copyright (C) 2016-2018 ÅAC Microtec AB
 *
 * Filename:    scet_rtems.c
 * Module name: scet
 *
 * Author(s):    ErZa, DaHe
 * Support:      support@aacmicrotec.com
 * Description:  SCET RTEMS driver implementation
 * Requirements: ÅAC SCET Driver Detailed Design Description
 *----------------------------------------------------------------------------*/

#include <fcntl.h>
#include <inttypes.h>
#include <rtems/imfs.h>
#include <rtems/libio.h>
#include <rtems/seterr.h>
#include <bsp.h>
#include <bsp/irq.h>

#include "scet.h"
#include "scet_rtems.h"
#include "toolchain_support.h"

/* Board definitions */
uint32_t _scet_base = AAC_BSP_SCET_BASE;
uint32_t _scet_IRQ = AAC_BSP_SCET_IRQ;

static rtems_id scet_pps_q;
static rtems_id scet_gp_trig0_q;
static rtems_id scet_gp_trig1_q;
static rtems_id scet_gp_trig2_q;
static rtems_id scet_gp_trig3_q;
static rtems_id scet_gp_trig4_q;
static rtems_id scet_gp_trig5_q;
static rtems_id scet_gp_trig6_q;
static rtems_id scet_gp_trig7_q;

/* The driver permits multiple readers but only one writer at a time */
#define INVALID_FD (-1)
static int writer_fd = INVALID_FD;
static rtems_id writer_mutex;
static uint32_t users;

void scet_interrupt_pps_arrived(void);
void scet_interrupt_pps_lost(void);
void scet_interrupt_pps_found(void);

void scet_interrupt_gp_trigger0(void);
void scet_interrupt_gp_trigger1(void);
void scet_interrupt_gp_trigger2(void);
void scet_interrupt_gp_trigger3(void);
void scet_interrupt_gp_trigger4(void);
void scet_interrupt_gp_trigger5(void);
void scet_interrupt_gp_trigger6(void);
void scet_interrupt_gp_trigger7(void);

scet_callbacks_t scet_callbacks = {
  .scet_pps_arrived = scet_interrupt_pps_arrived,
  .scet_pps_lost = scet_interrupt_pps_lost,
  .scet_pps_found = scet_interrupt_pps_found,
  .scet_gp_trig0 = scet_interrupt_gp_trigger0,
  .scet_gp_trig1 = scet_interrupt_gp_trigger1,
  .scet_gp_trig2 = scet_interrupt_gp_trigger2,
  .scet_gp_trig3 = scet_interrupt_gp_trigger3,
  .scet_gp_trig4 = scet_interrupt_gp_trigger4,
  .scet_gp_trig5 = scet_interrupt_gp_trigger5,
  .scet_gp_trig6 = scet_interrupt_gp_trigger6,
  .scet_gp_trig7 = scet_interrupt_gp_trigger7
};

void scet_interrupt_pps_arrived(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_PPS_ARRIVED;
  rtems_message_queue_broadcast(scet_pps_q, &msg, sizeof(uint32_t), &count);
}

void scet_interrupt_pps_lost(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_PPS_LOST;
  rtems_message_queue_broadcast(scet_pps_q, &msg, sizeof(uint32_t), &count);
}

void scet_interrupt_pps_found(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_PPS_FOUND;
  rtems_message_queue_broadcast(scet_pps_q, &msg, sizeof(uint32_t), &count);
}

void scet_interrupt_gp_trigger0(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_TRIGGER0;
  rtems_message_queue_broadcast(scet_gp_trig0_q, &msg, sizeof(uint32_t), &count);
}

void scet_interrupt_gp_trigger1(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_TRIGGER1;
  rtems_message_queue_broadcast(scet_gp_trig1_q, &msg, sizeof(uint32_t), &count);
}

void scet_interrupt_gp_trigger2(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_TRIGGER2;
  rtems_message_queue_broadcast(scet_gp_trig2_q, &msg, sizeof(uint32_t), &count);
}

void scet_interrupt_gp_trigger3(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_TRIGGER3;
  rtems_message_queue_broadcast(scet_gp_trig3_q, &msg, sizeof(uint32_t), &count);
}

void scet_interrupt_gp_trigger4(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_TRIGGER4;
  rtems_message_queue_broadcast(scet_gp_trig4_q, &msg, sizeof(uint32_t), &count);
}

void scet_interrupt_gp_trigger5(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_TRIGGER5;
  rtems_message_queue_broadcast(scet_gp_trig5_q, &msg, sizeof(uint32_t), &count);
}

void scet_interrupt_gp_trigger6(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_TRIGGER6;
  rtems_message_queue_broadcast(scet_gp_trig6_q, &msg, sizeof(uint32_t), &count);
}

void scet_interrupt_gp_trigger7(void)
{
  uint32_t count;
  uint32_t msg;

  msg = SCET_INTERRUPT_STATUS_TRIGGER7;
  rtems_message_queue_broadcast(scet_gp_trig7_q, &msg, sizeof(uint32_t), &count);
}

/*
 * Access functions
 */
static int rtems_scet_open(rtems_libio_t *iop,
                           const char    *path,
                           int            oflag,
                           mode_t         mode)
{
  int fd;
  int err;
  rtems_status_code status;

  fd = rtems_libio_iop_to_descriptor(iop);
  err = 0;

  DBG(DBG_SEVERITY_INFO, "Opening SCET device file descriptor %d.", fd);

  /* Use mutex to protect against simultaneous open from different tasks */
  status = rtems_semaphore_obtain(writer_mutex,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to obtain mutex for SCET driver: %s)",
        rtems_status_text(status));
    rtems_set_errno_and_return_minus_one(EIO);
  }

  /* Multiple readers, one writer allowed */
  if ((iop->flags & LIBIO_FLAGS_WRITE) == LIBIO_FLAGS_WRITE) {
    if (writer_fd != INVALID_FD) {
      DBG(DBG_SEVERITY_INFO,
          "SCET device already opened for writing. Aborting request.");
      err = EALREADY;
    }
    else {
      writer_fd = fd;
    }
  }

  status = rtems_semaphore_release(writer_mutex);

  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to release mutex for SCET driver: %s)",
        rtems_status_text(status));
  }

  if (err != 0) {
    rtems_set_errno_and_return_minus_one(err);
  }
  else {
    if (users == 0) {
      DBG(DBG_SEVERITY_INFO, "Enable SCET PPS interrupts.");
      /* Clear interrupt status and enable PPS interrupt sources */
      scet_set_interrupt_srcs(SCET_INTERRUPT_ENABLE_PPS_LOST  |
                              SCET_INTERRUPT_ENABLE_PPS_FOUND |
                              SCET_INTERRUPT_ENABLE_PPS_ARRIVED);
    }

    users++;

    return 0;
  }
}

static int rtems_scet_close(rtems_libio_t *iop)
{
  int fd;
  fd = rtems_libio_iop_to_descriptor(iop);

  DBG(DBG_SEVERITY_INFO, "Closing SCET device file descriptor %d.", fd);

  /* Multiple readers, one writer allowed. */
  if (fd == writer_fd) {
    DBG(DBG_SEVERITY_INFO, "Clear writer_fd.");
    writer_fd = INVALID_FD;
  }

  users--;

  if (users == 0) {
    DBG(DBG_SEVERITY_INFO, "Disable SCET interrupts.");
    /* Clear interrupt status and disable all interrupt sources */
    scet_set_interrupt_srcs(SCET_INTERRUPT_ENABLE_NONE);
  }

  return 0;
}

static ssize_t rtems_scet_read(rtems_libio_t *iop,
                               void          *buffer,
                               size_t         count)
{
  DBG(DBG_SEVERITY_INFO, "Reading from SCET device");

  /* The calling function in libcsupport checks permissions and buffer. */

  if (count != 6) {
    DBG(DBG_SEVERITY_WARN,
        "SCET driver invalid length of data. Must be 6 bytes, was %d.",
        count);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  scet_latch_and_get_time(buffer);

  return count;
}

static ssize_t rtems_scet_write(rtems_libio_t *iop,
                                const void    *buffer,
                                size_t         count)
{
  int16_t subsecs;
  int32_t secs;

  DBG(DBG_SEVERITY_INFO, "Writing to SCET device.");

  /* The calling function in libcsupport checks permissions and buffer. */

  if (count != 6) {
    DBG(DBG_SEVERITY_WARN,
        "SCET driver invalid length of data. Must be 6 bytes, was %d.",
        count);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  memcpy(&subsecs, buffer + sizeof(secs), sizeof(subsecs));
  memcpy(&secs, buffer, sizeof(secs));

  scet_write_subseconds(subsecs);
  scet_write_seconds(secs);

  return count;
}

static int rtems_scet_control(rtems_libio_t   *iop,
                              ioctl_command_t  command,
                              void            *buffer)
{
  int      rv;
  int      fd;
  uint32_t val;
  uint32_t rmw;

  fd = rtems_libio_iop_to_descriptor(iop);

  DBG(DBG_SEVERITY_INFO, "SCET IOCTL.");

  val = (uint32_t) buffer;

  switch (command) {
    case SCET_SET_PPS_SOURCE_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_SET_PPS_SOURCE_IOCTL %u", val);
      if (fd != writer_fd) {
        DBG(DBG_SEVERITY_WARN,
            "File descriptor not opened for writing. Aborting request.");
        rv = -EBADF;
      }
      else if (val > SCET_CONTROL_INTERNAL_PPS) {
        DBG(DBG_SEVERITY_INFO, "IOCTL: Value too large %u", val);
        rv = -EINVAL;
      }
      else {
        rmw = scet_read_reg(SCET_CONTROL);
        /* Clear current setting */
        rmw &= ~SCET_CONTROL_INTERNAL_PPS;
        /* Set new value */
        rmw |= val;
        scet_write_reg(SCET_CONTROL, rmw);
        rv = 0;
      }
      break;

    case SCET_GET_PPS_SOURCE_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_GET_PPS_SOURCE_IOCTL");
      rv = scet_read_reg(SCET_CONTROL) & SCET_CONTROL_INTERNAL_PPS;
      break;

    case SCET_GET_PPS_O_EN_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_GET_PPS_O_EN_IOCTL");
      rv =
        (scet_read_reg(SCET_CONTROL) & SCET_CONTROL_PPS_O_EN) >> SCET_CONTROL_PPS_O_EN_POS;
      break;

    case SCET_SET_PPS_O_EN_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_SET_PPS_O_EN_IOCTL %u", val);
      if (fd != writer_fd) {
        DBG(DBG_SEVERITY_WARN,
            "File descriptor not opened for writing. Aborting request.");
        rv = -EBADF;
      }
      else if (val > 1) {
        DBG(DBG_SEVERITY_INFO, "IOCTL: Value too large %u", val);
        rv = -EINVAL;
      }
      else {
        rmw = scet_read_reg(SCET_CONTROL);

        /* Clear current setting */
        rmw &= ~SCET_CONTROL_PPS_O_EN;

        /* Set new value */
        if (val == 1) {
          rmw |= SCET_CONTROL_PPS_O_EN;
        }
        scet_write_reg(SCET_CONTROL, rmw);
        rv = 0;
      }
      break;

    case SCET_SET_PPS_THRESHOLD_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_SET_PPS_THRESHOLD_IOCTL %u", val);
      if (fd != writer_fd) {
        DBG(DBG_SEVERITY_WARN,
            "File descriptor not opened for writing. Aborting request.");
        rv = -EBADF;
      }
      else if (val > UINT16_MAX) {
        DBG(DBG_SEVERITY_INFO, "IOCTL: Value too large %u", val);
        rv = -EINVAL;
      }
      else {
        scet_write_reg(SCET_PPS_THRESHOLD, val);
        rv = 0;
      }
      break;

    case SCET_GET_PPS_THRESHOLD_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_GET_PPS_THRESHOLD_IOCTL");
      rv = scet_read_reg(SCET_PPS_THRESHOLD);
      break;

    case SCET_GET_PPS_ARRIVE_COUNTER_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_GET_PPS_ARRIVE_COUNTER_IOCTL");
      rv = scet_read_reg(SCET_PPS_ARRIVED_COUNTER);
      break;

    case SCET_SET_GP_TRIGGER_LEVEL_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_SET_GP_TRIGGER_LEVEL_IOCTL 0x%02x", val);
      if (fd != writer_fd) {
        DBG(DBG_SEVERITY_WARN,
            "File descriptor not opened for writing. Aborting request.");
        rv = -EBADF;
      }
      else {
        rmw = scet_read_reg(SCET_CONTROL);
        rmw = (rmw & ~SCET_TRIGGERS_MASK) | (val << SCET_TRIGGERS_SHIFT);
        scet_write_reg(SCET_CONTROL, rmw);
        rv = 0;
      }
      break;

    case SCET_GET_GP_TRIGGER_LEVEL_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_GET_GP_TRIGGER_LEVEL_IOCTL");
      rv = scet_read_reg(SCET_CONTROL) >> SCET_TRIGGERS_SHIFT;
      break;

    case SCET_SET_GP_TRIGGER_ENABLE_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_SET_TRIGGER_ENABLE_IOCTL %u", val);
      if (fd != writer_fd) {
        DBG(DBG_SEVERITY_WARN,
            "File descriptor not opened for writing. Aborting request.");
        rv = -EBADF;
      }
      else {
        rmw = scet_read_reg(SCET_INTERRUPT_ENABLE);
        rmw = (rmw & ~SCET_TRIGGERS_MASK) | (val << SCET_TRIGGERS_SHIFT);
        scet_write_reg(SCET_INTERRUPT_ENABLE, rmw);
        rv = 0;
      }
      break;

    case SCET_GET_GP_TRIGGER_ENABLE_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_GET_TRIGGER_ENABLE_IOCTL");
      rv = scet_read_reg(SCET_INTERRUPT_ENABLE) >> SCET_TRIGGERS_SHIFT;
      break;

    case SCET_GET_GP_TRIGGER_COUNTER_IOCTL:
      DBG(DBG_SEVERITY_INFO, "IOCTL: SCET_GET_GP_TRIGGER_COUNTER_IOCTL");

      if (val >= SCET_NO_TRIGGERS) {
        DBG(DBG_SEVERITY_INFO, "IOCTL: Value too large %u", val);
        rv = -EINVAL;
      } else {
        /* Scale to 32 bit address offset */
        rv = scet_read_reg(SCET_TRIGGER_COUNTER0 + (val << 2));
      }
      break;

    default:
      DBG(DBG_SEVERITY_WARN, "IOCTL: unknown IOCTL");
      rv = -EINVAL;
      break;
  }

  if (rv < 0) {
    rtems_set_errno_and_return_minus_one(-rv);
  }
  else {
    return rv;
  }
}

static const rtems_filesystem_file_handlers_r rtems_scet_handlers = {
  .open_h      = rtems_scet_open,
  .close_h     = rtems_scet_close,
  .read_h      = rtems_scet_read,
  .write_h     = rtems_scet_write,
  .ioctl_h     = rtems_scet_control,
  .lseek_h     = rtems_filesystem_default_lseek_file,
  .fstat_h     = IMFS_stat,
  .ftruncate_h = rtems_filesystem_default_ftruncate,
  .fsync_h     = rtems_filesystem_default_fsync_or_fdatasync,
  .fdatasync_h = rtems_filesystem_default_fsync_or_fdatasync,
  .fcntl_h     = rtems_filesystem_default_fcntl,
  .readv_h     = rtems_filesystem_default_readv,
  .writev_h    = rtems_filesystem_default_writev,
};

static const IMFS_node_control
rtems_scet_node_control = IMFS_GENERIC_INITIALIZER(
  &rtems_scet_handlers,
  IMFS_node_initialize_generic,
  IMFS_node_destroy_default
);

rtems_device_driver rtems_scet_initialize(rtems_device_major_number major,
                                          rtems_device_minor_number minor,
                                          void *args)
{
  int rv;
  rtems_status_code rtems_result;

  DBG(DBG_SEVERITY_INFO, "Create SCET device node (M%d:m%d).", major, minor);
  /* Note: Since we are using generic IMFS nodes major and minor numbers do not
     matter. */

  users = 0;

  rv = IMFS_make_generic_node(RTEMS_SCET_DEVICE_NAME,
                              S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO,
                              &rtems_scet_node_control,
                              NULL);
  if(rv < 0) {
    DBG(DBG_SEVERITY_ERR, "Failed to create SCET device node (M%lu:m0): %s)",
        major, strerror(errno));
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_semaphore_create(rtems_build_name('S', 'W', 'R', 'T'),
                                  1,
                                  RTEMS_DEFAULT_ATTRIBUTES | RTEMS_SIMPLE_BINARY_SEMAPHORE,
                                  0,
                                  &writer_mutex);
  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR,
        "Failed to create mutex for SCET driver, error %d, %s)",
        rtems_result, rtems_status_text(rtems_result));
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_interrupt_handler_install(_scet_IRQ,
                                                 "scet",
                                                 RTEMS_INTERRUPT_UNIQUE,
                                                 scet_interrupt_handler,
                                                 NULL);
  if(rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to install interrupt handler %d, %s",
        rtems_result, rtems_status_text(rtems_result));
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_message_queue_create(
                             rtems_build_name('S', 'P', 'P', 'S'),
                             10,
                             sizeof(uint32_t),
                             RTEMS_DEFAULT_ATTRIBUTES,
                             &scet_pps_q);

  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register message queue. "
        "Did you enable CONFIGURE_MAXIMUM_MESSAGE_QUEUES? Errcode: %d, errstring: %s",
        rtems_result, rtems_status_text(rtems_result));

    rtems_interrupt_handler_remove(_scet_IRQ, scet_interrupt_handler, NULL);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_message_queue_create(
                             rtems_build_name('S', 'G', 'T', '0'),
                             10,
                             sizeof(uint32_t),
                             RTEMS_DEFAULT_ATTRIBUTES,
                             &scet_gp_trig0_q);

  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register message queue. "
        "Did you enable CONFIGURE_MAXIMUM_MESSAGE_QUEUES? Errcode: %d, errstring: %s",
        rtems_result, rtems_status_text(rtems_result));

    rtems_interrupt_handler_remove(_scet_IRQ, scet_interrupt_handler, NULL);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_message_queue_create(
                             rtems_build_name('S', 'G', 'T', '1'),
                             10,
                             sizeof(uint32_t),
                             RTEMS_DEFAULT_ATTRIBUTES,
                             &scet_gp_trig1_q);

  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register message queue. "
        "Did you enable CONFIGURE_MAXIMUM_MESSAGE_QUEUES? Errcode: %d, errstring: %s",
        rtems_result, rtems_status_text(rtems_result));

    rtems_interrupt_handler_remove(_scet_IRQ, scet_interrupt_handler, NULL);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_message_queue_create(
                             rtems_build_name('S', 'G', 'T', '2'),
                             10,
                             sizeof(uint32_t),
                             RTEMS_DEFAULT_ATTRIBUTES,
                             &scet_gp_trig2_q);

  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register message queue. "
        "Did you enable CONFIGURE_MAXIMUM_MESSAGE_QUEUES? Errcode: %d, errstring: %s",
        rtems_result, rtems_status_text(rtems_result));

    rtems_interrupt_handler_remove(_scet_IRQ, scet_interrupt_handler, NULL);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_message_queue_create(
                             rtems_build_name('S', 'G', 'T', '3'),
                             10,
                             sizeof(uint32_t),
                             RTEMS_DEFAULT_ATTRIBUTES,
                             &scet_gp_trig3_q);

  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register message queue. "
        "Did you enable CONFIGURE_MAXIMUM_MESSAGE_QUEUES? Errcode: %d, errstring: %s",
        rtems_result, rtems_status_text(rtems_result));

    rtems_interrupt_handler_remove(_scet_IRQ, scet_interrupt_handler, NULL);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_message_queue_create(
                             rtems_build_name('S', 'G', 'T', '4'),
                             10,
                             sizeof(uint32_t),
                             RTEMS_DEFAULT_ATTRIBUTES,
                             &scet_gp_trig4_q);

  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register message queue. "
        "Did you enable CONFIGURE_MAXIMUM_MESSAGE_QUEUES? Errcode: %d, errstring: %s",
        rtems_result, rtems_status_text(rtems_result));

    rtems_interrupt_handler_remove(_scet_IRQ, scet_interrupt_handler, NULL);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_message_queue_create(
                             rtems_build_name('S', 'G', 'T', '5'),
                             10,
                             sizeof(uint32_t),
                             RTEMS_DEFAULT_ATTRIBUTES,
                             &scet_gp_trig5_q);

  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register message queue. "
        "Did you enable CONFIGURE_MAXIMUM_MESSAGE_QUEUES? Errcode: %d, errstring: %s",
        rtems_result, rtems_status_text(rtems_result));

    rtems_interrupt_handler_remove(_scet_IRQ, scet_interrupt_handler, NULL);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_message_queue_create(
                             rtems_build_name('S', 'G', 'T', '6'),
                             10,
                             sizeof(uint32_t),
                             RTEMS_DEFAULT_ATTRIBUTES,
                             &scet_gp_trig6_q);

  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register message queue. "
        "Did you enable CONFIGURE_MAXIMUM_MESSAGE_QUEUES? Errcode: %d, errstring: %s",
        rtems_result, rtems_status_text(rtems_result));

    rtems_interrupt_handler_remove(_scet_IRQ, scet_interrupt_handler, NULL);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  rtems_result = rtems_message_queue_create(
                             rtems_build_name('S', 'G', 'T', '7'),
                             10,
                             sizeof(uint32_t),
                             RTEMS_DEFAULT_ATTRIBUTES,
                             &scet_gp_trig7_q);

  if (rtems_result != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_ERR, "Failed to register message queue. "
        "Did you enable CONFIGURE_MAXIMUM_MESSAGE_QUEUES? Errcode: %d, errstring: %s",
        rtems_result, rtems_status_text(rtems_result));

    rtems_interrupt_handler_remove(_scet_IRQ, scet_interrupt_handler, NULL);
    return RTEMS_INTERNAL_ERROR; /* EIO */
  }

  /* Clear interrupt status and disable all interrupt sources */
  scet_set_interrupt_srcs(SCET_INTERRUPT_ENABLE_NONE);

  return RTEMS_SUCCESSFUL;
}
