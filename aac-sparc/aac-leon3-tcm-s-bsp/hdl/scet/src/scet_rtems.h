/*-----------------------------------------------------------------------------
 * Copyright (C) 2016-2018 ÅAC Microtec AB
 *
 * Filename:     scet.h
 * Module name:  scet
 *
 * Author(s):    PeBr, ErZa, DaHe
 * Support:      support@aacmicrotec.com
 * Description:  ÅAC SCET RTEMS driver header file
 * Requirements: ÅAC SCET Driver Detailed Design Document
 *----------------------------------------------------------------------------*/

#ifndef _RTEMS_SCET_H_
#define _RTEMS_SCET_H_

#include "scet_regs.h"

#define RTEMS_SCET_DEVICE_NAME "/dev/scet"

rtems_device_driver rtems_scet_initialize(rtems_device_major_number major,
                                          rtems_device_minor_number minor,
                                          void *args);

#define AAC_SCET_DRIVER_TABLE_ENTRY { \
      rtems_scet_initialize,          \
      NULL,                           \
      NULL,                           \
      NULL,                           \
      NULL,                           \
      NULL }

#define SCET_SET_PPS_SOURCE_IOCTL           _IOW('S',  1, uint32_t)
#define SCET_GET_PPS_SOURCE_IOCTL           _IOR('S',  2, uint32_t)
#define SCET_SET_PPS_O_EN_IOCTL             _IOW('S',  3, uint32_t)
#define SCET_GET_PPS_O_EN_IOCTL             _IOR('S',  4, uint32_t)
#define SCET_SET_PPS_THRESHOLD_IOCTL        _IOW('S',  5, uint16_t)
#define SCET_GET_PPS_THRESHOLD_IOCTL        _IOR('S',  6, uint16_t)
#define SCET_GET_PPS_ARRIVE_COUNTER_IOCTL   _IOR('S',  7, uint32_t)

#define SCET_SET_GP_TRIGGER_LEVEL_IOCTL     _IOW('S',  8, uint8_t)
#define SCET_GET_GP_TRIGGER_LEVEL_IOCTL     _IOR('S',  9, uint8_t)
#define SCET_SET_GP_TRIGGER_ENABLE_IOCTL    _IOW('S', 10, uint8_t)
#define SCET_GET_GP_TRIGGER_ENABLE_IOCTL    _IOR('S', 11, uint8_t)

#define SCET_GET_GP_TRIGGER_COUNTER_IOCTL   _IOR('S', 12, uint32_t)

#define SCET_RW_CNT                         6
#define SCET_SECOND_SZ                      4
#define SCET_SECOND_OFFS                    0
#define SCET_SUBSECOND_SZ                   2
#define SCET_SUBSECOND_OFFS                 SCET_SECOND_SZ

#endif
