/*-----------------------------------------------------------------------------
 * Copyright (C) 2005-2016 ÅAC Microtec AB
 *
 * Filename:     scet.c
 * Module name:  scet
 *
 * Author(s):    PeBr, ErZa
 * Support:      support@aacmicrotec.com
 * Description:  ÅAC SCET bare metal driver source file
 * Requirements: ÅAC SCET Driver Detailed Design Document
 *****************************************************************************/

#include "toolchain_support.h"
#include "scet.h"
#include "scet_regs.h"
#include <string.h>

extern scet_callbacks_t scet_callbacks;

uint32_t scet_read_seconds()
{
  return REG32(SCET_BASE + SCET_SECONDS_COUNTER);
}

uint16_t scet_read_subseconds()
{
  return (uint16_t) REG32(SCET_BASE + SCET_SUBSECONDS_COUNTER);
}

void scet_write_seconds(int32_t new_sec)
{
  uint32_t rmw;
  /* Set seconds to adjust */
  REG32(SCET_BASE + SCET_SECONDS_ADJUSTMENT) = new_sec;

  /* Set the adjust second control bit */
  rmw = REG32(SCET_BASE + SCET_CONTROL);
  rmw |= SCET_CONTROL_ADJUST_SECONDS;

  REG32(SCET_BASE + SCET_CONTROL) = rmw;
}

void scet_write_subseconds(int16_t new_subsec)
{
  uint32_t rmw;

  /* Set subseconds to adjust */
  REG32(SCET_BASE + SCET_SUBSECONDS_ADJUSTMENT) = new_subsec;

  /* Set the adjust subsecond control bit */
  rmw = REG32(SCET_BASE + SCET_CONTROL);
  rmw |= SCET_CONTROL_ADJUST_SUBSECONDS;

  REG32(SCET_BASE + SCET_CONTROL) = rmw;
}

uint32_t scet_read_reg(uint32_t offset)
{
  return REG32(SCET_BASE + offset);
}

void scet_write_reg(uint32_t offset, uint32_t value)
{
  REG32(SCET_BASE + offset) = value;
}

AAC_INTERRUPT_HANDLER_SIGNATURE(scet_interrupt_handler, args)
{
  uint32_t irq_vect;

  /* Suppress unused warning */
  (void) args;

  /* Read interrupt enable and interrupt status and
     mask these together to get the active source(s) to serve */
  irq_vect = REG32(SCET_BASE + SCET_INTERRUPT_STATUS);
  irq_vect &= REG32(SCET_BASE + SCET_INTERRUPT_ENABLE);

  /* Clear IRQ source bit(s) after reading but before handling them, this avoids
   * potential interrupt misses if a new interrupt arrives after handling the
   * previous one but before clearing.
   *
   * An interrupt received during the ISR is guaranteed to queue up a new ISR
   * call afterwards, and is therefore OK. But the ISR must operate in such a
   * way that it can handle if a previous ISR handled both interrupts but did
   * not clear the second interrupt status bit. In this case this is deferred to
   * the ISR map creator (i.e. the RTEMS driver) and is thus someone else's
   * problem.
   */
  REG32(SCET_BASE + SCET_INTERRUPT_STATUS) = irq_vect;

  if (irq_vect & SCET_INTERRUPT_STATUS_PPS_ARRIVED) {
    if (scet_callbacks.scet_pps_arrived != NULL) {
      scet_callbacks.scet_pps_arrived();
    }
  }

  if (irq_vect & SCET_INTERRUPT_STATUS_PPS_LOST) {
    if (scet_callbacks.scet_pps_lost != NULL) {
      scet_callbacks.scet_pps_lost();
    }
  }

  if (irq_vect & SCET_INTERRUPT_STATUS_PPS_FOUND) {
    if (scet_callbacks.scet_pps_found != NULL) {
      scet_callbacks.scet_pps_found();
    }
  }

  if (irq_vect & SCET_INTERRUPT_STATUS_TRIGGER0) {
    if (scet_callbacks.scet_gp_trig0 != NULL) {
      scet_callbacks.scet_gp_trig0();
    }
  }

  if (irq_vect & SCET_INTERRUPT_STATUS_TRIGGER1) {
    if (scet_callbacks.scet_gp_trig1 != NULL) {
      scet_callbacks.scet_gp_trig1();
    }
  }

  if (irq_vect & SCET_INTERRUPT_STATUS_TRIGGER2) {
    if (scet_callbacks.scet_gp_trig2 != NULL) {
      scet_callbacks.scet_gp_trig2();
    }
  }

  if (irq_vect & SCET_INTERRUPT_STATUS_TRIGGER3) {
    if (scet_callbacks.scet_gp_trig3 != NULL) {
      scet_callbacks.scet_gp_trig3();
    }
  }

  if (irq_vect & SCET_INTERRUPT_STATUS_TRIGGER4) {
    if (scet_callbacks.scet_gp_trig4 != NULL) {
      scet_callbacks.scet_gp_trig4();
    }
  }

  if (irq_vect & SCET_INTERRUPT_STATUS_TRIGGER5) {
    if (scet_callbacks.scet_gp_trig5 != NULL) {
      scet_callbacks.scet_gp_trig5();
    }
  }

  if (irq_vect & SCET_INTERRUPT_STATUS_TRIGGER6) {
    if (scet_callbacks.scet_gp_trig6 != NULL) {
      scet_callbacks.scet_gp_trig6();
    }
  }

  if (irq_vect & SCET_INTERRUPT_STATUS_TRIGGER7) {
    if (scet_callbacks.scet_gp_trig7 != NULL) {
      scet_callbacks.scet_gp_trig7();
    }
  }
}

void scet_set_interrupt_srcs(uint32_t new_srcs)
{
  /* Clear all interrupt to get into a known state */
  REG32(SCET_BASE + SCET_INTERRUPT_STATUS) = 0xFFFFFFFF;
  REG32(SCET_BASE + SCET_INTERRUPT_ENABLE) = new_srcs;
}

void scet_latch_and_get_time(uint8_t *timestamp)
{
  uint32_t rmw;
  uint16_t scet_subsec;
  rmw = REG32(SCET_BASE + SCET_CONTROL);
  rmw |= SCET_CONTROL_LATCH_EN;

  /* Latch second and subsecond */
  REG32(SCET_BASE + SCET_CONTROL) = rmw;

  rmw = REG32(SCET_BASE + SCET_SECONDS_LATCH_TIMESTAMP);
  memcpy((timestamp + 0), &rmw, sizeof(uint32_t));

  scet_subsec = (uint16_t) REG32(SCET_BASE + SCET_SUBSECONDS_LATCH_TIMESTAMP);
  memcpy((timestamp + sizeof(uint32_t)), &scet_subsec, sizeof(uint16_t));
}
