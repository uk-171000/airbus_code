/*------------------------------------------------------------------------------
 * Copyright (C) 2016 ÅAC Microtec AB
 *
 * Filename:     hk_conv.c
 * Module name:  adc
 *
 * Author(s):    JaVi
 * Support:      support@aacmicrotec.com
 * Description:  Implementation file for ADC HK conversion functions
 * Requirements: Provides functions of conversion between raw
 *               ADC values and the actual of voltages, current and
 *               temperature measured.
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "hk_conv.h"

#define MULT_FACTOR_VIN         20575
#define MULT_FACTOR_3V3         5000
#define MULT_FACTOR_2V5         5000
#define MULT_FACTOR_1V2         2525
#define MULT_FACTOR_IIN         5000
#define MULT_FACTOR_TEMP        2500

inline uint16_t hk_conv(uint32_t mult_factor, uint32_t scale_factor, uint32_t raw_adc_in)
{
  uint32_t adc_adjust;

  adc_adjust = raw_adc_in;

  /* If necessary, truncate to 16 bits value first to avoid overflow */
  if (scale_factor > 16) {
    adc_adjust = adc_adjust >> (scale_factor - 16);
  }

  /* V_mV = (ADC_value*mult_factor)/(2^scale_factor) */
  adc_adjust = mult_factor*adc_adjust;

  if (scale_factor >= 16) {
    return (uint16_t) ((adc_adjust >> 16) & 0xffff);
  } else {
    return (uint16_t) ((adc_adjust >> scale_factor) & 0xffff);
  }
}

uint16_t hk_conv_vin(uint32_t scale_factor, uint32_t raw_adc_vin)
{
  return hk_conv(MULT_FACTOR_VIN, scale_factor, raw_adc_vin);
}

uint16_t hk_conv_3v3(uint32_t scale_factor, uint32_t raw_adc_3v3)
{
  return hk_conv(MULT_FACTOR_3V3, scale_factor, raw_adc_3v3);
}

uint16_t hk_conv_2v5(uint32_t scale_factor, uint32_t raw_adc_2v5)
{
  return hk_conv(MULT_FACTOR_2V5, scale_factor, raw_adc_2v5);
}

uint16_t hk_conv_1v2(uint32_t scale_factor, uint32_t raw_adc_1v2)
{
  return hk_conv(MULT_FACTOR_1V2, scale_factor, raw_adc_1v2);
}

uint16_t hk_conv_iin(uint32_t scale_factor, uint32_t raw_adc_iin)
{
  return hk_conv(MULT_FACTOR_IIN, scale_factor, raw_adc_iin);
}

uint16_t hk_conv_temp_mv(uint32_t scale_factor, uint32_t raw_adc_temp)
{
  return hk_conv(MULT_FACTOR_TEMP, scale_factor, raw_adc_temp);
}

int32_t hk_conv_temp_degc(uint16_t hk_temp_mv, uint16_t hk_3v3_mv)
{
  float temp;
  int32_t num;
  float denom;

  /* To get the temperature in degC, we need the 3V3_mV value as well.
     Temp_mC = (1000*(3V3_mV - Temp_mV) - Temp_mV*1210) / 0.00385*(Temp_mV - 3300) */
  num = (1000*(hk_3v3_mv - hk_temp_mv) - hk_temp_mv*1210);
  denom = 0.00385*(hk_temp_mv - 3300);
  temp = num / denom;
  /* Round up */
  temp += 0.5;

  return (int32_t) temp;
}
