/*------------------------------------------------------------------------------
 * Copyright (C) 2016 ÅAC Microtec AB
 *
 * Filename:     adc_rtems.c
 * Module name:  adc
 *
 * Author(s):    CoSj
 * Support:      support@aacmicrotec.com
 * Description:  ADC bare-metal driver implementation
 * Requirements: Adheres to the ÅAC ADC Driver Detailed Design Description Rev. A
 *----------------------------------------------------------------------------*/

#include <inttypes.h>
#include <rtems/libio.h>
#include <rtems/seterr.h>
#include <rtems/score/cpu.h>

#include <bsp.h>
#include <bsp/irq.h>

#include "toolchain_support.h"
#include "adc.h"
#include "adc_rtems.h"

#define AAC_BSP_ADC_COUNT 1
#define NO_ADC_CH_ENABLED (-1)

#define ADC_MSG_SEM_ATTR    (RTEMS_FIFO                               | \
                             RTEMS_SIMPLE_BINARY_SEMAPHORE            | \
                             RTEMS_NO_MULTIPROCESSOR_RESOURCE_SHARING | \
                             RTEMS_LOCAL)

typedef struct adc_info {
  const char *name;
  const uint32_t irq;
  uint8_t users;
  rtems_id semaphore;
  rtems_libio_rw_args_t *args;
  uint32_t adc_ch_values[ADC_NUM_CHANNELS];
  int32_t enabled_adc_ch;
  uint32_t channels_read;
} adc_info_t;

const uint32_t adc_base_addr = AAC_BSP_ADC_BASE;

static adc_info_t adcs = {ADC_DEVICE_NAME, AAC_BSP_ADC_IRQ};

static void rtems_adc_interrupt_handler(void *arg);

/*
 * ADC callback functions
 */
adc_callbacks_t adc_callbacks = {
  .adc_interrupt_callback = rtems_adc_interrupt_handler
};

static void rtems_adc_interrupt_handler(void *arg)
{
  adc_info_t *adc_info;
  rtems_status_code status;
  uint32_t adc_status;
  uint8_t adc_status_reg;
  uint32_t read_adc_value;
  uint32_t adc_channel;

  adc_info = (adc_info_t *) arg;

  /* Disable the interrupt before clearing it, extra interrupts with measurements delayed by
   * a fixed number of read calls have been observed, which are avoided by disabling before
   * clearing.
   */
  adc_status = adc_int_disable(ADC_INT_TRANSFER_COMPLETE);
  if(ENOERR != adc_status) {
    DBG(DBG_SEVERITY_WARN, "Failed to disable interrupt (0x%08X)!", adc_status);
  }

  /* This will clear the interrupt */
  adc_status = adc_get_status(&adc_status_reg);
  if (ENOERR != adc_status) {
    DBG(DBG_SEVERITY_WARN, "Failed to read status (0x%08X)!", adc_status);
  }

  if((adc_status_reg & ADC_TRANSFER_COMPLETE) != ADC_TRANSFER_COMPLETE) {
    /* A zero-status interrupt currently occurs as a second interrupt at every read despite
     * the above attempt to disable and clear the interrupts after the first one.
     *
     * If we want to use this as a workaround, this warning needs to be demoted to info.
     */
    DBG(DBG_SEVERITY_INFO, "Unexpected ADC interrupt, ignored.");
    return;
  }

  adc_channel = 0;
  adc_status = adc_get_conversion(&read_adc_value, sizeof(uint32_t));
  if (ENOERR != adc_status) {
    DBG(DBG_SEVERITY_WARN, "Failed to read ADC value (0x%08X)!", adc_status);
  } else
  {
    adc_channel = read_adc_value & 0x0F;
  }

  adc_info->channels_read |= 1 << adc_channel;

  if(adc_channel == adc_info->enabled_adc_ch) {
    /* We have data for the correct channel. */
    adc_info->adc_ch_values[adc_channel] = read_adc_value;
  } else {
    DBG(DBG_SEVERITY_INFO,
        "ADC channel mismatch, expected %" PRIu32 ", got %" PRIu32,
        adc_info->enabled_adc_ch,
        adc_channel);
  }

  status = rtems_semaphore_release(adc_info->semaphore);
  if (status != RTEMS_SUCCESSFUL) {
    DBG(DBG_SEVERITY_WARN, "%s, Failed to release semaphore\n", adc_info->name);
  }
}

static uint32_t convert_from_ioctl_sample_rate(const uint32_t ioctl_sample_rate,
                                               uint32_t * const sample_rate)
{
  switch(ioctl_sample_rate) {
  case ADC_IOCTL_SPS_31250:
    *sample_rate = ADC_SPS_31250;
    break;
  case ADC_IOCTL_SPS_15625:
    *sample_rate = ADC_SPS_15625;
    break;
  case ADC_IOCTL_SPS_10417:
    *sample_rate = ADC_SPS_10417;
    break;
  case ADC_IOCTL_SPS_5208:
    *sample_rate = ADC_SPS_5208;
    break;
  case ADC_IOCTL_SPS_2597:
    *sample_rate = ADC_SPS_2597;
    break;
  case ADC_IOCTL_SPS_1007:
    *sample_rate = ADC_SPS_1007;
    break;
  case ADC_IOCTL_SPS_503_8:
    *sample_rate = ADC_SPS_503_8;
    break;
  case ADC_IOCTL_SPS_381:
    *sample_rate = ADC_SPS_381;
    break;
  case ADC_IOCTL_SPS_200_3:
    *sample_rate = ADC_SPS_200_3;
    break;
  case ADC_IOCTL_SPS_100_5:
    *sample_rate = ADC_SPS_100_5;
    break;
  case ADC_IOCTL_SPS_59_52:
    *sample_rate = ADC_SPS_59_52;
    break;
  case ADC_IOCTL_SPS_49_68:
    *sample_rate = ADC_SPS_49_68;
    break;
  case ADC_IOCTL_SPS_20_01:
    *sample_rate = ADC_SPS_20_01;
    break;
  case ADC_IOCTL_SPS_16_63:
    *sample_rate = ADC_SPS_16_63;
    break;
  case ADC_IOCTL_SPS_10:
    *sample_rate = ADC_SPS_10;
    break;
  case ADC_IOCTL_SPS_5:
    *sample_rate = ADC_SPS_5;
    break;
  case ADC_IOCTL_SPS_2_5:
    *sample_rate = ADC_SPS_2_5;
    break;
  case ADC_IOCTL_SPS_1_25:
    *sample_rate = ADC_SPS_1_25;
    break;

  default:
    DBG(DBG_SEVERITY_WARN, "Unknown sample rate 0x%08X. Ignoring request", ioctl_sample_rate);
    return EINVAL;
    break;
  }

  return ENOERR;
}

static uint32_t convert_to_ioctl_sample_rate(const uint32_t sample_rate,
                                             uint32_t * const ioctl_sample_rate)
{
  switch(sample_rate) {
  case ADC_SPS_31250:
    *ioctl_sample_rate = ADC_IOCTL_SPS_31250;
    break;
  case ADC_SPS_15625:
    *ioctl_sample_rate = ADC_IOCTL_SPS_15625;
    break;
  case ADC_SPS_10417:
    *ioctl_sample_rate = ADC_IOCTL_SPS_10417;
    break;
  case ADC_SPS_5208:
    *ioctl_sample_rate = ADC_IOCTL_SPS_5208;
    break;
  case ADC_SPS_2597:
    *ioctl_sample_rate = ADC_IOCTL_SPS_2597;
    break;
  case ADC_SPS_1007:
    *ioctl_sample_rate = ADC_IOCTL_SPS_1007;
    break;
  case ADC_SPS_503_8:
    *ioctl_sample_rate = ADC_IOCTL_SPS_503_8;
    break;
  case ADC_SPS_381:
    *ioctl_sample_rate = ADC_IOCTL_SPS_381;
    break;
  case ADC_SPS_200_3:
    *ioctl_sample_rate = ADC_IOCTL_SPS_200_3;
    break;
  case ADC_SPS_100_5:
    *ioctl_sample_rate = ADC_IOCTL_SPS_100_5;
    break;
  case ADC_SPS_59_52:
    *ioctl_sample_rate = ADC_IOCTL_SPS_59_52;
    break;
  case ADC_SPS_49_68:
    *ioctl_sample_rate = ADC_IOCTL_SPS_49_68;
    break;
  case ADC_SPS_20_01:
    *ioctl_sample_rate = ADC_IOCTL_SPS_20_01;
    break;
  case ADC_SPS_16_63:
    *ioctl_sample_rate = ADC_IOCTL_SPS_16_63;
    break;
  case ADC_SPS_10:
    *ioctl_sample_rate = ADC_IOCTL_SPS_10;
    break;
  case ADC_SPS_5:
    *ioctl_sample_rate = ADC_IOCTL_SPS_5;
    break;
  case ADC_SPS_2_5:
    *ioctl_sample_rate = ADC_IOCTL_SPS_2_5;
    break;
  case ADC_SPS_1_25:
    *ioctl_sample_rate = ADC_IOCTL_SPS_1_25;
    break;

  default:
    DBG(DBG_SEVERITY_WARN, "Unknown sample rate 0x%08X. Ignoring request", sample_rate);
    return EINVAL;
    break;
  }

  return ENOERR;
}

static uint32_t convert_from_ioctl_clock_divisor(const uint32_t ioctl_clock_divisor,
                                               uint8_t * const clock_divisor)
{
  if (ioctl_clock_divisor > 0xFF) {
    DBG(DBG_SEVERITY_WARN, "Unknown clock divisor 0x%08X. Ignoring request",
        ioctl_clock_divisor);
    return EINVAL;
  }

  *clock_divisor = (uint8_t)ioctl_clock_divisor;

  return ENOERR;
}

static uint32_t convert_to_ioctl_clock_divisor(const uint8_t clock_divisor,
                                              uint8_t * const ioctl_clock_divisor)
{
  *ioctl_clock_divisor = clock_divisor;

  return ENOERR;
}

static uint32_t convert_from_ioctl_channel(const uint32_t ioctl_channel,
                                          uint8_t * const channel)
{
  if (ioctl_channel >= ADC_CHANNELS) {
    DBG(DBG_SEVERITY_WARN, "Unknown channel 0x%08X. Ignoring request",
        ioctl_channel);
    return EINVAL;
  }

  *channel = (uint8_t)ioctl_channel;

  return ENOERR;
}

rtems_device_driver rtems_adc_initialize(rtems_device_major_number major,
                                         rtems_device_minor_number minor,
                                         void *args)
{
  rtems_status_code status;
  uint32_t channels;
  DBG(DBG_SEVERITY_INFO, "Initializing ADC device (M%lu:m%lu)", major, minor);

  if (minor > AAC_BSP_ADC_COUNT - 1) {
    DBG(DBG_SEVERITY_ERR, "Invalid ADC minor %d", minor);
    rtems_set_errno_and_return_minus_one(ENODEV);
  }

  for (minor = 0; minor < AAC_BSP_ADC_COUNT; minor++) {
    DBG(DBG_SEVERITY_INFO, "Registering ADC driver (M%lu:m%lu)", major, minor);

    status = rtems_io_register_name(adcs.name, major, minor);
    if(status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR, "Failed to register ADC driver (M%lu:m%lu), error %d, %s)",
          major, minor, rtems_status_text(status));
      rtems_set_errno_and_return_minus_one(ENODEV);
    }

    status = rtems_interrupt_handler_install(adcs.irq,
                                             adcs.name,
                                             RTEMS_INTERRUPT_UNIQUE,
                                             adc_isr,
                                             &adcs);

    if(status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR, "Failed to install interrupt handler %d, %s",
          status, rtems_status_text(status));
      rtems_io_unregister_driver(major);
      rtems_set_errno_and_return_minus_one(EAGAIN);
    }

    status = rtems_semaphore_create(rtems_build_name('A', 'D', 'C', '0' + minor),
                                    0,
                                    ADC_MSG_SEM_ATTR,
                                    RTEMS_NO_PRIORITY_CEILING,
                                    &adcs.semaphore);
    if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_ERR, "Failed to create semaphore, error %d, %s",
          status, rtems_status_text(status));
      rtems_io_unregister_driver(major);
      rtems_set_errno_and_return_minus_one(EAGAIN);
    }

    if (ENOERR != adc_init() ) {
      DBG(DBG_SEVERITY_WARN,
          "ADC driver (M%lu:m%lu) failed to initialize driver. Aborting request.",
          major, minor);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }

    for(channels = 0; channels < ADC_NUM_CHANNELS; channels++) {
      adcs.adc_ch_values[channels] = channels;
    }

    adcs.users = 0;
  }


  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_adc_open(rtems_device_major_number major,
                                   rtems_device_minor_number minor,
                                   void *args)
{
  rtems_libio_open_close_args_t *rw_args;

  rw_args = (rtems_libio_open_close_args_t *) args;

  DBG(DBG_SEVERITY_INFO, "Opening ADC device (M%lu:m%lu).", major, minor);

  if((rw_args->flags & LIBIO_FLAGS_OPEN) == true) {
    DBG(DBG_SEVERITY_WARN, "ADC driver (M%lu:m%lu) already opened. Aborting request.",
        major, minor);
    rtems_set_errno_and_return_minus_one(EALREADY);
  }

  if (adcs.users != 0) {
    DBG(DBG_SEVERITY_WARN, "ADC driver (M%lu:m%lu) already opened. Aborting request.",
        major, minor);
    rtems_set_errno_and_return_minus_one(EEXIST);
  }

  if (rw_args->flags & LIBIO_FLAGS_WRITE) {
    DBG(DBG_SEVERITY_WARN,
        "ADC driver (M%lu:m%lu) write not supported. Aborting request.", major, minor);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  if (rw_args->flags & LIBIO_FLAGS_READ) {
    DBG(DBG_SEVERITY_INFO, "ADC driver (M%lu:m%lu). Enabling driver.", major, minor);
  }

  adcs.users = 1;

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_adc_close(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *args)
{
  DBG(DBG_SEVERITY_INFO, "Closing ADC device (M%lu:m%lu).", major, minor);

  if (adcs.users != 1) {
    DBG(DBG_SEVERITY_WARN, "ADC driver (M%lu:m%lu) not opened. Aborting request.",
        major, minor);
    rtems_set_errno_and_return_minus_one(EFAULT);
  }

  adcs.users    = 0;

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_adc_read(rtems_device_major_number major,
                                   rtems_device_minor_number minor,
                                   void *args)
{
  rtems_libio_rw_args_t *rw_args;
  rtems_status_code status;
  uint32_t adc_status;
  uint8_t adc_status_reg;

  DBG(DBG_SEVERITY_INFO, "Reading from ADC device (M%lu:m%lu).", major, minor);

  rw_args = (rtems_libio_rw_args_t *) args;
  rw_args->bytes_moved = 0;
  adcs.args = rw_args;

  if(rw_args->count < 1) {

    rtems_set_errno_and_return_minus_one(EINVAL);
  }
  if((rw_args->flags & LIBIO_FLAGS_OPEN) == false) {
    DBG(DBG_SEVERITY_WARN, "ADC driver (M%lu:m%lu) not opened. Aborting request.",
        major, minor);
    rtems_set_errno_and_return_minus_one(EPERM);
  }

  if(adcs.enabled_adc_ch == NO_ADC_CH_ENABLED) {
    DBG(DBG_SEVERITY_WARN,
        "Failed to read from ADC driver (M%lu:m%lu) since no channel enabled",
        major, minor);
    rtems_set_errno_and_return_minus_one(EINVAL);
  }

  /* We can get data for the wrong channel occasionally for unknown reasons (aacbug #2635).
   * Retry single reads until we get the correct channel.
   */

  /* Mark enabled channel as unread. */
  adcs.channels_read &= ~(1 << adcs.enabled_adc_ch);
  while(!(adcs.channels_read & (1 << adcs.enabled_adc_ch))) {
    adc_status = adc_get_status(&adc_status_reg);
    if (adc_status != ENOERR) {
      DBG(DBG_SEVERITY_WARN,
          "ADC driver (M%lu:m%lu) failed to start conversion",
          major, minor);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }

    adc_status = adc_int_enable(ADC_INT_TRANSFER_COMPLETE);
    if(ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN,
          "ADC driver (M%lu:m%lu) failed to enable interrupt. Aborting request.",
          major, minor);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }

    adc_status = adc_start_conversion();
    if (adc_status != ENOERR) {
      DBG(DBG_SEVERITY_WARN,
          "ADC driver (M%lu:m%lu) failed to start conversion",
          major, minor);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }

    /* Wait and block till a message is received */
    status = rtems_semaphore_obtain(adcs.semaphore,
        RTEMS_WAIT,
        RTEMS_NO_TIMEOUT);
    if (status != RTEMS_SUCCESSFUL) {
      DBG(DBG_SEVERITY_WARN, "Failed to obtain semaphore, error: %d, %s",
          status, rtems_status_text(status));
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
  }

  memcpy(rw_args->buffer, &adcs.adc_ch_values[adcs.enabled_adc_ch], sizeof(uint32_t));
  rw_args->bytes_moved = 4;
  return RTEMS_SUCCESSFUL;
}

rtems_device_driver rtems_adc_write(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *args)
{
  DBG(DBG_SEVERITY_WARN, "Writing to ADC (M%lu:m%lu) not supported. Aborting request.",
      major, minor);

  rtems_set_errno_and_return_minus_one(EINVAL);
}

rtems_device_driver rtems_adc_control(rtems_device_major_number major,
                                      rtems_device_minor_number minor,
                                      void *args)
{
  rtems_libio_ioctl_args_t *ioctl_args;
  uint32_t value;
  uint8_t adc_status;
  uint32_t adc_sample_rate;
  uint32_t ioctl_adc_sample_rate;
  uint8_t adc_clock_divior;
  uint8_t ioctl_adc_clock_divior;
  uint8_t adc_channel;

  ioctl_args = (rtems_libio_ioctl_args_t *) args;
  value = (uint32_t) ioctl_args->buffer;

  DBG(DBG_SEVERITY_INFO, "Setting ADC control options to device (M%lu:m%lu).",
      major, minor);

  switch(ioctl_args->command) {

  case ADC_SET_SAMPLE_RATE_IOCTL:
    adc_status = convert_from_ioctl_sample_rate(value, &adc_sample_rate);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_ERR, "Failed to convert sample rate (0x%08X)!", value);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    adc_status = adc_set_sample_rate(adc_sample_rate);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_ERR, "Failed to set sample rate (0x%08X)!", value);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    ioctl_args->ioctl_return = RTEMS_SUCCESSFUL;
    break;

  case ADC_GET_SAMPLE_RATE_IOCTL:
    adc_status = adc_get_sample_rate(&adc_sample_rate);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN, "Failed to get sample rate (0x%08X)!", adc_sample_rate);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    adc_status = convert_to_ioctl_sample_rate(adc_sample_rate,
                                              &ioctl_adc_sample_rate);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN, "Failed to convert sample rate (0x%08X)!", adc_sample_rate);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    *(uint32_t *) ioctl_args->buffer = ioctl_adc_sample_rate;
    ioctl_args->ioctl_return = RTEMS_SUCCESSFUL;
    break;

  case ADC_SET_CLOCK_DIVISOR_IOCTL:
    adc_status = convert_from_ioctl_clock_divisor(value, &adc_clock_divior);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN, "Failed to convert clock divisor (0x%08X)!", value);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    adc_status = adc_set_clock_divisor(adc_clock_divior);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN, "Failed to set clock divisor (0x%08X)!", value);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    ioctl_args->ioctl_return = RTEMS_SUCCESSFUL;
    break;

  case ADC_GET_CLOCK_DIVISOR_IOCTL:
    adc_status = adc_get_clock_divisor(&adc_clock_divior);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN, "Failed to get clock divisor (0x%08X)!", adc_clock_divior);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    adc_status = convert_to_ioctl_clock_divisor(adc_clock_divior,
                                                &ioctl_adc_clock_divior);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN, "Failed to convert clock divisor (0x%08X)!", adc_clock_divior);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    *(uint32_t *) ioctl_args->buffer = ioctl_adc_clock_divior;
    ioctl_args->ioctl_return = RTEMS_SUCCESSFUL;
    break;

  case ADC_ENABLE_CHANNEL_IOCTL:
    adc_status = convert_from_ioctl_channel(value, &adc_channel);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN, "Failed to convert channel (0x%08X)!", value);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    adc_status = adc_enable_channel(adc_channel);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN, "Failed to enable channel (0x%08X)!", value);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    adcs.enabled_adc_ch = adc_channel;
    ioctl_args->ioctl_return = RTEMS_SUCCESSFUL;
    break;

  case ADC_DISABLE_CHANNEL_IOCTL:
    adc_status = convert_from_ioctl_channel(value, &adc_channel);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN, "Failed to convert channel (0x%08X)!", value);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    adc_status = adc_disable_channel(adc_channel);
    if (ENOERR != adc_status) {
      DBG(DBG_SEVERITY_WARN, "Failed to disable channel (0x%08X)!", value);
      rtems_set_errno_and_return_minus_one(EINVAL);
    }
    adcs.enabled_adc_ch = NO_ADC_CH_ENABLED;
    ioctl_args->ioctl_return = RTEMS_SUCCESSFUL;
    break;

  default:
    DBG(DBG_SEVERITY_WARN, "Unknown iotcl command 0x%08X. Ignoring request:",
        ioctl_args->command);
    rtems_set_errno_and_return_minus_one(EINVAL);
    break;
  }

  return RTEMS_SUCCESSFUL;
}
