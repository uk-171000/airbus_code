/*------------------------------------------------------------------------------
 * Copyright (C) 2016 ÅAC Microtec AB
 *
 * Filename:     adc_internal.h
 * Module name:  adc
 *
 * Author(s):    CoSj
 * Support:      support@aacmicrotec.com
 * Description:  ADC bare-metal driver implementation
 * Requirements: Adheres to the ÅAC ADC Driver Detailed Design Description Rev. A
 *----------------------------------------------------------------------------*/

#ifndef _ADC_INTERNAL_H_
#define _ADC_INTERNAL_H_

/* ADC register map */
#define ADC_SPI_CONTROL_REG     (0x00U)
#define ADC_SPI_DIVISOR_REG     (0x04U)
#define ADC_STATUS_REG          (0x08U)
#define ADC_INT_SOURCE_REG      (0x0CU)
#define ADC_INT_ENABLE_REG      (0x10U)
#define ADC_RX_DATA_REG         (0x14U)
#define ADC_RX_DST_ADDR_REG     (0x20U)
#define ADC_RX_DMA_LENGTH_REG   (0x24U)
#define ADC_ACTIVE_CHANNELS_REG (0x30U)
#define ADC_TIMEOUT_REG         (0x34U)
#define ADC_CHANNEL_DATA_REG    (0x40U)

/* Status register bit masks/mappings */
#define ADC_STATUS_BUSY_POS     (0U)
#define ADC_STATUS_BUSY_MASK    (0x1U << ADC_STATUS_BUSY_POS)

/* Interrupt status register bit masks/mappings */
#define ADC_INT_SOURCE_TRANSFER_POS     (0U)
#define ADC_INT_SOURCE_TRANSFER_MASK    (0x1U << ADC_INT_SOURCE_TRANSFER_POS)

/* Interrupt enable register bit masks/mappings */
#define ADC_INT_ENABLE_TRANSFER_POS     (0U)
#define ADC_INT_ENABLE_TRANSFER_MASK    (0x1U << ADC_INT_ENABLE_TRANSFER_POS)

/* AD7173 register offset */
#define ADC_AD7173_REG_OFFSET   (0x100U)
#define ADC_AD7173_REG_POS      (0x2U)

#endif // _ADC_INTERNAL_H_
