/*------------------------------------------------------------------------------
 * Copyright (C) 2016 ÅAC Microtec AB
 *
 * Filename:     adc.h
 * Module name:  adc
 *
 * Author(s):    CoSj
 * Support:      support@aacmicrotec.com
 * Description:  ADC bare-metal driver implementation
 * Requirements: Adheres to the ÅAC ADC Driver Detailed Design Description Rev. A
 *----------------------------------------------------------------------------*/

#ifndef _ADC_H_
#define _ADC_H_

#define ENOERR                    (0U)

#define ADC_NUM_CHANNELS          (16U)

/* Status register bits */
#define ADC_TRANSFER_COMPLETE     (0x1U)

/* Interrupt register bits */
#define ADC_INT_TRANSFER_COMPLETE (0x1U)

/* Polls of status register until driver is considered busy */
#define ADC_POLLS_UNTIL_BUSY      (500U)

/* ADC sample rates */
typedef enum {
  ADC_SPS_31250     = (0U),
  ADC_SPS_15625     = (6U),
  ADC_SPS_10417     = (7U),
  ADC_SPS_5208      = (8U),
  ADC_SPS_2597      = (9U),
  ADC_SPS_1007      = (10U),
  ADC_SPS_503_8     = (11U),
  ADC_SPS_381       = (12U),
  ADC_SPS_200_3     = (13U),
  ADC_SPS_100_5     = (14U),
  ADC_SPS_59_52     = (15U),
  ADC_SPS_49_68     = (16U),
  ADC_SPS_20_01     = (17U),
  ADC_SPS_16_63     = (18U),
  ADC_SPS_10        = (19U),
  ADC_SPS_5         = (20U),
  ADC_SPS_2_5       = (21U),
  ADC_SPS_1_25      = (22U)
} adc_sample_rate_e;

typedef struct adc_callbacks
{
  /*
   * Name:        adc_interrupt_callback
   * @brief       Interrupt callback function.
   * @param[in]   arg  Pointer to function arguments.
   */
  void (*adc_interrupt_callback)(void *arg);
} adc_callbacks_t;

/* Name:        adc_init
 * @brief       Initializes the ADC to its default state.
 * @return      ENOERR       Operation was successful.
 *              EIO          Input/output error.
 */
uint32_t adc_init(void);

/*
 * Name:        adc_isr
 * @brief       Interrupt service routine for handling all interrupt events.
 * @param[in]   arg  Pointer to function arguments.
 */
AAC_INTERRUPT_HANDLER_SIGNATURE(adc_isr, arg);

/* Name:        adc_enable_channel
 * @brief       Enable ADC channel.
 * @param[in]   channel      Adc channel to enable.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
uint32_t adc_enable_channel(const uint8_t channel);

/* Name:        adc_disable_channel
 * @brief       Disable ADC channel.
 * @param[in]   channel      ADC channel to disable.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
uint32_t adc_disable_channel(const uint8_t channel);

/* Name:        adc_set_clock_divisor
 * @brief       Set the SPI clock divisor.
 * @param[in]   divisor      Clock divisor to set.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 */
uint32_t adc_set_clock_divisor(const uint8_t divisor);

/* Name:        adc_get_clock_divisor
 * @brief       Get the SPI clock divisor.
 * @param[out]  divisor      Current clock divisor.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 */
uint32_t adc_get_clock_divisor(uint8_t * const divisor);

/* Name:        adc_set_sample_rate
 * @brief       Set the ADC sample rate.
 * @param[in]   sample_rate  Sample rate to set.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
uint32_t adc_set_sample_rate(const uint32_t sample_rate);

/* Name:        adc_get_sample_rate
 * @brief       Get the ADC sample rate.
 * @param[out]  sample_rate  Current sample rate.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
uint32_t adc_get_sample_rate(uint32_t * const sample_rate);

/* Name:        adc_get_status
 * @brief       Get and clear the status of the ADC.
 * @param[out]  adc_status   Status of the ADC.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
uint32_t adc_get_status(uint8_t * const adc_status);

/* Name:        adc_int_enable
 * @brief       Enables ADC interrupt.
 * @param[in]   int_source   Interrupt source(s) to enable.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
uint32_t adc_int_enable(const uint8_t int_source);

/* Name:        adc_int_disable
 * @brief       Disables ADC interrupt.
 * @param[in]   int_source   Interrupt source(s) to disable.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
uint32_t adc_int_disable(const uint8_t int_source);

/* Name:        adc_start_conversion
 * @brief       Starts a ADC conversion
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
uint32_t adc_start_conversion(void);

/* Name:        adc_get_conversion
 * @brief       Get the ADC conversion result
 * @param[in]   buffer       Buffer to store the data.
 * @param[in]   count        Number of samples.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
uint32_t adc_get_conversion(uint32_t * const buffer, const uint32_t count);

#endif // _ADC_H_
