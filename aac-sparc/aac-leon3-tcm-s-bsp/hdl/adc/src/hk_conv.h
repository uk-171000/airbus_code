/*------------------------------------------------------------------------------
 * Copyright (C) 2016 ÅAC Microtec AB
 *
 * Filename:     hk_conv.h
 * Module name:  adc
 *
 * Author(s):    JaVi
 * Support:      support@aacmicrotec.com
 * Description:  Header file for ADC HK conversion functions
 * Requirements: Provides function declarations of conversion between raw
 *               ADC values and the actual of voltages, current and
 *               temperature measured.
 *----------------------------------------------------------------------------*/

#ifndef HK_CONV_H
#define HK_CONV_H

#include <stdint.h>

/* Name:        hk_conv_vin
 * @brief       Convert the raw ADC value for Vin to mV
 * @param[in]   scale_factor, i.e. number of bits of raw ADC value, typically 24
 * @param[in]   raw_adc_vin, the raw ADC value for Vin
 * @return      The Vin value in mV
 */
uint16_t hk_conv_vin(uint32_t scale_factor, uint32_t raw_adc_vin);

/* Name:        hk_conv_3v3
 * @brief       Convert the raw ADC value for 3.3 V to mV
 * @param[in]   scale_factor, i.e. number of bits of raw ADC value, typically 24
 * @param[in]   raw_adc_3v3, the raw ADC value for 3.3 V
 * @return      The 3.3 V value in mV
 */
uint16_t hk_conv_3v3(uint32_t scale_factor, uint32_t raw_adc_3v3);

/* Name:        hk_conv_2v5
 * @brief       Convert the raw ADC value for 2.5 V to mV
 * @param[in]   scale_factor, i.e. number of bits of raw ADC value, typically 24
 * @param[in]   raw_adc_2v5, the raw ADC value for 2.5 V
 * @return      The 2.5 V value in mV
 */
uint16_t hk_conv_2v5(uint32_t scale_factor, uint32_t raw_adc_2v5);

/* Name:        hk_conv_1v2
 * @brief       Convert the raw ADC value for 1.2 V to mV
 * @param[in]   scale_factor, i.e. number of bits of raw ADC value, typically 24
 * @param[in]   raw_adc_1v2, the raw ADC value for 1.2 V
 * @return      The 1.2 V value in mV
 */
uint16_t hk_conv_1v2(uint32_t scale_factor, uint32_t raw_adc_1v2);

/* Name:        hk_conv_iin
 * @brief       Convert the raw ADC value for Iin to mA
 * @param[in]   scale_factor, i.e. number of bits of raw ADC value, typically 24
 * @param[in]   raw_adc_iin, the raw ADC value for Iin
 * @return      The Iin value in mA
 */
uint16_t hk_conv_iin(uint32_t scale_factor, uint32_t raw_adc_iin);

/* Name:        hk_conv_temp_mv
 * @brief       Convert the raw ADC value for temperature to mV
 * @param[in]   scale_factor, i.e. number of bits of raw ADC value, typically 24
 * @param[in]   raw_adc_temp, the raw ADC value for temperature
 * @return      The temperature value in mV
 * @note        To convert the raw ADC value for temperature to degC is a two-step
 *              equation, this is only the first step.
 */
uint16_t hk_conv_temp_mv(uint32_t scale_factor, uint32_t raw_adc_temp);

/* Name:        hk_conv_temp_degc
 * @brief       Convert the temperature from mV to mdegC
 * @param[in]   hk_temp_mv, temperature in mV
 * @param[in]   hk_3v3_mv, 3.3 V in mV
 * @return      The temperature value in mdegC
 * @note        To convert the raw ADC value for temperature to degC is a two-step
 *              equation, this is the second step which also requires the 3V3 in mV.
 *              The equation make use of truncated values for temperature and 3V3 in mV
 *              and will thus incur a small error in calculations, but it should not be
 *              off by more than +/- .2 degC in the worst case.
 */
int32_t  hk_conv_temp_degc(uint16_t hk_temp_mv, uint16_t hk_3v3_mv);

#endif // HK_CONV_H
