/*------------------------------------------------------------------------------
 * Copyright (C) 2016 ÅAC Microtec AB
 *
 * Filename:     adc.c
 * Module name:  adc
 *
 * Author(s):    CoSj
 * Support:      support@aacmicrotec.com
 * Description:  ADC bare-metal driver implementation
 * Requirements: Adheres to the ÅAC ADC Driver Detailed Design Description Rev. A
 *----------------------------------------------------------------------------*/

#include "toolchain_support.h"
#include <errno.h>
#include <string.h>
#include "adc.h"
#include "adc_internal.h"
#include "ad7173.h"

extern adc_callbacks_t adc_callbacks;
extern const uint32_t adc_base_addr;

/* Name:        adc_wait_on_busy_status
 * @brief       Tight polling wait until ADC status indicates not busy.
 */
static inline void adc_wait_on_busy_status(void)
{
  while ((REG32(adc_base_addr + ADC_STATUS_REG) & ADC_STATUS_BUSY_MASK) ==
         ADC_STATUS_BUSY_MASK);
}

/* Name:        adc_read_reg
 * @brief       Read a register.
 * @param[in]   reg          Register to read.
 * @param[in]   data         Buffer to read data into.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 */
static uint32_t adc_read_reg(const uint32_t reg, uint32_t * const data)
{
  uint32_t status = ENOERR;
  uint32_t local_data;

  /* Check parameter */
  if (NULL == data) {
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR, "Failed to validate parameter!");
  }

  adc_wait_on_busy_status();

  /* Read register */
  if (ENOERR == status) {
    local_data = REG32(adc_base_addr + reg);
    *data = local_data;
#if 0
    DBG(DBG_SEVERITY_INFO, "Read reg (0x%lX) = 0x%lX", reg, local_data);
#endif
  }

  return status;
}

/* Name:        adc_write_reg
 * @brief       Write a register.
 * @param[in]   reg          Register to write.
 * @param[in]   data         Buffer to write data from.
 * @return      ENOERR       Operation was successful.
 */
static uint32_t adc_write_reg(const uint32_t reg, const uint32_t data)
{
  uint32_t status = ENOERR;

  adc_wait_on_busy_status();

  /* Write register */
  REG32(adc_base_addr + reg) = data;
#if 0
  DBG(DBG_SEVERITY_INFO, "Write reg (0x%lX) = 0x%lX", reg, data);
#endif

  return status;
}


/* Name:        adc_read_chip_reg
 * @brief       Read a chip register.
 * @param[in]   reg          Register to read.
 * @param[in]   data         Buffer to read data into.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
static uint32_t adc_read_chip_reg(const uint32_t reg, uint32_t * const data)
{
  uint32_t status = ENOERR;
  uint32_t local_data;
  uint8_t adc_status;
  uint32_t loop_cnt;

  /* Check parameter */
  if (NULL == data) {
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR, "Failed to validate parameter!");
  }

  status = adc_get_status(&adc_status);
  if (ENOERR != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to read register! (0x%X)", ADC_STATUS_REG);
  }

  /* Read register */
  if (ENOERR == status) {
    status = adc_read_reg(ADC_AD7173_REG_OFFSET + (reg << ADC_AD7173_REG_POS), &local_data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read register! (0x%X)", ADC_AD7173_REG_OFFSET);
    }
  }

  /* Wait for transfer complete*/
  if (ENOERR == status) {
    for (loop_cnt = 0; loop_cnt < ADC_POLLS_UNTIL_BUSY; loop_cnt++) {
      status = adc_get_status(&adc_status);
      if (ENOERR != status) {
        DBG(DBG_SEVERITY_ERR, "Failed to read register! (0x%X)", ADC_STATUS_REG);
        break;
      }

      if (adc_status & ADC_INT_SOURCE_TRANSFER_MASK) {
        /* Transfer complete */
        break;
      }
    }

    if (loop_cnt == ADC_POLLS_UNTIL_BUSY) {
      status = EIO;
      DBG(DBG_SEVERITY_ERR, "ADC busy!");
    }
  }

  /* Read data register */
  if (ENOERR == status) {
    status = adc_read_reg(ADC_RX_DATA_REG, &local_data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read register! (0x%X)", ADC_RX_DATA_REG);
    }
  }

  /* Copy data */
  if (ENOERR == status) {
    *data = local_data;
  }

  return status;
}

/* Name:        adc_write_chip_reg
 * @brief       Write a chip register.
 * @param[in]   reg          Register to write.
 * @param[in]   data         Buffer to write data from.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
static uint32_t adc_write_chip_reg(const uint32_t reg, const uint32_t data)
{
  uint32_t status;
  uint8_t adc_status;
  uint32_t loop_cnt;

  status = adc_get_status(&adc_status);
  if (ENOERR != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to read register! (0x%X)", ADC_STATUS_REG);
  }

  /* Write register */
  status = adc_write_reg(ADC_AD7173_REG_OFFSET + (reg << ADC_AD7173_REG_POS), data);
  if (ENOERR != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to write register! (0x%X)", ADC_AD7173_REG_OFFSET);
  }

  /* Wait for transfer complete*/
  if (ENOERR == status) {
    for (loop_cnt = 0; loop_cnt < ADC_POLLS_UNTIL_BUSY; loop_cnt++) {
      status = adc_get_status(&adc_status);
      if (ENOERR != status) {
        DBG(DBG_SEVERITY_ERR, "Failed to read register! (0x%X)", ADC_STATUS_REG);
        break;
      }

      if (adc_status & ADC_INT_SOURCE_TRANSFER_MASK) {
        /* Transfer complete */
        break;
      }
    }

    if (loop_cnt == ADC_POLLS_UNTIL_BUSY) {
      status = EIO;
      DBG(DBG_SEVERITY_ERR, "ADC busy!");
    }
  }

  return status;
}

/* Name:        adc_double_read_chip_reg
 * @brief       Double read a chip register.
 * @param[in]   reg          Register to read.
 * @param[in]   data         Buffer to read data into.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
static uint32_t adc_double_read_chip_reg(const uint32_t reg,
                                         uint32_t * const data)
{
  uint32_t status;
  uint32_t first_data;
  uint32_t second_data;

  /* Read register */
  status = adc_read_chip_reg(reg, &first_data);
  if (ENOERR != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to read register! (0x%lX)", reg);
  }

  /* Perform a read-back */
  if (ENOERR == status) {
    status = adc_read_chip_reg(reg, &second_data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read register! (0x%lX)", reg);
    }
    else if (first_data != second_data) {
      status = EIO;
      DBG(DBG_SEVERITY_ERR,
          "Failed to verify register! (0x%lX) (0x%lX != 0x%lX)",
          reg, first_data, second_data);
    }
    else {
      /* Fall-through */
    }
  }

  if (ENOERR == status) {
    *data = first_data;
  }

  return status;
}

/* Name:        adc_verify_write_chip_reg
 * @brief       Write and verify a chip register.
 * @param[in]   reg          Register to write.
 * @param[in]   data         Buffer to write data from.
 * @return      ENOERR       Operation was successful.
 *              EINVAL       Invalid parameter.
 *              EIO          Input/output error.
 */
static uint32_t adc_verify_write_chip_reg(const uint32_t reg,
                                          const uint32_t data)
{
  uint32_t status;
  uint32_t local_data;

  /* Write register */
  status = adc_write_chip_reg(reg, data);
  if (ENOERR != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to write register! (0x%lX)", reg);
  }

  /* Perform a read-back */
  if (ENOERR == status) {
    status = adc_read_chip_reg(reg, &local_data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read register! (0x%lX)", reg);
    }
    else if (data != local_data) {
      status = EIO;
      DBG(DBG_SEVERITY_ERR,
          "Failed to verify register! (0x%lX) (0x%lX != 0x%lX)",
          reg, data, local_data);
    }
    else {
      /* Fall-through */
    }
  }

  return status;
}

uint32_t adc_init(void)
{
  uint32_t status;
  uint32_t data;
  uint8_t channel;


  adc_set_clock_divisor(4);
  adc_set_sample_rate(7);

  /* Verify chip ID */
  status = adc_read_chip_reg(AD7173_ID_REG, &data);
  if (ENOERR != status) {
    status = EIO;
    DBG(DBG_SEVERITY_ERR, "Failed to read chip ID!");
  }
  else if ((data & AD7173_ID_MASK) != AD7173_ID) {
    status = EIO;
    DBG(DBG_SEVERITY_ERR, "Failed to verify chip ID! (ID = 0x%lX)", data);
  }
  else {
    /* Fall-through */
  }

  /* Setup interface mode register */
  if (ENOERR == status) {
    status = adc_verify_write_chip_reg(AD7173_IFMODE_REG,
                                       (AD7173_IFMODE_HIDE_DELAY_N   |
                                        AD7173_IFMODE_DATA_STAT      |
                                        AD7173_IFMODE_CRC_EN_DISABLE) );
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to setup interface mode register!");
    }
  }

  /* Setup GPIO configuration register */
  if (ENOERR == status) {
    status = adc_verify_write_chip_reg(AD7173_GPIOCON_REG, 0U);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to setup GPIO configuration register!");
    }
  }

  /* Setup configuration register */
  if (ENOERR == status) {
    status = adc_verify_write_chip_reg(AD7173_SETUPCON0_REG,
                                       (AD7173_SETUPCONX_REF_BUF_X_ENABLED |
                                        AD7173_SETUPCONX_AIN_BUF_X_ENABLED |
                                        AD7173_SETUPCONX_REF_SELX_EXT_REF) );
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to setup configuration register!");
    }
  }

  /* Setup filter register */
  if (ENOERR == status) {
    status = adc_verify_write_chip_reg(AD7173_FILTCON0_REG, AD7173_FILTCONX_ODRX_31250SPS);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to setup filter register!");
    }
  }

  for (channel = 0; channel < ADC_NUM_CHANNELS; channel++) {

    /* Setup channel register */
    if (ENOERR == status) {
      status = adc_verify_write_chip_reg(AD7173_CH0_REG + channel,
                                         (AD7173_CHX_SETUP_SELX_0             |
                                          (channel << AD7173_CHX_AINPOSX_POS) |
                                          AD7173_CHX_AINNEGX_REF_N) );
      if (ENOERR != status) {
        DBG(DBG_SEVERITY_ERR, "Failed to setup channel register! (CH%d)", channel);
      }
    }
  }

  /* Setup ADC mode register */
  if (ENOERR == status) {
    status = adc_verify_write_chip_reg(AD7173_ADCMODE_REG,
                                       (AD7173_ADCMODE_DELAY_0US        |
                                        AD7173_ADCMODE_MODE_CONT_CONV   |
                                        AD7173_ADCMODE_CLOCKSEL_INT_OSC) );
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to setup ADC mode register!");
    }
  }

  return status;
}

AAC_INTERRUPT_HANDLER_SIGNATURE(adc_isr, arg)
{
  /* Interrupt cleared in callback */
  if(NULL != adc_callbacks.adc_interrupt_callback) {
    adc_callbacks.adc_interrupt_callback(arg);
  }
}

uint32_t adc_enable_channel(const uint8_t channel)
{
  uint32_t status = ENOERR;
  uint32_t data;

  /* Check parameter */
  if (channel >= ADC_NUM_CHANNELS) {
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR, "Failed to validate parameter! (channel = %d)", channel);
  }

  /* Read channel register */
  if (ENOERR == status) {
    status = adc_double_read_chip_reg(AD7173_CH0_REG + channel, &data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read channel register! (CH%d)", channel);
    }
  }

  /* Enable and write channel register */
  if (ENOERR == status) {
    data |= AD7173_CHX_CH_ENX;
    status = adc_verify_write_chip_reg(AD7173_CH0_REG + channel, data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to write channel register! (CH%d)", channel);
    }
  }

  return status;
}

uint32_t adc_disable_channel(const uint8_t channel)
{
  uint32_t status = ENOERR;
  uint32_t data;

  /* Check parameter */
  if (channel >= ADC_NUM_CHANNELS) {
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR, "Failed to validate parameter! (channel = %d)", channel);
  }

  /* Read channel register */
  if (ENOERR == status) {
    status = adc_double_read_chip_reg(AD7173_CH0_REG + channel, &data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read channel register! (CH%d)", channel);
    }
  }

  /* Disable and write channel register */
  if (ENOERR == status) {
    data &= ~AD7173_CHX_CH_ENX;
    status = adc_verify_write_chip_reg(AD7173_CH0_REG + channel, data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to write channel register! (CH%d)", channel);
    }
  }

  return status;
}

uint32_t adc_set_clock_divisor(const uint8_t divisor)
{
  uint32_t status = ENOERR;
  uint32_t data;

  /* Set and write divisor register */
  if (ENOERR == status) {
    data = divisor;
    status = adc_write_reg(ADC_SPI_DIVISOR_REG, data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to write divisor register!");
    }
  }

  return status;
}

uint32_t adc_get_clock_divisor(uint8_t * const divisor)
{
  uint32_t status = ENOERR;
  uint32_t data;

  /* Check parameter */
  if (NULL == divisor) {
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR, "Failed to validate parameter!");
  }

  /* Read divisor register */
  if (ENOERR == status) {
    status = adc_read_reg(ADC_SPI_DIVISOR_REG, &data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read divisor register!");
    }
  }

  if (ENOERR == status) {
    *divisor = data;
  }

  return status;
}

uint32_t adc_set_sample_rate(const uint32_t sample_rate)
{
  uint32_t status = ENOERR;
  uint32_t data;

  /* Check parameter */
  if ( (sample_rate > ADC_SPS_1_25) ||
       ( (sample_rate > ADC_SPS_31250) && (sample_rate < ADC_SPS_15625) ) ){
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR,
        "Failed to validate parameter! (sample_rate = %ld)",
        sample_rate);
  }

  /* Read filter register */
  if (ENOERR == status) {
    status = adc_double_read_chip_reg(AD7173_FILTCON0_REG, &data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read filter register!");
    }
  }

  /* Set and write divisor register */
  if (ENOERR == status) {
    data &= ~AD7173_FILTCONX_ODRX_MASK;
    data |= (sample_rate & AD7173_FILTCONX_ODRX_MASK);
    status = adc_verify_write_chip_reg(AD7173_FILTCON0_REG, data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to write filter register!");
    }
  }

  return status;
}

uint32_t adc_get_sample_rate(uint32_t * const sample_rate)
{
  uint32_t status = ENOERR;
  uint32_t data;

  /* Check parameter */
  if (NULL == sample_rate) {
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR, "Failed to validate parameters!");
  }

  /* Read filter register */
  if (ENOERR == status) {
    status = adc_double_read_chip_reg(AD7173_FILTCON0_REG, &data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read filter register!");
    }
  }

  if (ENOERR == status) {
    *sample_rate = (data & AD7173_FILTCONX_ODRX_MASK);
  }

  return status;
}

uint32_t adc_get_status(uint8_t * const adc_status)
{
  uint32_t status = ENOERR;
  uint32_t status_reg;

  /* Check parameter */
  if (NULL == adc_status) {
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR, "Failed to validate parameters!");
  }

  /* Read interrupt status register */
  if (ENOERR == status) {
    status = adc_read_reg(ADC_INT_SOURCE_REG, &status_reg);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read interrupt source register!");
    }
  }

  /* Clear status bits */
  if (ENOERR == status) {
    status = adc_write_reg(ADC_INT_SOURCE_REG, status_reg);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to write interrupt source register!");
    }
  }

  if (ENOERR == status) {
    *adc_status = status_reg;
  }


  return status;
}

uint32_t adc_int_enable(const uint8_t int_source)
{
  uint32_t status = ENOERR;
  uint32_t int_en_reg = 0;

  /* Check parameters */
  if (0U == (int_source & ADC_INT_TRANSFER_COMPLETE) ) {
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR, "Failed to validate interrupt source (0x%X)!", int_source);
  }

  if (ENOERR == status) {
    status = adc_read_reg(ADC_INT_ENABLE_REG, &int_en_reg);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read interrupt enable register!");
    }
    int_en_reg |= (int_source & ADC_INT_ENABLE_TRANSFER_MASK);
  }

  if (ENOERR == status) {
    status = adc_write_reg(ADC_INT_ENABLE_REG, int_en_reg);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to write interrupt enable register!");
    }
  }

  return status;
}

uint32_t adc_int_disable(const uint8_t int_source)
{
  uint32_t status = ENOERR;
  uint32_t int_en_reg = 0;

  /* Check parameters */
  if (0U == (int_source & ADC_INT_TRANSFER_COMPLETE) ) {
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR, "Failed to validate interrupt source (0x%X)!", int_source);
  }

  if (ENOERR == status) {
    status = adc_read_reg(ADC_INT_ENABLE_REG, &int_en_reg);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read interrupt enable register!");
    }
    int_en_reg &= ~(int_source & ADC_INT_ENABLE_TRANSFER_MASK);
  }

  if (ENOERR == status) {
    status = adc_write_reg(ADC_INT_ENABLE_REG, int_en_reg);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to write interrupt enable register!");
    }
  }

  return status;
}

uint32_t adc_start_conversion(void)
{
  uint32_t status;

  status = adc_write_reg(ADC_SPI_CONTROL_REG, 1);
  if (ENOERR != status) {
    DBG(DBG_SEVERITY_ERR, "Failed to write control register!");
  }

  return status;
}

uint32_t adc_get_conversion(uint32_t * const buffer, const uint32_t count)
{
  uint32_t status = ENOERR;
  uint32_t local_data;

  /*FIXME: Unused for now */
  (void)count;

  /* Check parameter */
  if (NULL == buffer) {
    status = EINVAL;
    DBG(DBG_SEVERITY_ERR, "Failed to validate parameters!");
  }

  /* Read channel data register */
  if (ENOERR == status) {
    status = adc_read_reg(ADC_CHANNEL_DATA_REG, &local_data);
    if (ENOERR != status) {
      DBG(DBG_SEVERITY_ERR, "Failed to read channel data register!");
    }
  }

  if (ENOERR == status) {
    *buffer = local_data;
  }

  return status;
}
