/*
 * Copyright (C) 2017 ÅAC Microtec AB
 *
 * Filename:     test_helpers.h
 * Module name:  common
 *
 * Author(s):    MaWe, ErZa
 * Support:      support@aacmicrotec.com
 * Description:  Test helper definitions
 * Requirements:
 */

#ifndef TEST_HELPERS_
#define TEST_HELPERS_

#include <stdint.h>

/* addresses for SoC info */
#define RTL_TIME_STAMP 0xC1000000
#define RTL_TYPE       0xC1000004
#define RTL_VERSION    0xC1000008

/** possible values for RTL_TYPE */
typedef enum {
  SOC_ID_OBC_S_BB = 0,
  SOC_ID_OBC_SR,
  SOC_ID_OBC_S_FM = 0x08,
  SOC_ID_OBC_SR_FM,
  SOC_ID_TCM_S_BB = 0x10,
  SOC_ID_TCM_SR,
  SOC_ID_TCM_S_FM = 0x18,
  SOC_ID_TCM_SR_FM
} soc_t;

/** mask to determine FM/BB from RTL_TYPE */
#define IS_FM_MASK  (1 << 3)
/** mask to determine TCM/OBC from RTL_TYPE */
#define IS_TCM_MASK (1 << 4)

/**
 * @brief Print PASS or FAIL at test end/abort and then loop indefinitely
 * @param [in] test_res Status code, will generate FAIL plus error code printout if non-zero
 */
void finish(int32_t test_res);

/**
 * @brief Print System-on-Chip info
 *
 * Prints timestamp, version and breadboard/flight-model type for
 * System-on-Chip, and prints timestamp for application.
 */
void print_soc_info(void);

#endif
