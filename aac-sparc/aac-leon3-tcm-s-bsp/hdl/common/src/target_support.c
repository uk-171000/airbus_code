/*-----------------------------------------------------------------------------
 * Copyright (C) 2018 ÅAC Microtec AB
 *
 * Filename:     target_support.c
 * Module name:  target_support
 *
 * Author(s):    Pebr
 * Support:      support@aacmicrotec.com
 * Description:  Holds bare-metal globals 
 * Requirements: -
 *----------------------------------------------------------------------------*/
#include <target_support.h>

/* Definitons required for LEON3 bare metal */
uint32_t _or1k_board_clk_freq = AAC_BSP_CLOCK_FREQ;

uint32_t _uart0_base = AAC_BSP_UART0_BASE;
uint32_t _uart0_IRQ = AAC_BSP_UART0_IRQ;
uint32_t _uart1_base = AAC_BSP_UART1_BASE;
uint32_t _uart1_IRQ = AAC_BSP_UART1_IRQ;
uint32_t _uart2_base = AAC_BSP_UART2_BASE;
uint32_t _uart2_IRQ = AAC_BSP_UART2_IRQ;
uint32_t _uart3_base = AAC_BSP_UART3_BASE;
uint32_t _uart3_IRQ = AAC_BSP_UART3_IRQ;

uint32_t _ccsds_base = AAC_BSP_CCSDS_BASE;
uint32_t _ccsds_IRQ = AAC_BSP_CCSDS_IRQ;

uint32_t _wd_base = AAC_BSP_WDT_BASE;

uint32_t _nvram_base = AAC_BSP_NVRAM_BASE;

uint32_t _sysflash_base = AAC_BSP_SYSFLASH_BASE;
uint32_t _sysflash_IRQ = AAC_BSP_SYSFLASH_IRQ;
uint32_t _errorman_IRQ = AAC_BSP_ERRMAN_IRQ;

uint32_t _errmgr_base = AAC_BSP_ERRMAN_BASE;
uint32_t _errmgr_IRQ = AAC_BSP_ERRMAN_IRQ;

uint32_t _massmem_base = AAC_BSP_MASSMEM_BASE;
uint32_t _massmem_IRQ = AAC_BSP_MASSMEM_IRQ;

uint32_t _scet_base = AAC_BSP_SCET_BASE;
uint32_t _scet_IRQ = AAC_BSP_SCET_IRQ;
