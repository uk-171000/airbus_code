/*-----------------------------------------------------------------------------
 * Copyright (C) 2015 - 2016 ÅAC Microtec AB
 *
 * Filename:     toolchain_support.h
 * Module name:  toolchain_support
 *
 * Author(s):    InSi, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  Common declarations for supporting both bare-metal and RTEMS
 * Requirements: -
 *----------------------------------------------------------------------------*/

#ifndef __TOOLCHAIN_SUPPORT_H__
#define __TOOLCHAIN_SUPPORT_H__

#include <assert.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define SUCCESS 0

#ifdef __rtems__
  /* This section is valid only under RTEMS */
  #include <rtems.h>
  #include <rtems/score/cpu.h>
  #include <bspopts.h>

  #define REG8(addr)  *((volatile uint8_t *) (addr))
  #define REG16(addr) *((volatile uint16_t *) (addr))
  #define REG32(addr) *((volatile uint32_t *) (addr))

  /* For printk(), needed to compile bare-metal in rtems with DBG(). */
  #include <rtems/bspIo.h>

  #define PRINTX printk

  #if !defined(DISABLE_DEBUG_OUTPUT) && defined(BSP_AAC_DISABLE_DEBUG_OUTPUT)
    #define DISABLE_DEBUG_OUTPUT
  #endif

  /* The interrupt handler signature is different for gaisler bcc2 compared to or1k bare
   * metal and rtems, this macro is used to allow a single definition without ifdef:s in
   * driver modules.
   */
  #define AAC_INTERRUPT_HANDLER_SIGNATURE(name, arg) \
    void (name)(void *(arg))
#elif __linux__
  #define PRINTX printf
#else
  /* This section is valid only under bare-metal */
  #include <stdio.h>
  #include <string.h>

#ifdef __or1k__
  #include <or1k-support.h>
  #include <or1k-sprs.h>
  #define aac_interrupt_handler_add(irq, handler, arg) or1k_interrupt_handler_add(irq, handler, arg)
  #define aac_global_interrupts_enable(level) or1k_global_interrupts_enable(level)
  #define aac_global_interrupts_disable() or1k_global_interrupts_disable()
  #define aac_interrupt_disable(irq) or1k_interrupt_disable(irq)
  #define aac_interrupt_enable(irq) or1k_interrupt_enable(irq)
  #define aac_interrupts_enable() or1k_interrupts_enable()
  #define aac_interrupts_disable() or1k_interrupts_disable()
  #define aac_dcache_disable() or1k_dcache_disable();
  #define aac_icache_disable() or1k_icache_disable();

  /* The interrupt handler signature is different for gaisler bcc2 compared to or1k bare
   * metal and rtems, this macro is used to allow a single definition without ifdef:s in
   * driver modules.
   */
  #define AAC_INTERRUPT_HANDLER_SIGNATURE(name, arg) \
    void (name)(void *(arg))
#endif /* __or1k__ */

#ifdef __sparc__
  #include <bcc/bcc.h>
  #include <bcc/bcc_param.h>
  #include <target_support.h>

  #define aac_interrupt_handler_add(irq, handler, arg) bcc_isr_register(irq, handler, arg)
  #define aac_global_interrupts_enable(level) bcc_int_enable(level)
  #define aac_global_interrupts_disable() bcc_int_disable()
  #define aac_interrupt_disable(irq) bcc_int_mask(irq)
  #define aac_interrupt_enable(irq) bcc_int_unmask(irq)
  #define aac_interrupts_enable() bcc_int_enable(bcc_int_disable())
  #define aac_interrupts_disable() bcc_int_disable()
  #define aac_dcache_disable() leon3_dcache_disable()
  #define aac_icache_disable() leon3_icache_disable()

/* Disable of data cache on LEON3 is based on
Aeroflex Application Note UT699-AN-09 */
static inline void leon3_dcache_disable()
{
  uint32_t ccr_value;

  ccr_value = bcc_get_ccr();
  ccr_value &= 0xfffffff3;

  /* Clear bits 2,3 of CCR to disable instruction cache */
  bcc_set_ccr(ccr_value);
}

/* Disable of instruction cache on LEON3 is based on
Aeroflex Application Note UT699-AN-09 */
static inline void leon3_icache_disable()
{
  uint32_t ccr_value;

  ccr_value = bcc_get_ccr();
  ccr_value &= 0xfffffffc;

  /* Clear bits 0,1 of CCR to disable instruction cache */
  bcc_set_ccr(ccr_value);
}

  /* The interrupt handler signature is different for gaisler bcc2 compared to or1k bare
   * metal and rtems, this macro is used to allow a single definition without ifdef:s in
   * driver modules.
   */
  #define AAC_INTERRUPT_HANDLER_SIGNATURE(name, arg) \
    void (name)(void *(arg), int not_used __attribute__ ((unused)))
#endif /* __sparc__ */

  #define PRINTX printf
  #define REG8(add) *((volatile unsigned char *) (add))
  #define REG16(add) *((volatile unsigned short *) (add))
  #define REG32(add) *((volatile unsigned long *) (add))
#endif /* __rtems__ */

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#ifdef DISABLE_DEBUG_OUTPUT
  #define PRINT_ANY_DEBUG false
#else
  #define PRINT_ANY_DEBUG true
#endif

#ifdef DEBUG
  #define PRINT_DEBUG_INFO true
#else
  #define PRINT_DEBUG_INFO false
#endif

#define log_info(M, ...)                                                        \
  do {                                                                          \
    if (PRINT_ANY_DEBUG && PRINT_DEBUG_INFO)                                    \
      PRINTX("[I] (%s:%03d) " M "\r\n", __FILENAME__, __LINE__, ##__VA_ARGS__); \
  } while (0)

#define log_warn(M, ...)                                                        \
  do {                                                                          \
    if (PRINT_ANY_DEBUG)                                                        \
      PRINTX("[W] (%s:%03d) " M "\r\n", __FILENAME__, __LINE__, ##__VA_ARGS__); \
  } while (0)

#define log_err(M, ...)                                                         \
  do {                                                                          \
    if (PRINT_ANY_DEBUG)                                                        \
      PRINTX("[E] (%s:%03d) " M "\r\n", __FILENAME__, __LINE__, ##__VA_ARGS__); \
  } while (0)

#define DBG_SEVERITY_INFO 1
#define DBG_SEVERITY_WARN 2
#define DBG_SEVERITY_ERR  3

#define DBG(S, M, ...) do {                                             \
    if(S == DBG_SEVERITY_INFO)                                          \
      log_info(M, ##__VA_ARGS__);                                       \
    else if (S == DBG_SEVERITY_WARN)                                    \
      log_warn(M, ##__VA_ARGS__);                                       \
    else if (S == DBG_SEVERITY_ERR)                                     \
      log_err(M, ##__VA_ARGS__);                                        \
    else {                                                              \
      if (PRINT_ANY_DEBUG)                                              \
        PRINTX("Invalid debug severity level!");                        \
      assert(0);                                                        \
    }                                                                   \
  } while (0)

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

/* Using these over REG* macros allows for faking memory access in testing */
static inline void set_reg8(uint32_t addr, uint8_t val)
{
  REG8(addr) = val;
}

static inline void set_reg16(uint32_t addr, uint16_t val)
{
  REG16(addr) = val;
}

static inline void set_reg32(uint32_t addr, uint32_t val)
{
  REG32(addr) = val;
}

static inline uint8_t get_reg8(uint32_t addr)
{
  return REG8(addr);
}

static inline uint16_t get_reg16(uint32_t addr)
{
  return REG16(addr);
}

static inline uint32_t get_reg32(uint32_t addr)
{
  return REG32(addr);
}

/* Use this indirection to allow controlling infinite loops in tests */
//#define TRUE true

#endif /* __TOOLCHAIN_SUPPORT_H__ */
