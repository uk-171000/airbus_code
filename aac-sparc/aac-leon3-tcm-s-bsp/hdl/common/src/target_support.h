/*-----------------------------------------------------------------------------
 * Copyright (C) 2018 ÅAC Microtec AB
 *
 * Filename:     target_support.h
 * Module name:  target_support
 *
 * Author(s):    JoZe, PeBr
 * Support:      support@aacmicrotec.com
 * Description:  Common declarations for supporting bare-metal on OBC/TCM
 *               In RTEMS these are found in bsp.h
 * Requirements: -
 *----------------------------------------------------------------------------*/

#ifndef __TARGET_SUPPORT_H__
#define __TARGET_SUPPORT_H__

#include <stdint.h>

/* CLK */
#define AAC_BSP_CLOCK_FREQ     50000000 /* 50 MHz */

/* UARTs */
#define AAC_BSP_UARTDBG_BAUD   115200
#define AAC_BSP_UARTDBG_BASE   0x10000000
#define AAC_BSP_UARTDBG_IRQ    2

#define AAC_BSP_UART_COUNT     8

#define AAC_BSP_UART0_BASE     0x91000000
#define AAC_BSP_UART0_IRQ      12

#define AAC_BSP_UART1_BASE     0x92000000
#define AAC_BSP_UART1_IRQ      4

#define AAC_BSP_UART2_BASE     0x93000000
#define AAC_BSP_UART2_IRQ      5

#define AAC_BSP_UART3_BASE     0x94000000
#define AAC_BSP_UART3_IRQ      6

#define AAC_BSP_UART4_BASE     0x95000000
#define AAC_BSP_UART4_IRQ      7

#define AAC_BSP_UART5_BASE     0x96000000
#define AAC_BSP_UART5_IRQ      8

#define AAC_BSP_UART6_BASE     0x97000000
#define AAC_BSP_UART6_IRQ      9

#define AAC_BSP_UART7_BASE     0x98000000
#define AAC_BSP_UART7_IRQ      10

/* TODO: I2C */
#define AAC_BSP_I2C_COUNT      0

#define AAC_BSP_I2C0_BASE      0x9A000000
#define AAC_BSP_I2C0_IRQ       13

#define AAC_BSP_I2C1_BASE      0x9B000000
#define AAC_BSP_I2C1_IRQ       14

/* TODO: Ethernet MAC */
#define AAC_BSP_ETHMAC_BASE    0xA0000000
#define AAC_BSP_ETHMAC_IRQ     24

/* SpaceWire */
#define AAC_BSP_SPW_COUNT      1

#define AAC_BSP_SPW0_BASE      0xA1000000
#define AAC_BSP_SPW0_IRQ       22

/* TODO: SPW1 */
#define AAC_BSP_SPW1_BASE      0xA2000000
#define AAC_BSP_SPW1_IRQ       19

/* NVRAM */
#define AAC_BSP_NVRAM_BASE     0xB0000000

/* System flash */
#define AAC_BSP_SYSFLASH_BASE  0xB2000000
#define AAC_BSP_SYSFLASH_IRQ   20

/* Mass memory flash */
#define AAC_BSP_MASSMEM_BASE   0xB3000000
#define AAC_BSP_MASSMEM_IRQ    21

/* ADC controller */
#define AAC_BSP_ADC_BASE       0xB5000000
#define AAC_BSP_ADC_IRQ        11

/* ADC internal HK channels */
#define AAC_BSP_ADC_CH_1V2     8
#define AAC_BSP_ADC_CH_2V5     9
#define AAC_BSP_ADC_CH_3V3     10
#define AAC_BSP_ADC_CH_VIN     11
#define AAC_BSP_ADC_CH_IIN     12
#define AAC_BSP_ADC_CH_TEMP    13

/* GPIO */
#define AAC_BSP_GPIO_COUNT     16
#define AAC_BSP_GPIO_BASE      0xBA000000
#define AAC_BSP_GPIO_IRQ       25

/* TODO: SPI */
#define AAC_BSP_SPI_COUNT      0

#define AAC_BSP_SPI0_BASE      0xBB000000
#define AAC_BSP_SPI0_IRQ       26

#define AAC_BSP_SPI1_BASE      0xBC000000
#define AAC_BSP_SPI1_IRQ       27

/* Error manager */
#define AAC_BSP_ERRMAN_BASE    0xC0000000
#define AAC_BSP_ERRMAN_IRQ     18

/* System-on-Chip info */
#define AAC_BSP_SOCINFO_BASE   0xC1000000

/* SpaceCraft Elapsed Timer */
#define AAC_BSP_SCET_BASE      0xCA000000
#define AAC_BSP_SCET_IRQ       17

/* Watchdog timer */
#define AAC_BSP_WDT_BASE       0xCB000000

/* TODO: CCSDS */
#define AAC_BSP_CCSDS_BASE     0xE0000000
#define AAC_BSP_CCSDS_IRQ      23

/* Definitions required for LEON3 bare metal */
extern uint32_t _or1k_board_clk_freq;

extern uint32_t _uart0_base;
extern uint32_t _uart0_IRQ;
extern uint32_t _uart1_base;
extern uint32_t _uart1_IRQ;
extern uint32_t _uart2_base;
extern uint32_t _uart2_IRQ;
extern uint32_t _uart3_base;
extern uint32_t _uart3_IRQ;

extern uint32_t _ccsds_base;
extern uint32_t _ccsds_IRQ;

extern uint32_t _errmgr_base;
extern uint32_t _errmgr_IRQ;

extern uint32_t _massmem_base;
extern uint32_t _massmem_IRQ;

extern uint32_t _scet_base;
extern uint32_t _scet_IRQ;

extern uint32_t _wd_base;

#endif /* __TARGET_SUPPORT_H__ */
