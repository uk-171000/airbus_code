/******************************************************************************
 * Copyright (C) 2015 ÅAC Microtec AB
 *
 *
 * Filename: timer.c
 *
 * Author(s): Per Brolin <per.brolin@aacmicrotec.com>
 * Description: Timer functionality
 * Requirements:
 *****************************************************************************/

#ifdef LEON3
#include <bcc/bcc.h>
#else
#include <or1k-support.h>
#endif

#include <stdlib.h>
#include <math.h>

#include "timer.h"

extern uint32_t _board_clk_freq;

void timer_init(void) {}

void timer_reset(void)
{
#ifdef LEON3
  // Do nothing
#else
  or1k_timer_reset();
#endif
}

inline unsigned long int timer_ticks(void)
{
#ifdef LEON3
  return (unsigned long int) bcc_timer_get_us();
#else
  return or1k_timer_get_ticks();
#endif
}

void mdelay(const uint32_t delay_ms)
{
#ifdef LEON3
  unsigned int timeout = bcc_timer_get_us() + (delay_ms*1000);

  // wait until timer() reaches or exceeds timeout value
  while ((int32_t) (timeout - bcc_timer_get_us()) > 0) {}
#else
  or1k_timer_init(1000);
  or1k_timer_reset();
  or1k_timer_enable();

  while (delay_ms >= or1k_timer_get_ticks()) {}
#endif
}

void udelay(const uint32_t delay_us)
{
#ifdef LEON3
  unsigned int timeout = bcc_timer_get_us() + delay_us;

  // wait until timer() reaches or exceeds timeout value
  while ((int32_t) (timeout - bcc_timer_get_us()) > 0) {}
#else
  or1k_timer_init(1000000);
  or1k_timer_reset();
  or1k_timer_enable();

  while (delay_us >= or1k_timer_get_ticks()) {}
#endif
}
