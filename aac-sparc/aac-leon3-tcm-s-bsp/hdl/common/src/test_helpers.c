/*
 * Copyright (C) 2017 ÅAC Microtec AB
 *
 * Filename:     test_helpers.c
 * Module name:  common
 *
 * Author(s):    MaWe, ErZa
 * Support:      support@aacmicrotec.com
 * Description:  Test helper implementation
 * Requirements:
 */

#include "test_helpers.h"
#include "toolchain_support.h"

#include <stdio.h>
#include <inttypes.h>
#include <unistd.h>
#include <time.h>

void finish(int32_t test_res)
{
  if (test_res == 0) {
    printf("\nPASS\r\n");
  } else {
    printf("\nFAIL, error code %" PRId32 "\n", test_res);
  }

  while (true) {
    sleep(100);
  }
}

void print_soc_info(void)
{
  time_t ts;
  uint32_t soc_version;

  ts = (time_t)REG32(RTL_TIME_STAMP);
  printf("\n\nRTL time stamp: %s\n", ctime(&ts));

  soc_version = REG32(RTL_VERSION);
  printf("RTL Version: %" PRIu32 ".%" PRIu32 ".%" PRIu32 "\n",
         (soc_version & 0x00FF0000) >> 16,
         (soc_version & 0x0000FF00) >>  8,
         (soc_version & 0x000000FF) >>  0);

  printf("SoC Type: ");
  switch (REG32(RTL_TYPE) & 0xFF) {
    case SOC_ID_OBC_S_BB:
    case SOC_ID_OBC_SR:
      printf("OBC-S BB\n");
      break;

    case SOC_ID_OBC_S_FM:
    case SOC_ID_OBC_SR_FM:
      printf("OBC-S FM\n");
      break;

    case SOC_ID_TCM_S_BB:
    case SOC_ID_TCM_SR:
      printf("TCM-S BB\n");
      break;

    case SOC_ID_TCM_S_FM:
    case SOC_ID_TCM_SR_FM:
      printf("TCM-S FM\n");
      break;

    default:
      printf("Unknown\n");
      break;
  }

  printf("Binary compilation date: %s, %s\n", __DATE__, __TIME__);
}
