/******************************************************************************
 * Copyright (C) 2015 ÅAC Microtec AB
 *
 *
 * Filename: timer.h
 *
 * Author(s): Per Brolin <per.brolin@aacmicrotec.com>
 * Description: Timer functionality
 * Requirements:
 *****************************************************************************/

#ifndef _TIMER_H_
#define _TIMER_H_
#include <stdint.h>

#define TIMER_EXPIRED     ( 2 )
#define TIMER_NOT_EXPIRED ( 3 )
#define TIMER_OK          ( 0 )
#define TIMER_ERROR       ( -1 )

/*
 * Name:       timer_init
 * Description:
 * Arguments:
 * Return:
 */
void timer_init(void);

/*
 * Name:       timer_reset
 * Description:
 * Arguments:
 * Return:
 */
void timer_reset(void);

/*
 * Name:       timer_ticks
 * Description:
 * Arguments:
 * Return:
 */
unsigned long int timer_ticks(void);

/*
 * Name:       set_timer
 * Description:
 * Arguments:
 * Return:
 */
uint16_t  set_timer(double seconds);

/*
 * Name:       has_timer_expired
 * Description:
 * Arguments:
 * Return:
 */
uint16_t  has_timer_expired();


/*
 * Name:       mdelay
 * Description: Will stall the cpu for the requested number of milliseconds
 * Arguments: delay_ms Delay in ms
 */
void mdelay(const uint32_t delay_ms);

/*
 * Name:       udelay
 * Description: Will stall the cpu for the requested number of microseconds
 * Arguments: delay_us Delay in us
 */
void udelay(const uint32_t delay_us);


#endif
