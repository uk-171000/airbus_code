#------------------------------------------------------------------------------
# Copyright (C) 2019 ÅAC Microtec AB
#
# Filename:     module_support.mk
# Module name:  TCM-S
#
# Author(s):    MaWe
# Support:      support@aacmicrotec.com
# Description:  Makefile for supporting in-module develop builds using the top
#               project compiled BSP.
# Requirements: -
#------------------------------------------------------------------------------

# Since this file is included, it cannot know its own location, so the Makefile
# that includes it must give the path of it, in this case via the path to top
# level project ("superproject") root.
ifeq ($(SUPERPROJECT_ROOT),)
	$(error SUPERPROJECT_ROOT must be set before including this file)
endif
RTEMS_MAKEFILE_PATH ?= $(SUPERPROJECT_ROOT)/src/librtems/sparc-aac-rtems4.11/leon3

BARE_METAL_BSP ?= leon3
BARE_METAL_BSP_MODULES_PATH ?= $(SUPERPROJECT_ROOT)/hdl
