/******************************************************************************
* Copyright (C) 2005-2017, 2018 ÅAC Microtec AB
*
* Filename:     nvram_program.c
*
* Author(s):    PeBr, ErZa, MaWe
* Support:      support@aacmicrotec.com
* Description:  Test code for nvram_handler
* Requirements:
*****************************************************************************/

#include <stdio.h>
#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>

#include <bsp/wdt_rtems.h>
#include <bsp/toolchain_support.h>

#include "nvram_common.h"

/* nvram config specification */
#include "config.h"

#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_SPI_RAM_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_WDT_DRIVER

#define CONFIGURE_UNIFIED_WORK_AREAS
#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS SHRT_MAX
#define CONFIGURE_MAXIMUM_DRIVERS

#ifdef LEON3
#define CONFIGURE_INIT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#endif

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

/** Write area header to nvram.
 *
 * Write the area header count and size to the given offset in nvram.
 *
 * @param[in] fd Nvram file descriptor
 * @param[in] offset Byte offset in nvram
 * @param[in] element_count Element count to write
 * @param[in] element_size Element size to write
 *
 * @retval 0 Success
 * @retval -1 Failure
 */
static int32_t write_header(
    const int fd,
    const off_t offset,
    const uint16_t element_count,
    const uint16_t element_size)
{
  if (lseek(fd, offset, SEEK_SET) < 0) {
    perror("Failed to seek in NVRAM");
    return -1;
  }

  const uint32_t header =
    (element_count << NVRAM_AREA_HEADER_ELEM_COUNT_BITS) | element_size;
  assert(sizeof(header) == NVRAM_AREA_HEADER_SIZE);

  const ssize_t size = write(fd, &header, NVRAM_AREA_HEADER_SIZE);
  if (size < 0) {
    perror("Failed to write to NVRAM");
    return -1;
  }

  return 0;
}

/** Write area configuration data to nvram.
 *
 * Write the given data as the area config at the given offset in nvram
 *
 * @param[in] fd Nvram file descriptor
 * @param[in] offset Byte offset in nvram
 * @param[in] data Pointer to configuration data
 * @param[in] data_size Size of configuration data to write in bytes
 *
 * @retval 0 Success
 * @retval -1 Failure
 */
static int32_t write_data(
    const int fd,
    const off_t offset,
    const void *const data,
    const size_t size)
{
  if (lseek(fd, offset, SEEK_SET) < 0) {
    perror("Failed to seek in NVRAM");
    return -1;
  }

  if (write(fd, data, size) < 0) {
    perror("Failed to write to NVRAM");
    return -1;
  }

  return 0;
}

/** Parameter struct for write_config_with_header(). */
typedef struct {
    /** Nvram file descriptor. */
    int fd;
    /** Byte offset in nvram. */
    off_t offset;
    /** Element count for area header and for calculating configuration size */
    uint16_t element_count;
    /** Element size for area header and for calculating configuration size */
    uint16_t element_size;
    /** Pointer to configuration data */
    const void *data;
} write_config_with_header_params_t;
/** Write area header and area configuration data to nvram.
 *
 * Write the area header count and size to the given offset, and then write the given data as
 * the area config after the header. The configuratiion data size to write is calculated
 * based on the element count and size.
 *
 * @param[in] params Parameter struct, see @ref write_config_with_header_params_t
 *
 * @retval 0 Success
 * @retval -1 Failure
 */
static int32_t write_config_with_header(write_config_with_header_params_t params)
{
  if (write_header(
        params.fd,
        params.offset,
        params.element_count,
        params.element_size) != 0) {
    fprintf(stderr, "Failed to write config area header\n");
    return -1;
  }

  if (write_data(
        params.fd,
        params.offset + NVRAM_AREA_HEADER_SIZE,
        params.data,
        params.element_count * params.element_size) != 0) {
    fprintf(stderr, "Failed to write config area\n");
    return -1;
  }

  return 0;
}

static int32_t setup_mm_partitions(int fd)
{
  write_config_with_header_params_t params;

  params.fd = fd;
  params.element_count = configured_partitions;
  params.element_size = PART_CFG_SIZE;
  params.data = part_cfg;

  params.offset = UA_NVRAM_PART_CFG_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write update area massmem partition config\n");
    return -1;
  }

  params.offset = SA_NVRAM_PART_CFG_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write safe area massmem partition config\n");
    return -1;
  }

  return 0;
}

static int32_t setup_spw_paths(int fd)
{
  write_config_with_header_params_t params;

  params.fd = fd;
  params.element_count = configured_spw_paths;
  params.element_size = NVRAM_SPW_PATH_SIZE;
  params.data = spw_path;

  params.offset = UA_NVRAM_SPW_PATH_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write update area spacewire path config\n");
    return -1;
  }

  params.offset = SA_NVRAM_SPW_PATH_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write safe update area spacewire path config\n");
    return -1;
  }

  return 0;
}

static int32_t setup_tm_config(int fd)
{
  write_config_with_header_params_t params;

  params.fd = fd;
  params.element_count = NVRAM_TM_NO_OF_CONFIGS;
  params.element_size = NVRAM_TM_CONFIG_SIZE;
  params.data = tm_config;

  params.offset = UA_NVRAM_TM_CONFIG_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write update area tm config\n");
    return -1;
  }

  params.offset = SA_NVRAM_TM_CONFIG_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write safe area tm config\n");
    return -1;
  }

  return 0;
}

static int32_t setup_tc_config(int fd)
{
  write_config_with_header_params_t params;

  params.fd = fd;
  params.element_count = NVRAM_TC_NO_OF_CONFIGS;
  params.element_size = NVRAM_TC_CONFIG_SIZE;
  params.data = tc_config;

  params.offset = UA_NVRAM_TC_CONFIG_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write update area tc config\n");
    return -1;
  }

  params.offset = SA_NVRAM_TC_CONFIG_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write safe area tc config\n");
    return -1;
  }

  return 0;
}

static int32_t setup_apid_routing(int fd)
{
  write_config_with_header_params_t params;

  params.fd = fd;
  params.element_count = configured_apids;
  params.element_size = NVRAM_APID_SIZE;
  params.data = apid_cfg;

  params.offset = UA_NVRAM_APID_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write update area apid routing config\n");
    return -1;
  }

  params.offset = SA_NVRAM_APID_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write safe area apid routing config\n");
    return -1;
  }

  return 0;
}

static int32_t setup_tc_handler_apid(int fd)
{
  write_config_with_header_params_t params;

  params.fd = fd;
  params.element_count = NVRAM_TC_APID_NO_OF_CONFIGS;
  params.element_size = NVRAM_TC_APID_CONFIG_SIZE;
  params.data = tc_handler_apid_config;

  params.offset = UA_NVRAM_TC_APID_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write update area tc handler apid config\n");
    return -1;
  }

  params.offset = SA_NVRAM_TC_APID_CFG_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write safe area tc handler apid config\n");
    return -1;
  }

  return 0;
}

static int32_t setup_uart_config(int fd)
{
  write_config_with_header_params_t params;

  params.fd = fd;
  params.element_count = configured_uarts;
  params.element_size = NVRAM_UART_CONFIG_SIZE;
  params.data = uart_cfg;

  params.offset = UA_NVRAM_UART_CONFIG_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write update area uart config\n");
    return -1;
  }

  params.offset = SA_NVRAM_UART_CONFIG_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write safe area uart config\n");
    return -1;
  }

  return 0;
}

static int32_t setup_uart_routing(int fd)
{
  write_config_with_header_params_t params;

  params.fd = fd;
  params.element_count = configured_uart_routing;
  params.element_size = NVRAM_UART_ROUTING_SIZE;
  params.data = uart_routing_cfg;

  params.offset = UA_NVRAM_UART_ROUTING_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write update area uart routing config\n");
    return -1;
  }

  params.offset = SA_NVRAM_UART_ROUTING_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to write safe area uart routing config\n");
    return -1;
  }

  return 0;
}

static int32_t clear_mm_ops(int fd)
{
  write_config_with_header_params_t params;

  static const mm_nvram_op_t ops[NVRAM_MM_OP_ELEMS];

  params.fd = fd;
  params.element_count = NVRAM_MM_OP_ELEMS;
  params.element_size = NVRAM_MM_OP_ELEM_SIZE;
  params.data = ops;

  params.offset = UA_NVRAM_MM_OP_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to clear update area massmem ongoing operations\n");
    return -1;
  }

  return 0;
}

static int32_t clear_mm_write_ptrs(int fd)
{
  write_config_with_header_params_t params;

  static const mm_addr_t write_pointers[NVRAM_MM_WR_PTRS_NO_OF];

  params.fd = fd;
  params.element_count = NVRAM_MM_WR_PTRS_NO_OF;
  params.element_size = NVRAM_MM_WR_PTRS_SIZE;
  params.data = write_pointers;

  params.offset = UA_NVRAM_MM_WR_PTRS_AREA_OFFSET;
  if (write_config_with_header(params) != 0) {
    fprintf(stderr, "Failed to clear update area massmem internal write pointers\n");
    return -1;
  }

  return 0;
}

static int32_t setup_version_info(int fd)
{
  struct {
    struct {
      uint16_t item_count;
      uint16_t item_size;
    } header;
    uint32_t configured;
    uint32_t version;
  } version_info_area;

  assert(sizeof(version_info_area) == NVRAM_VER_INFO_AREA_SIZE);

  version_info_area.header.item_size = NVRAM_CONFIGURED_SIZE;
  version_info_area.header.item_count = 2;
  version_info_area.configured = NVRAM_CONFIGURED;
  version_info_area.version = NVRAM_VERSION;

  if (write_data(
        fd,
        UA_NVRAM_VER_INFO_AREA_OFFSET,
        &version_info_area,
        NVRAM_VER_INFO_AREA_SIZE) != 0) {
    fprintf(stderr, "Failed to write update area version info\n");
    return -1;
  }

  if (write_data(
        fd,
        SA_NVRAM_VER_INFO_AREA_OFFSET,
        &version_info_area,
        NVRAM_VER_INFO_AREA_SIZE) != 0) {
    fprintf(stderr, "Failed to write safe area version info\n");
    return -1;
  }

  return 0;
}

static int32_t setup_nvram(void)
{
  /* System flash bad blocks are set by nvram programmer,
   * massmem flash bad blocks are set by board_initialiser.
   */

  const int fd = open(SPI_RAM_DEVICE_NAME, O_WRONLY);
  if (fd < 0) {
    perror("Failed to open nvram for writing");
    return -1;
  }

  if (setup_uart_config(fd) != 0) {
    fprintf(stderr, "Failed to setup uart config\n");
    return -1;
  }

  if (setup_uart_routing(fd) != 0) {
    fprintf(stderr, "Failed to setup uart routing\n");
    return -1;
  }

  if (setup_mm_partitions(fd) != 0) {
    fprintf(stderr, "Failed to setup mm partitions\n");
    return -1;
  }

  if (setup_apid_routing(fd) != 0) {
    fprintf(stderr, "Failed to setup apid routing\n");
    return -1;
  }

  if (setup_spw_paths(fd) != 0) {
    fprintf(stderr, "Failed to setup spw paths\n");
    return -1;
  }

  /* TODO: Setup CPDU routing configuration */
  /* TODO: Setup TM error configuration */
  /* TODO: Setup TC error configuration */

  if (setup_tm_config(fd) != 0) {
    fprintf(stderr, "Failed to setup tm config\n");
    return -1;
  }

  if (setup_tc_config(fd) != 0) {
    fprintf(stderr, "Failed to setup tc config\n");
    return -1;
  }

  if (setup_tc_handler_apid(fd) != 0) {
    fprintf(stderr, "Failed to setup tc handler APID\n");
    return -1;
  }

  if (clear_mm_ops(fd) != 0) {
    fprintf(stderr, "Failed to clear mm pending operations\n");
    return -1;
  }

  if (clear_mm_write_ptrs(fd) != 0) {
    fprintf(stderr, "Failed to clear mm write pointers\n");
    return -1;
  }

  if (setup_version_info(fd) != 0) {
    fprintf(stderr, "Failed to setup version info\n");
    return -1;
  }

  printf("*** Program successful ***\n");
  return 0;
}

rtems_task Init(rtems_task_argument arg)
{
  int wdt_fd;
  int result;

  wdt_fd = open(RTEMS_WATCHDOG_DEVICE_NAME, O_WRONLY);
  if (wdt_fd < 0)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to open watchdog");
  }

  result = ioctl(wdt_fd, WATCHDOG_ENABLE_IOCTL, WATCHDOG_DISABLE);
  if (result < 0)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to disable watchdog");
  }

  printf("\n***************** NVRAM programming started *****************\n");
  fflush(stdout);

  if (setup_nvram() != 0) {
    printf("*** Program failed ***\n");
  }

  printf("***************** NVRAM programming finished ****************\n");
  printf("***************** System can be power cycled ****************\n");

  result = ioctl(wdt_fd, WATCHDOG_ENABLE_IOCTL, WATCHDOG_ENABLE);
  if (result < 0)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to enable watchdog");
  }

  rtems_task_delete(RTEMS_SELF);
}
