/*-------------------------------------------------------------------------------------------
 * Copyright (C) 2017 ÅAC Microtec AB
 *
 * Filename:     rtems_init.h
 * Module name:  nv_config
 *
 * Author(s):    MaWe
 * Support:      support@aacmicrotec.com
 * Description:  RTEMS configuration.
 * Requirements: -
 *-----------------------------------------------------------------------------------------*/

#ifndef RTEMS_INIT_H
#define RTEMS_INIT_H

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_MASSMEM_FLASH_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_SPI_RAM_DRIVER

#define CONFIGURE_UNIFIED_WORK_AREAS
#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS UINT16_MAX
#define CONFIGURE_MAXIMUM_DRIVERS UINT16_MAX

#ifdef LEON3
#define CONFIGURE_INIT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#endif

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#define CONFIGURE_INIT

#include <bsp.h>
#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

#endif /* RTEMS_INIT_H */
