/*-------------------------------------------------------------------------------------------
 * Copyright (C) 2017 ÅAC Microtec AB
 *
 * Filename:     board_initialiser.c
 * Module name:  nv_config
 *
 * Author(s):    MaWe
 * Support:      support@aacmicrotec.com
 * Description:  Board initialiser utility, handles massmem factory bad blocks scanning and
 *               initialisation of the bad blocks list in nvram.
 * Requirements: -
 *-----------------------------------------------------------------------------------------*/

#include "nvram.h"
#include <bsp.h>
#include <bsp/massmem_flash_rtems.h>
#include <bsp/toolchain_support.h>
#include <rtems.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <inttypes.h>
#include <errno.h>
#include <stdio.h>

void finish(rtems_fatal_code error)
{
  if (error) {
    sleep(1);
    rtems_fatal(RTEMS_FATAL_SOURCE_APPLICATION, error);
  }

  printf("Board initialisation completed successfully.\n");
  while (true) {
    sleep(100);
  }
}

int update_mm_bb_list()
{
  int fd;
  uint32_t block;
  int status;
  static uint32_t bad_blocks_list[NVRAM_MM_BB_SIZE / sizeof(uint32_t)];
  assert(NVRAM_MM_BB_SIZE * 8 == MASSMEM_BLOCKS);

  fd = open("/dev/massmem", O_RDONLY);
  if (fd < 0) {
    return -1;
  }

  for (block = 0; block < MASSMEM_BLOCKS; ++block) {
    status = ioctl(fd, MASSMEM_IO_BAD_BLOCK_CHECK, block);
    if (status == -1) {
      printf("Failed to check massmem factory bad blocks");
      return -1;
    } else if (status < 0) {
      printf("Failed to check massmem factory bad blocks: %s\n", strerror(-status));
      return -1;
    } else if (status == MASSMEM_FACTORY_BAD_BLOCK_MARKED) {
      printf("Block %" PRIu32 " is marked as factory bad block\n", block);
      const uint32_t word = block / (sizeof(uint32_t) * 8);
      const uint8_t shift = block % (sizeof(uint32_t) * 8);
      bad_blocks_list[word] |= 1 << shift;
    }
  }

  status = nvram_set_data(MM_BAD_BLOCKS_AREA, 0, sizeof(bad_blocks_list),
                          (uint8_t *)bad_blocks_list);
  if (status != sizeof(bad_blocks_list)) {
    return -1;
  }

  return 0;

}

rtems_task Init(rtems_task_argument ignored)
{
  (void)ignored;

  printf("Board initialisation started. A debugger must be connected.\n");

  if (nvram_init() != NVRAM_OK)  {
    finish(1);
  }

  /* If running on TCM, factory bad blocks shall be written to NVRAM */
  if (update_mm_bb_list() != 0) {
    finish(2);
  }

  finish(0);
}

#include "rtems_init.h"
