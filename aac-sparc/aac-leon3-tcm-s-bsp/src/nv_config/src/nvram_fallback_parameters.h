/******************************************************************************
 * Copyright (C) 2005-2018 ÅAC Microtec AB
 *
 *
 * Filename:     nvram_fallback_parameters.h
 *
 * Author(s):    PeBr
 * Support:      support@aacmicrotec.com
 * Description:  Definitions/allocation of fallback parameters
 * Requirements:
 *****************************************************************************/
#include "nvram_common.h"
#ifndef _NVRAM_FALLBACK_PARAMETERS_H_
#define _NVRAM_FALLBACK_PARAMETERS_H_

#define NVRAM_FB_NO_OF_UART_ROUTINGS    ( 7 )
#define NVRAM_FB_NO_OF_UART_CONFIGS     ( 7 )
#define NVRAM_FB_NO_OF_PART_CONFIGS     ( 3 )
#define NVRAM_FB_NO_OF_SPW_PATHS        ( 3 )
#define NVRAM_FB_NO_OF_APID_ROUTINGS    ( 4 )
#define NVRAM_FB_NO_OF_TM_CONFIGS       ( 1 )
#define NVRAM_FB_NO_OF_TC_CONFIGS       ( 1 )
#define NVRAM_FB_NO_OF_TC_APID_CONFIGS  ( 1 )

/* UART_ROUTING_AREA */
static const uint8_t fb_uart_routing_cfg[NVRAM_AREA_HEADER_SIZE +
  NVRAM_FB_NO_OF_UART_ROUTINGS * NVRAM_UART_ROUTING_SIZE] =
{
    0x00, // No of UART routings
    NVRAM_FB_NO_OF_UART_ROUTINGS,
    0x00, // Size of UART routing
    NVRAM_UART_ROUTING_SIZE,
    0x00, // UART 0
    0x00, //Address
    0x00,
    0x00,
    0x00,
    0xFF, // Ext adress
    0x00, // Index to first spw path
    0x00,
    0x01, // UART 1
    0x00, // Address
    0x00,
    0x00,
    0x01,
    0xFF, // Ext adress
    0x00, // Index to first spw path
    0x00,
    0x00, // UART 2
    0x02, // Address
    0x00,
    0x00,
    0x02,
    0xFF, // Ext adress
    0x00, // Index to first spw path
    0x00,
    0x00, // UART 3
    0x03, // Address
    0x00,
    0x00,
    0x03,
    0xFF, // Ext adress
    0x00, // Index to first spw path
    0x00,
    0x00, // UART 4
    0x04, // Address
    0x00,
    0x00,
    0x04,
    0xFF, // Ext adress
    0x00, // Index to first spw path
    0x00,
    0x00, // PSU CTRL
    0x05, // Address
    0x00,
    0x00,
    0x05,
    0xFF, // Ext adress
    0x00, // Index to first spw path
    0x00,
    0x00, // SAFE BUS
    0x06, // Address
    0x00,
    0x00,
    0x06,
    0xFF, // Ext adress
    0x00, // Index to first spw path
    0x00
};
     
/* UART_CONFIG_AREA */
static const uint8_t fb_uart_cfg[NVRAM_AREA_HEADER_SIZE +
  NVRAM_FB_NO_OF_UART_CONFIGS * NVRAM_UART_CONFIG_SIZE] =
{
    0x00, // No of UART configs
    NVRAM_FB_NO_OF_UART_CONFIGS,
    0x00, // Size of UART config
    NVRAM_UART_CONFIG_SIZE,
    0x00, // UART0
    0x08, // 115200 Baud
    0x00, // RS422
    0x00, // Reserved
    0x01, // UART1
    0x08, // 115200 Baud
    0x00, // RS422
    0x00, // Reserved
    0x02, // UART2
    0x08, // 115200 Baud
    0x00, // RS422
    0x00, // Reserved
    0x03, // UART3
    0x08, // 115200 Baud
    0x00, // RS422
    0x00, // Reserved
    0x04, // UART4
    0x08, // 115200 Baud
    0x00, // RS422
    0x00, // Reserved
    0x05, // PSU Control
    0x08, // 115200 Baud
    0x01, // RS485
    0x00, // Reserved
    0x06, // Safe Bus
    0x08, // 115200 Baud
    0x01, // RS485
    0x00 // Reserved
};
   
/* PART_CONFIGS_AREA */
static const uint8_t fb_part_cfgs[NVRAM_AREA_HEADER_SIZE +
  PART_CFG_SIZE * NVRAM_FB_NO_OF_PART_CONFIGS ] =
{
    0x00, // No of partition configs
    NVRAM_FB_NO_OF_PART_CONFIGS,
    0x00, // Size of partition config
    PART_CFG_SIZE,
    0x00, // Partition 0, Block start
    0x00,
    0x00,
    0x00,
    0x00, // Block end
    0x00,
    0x00,
    0x63,
    MM_DIRECT_MODE,
    MM_PUS_PKT,
    0x01,
    MM_SEGMENTS_32K,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00, // Partiton 1, Block start
    0x00,
    0x00,
    0x64,
    0x00, // Block end
    0x00,
    0x13,
    0x88,
    MM_CONTINUOUS_MODE,
    MM_PUS_PKT,
    0x01,
    MM_SEGMENTS_32K,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00, // Partition 2, Block start
    0x00,
    0x13,
    0x89,
    0x00, // Block end
    0x00,
    0x1D,
    0x4C,
    MM_CIRCULAR_MODE,
    MM_PUS_PKT,
    0x01,
    MM_SEGMENTS_32K,
    0x00,
    0x00,
    0x00,
    0x00,

};

/* APID_AREA */
static const uint8_t fb_apid_cfg[NVRAM_AREA_HEADER_SIZE +
  NVRAM_FB_NO_OF_APID_ROUTINGS * NVRAM_APID_SIZE] =
{
    0x00, // No of APID configs
    NVRAM_FB_NO_OF_APID_ROUTINGS,
    0x00, // Size of APID config
    NVRAM_APID_SIZE,
    // APID Routing 0
    0xC0, 0x0A,
    0xC1, 0x50,
    0x00, 0x00,
    0x00, 0x00,
    // APID Routing 1
    0x81, 0x51,
    0x83, 0x00,
    0x00, 0x00,
    0x00, 0x00,
    // APID Routing 2
    0x83, 0x01,
    0x84, 0x50,
    0x00, 0x00,
    0x00, 0x00,
    // APID Routing 3
    0x84, 0x51,
    0x86, 0x00,
    0x00, 0x00,
    0x00, 0x00,

};
      
/* SPW_PATHS_AREA */
static const uint8_t fb_spw_path[NVRAM_AREA_HEADER_SIZE +
  NVRAM_FB_NO_OF_SPW_PATHS * NVRAM_SPW_PATH_SIZE] =
{
    0x00, // No of SpW path configs
    NVRAM_FB_NO_OF_SPW_PATHS,
    0x00, // Size of SpW config
    NVRAM_SPW_PATH_SIZE,
    0x01, // SpW Path 0
    0x03,
    0xFE,
    '\0',
    0x01, // SpW Path 1
    0x01,
    0x03,
    0xFE,
    '\0',
    0x02, // SpW Path 2
    0xFE,
    '\0'
};     
       
/* TM_CONFIG_AREA */
static const uint8_t fb_tm_config[NVRAM_TM_CONFIG_SIZE + NVRAM_AREA_HEADER_SIZE] =
{
  0x00, // No of TM configs
  NVRAM_FB_NO_OF_TM_CONFIGS,
  0x00, // Size of TM config
  NVRAM_TM_CONFIG_SIZE,
  0x00,
  0xFA,
  0x00,
  NVRAM_TM_CONFIG_CLCW_ON |
  NVRAM_TM_CONFIG_FECF_ON |
  NVRAM_TM_CONFIG_MASTER_CH_ON |
  NVRAM_TM_CONFIG_IDLE_ON |
  NVRAM_TM_CONFIG_RANDOMIZER_OFF |
  NVRAM_TM_CONFIG_CONV_ENC_OFF |
  NVRAM_TM_CONFIG_RS_ENC_ON
};     

/* TC_CONFIG_AREA */
static const uint8_t fb_tc_config[NVRAM_TC_CONFIG_SIZE + NVRAM_AREA_HEADER_SIZE] =
{
  0x00, // No of TC configs
  NVRAM_FB_NO_OF_TC_CONFIGS,
  0x00, // Size of TC config
  NVRAM_TC_CONFIG_SIZE,
  0x00,
  0x00,
  0x00,
  NVRAM_TC_CONFIG_DERANDOMIZER_OFF |
  NVRAM_TC_CONFIG_BCH_DEC_ON
};  

/* TC APID CONFIG_AREA */
static const uint8_t fb_tc_apid_config[NVRAM_TC_APID_CONFIG_SIZE + NVRAM_AREA_HEADER_SIZE] =
{
  0x00, // No of TC configs
  NVRAM_FB_NO_OF_TC_APID_CONFIGS,
  0x00, // Size of TC config
  NVRAM_TC_APID_CONFIG_SIZE,
  0x00,
  0x00,
  0x00,
  0xAF
};

#endif /* _NVRAM_FALLBACK_PARAMETERS_H_ */
