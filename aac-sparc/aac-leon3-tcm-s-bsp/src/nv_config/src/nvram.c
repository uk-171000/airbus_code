/******************************************************************************
 * Copyright (C) 2005-2018 ÅAC Microtec AB
 *
 *
 * Filename:     nvram.c
 *
 * Author(s):    PeBr, InSi, MaWe, ErZa
 * Support:      support@aacmicrotec.com
 * Description:  Implementation of NVRAM-handler
 * Requirements:
 *****************************************************************************/
#include <rtems.h>
#include <fcntl.h>
#include <errno.h>
#include <inttypes.h>
#include <rtems/libio.h>
#include <rtems/seterr.h>
#include <bsp/spi_ram_rtems.h>
#include <bsp/error_manager_rtems.h>
#include <bsp/toolchain_support.h>
#include "nvram.h"
#include "nvram_fallback_parameters.h"

#define NVRAM_NAME_RDWR_SEM rtems_build_name('N', 'V', 'S', 'R')

#define NVRAM_SEM_ATTR         (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_SIMPLE_BINARY_SEMAPHORE)
#define NVRAM_BLOCKS_PER_WORD  ( 32 )
#define NVRAM_FALLBACK_INIT_NOT_ACTIVE  ( 0 )
#define NVRAM_FALLBACK_INIT_ACTIVE      ( 1 )

/* Both system and massmem flash has the same amount of blocks (page size
 * differs, but that doesn't matter here).
 */
#define FLASH_LAST_BLOCK_N  ( 8192 - 1 )

/* Data-type to keep track of which parameters are using fallback
solution. This data is intended to export as HK data later on...*/
typedef struct
{
  uint8_t part_configs_fb_active;
  uint8_t uart_routing_fb_active;
  uint8_t uart_configs_fb_active;
  uint8_t spw_paths_fb_active;
  uint8_t apid_routing_fb_active;
  uint8_t tm_config_fb_active;
  uint8_t tc_config_fb_active;
  uint8_t tc_apid_config_fb_active;
} nv_area_fallback_active_t;

int fd_nvram;
rtems_id nvram_sem;
static uint32_t area_fw_offset;

static nv_area_fallback_active_t nv_area_fb_active =
{
  .part_configs_fb_active = NVRAM_FALLBACK_INIT_NOT_ACTIVE,
  .uart_routing_fb_active = NVRAM_FALLBACK_INIT_NOT_ACTIVE,
  .uart_configs_fb_active = NVRAM_FALLBACK_INIT_NOT_ACTIVE,
  .spw_paths_fb_active = NVRAM_FALLBACK_INIT_NOT_ACTIVE,
  .apid_routing_fb_active = NVRAM_FALLBACK_INIT_NOT_ACTIVE,
  .tm_config_fb_active = NVRAM_FALLBACK_INIT_NOT_ACTIVE,
  .tc_config_fb_active = NVRAM_FALLBACK_INIT_NOT_ACTIVE,
  .tc_apid_config_fb_active = NVRAM_FALLBACK_INIT_NOT_ACTIVE,
};


/* Define for Error Manager register holding running FW version */
/* This is a temporary solution until errman-driver is fixed
to allow multiple users */
extern uint32_t _errmgr_base;
#define ERRMAN_RUNNING_FW_REG  (_errmgr_base + ERRMAN_RUNFW)

/* Local functions */
static int32_t write_to_nvram(off_t offset, const void *data, ssize_t no_of_bytes);
static int32_t read_from_nvram(off_t offset, void *data, ssize_t no_of_bytes);
static int32_t rel_sem(void);
static off_t get_area_offset(nvram_area_t area);
static int32_t nvram_get_fb_data(nvram_area_t area, uint16_t offset,
  uint16_t length, uint8_t *buf);
static int32_t nvram_get_fb_size(nvram_area_t area, uint16_t *elem_size,
  uint16_t * no_of_elems);
static int32_t nvram_set_area_fb_status(nvram_area_t area);
static int32_t nvram_get_area_fb_status(nvram_area_t area);

static int32_t write_to_nvram(off_t offset, const void *data, ssize_t no_of_bytes)
{
  int32_t ret_val = NVRAM_ERROR;
  off_t loc_offset;
  ssize_t bytes_written;

  rtems_status_code status;

  /* TODO: Decide semaphore time-out!! */
  status = rtems_semaphore_obtain(nvram_sem,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);
  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR,
        "Failed to obtain semaphore nvram_sem (%08X:%s).",
        status, rtems_status_text(status));
    errno = EAGAIN;
    ret_val = NVRAM_ERROR;
    return ret_val;
  }

  if(fd_nvram > 0)
  {
    /* Set offset to RD pointer */
    loc_offset = lseek(fd_nvram, offset, SEEK_SET);
    if(loc_offset != offset )
    {
      DBG(DBG_SEVERITY_ERR, "lseek of /dev/spi-ram failed");
      errno = EAGAIN;
      rel_sem();
      return NVRAM_ERROR;
    }

    /* Write data */
    bytes_written = write(fd_nvram, data, no_of_bytes);
    if(bytes_written != no_of_bytes)
    {
      DBG(DBG_SEVERITY_ERR, "write to /dev/spi-ram failed");
      errno = EAGAIN;
      rel_sem();
      return NVRAM_ERROR;
    } else
    {
      ret_val = bytes_written;
    }
  } else
  {
    DBG(DBG_SEVERITY_ERR, "Device /dev/spi-ram not valid");
    errno = EAGAIN;
    rel_sem();
    ret_val = NVRAM_ERROR;
  }

  rel_sem();
  return ret_val;
}

/** Read a chunk of data from NVRAM.
 *
 * Includes mutex protection around access to the NVRAM device.
 *
 * Includes re-writing of data if correctable EDAC errors are reported.
 *
 * @param offset NVRAM offset in bytes.
 * @param data Pointer to target buffer.
 * @param no_of_bytes Amount of bytes to read.
 *
 * @retval NVRAM_OK Success.
 * @retval NVRAM_ERROR Failure to access NVRAM.
 */
static int32_t read_from_nvram(off_t offset, void *data, ssize_t no_of_bytes)
{
  off_t loc_offset;
  ssize_t bytes_read;
  ssize_t bytes_written;
  rtems_status_code status;

  status = rtems_semaphore_obtain(nvram_sem,
                                  RTEMS_WAIT,
                                  RTEMS_NO_TIMEOUT);
  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR,
        "Failed to obtain semaphore nvram_sem (%08X:%s).",
        status, rtems_status_text(status));
    return NVRAM_ERROR;
  }

  /* Set offset to RD pointer */
  loc_offset = lseek(fd_nvram, offset, SEEK_SET);
  if(offset != loc_offset)
  {
    DBG(DBG_SEVERITY_ERR, "lseek of /dev/spi-ram failed");
    rel_sem();
    return NVRAM_ERROR;
  }

  /* Read data */
  errno = 0;
  bytes_read = read(fd_nvram, data, no_of_bytes);
  if(bytes_read != no_of_bytes)
  {
    DBG(DBG_SEVERITY_ERR, "read from /dev/spi-ram failed %s", strerror(errno));
    rel_sem();
    return NVRAM_ERROR;
  }

  if (errno == EIO)
  {
    DBG(DBG_SEVERITY_WARN, "Correctable EDAC error when reading from nvram, re-writing");
    loc_offset = lseek(fd_nvram, offset, SEEK_SET);
    if(offset != loc_offset)
    {
      DBG(DBG_SEVERITY_ERR, "lseek of /dev/spi-ram failed");
      rel_sem();
      return NVRAM_ERROR;
    }
    bytes_written = write(fd_nvram, data, no_of_bytes);
    if (bytes_written != no_of_bytes)
    {
      DBG(DBG_SEVERITY_WARN, "Error when re-writing EDAC-corrected data to nvram, skipping");
    }

    /* Correctable EDAC error is treated as a successful read, regardless if re-writing
     * succeeded or not.
     */
  }

  rel_sem();
  return NVRAM_OK;
}

static int32_t rel_sem(void)
{
  int32_t ret_val;
  rtems_status_code status;
  status = rtems_semaphore_release(nvram_sem);
  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR,
        "Failed to release semaphore nvram_sem (%08X:%s).",
        status, rtems_status_text(status));
    errno = EAGAIN;
    ret_val = NVRAM_ERROR;
  }
  return ret_val;
}

/**
 * @brief Get the address offset of an nvram area
 *
 * @param[in] area Nvram area.
 * @retval >=0 Nvram offset for given area.
 * @retval -EINVAL Invalid area.
 */
static off_t get_area_offset(nvram_area_t area)
{

  switch(area)
  {
    case SF_BAD_BLOCKS_AREA:
      return SA_NVRAM_SF_BAD_BLOCKS_AREA_OFFSET + area_fw_offset;

    case UART_ROUTING_AREA:
      return SA_NVRAM_UART_ROUTING_AREA_OFFSET + area_fw_offset;

    case UART_CONFIG_AREA:
      return SA_NVRAM_UART_CONFIG_AREA_OFFSET + area_fw_offset;

    case PART_CONFIGS_AREA:
      return SA_NVRAM_PART_CFG_AREA_OFFSET + area_fw_offset;

    case APID_AREA:
      return SA_NVRAM_APID_AREA_OFFSET + area_fw_offset;

    case SPW_PATHS_AREA:
      return SA_NVRAM_SPW_PATH_AREA_OFFSET + area_fw_offset;

    case CPDU_ROUTING_AREA:
      return SA_NVRAM_CPDU_ROUTING_AREA_OFFSET + area_fw_offset;

    case VERSION_INFO_AREA:
      return SA_NVRAM_VER_INFO_AREA_OFFSET + area_fw_offset;

    case TM_CONFIG_AREA:
      return SA_NVRAM_TM_CONFIG_AREA_OFFSET + area_fw_offset;

    case TC_CONFIG_AREA:
      return SA_NVRAM_TC_CONFIG_AREA_OFFSET + area_fw_offset;

    case TC_APID_AREA:
      return SA_NVRAM_TC_APID_CFG_OFFSET + area_fw_offset;

    case MM_WR_PTRS_AREA:
      return UA_NVRAM_MM_WR_PTRS_AREA_OFFSET;

    case MM_OP_AREA:
      return UA_NVRAM_MM_OP_AREA_OFFSET;

    case MM_BAD_BLOCKS_AREA:
      return UA_NVRAM_MM_BAD_BLOCKS_AREA_OFFSET;

    default:
      break;
  }

  DBG(DBG_SEVERITY_WARN, "Unexpected nvram area");
  return -EINVAL;
}

static int32_t nvram_get_fb_data(nvram_area_t area, uint16_t offset,
  uint16_t length, uint8_t *buf)
{

  switch(area)
  {
    case  UART_ROUTING_AREA:
      memcpy(buf, &fb_uart_routing_cfg[offset], length);
    break;

    case UART_CONFIG_AREA:
      memcpy(buf, &fb_uart_cfg[offset], length);
    break;

    case PART_CONFIGS_AREA:
      memcpy(buf, &fb_part_cfgs[offset], length);
    break;

    case APID_AREA:
      memcpy(buf, &fb_apid_cfg[offset], length);
    break;

    case SPW_PATHS_AREA:
      memcpy(buf, &fb_apid_cfg[offset], length);
    break;

    case TM_CONFIG_AREA:
      memcpy(buf, &fb_tm_config[offset], length);
    break;

    case TC_CONFIG_AREA:
      memcpy(buf, &fb_tc_config[offset], length);
    break;

    case TC_APID_AREA:
      memcpy(buf, &fb_tc_apid_config[offset], length);
    break;

    default:
      return -EINVAL;
    break;
  }

  return length;
}

static int32_t nvram_get_fb_size(nvram_area_t area, uint16_t *elem_size,
  uint16_t *no_of_elems)
{

  switch(area)
  {
    case  UART_ROUTING_AREA:
      memcpy(no_of_elems, &fb_uart_routing_cfg[0], sizeof(uint16_t));
      memcpy(elem_size, &fb_uart_routing_cfg[2], sizeof(uint16_t));
    break;

    case UART_CONFIG_AREA:
      memcpy(no_of_elems, &fb_uart_cfg[0], sizeof(uint16_t));
      memcpy(elem_size, &fb_uart_cfg[2], sizeof(uint16_t));
    break;

    case PART_CONFIGS_AREA:
      memcpy(no_of_elems, &fb_part_cfgs[0], sizeof(uint16_t));
      memcpy(elem_size, &fb_part_cfgs[2], sizeof(uint16_t));
    break;

    case APID_AREA:
      memcpy(no_of_elems, &fb_apid_cfg[0], sizeof(uint16_t));
      memcpy(elem_size, &fb_apid_cfg[2], sizeof(uint16_t));
    break;

    case SPW_PATHS_AREA:
      memcpy(no_of_elems, &fb_spw_path[0], sizeof(uint16_t));
      memcpy(elem_size, &fb_spw_path[2], sizeof(uint16_t));
    break;

    case TM_CONFIG_AREA:
      memcpy(no_of_elems, &fb_tm_config[0], sizeof(uint16_t));
      memcpy(elem_size, &fb_tm_config[2], sizeof(uint16_t));
    break;

    case TC_CONFIG_AREA:
      memcpy(no_of_elems, &fb_tc_config[0], sizeof(uint16_t));
      memcpy(elem_size, &fb_tc_config[2], sizeof(uint16_t));
    break;

    case TC_APID_AREA:
      memcpy(no_of_elems, &fb_tc_apid_config[0], sizeof(uint16_t));
      memcpy(elem_size, &fb_tc_apid_config[2], sizeof(uint16_t));
    break;

    default:
      return -EINVAL;
    break;
  }

  return NVRAM_OK;
}

static int32_t nvram_set_area_fb_status(nvram_area_t area)
{
  switch(area)
  {
    case UART_ROUTING_AREA:
      nv_area_fb_active.uart_routing_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
    break;

    case UART_CONFIG_AREA:
      nv_area_fb_active.uart_configs_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
    break;

    case PART_CONFIGS_AREA:
      nv_area_fb_active.part_configs_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
    break;

    case APID_AREA:
      nv_area_fb_active.apid_routing_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
    break;

    case SPW_PATHS_AREA:
      nv_area_fb_active.spw_paths_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
    break;

    case TM_CONFIG_AREA:
      nv_area_fb_active.tm_config_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
    break;

    case TC_CONFIG_AREA:
      nv_area_fb_active.tc_config_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
    break;

    case TC_APID_AREA:
      nv_area_fb_active.tc_apid_config_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
    break;

    default:
      return -EINVAL;
    break;
  }

  return NVRAM_OK;
}

static int32_t nvram_get_area_fb_status(nvram_area_t area)
{
  switch(area)
  {
    case UART_ROUTING_AREA:
      return nv_area_fb_active.uart_routing_fb_active;
    break;

    case UART_CONFIG_AREA:
      return nv_area_fb_active.uart_configs_fb_active;
    break;

    case PART_CONFIGS_AREA:
      return nv_area_fb_active.part_configs_fb_active;
    break;

    case APID_AREA:
      return nv_area_fb_active.apid_routing_fb_active;
    break;

    case SPW_PATHS_AREA:
      return nv_area_fb_active.spw_paths_fb_active;
    break;

    case TM_CONFIG_AREA:
      return nv_area_fb_active.tm_config_fb_active;
    break;

    case TC_CONFIG_AREA:
      return nv_area_fb_active.tc_config_fb_active;
    break;

    case TC_APID_AREA:
      return nv_area_fb_active.tc_apid_config_fb_active;
    break;

    default:
      return -EINVAL;
    break;
  }

  return NVRAM_OK;
}

int32_t nvram_init(void)
{
  rtems_status_code status;
  uint32_t running_fw;

  /*
  Get the FW version running. Safe Images will use parameter
  from Safe Area on NVRAM. Updated Image will use parameters from
  Update Area on NVRAM
  */
  running_fw = get_reg32(ERRMAN_RUNNING_FW_REG);

  if((ERRMAN_SELFW_PROGRAM_FW_FROM_POWER_ON == running_fw) ||
    (ERRMAN_SELFW_PROGRAM_FW == running_fw) ||
    (ERRMAN_SELFW_PROGRAM_FW_BACKUP_COPY == running_fw))
  {
    area_fw_offset = UA_NVRAM_OFFSET;
  } else
  {
    area_fw_offset = 0;
  }

  fd_nvram = open(SPI_RAM_DEVICE_NAME, O_RDWR);
  if(fd_nvram < 0)
  {
    DBG(DBG_SEVERITY_ERR, "Error open /dev/spi_ram: %d (%d, %s)\r\n", fd_nvram,
       errno, strerror(errno));
    errno = EAGAIN;
    return NVRAM_ERROR;
  }

  DBG(DBG_SEVERITY_INFO, "Succesfully opened /dev/spi_ram, fd_nvram: %d", fd_nvram);

  /* Create semaphore in with one position already available */
  status = rtems_semaphore_create(NVRAM_NAME_RDWR_SEM,
                                  1,
                                  NVRAM_SEM_ATTR,
                                  0,
                                  &nvram_sem);
  if(status != RTEMS_SUCCESSFUL)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to create semaphore for NVRAM. Status: %d", status);
    errno = EAGAIN;
    return NVRAM_ERROR;
  }

  return NVRAM_OK;
}

void nvram_fallback_init(void)
{
  /* Set fallback for all areas */
  nv_area_fb_active.uart_routing_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
  nv_area_fb_active.uart_configs_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
  nv_area_fb_active.part_configs_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
  nv_area_fb_active.apid_routing_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
  nv_area_fb_active.spw_paths_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
  nv_area_fb_active.tm_config_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
  nv_area_fb_active.tc_config_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
  nv_area_fb_active.tc_apid_config_fb_active = NVRAM_FALLBACK_INIT_ACTIVE;
}

int32_t nvram_get_data(nvram_area_t area, uint16_t offset, uint16_t length, uint8_t *buf)
{
  int32_t ret_val;
  off_t local_offset;

  /* Read from fallback area if active */
  if(nvram_get_area_fb_status(area) == NVRAM_FALLBACK_INIT_ACTIVE)
  {
    ret_val = nvram_get_fb_data(area, offset, length, buf);
    assert(ret_val >= 0);
    return ret_val;
  }

  local_offset = get_area_offset(area);
  if(local_offset < 0)
  {
    errno = EINVAL;
    return NVRAM_ERROR;
  }

  local_offset += offset;

  ret_val = read_from_nvram(local_offset, buf,length);
  if(ret_val == NVRAM_ERROR)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to read from NVRAM");
    errno = EAGAIN;
    ret_val = NVRAM_ERROR;

    /*  
    An error occured when reading from NVRAM, set fallback active
    for area and read fallback parameter 
    */
    if(nvram_set_area_fb_status(area) != -EINVAL)
    {
      ret_val = nvram_get_fb_data(area, offset, length, buf);
      assert(ret_val >= 0);
      return ret_val;
    }
  }

  return ret_val;
}

int32_t nvram_set_data(nvram_area_t area, uint16_t offset, uint16_t length,
                       const uint8_t *buf)
{
  int32_t ret_val;
  off_t local_offset;

  local_offset = get_area_offset(area);
  if(local_offset < 0)
  {
    errno = EINVAL;
    return NVRAM_ERROR;
  }

  local_offset += offset;

  ret_val = write_to_nvram(local_offset, buf,length);
  if(ret_val != length)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to write to NVRAM");
    errno = EAGAIN;
    ret_val = NVRAM_ERROR;
  }

  return ret_val;
}

int32_t nvram_set_block_info(nvram_area_t area, uint16_t block, uint8_t value)
{
  off_t local_offset;
  uint16_t offset = 0;
  uint32_t read_data;
  uint32_t written_data;
  uint32_t bit_num;
  int32_t ret_val;

  /* Check if block parameter is within massmem/sysflash size */
  if(block > FLASH_LAST_BLOCK_N)
  {
    errno = EINVAL;
    return NVRAM_ERROR;
  }

  /* Check if area parameter is valid */
  switch(area)
  {
    case MM_BAD_BLOCKS_AREA:
    case SF_BAD_BLOCKS_AREA:
    break;

    default:
      errno = EINVAL;
      return NVRAM_ERROR;
  }

  local_offset = get_area_offset(area);
  if(local_offset < 0)
  {
    errno = EINVAL;
    return NVRAM_ERROR;
  }

  /* Calculate the position of the 32-bit word storing the actual block */
  offset = block / NVRAM_BLOCKS_PER_WORD;
  /* Accessed to NVRAM must be 32-bit aligned */
  local_offset = local_offset + 4*offset;

  ret_val = read_from_nvram(local_offset, &read_data, sizeof(read_data));
  if(ret_val == NVRAM_ERROR)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to read from NVRAM");
    errno = EAGAIN;
    return NVRAM_ERROR;
  }

  bit_num = block % NVRAM_BLOCKS_PER_WORD;
  written_data = read_data;
  written_data |= (value << bit_num);

  ret_val = write_to_nvram(local_offset, &written_data, sizeof(written_data));
  if(ret_val != sizeof(written_data))
  {
    DBG(DBG_SEVERITY_ERR, "Failed to write to NVRAM");
    errno = EAGAIN;
    return NVRAM_ERROR;
  }

  return ret_val;
}

int32_t nvram_get_block_info(nvram_area_t area, uint16_t block)
{
  off_t local_offset;
  uint16_t offset = 0;
  uint32_t read_data;
  uint32_t bit_num;
  int32_t ret_val;

  /* Check if block parameter is within massmem/sysflash size */
  if(block > FLASH_LAST_BLOCK_N)
  {
    errno = EINVAL;
    return NVRAM_ERROR;
  }

  /* Check if area parameter is valid */
  switch(area)
  {
    case MM_BAD_BLOCKS_AREA:
    case SF_BAD_BLOCKS_AREA:
    break;

    default:
      errno = EINVAL;
      return NVRAM_ERROR;
  }

  local_offset = get_area_offset(area);
  if(local_offset < 0)
  {
    errno = EINVAL;
    return NVRAM_ERROR;
  }

  /* Calculate the position of the 32-bit word storing the actual block */
  offset = block / NVRAM_BLOCKS_PER_WORD;

  /* Accessed to NVRAM must be 32-bit aligned */
  local_offset = local_offset + 4*offset;

  ret_val = read_from_nvram(local_offset, &read_data, sizeof(read_data));
  if(ret_val == NVRAM_ERROR)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to read from NVRAM");
    errno = EAGAIN;
    return NVRAM_ERROR;
  }

  bit_num = block % NVRAM_BLOCKS_PER_WORD;
  read_data = read_data & (0x01 << bit_num);
  return (read_data >> bit_num);
}

int32_t nvram_get_size(nvram_area_t area, uint16_t *elem_size,
                       uint16_t *no_of_elems)
{
  int32_t ret_val;
  off_t local_offset;
  uint32_t area_header;
  uint32_t temp_uint32;

  /* Read from fallback area if active */
  if(nvram_get_area_fb_status(area) == NVRAM_FALLBACK_INIT_ACTIVE)
  {
    return nvram_get_fb_size(area, elem_size, no_of_elems);
  }

  local_offset = get_area_offset(area);
  if(local_offset < 0)
  {
    errno = EINVAL;
    return NVRAM_ERROR;
  }

  ret_val = read_from_nvram(local_offset, &area_header, 4);
  if(ret_val == NVRAM_ERROR)
  {
    /* 
    An error occured when reading from NVRAM, set fallback active
    for area and read fallback parameter 
    */
    if(nvram_set_area_fb_status(area) != -EINVAL)
    {
       return nvram_get_fb_size(area, elem_size, no_of_elems);
    }

    DBG(DBG_SEVERITY_ERR, "Failed to read to NVRAM");
    errno = EAGAIN;
    return NVRAM_ERROR;
  } else
  {
    /* Return no of available bytes */
    temp_uint32 = area_header;
    temp_uint32 &= 0xffff0000;
    temp_uint32 = temp_uint32 >> NVRAM_AREA_HEADER_ELEM_SIZE_BITS;
    *no_of_elems = temp_uint32;
    *elem_size = area_header & 0x0000ffff;
    ret_val = NVRAM_OK;
  }

  return ret_val;
}

int32_t nvram_get_version_info(uint32_t *const version)
{
  off_t offset;
  int32_t status;
  struct {
    struct {
      uint16_t item_size;
      uint16_t item_count;
    } header;
    uint32_t configured;
    uint32_t version;
  } version_info_area;

  assert(sizeof(version_info_area) == NVRAM_VER_INFO_AREA_SIZE);

  offset = get_area_offset(VERSION_INFO_AREA);
  /* should always succeed */
  assert(offset >= 0);

  status = read_from_nvram(offset, &version_info_area, sizeof(version_info_area));
  if(status == NVRAM_ERROR)
  {
    DBG(DBG_SEVERITY_ERR, "Failed to read from NVRAM");
    errno = EAGAIN;
    return NVRAM_ERROR;
  }

  if(version_info_area.configured != NVRAM_CONFIGURED)
  {
    DBG(DBG_SEVERITY_ERR, "NVRAM not configured");
    errno = ENOMSG;
    return NVRAM_ERROR;
  }

  *version = version_info_area.version;
  return NVRAM_OK;
}

int32_t nvram_check_version(void)
{
  uint32_t version;

  /* Check if NVRAM has been configured */
  if(nvram_get_version_info(&version) != NVRAM_OK)
  {
    switch(errno)
    {
      case EAGAIN:
        DBG(DBG_SEVERITY_ERR, "Failed to read config from NVRAM");
        return NVRAM_ERROR;

      case ENOMSG:
        DBG(DBG_SEVERITY_ERR, "NVRAM is not configured");
        return NVRAM_ERROR;

      default:
        DBG(DBG_SEVERITY_ERR, "NVRAM Unexpected error");
        errno = EAGAIN;
        return NVRAM_ERROR;
    }
  }

  if(version != NVRAM_VERSION) {
    DBG(DBG_SEVERITY_WARN,
        "NVRAM configuration version mismatch, NVRAM: %" PRIu32 ", application: %d",
        version, NVRAM_VERSION);
    errno = EBADF;
    return NVRAM_ERROR;
  }

  return NVRAM_OK;
}
