/******************************************************************************
 * Copyright (C) 2005-2018 ÅAC Microtec AB
 *
 *
 * Filename:     example.h
 *
 * Author(s):    PeBr, ErZa, MaWe
 * Support:      support@aacmicrotec.com
 * Description:  Example NVRAM setup.
 * Requirements:
 *****************************************************************************/
#ifndef _EXAMPLE_H_
#define _EXAMPLE_H_

#include <stdint.h>

#include "nvram_common.h"

/* In this example configuration, the TCM-S is connected by SpW
to three other nodes OBC_1, OBC_2 and PAYLOAD. The three different
routing paths from the TCM to the nodes are defined in the struct below. */
uint8_t spw_path[][NVRAM_SPW_PATH_SIZE] = {
  {
    0x01, 0x03, 0xFE, '\0', /* OBC_1_SPW_PATH */
  },
  {
    0x01, 0x01, 0x03, 0xFE, '\0', /* OBC_2_SPW_PATH */
  },
  {
    0x02, 0x03, 0xFE, '\0', /* PAYLOAD_SPW_PATH */
  }
};

const uint8_t configured_spw_paths = sizeof(spw_path) / NVRAM_SPW_PATH_SIZE;

#define OBC_1_SPW_PATH        (0)
#define OBC_2_SPW_PATH        (1)
#define PAYLOAD_SPW_PATH      (2)

#define APID_OBC_1_LOWER      (3)
#define APID_OBC_1_UPPER      (49)
#define APID_OBC_2_LOWER      (50)
#define APID_OBC_2_UPPER      (99)
#define APID_PAYLOAD_LOWER    (100)
#define APID_PAYLOAD_UPPER    (149)
#define APID_TCM_LOWER        (150)
#define APID_TCM_UPPER        (199)

#define APID_RANGE            (0x8000)
#define APID_INTERNAL         (0x4000)

/* Telecommands received by the TCM-S can either be routed on the
SpW network or handled by the TCM-S. The configuration configures how-to
route the commands depending on the APID */
uint16_t apid_cfg[][NVRAM_APID_SIZE / sizeof(uint16_t)] = {
  {
    /* APID in range APID_OBC_1_LOWER .. APID_OBC_1_UPPER
    will be routed on 0x01, 0x03, 0xFE (OBC1_SPW_PATH)*/
    APID_RANGE | APID_OBC_1_LOWER,
    APID_RANGE | APID_OBC_1_UPPER,
    OBC_1_SPW_PATH
  },
  {
    /* APID in range APID_OBC_2_LOWER .. APID_OBC_2_UPPER
    will be routed on 0x01, 0x01, 0x03, 0xFE (OBC_2_SPW_PATH) */
    APID_RANGE | APID_OBC_2_LOWER,
    APID_RANGE | APID_OBC_2_UPPER,
    OBC_2_SPW_PATH
  },
  {
    /* APID in range APID_PAYLOAD_LOWER .. APID_PAYLOAD_UPPER
    will be routed on 0x02, 0x03, 0xFE (PAYLOAD_SPW_PATH) */
    APID_RANGE | APID_PAYLOAD_LOWER,
    APID_RANGE | APID_PAYLOAD_UPPER,
    PAYLOAD_SPW_PATH
  },
  {
    /* APID in range APID_TCM_LOWER .. APID_TCM_UPPER
    will not be routed, but will be handled by TCM */
    APID_RANGE | APID_INTERNAL | APID_TCM_LOWER,
    APID_RANGE | APID_INTERNAL | APID_TCM_UPPER
  }
};
const uint8_t configured_apids = sizeof(apid_cfg) / NVRAM_APID_SIZE;

/* Configure one partition of each type */
mm_h_part_t part_cfg[] = {
  {
    /* partition 0 */
    .blk_start = 0,
    .blk_end = 99,
    .mode = MM_DIRECT_MODE,
    .data_type = MM_PUS_PKT,
    .vc = 1,
    .segment_size = MM_SEGMENTS_32K,
    .data_source_id = 0
  },
  {
    /* partition 1 */
    .blk_start = 100,
    .blk_end = 1099,
    .mode = MM_CONTINUOUS_MODE,
    .data_type = MM_PUS_PKT,
    .vc = 1,
    .segment_size = MM_SEGMENTS_32K,
    .data_source_id = 0
  },
  {
    /* partition 2 */
    .blk_start = 1100,
    .blk_end = 2099,
    .mode = MM_CIRCULAR_MODE,
    .data_type = MM_PUS_PKT,
    .vc = 1,
    .segment_size = MM_SEGMENTS_64K,
    .data_source_id = 0
  },
  {
    /* partition 3 */
    .blk_start = 2100,
    .blk_end = 3099,
    .mode = MM_AUTOPAD_CONTINUOUS_MODE,
    .data_type = MM_PUS_PKT,
    .vc = 1,
    .segment_size = MM_SEGMENTS_64K,
    .data_source_id = 0
  },
  {
    /* partition 4 */
    .blk_start = 3100,
    .blk_end = 4099,
    .mode = MM_AUTOPAD_CIRCULAR_MODE,
    .data_type = MM_PUS_PKT,
    .vc = 1,
    .segment_size = MM_SEGMENTS_32K,
    .data_source_id = 0
  },
};
const uint8_t configured_partitions = sizeof(part_cfg) / sizeof(*part_cfg);

uint8_t uart_cfg[][NVRAM_UART_CONFIG_SIZE] = {
  {
    0x00, /* UART0 */
    0x00, /* 1200 baud */
    0x02, /* Loopback mode */
  },
  {
    0x01, /* UART1 */
    0x01, /* 2400 baud */
    0x02, /* Loopback mode */
  },
  {
    0x02, /* UART2 */
    0x02, /* 4800 baud */
    0x02, /* Loopback mode */
  },
  {
    0x03, /* UART3 */
    0x03, /* 9600 baud */
    0x00, /* RS 422 */
  },
  {
    0x04, /* UART4 */
    0x04, /* 19200 baud */
    0x01, /* RS485 */
  },
  {
    0x05, /* PSU Ctrl */
    0x05, /* 38400 baud */
    0x01, /* RS485 */
  },
  {
    0x06, /* Safe bus */
    0x06, /* 57600 baud */
    0x01  /* RS485 */
  }
};
const uint8_t configured_uarts = sizeof(uart_cfg) / NVRAM_UART_CONFIG_SIZE;

/* Data received on the UARTS are routed to other nodes on the SpW-network
from the TCM-S by the configuration below. The fields of the elements are
Byte 0 : Uart
Byte 1 - 4: Address (RMAP)
Byte 5: Extended address (RMAP)
Byte 6: Reserved
Byte 7: SpW path to use for routing */
uint8_t uart_routing_cfg[][NVRAM_UART_ROUTING_SIZE] = {
  {
    0x00, /* UART 0 */
    0x01, /* Address */
    0x02,
    0x03,
    0x04,
    0x05, /* Ext. Address */
    0x00, /* Reserved */
    OBC_1_SPW_PATH
  },
  {
    0x01, /* UART 1*/
    0x11, /* Address */
    0x12,
    0x13,
    0x14,
    0x15, /* Ext. Address */
    0x00, /* Reserved */
    OBC_2_SPW_PATH
  },
  {
    0x02, /* UART 2 */
    0x21, /* Address */
    0x22,
    0x23,
    0x24,
    0x25, /* Ext. Address */
    0x00, /* Reserved */
    PAYLOAD_SPW_PATH
  },
  {
    0x03, /* UART 3 */
    0x31, /* Address */
    0x32,
    0x33,
    0x34,
    0x35, /* Ext. Address */
    0x00, /* Reserved */
    OBC_1_SPW_PATH
  },
  {
    0x04, /* UART 4 */
    0x41, /* Address */
    0x42,
    0x43,
    0x44,
    0x45, /* Ext. Address */
    0x00, /* Reserved */
    OBC_2_SPW_PATH
  },
  {
    0x05, /* PSU Ctrl */
    0x51, /* Address */
    0x52,
    0x53,
    0x54,
    0x55, /* Ext. Address */
    0x00, /* Reserved */
    PAYLOAD_SPW_PATH
  },
  {
    0x06, /* Safe Bus */
    0x61, /* Address */
    0x62,
    0x63,
    0x64,
    0x65, /* Ext. Address */
    0x00, /* Reserved */
    OBC_1_SPW_PATH
  }
};
const uint8_t configured_uart_routing =
  sizeof(uart_routing_cfg) / NVRAM_UART_ROUTING_SIZE;

/* The configuration of the TM path. See Sirius OBC and TCM user manual for
further info */
const uint16_t tm_config[NVRAM_TM_CONFIG_SIZE / sizeof(uint16_t)] = {
  25, /* Clk div = Sys freq / 2 / div = bitrate = 50e6 / 2 / 25 = 1 Mbps */
  NVRAM_TM_CONFIG_CLCW_ON |
  NVRAM_TM_CONFIG_FECF_ON |
  NVRAM_TM_CONFIG_MASTER_CH_ON |
  NVRAM_TM_CONFIG_IDLE_ON |
  NVRAM_TM_CONFIG_RANDOMIZER_OFF |
  NVRAM_TM_CONFIG_CONV_ENC_OFF |
  NVRAM_TM_CONFIG_RS_ENC_ON /* No-op */
};

/* The configuration of the TC path. See Sirius OBC and TCM user manual for
further info */
const uint32_t tc_config[NVRAM_TC_CONFIG_SIZE / sizeof(uint32_t)] = {
  NVRAM_TC_CONFIG_DERANDOMIZER_OFF |
  NVRAM_TC_CONFIG_BCH_DEC_ON /* No-op */
};

const uint32_t tc_handler_apid_config[NVRAM_TC_APID_CONFIG_SIZE / sizeof(uint32_t)] =
{ 175 };
#endif
