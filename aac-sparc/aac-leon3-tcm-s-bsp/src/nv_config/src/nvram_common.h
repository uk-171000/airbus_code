/******************************************************************************
* Copyright (C) 2005-2018 ÅAC Microtec AB
*
* Filename:     nvram_common.h
*
* Author(s):    PeBr, MaWe
* Support:      support@aacmicrotec.com
* Description:  Common definitions of areas, constants etc of  NVRAM
* Requirements: 204979
*****************************************************************************/
#ifndef _NVRAM_COMMON_H_
#define _NVRAM_COMMON_H_

#include <stdint.h>

/* Format version, increment on incompatible area format/offset changes. */
#define NVRAM_VERSION 4

/* Areas on NVRAM */
typedef enum {
  SF_BAD_BLOCKS_AREA,
  UART_ROUTING_AREA,
  UART_CONFIG_AREA,
  PART_CONFIGS_AREA,
  APID_AREA,
  SPW_PATHS_AREA,
  CPDU_ROUTING_AREA,
  TM_ERR_ROUTING_AREA,
  TC_ERR_ROUTING_AREA,
  VERSION_INFO_AREA,
  TM_CONFIG_AREA,
  TC_CONFIG_AREA,
  TC_APID_AREA,
  MM_OP_AREA,
  MM_WR_PTRS_AREA,
  MM_BAD_BLOCKS_AREA
} nvram_area_t;

/* Definitions for data stored in NVRAM */
/* Note that offsets and size must be 32-bit aligned....*/
/* Common */
#define NVRAM_AREA_HEADER_SIZE            ( 2 * sizeof(uint16_t) )
#define NVRAM_AREA_HEADER_ELEM_COUNT_BITS ( sizeof(uint16_t) * CHAR_BIT )
#define NVRAM_AREA_HEADER_ELEM_SIZE_BITS  ( sizeof(uint16_t) * CHAR_BIT )

/* UART Routing */
#define NVRAM_NO_OF_UARTS             ( 7 )

#define NVRAM_UART_ROUTING_SIZE       ( 8 )
#define NVRAM_UART_ROUTING_AREA_SIZE  ( NVRAM_AREA_HEADER_SIZE + \
                                        NVRAM_NO_OF_UARTS * NVRAM_UART_ROUTING_SIZE)
/* UART Config */
#define NVRAM_UART_CONFIG_SIZE        ( 4 )
#define NVRAM_UART_CONFIG_AREA_SIZE   ( NVRAM_AREA_HEADER_SIZE + \
 NVRAM_NO_OF_UARTS * NVRAM_UART_CONFIG_SIZE)
#define NVRAM_UART_SRC_SIZE           ( 1 )
#define NVRAM_UART_SRC_OFFSET         ( 0 )
#define NVRAM_UART_SPW_ADDR_SIZE      ( 4 )
#define NVRAM_UART_SPW_ADDR_OFFSET    ( NVRAM_UART_SRC_OFFSET + \
                                        NVRAM_UART_SRC_SIZE )
#define NVRAM_UART_SPW_EXTADDR_SIZE   ( 1 )
#define NVRAM_UART_SPW_EXTADDR_OFFSET ( NVRAM_UART_SPW_ADDR_OFFSET + \
                                        NVRAM_UART_SPW_ADDR_SIZE )
#define NVRAM_UART_SPW_IDX_SIZE       ( 2 )
#define NVRAM_UART_SPW_IDX_OFFSET     ( NVRAM_UART_SPW_EXTADDR_OFFSET + \
                                        NVRAM_UART_SPW_EXTADDR_SIZE )

/* Partition configs */
#define NVRAM_NO_OF_PARTITIONS      ( 16 )

typedef struct __attribute__((__packed__)) {
  uint32_t blk_start;         /** Starting block  */
  uint32_t blk_end;           /** Ending block    */
  uint8_t  mode;              /** Partition mode  */
  uint8_t  data_type;         /** Data type       */
  uint8_t  vc;                /** Virtual channel */
  uint8_t  segment_size;      /** Segment size (in 16KiB increments) */
  uint32_t data_source_id;    /** Data source identifier for the partition */
} mm_h_part_t;

#define PART_CFG_SIZE               ( sizeof(mm_h_part_t) )
#define NVRAM_PART_CFG_AREA_SIZE    ( NVRAM_AREA_HEADER_SIZE + \
                                      NVRAM_NO_OF_PARTITIONS * PART_CFG_SIZE )

/* Partition mode for use in massmem partition config */
enum {
  MM_DIRECT_MODE,
  MM_CONTINUOUS_MODE,
  MM_CIRCULAR_MODE,
  MM_AUTOPAD_CONTINUOUS_MODE,
  MM_AUTOPAD_CIRCULAR_MODE
};

/* Data type for use in massmem partition config */
enum {
  MM_PUS_PKT,
  MM_RAW_DATA
};

/* Segment size for use in massmem partition config.
 *
 * The indexing needs to start from 1 since the massmem handler utilises the segment size
 * config value as a scaling factor, that is, multiples it with 16KiB to get the segment byte
 * size.
 * */
enum {
  MM_SEGMENTS_16K = 1,
  MM_SEGMENTS_32K = 2,
  MM_SEGMENTS_64K = 4
};

/* Operation data structure */
typedef struct __attribute__((__packed__))
{
  uint32_t type;                /* Operation type */
  uint32_t block;
  uint32_t page;
} mm_nvram_op_t;

#define NVRAM_MM_OP_ELEM_SIZE       ( sizeof(mm_nvram_op_t) )

/* MM Block Markings */
#define NVRAM_MM_OP_ELEMS           ( 2 )
#define NVRAM_MM_OP_BLK_OFFSET      ( 0 )
#define NVRAM_MM_OP_TRANS_OFFSET    ( NVRAM_MM_OP_BLK_OFFSET + \
                                      NVRAM_MM_OP_ELEM_SIZE)
#define NVRAM_MM_OP_SIZE            ( NVRAM_MM_OP_ELEMS * NVRAM_MM_OP_ELEM_SIZE)
#define NVRAM_MM_OP_AREA_SIZE       ( NVRAM_AREA_HEADER_SIZE + \
                                      NVRAM_MM_OP_SIZE)

/* Operation type for use in massmem unfinished operation markers */
typedef enum {
  MM_NVRAM_OP_NONE,
  MM_NVRAM_OP_ERASE,
  MM_NVRAM_OP_WRITE,
  MM_NVRAM_OP_COPY
} mm_nvram_op_type_t;

/* APID */
#define NVRAM_NO_OF_APIDS           ( 250 )
#define NVRAM_APID_SIZE             ( 8 )
#define NVRAM_APID_AREA_SIZE        ( NVRAM_AREA_HEADER_SIZE + \
 NVRAM_NO_OF_APIDS * NVRAM_APID_SIZE)
#define NVRAM_APID_LOW_SIZE         ( 2 )
#define NVRAM_APID_LOW_OFFSET       ( 0 )
#define NVRAM_APID_HIGH_SIZE        ( 2 )
#define NVRAM_APID_HIGH_OFFSET      ( NVRAM_APID_LOW_OFFSET + NVRAM_APID_LOW_SIZE )
#define NVRAM_APID_SPW_IDX_SIZE     ( 2 )
#define NVRAM_APID_SPW_IDX_OFFSET   ( NVRAM_APID_HIGH_OFFSET + NVRAM_APID_HIGH_SIZE )
#define NVRAM_APID_RANGE            (1<<15)
#define NVRAM_APID_TCM_INTERN       (1<<14)
#define NVRAM_APID_MASK             ( 0x7FF )
#define NVRAM_APID_IS_RANGE(apid)   ((apid & NVRAM_APID_RANGE) == NVRAM_APID_RANGE)
#define NVRAM_APID_IS_TCM(apid)     ((apid & NVRAM_APID_TCM_INTERN) == NVRAM_APID_TCM_INTERN)
/* SpW paths */
#define NVRAM_SPW_PATH_SIZE         ( 8 )
#define NVRAM_NO_OF_PATHS           ( 10 )
#define NVRAM_SPW_PATH_AREA_SIZE    ( NVRAM_AREA_HEADER_SIZE + \
                                      NVRAM_NO_OF_PATHS * NVRAM_SPW_PATH_SIZE)

/* CPDU routing */
#define NVRAM_CPDU_ROUTING_SIZE       ( 8 )
#define NVRAM_CPDU_ROUTING_AREA_SIZE  ( NVRAM_AREA_HEADER_SIZE + \
                                        NVRAM_CPDU_ROUTING_SIZE)
/* TM Error routing */
#define NVRAM_TM_ERR_ROUTING_SIZE       ( 8 )
#define NVRAM_TM_ERR_ROUTING_AREA_SIZE  ( NVRAM_AREA_HEADER_SIZE + \
                                          NVRAM_TM_ERR_ROUTING_SIZE)
/* TC Error routing */
#define NVRAM_TC_ERR_ROUTING_SIZE       ( 8 )
#define NVRAM_TC_ERR_ROUTING_AREA_SIZE  ( NVRAM_AREA_HEADER_SIZE + \
                                          NVRAM_TC_ERR_ROUTING_SIZE)
/* MM Bad blocks */
#define NVRAM_MM_BB_SIZE              ( 1024 )
#define NVRAM_MM_BB_AREA_SIZE         ( NVRAM_MM_BB_SIZE )

/* SF Bad blocks */
#define NVRAM_SF_BB_SIZE              ( 1024 )
#define NVRAM_SF_BB_AREA_SIZE         ( NVRAM_SF_BB_SIZE )

/* Version info */
#define NVRAM_CONFIGURED_OFFSET       ( 0 )
#define NVRAM_CONFIGURED_SIZE         ( 4 )
#define NVRAM_CONFIGURED              ( 0xDEADBEEF )
#define NVRAM_VERSION_OFFSET          ( NVRAM_CONFIGURED_OFFSET + NVRAM_CONFIGURED_SIZE )
#define NVRAM_VERSION_SIZE            ( 4 )
#define NVRAM_VER_INFO_AREA_SIZE      ( NVRAM_AREA_HEADER_SIZE + \
                                        NVRAM_CONFIGURED_SIZE + \
                                        NVRAM_VERSION_SIZE )

/* TM Config */
#define NVRAM_TM_NO_OF_CONFIGS        ( 1 )
#define NVRAM_TM_CONFIG_SIZE          ( 4 )
#define NVRAM_TM_CONFIG_AREA_SIZE     ( NVRAM_AREA_HEADER_SIZE + \
                                        NVRAM_TM_CONFIG_SIZE )

#define NVRAM_TM_CONFIG_CLCW_ON          (1 << 0)
#define NVRAM_TM_CONFIG_FECF_ON          (1 << 1)
#define NVRAM_TM_CONFIG_MASTER_CH_ON     (1 << 2)
#define NVRAM_TM_CONFIG_IDLE_ON          (1 << 3)
#define NVRAM_TM_CONFIG_RANDOMIZER_ON    (1 << 4)
#define NVRAM_TM_CONFIG_CONV_ENC_ON      (1 << 5)
#define NVRAM_TM_CONFIG_RS_ENC_ON        (1 << 6)

#define NVRAM_TM_CONFIG_CLCW_OFF         (0 << 0)
#define NVRAM_TM_CONFIG_FECF_OFF         (0 << 1)
#define NVRAM_TM_CONFIG_MASTER_CH_OFF    (0 << 2)
#define NVRAM_TM_CONFIG_IDLE_OFF         (0 << 3)
#define NVRAM_TM_CONFIG_RANDOMIZER_OFF   (0 << 4)
#define NVRAM_TM_CONFIG_CONV_ENC_OFF     (0 << 5)
#define NVRAM_TM_CONFIG_RS_ENC_OFF       (0 << 6)

/* TC Config */
#define NVRAM_TC_NO_OF_CONFIGS        ( 1 )
#define NVRAM_TC_CONFIG_SIZE          ( 4 )
#define NVRAM_TC_CONFIG_AREA_SIZE     ( NVRAM_AREA_HEADER_SIZE + \
                                        NVRAM_TC_CONFIG_SIZE )

/* TC Config */
#define NVRAM_TC_APID_NO_OF_CONFIGS   ( 1 )
#define NVRAM_TC_APID_CONFIG_SIZE     ( 4 )
#define NVRAM_TC_APID_AREA_SIZE       ( NVRAM_AREA_HEADER_SIZE + \
                                        NVRAM_TC_APID_CONFIG_SIZE )

/* All assumed to be active high */
#define NVRAM_TC_CONFIG_DERANDOMIZER_ON  (1 << 0)
#define NVRAM_TC_CONFIG_BCH_DEC_ON       (1 << 1)

#define NVRAM_TC_CONFIG_DERANDOMIZER_OFF (0 << 0)
#define NVRAM_TC_CONFIG_BCH_DEC_OFF      (0 << 1)

/* Partition write pointers */
#define NVRAM_MM_WR_PTRS_NO_OF           ( NVRAM_NO_OF_PARTITIONS )

/* Address for use by massmem handler and internal write pointers in nvram.
 * Limited to 4 GiB logical partition size.
 */
typedef uint32_t mm_addr_t;
#define MM_ADDR_MAX UINT32_MAX
#define MM_ADDR_MIN 0
#define PRI_mm_addr_t PRIu32

#define NVRAM_MM_WR_PTRS_SIZE            ( sizeof(mm_addr_t) )
#define NVRAM_MM_WR_PTRS_AREA_SIZE       ( NVRAM_AREA_HEADER_SIZE + \
                                           NVRAM_MM_WR_PTRS_NO_OF * NVRAM_MM_WR_PTRS_SIZE )

/* Safe Area offsets */
#define SA_NVRAM_VER_INFO_AREA_OFFSET          ( 0x0000 )
#define SA_NVRAM_UART_ROUTING_AREA_OFFSET      ( 0x0010 )
#define SA_NVRAM_UART_CONFIG_AREA_OFFSET       ( 0x0050 )
#define SA_NVRAM_PART_CFG_AREA_OFFSET          ( 0x0070 )
#define SA_NVRAM_APID_AREA_OFFSET              ( 0x0174 )
#define SA_NVRAM_SPW_PATH_AREA_OFFSET          ( 0x0948 )
#define SA_NVRAM_CPDU_ROUTING_AREA_OFFSET      ( 0x0A00 )
#define SA_NVRAM_TM_ERR_ROUTING_AREA_OFFSET    ( 0x0A10 )
#define SA_NVRAM_TC_ERR_ROUTING_AREA_OFFSET    ( 0x0A20 )
#define SA_NVRAM_TM_CONFIG_AREA_OFFSET         ( 0x0A30 )
#define SA_NVRAM_TC_CONFIG_AREA_OFFSET         ( 0x0A40 )
#define SA_NVRAM_TC_APID_CFG_OFFSET            ( 0x0A50 )
#define SA_NVRAM_SF_BAD_BLOCKS_AREA_OFFSET     ( 0x0E00 )

/* Update Area offset */
#define UA_NVRAM_SF_BAD_BLOCKS_AREA_OFFSET     ( 0x1000 )
#define UA_NVRAM_OFFSET                        ( 0x1200 )
#define UA_NVRAM_VER_INFO_AREA_OFFSET          ( UA_NVRAM_OFFSET )
#define UA_NVRAM_UART_ROUTING_AREA_OFFSET      ( UA_NVRAM_OFFSET + SA_NVRAM_UART_ROUTING_AREA_OFFSET )
#define UA_NVRAM_UART_CONFIG_AREA_OFFSET       ( UA_NVRAM_OFFSET + SA_NVRAM_UART_CONFIG_AREA_OFFSET )
#define UA_NVRAM_PART_CFG_AREA_OFFSET          ( UA_NVRAM_OFFSET + SA_NVRAM_PART_CFG_AREA_OFFSET )
#define UA_NVRAM_APID_AREA_OFFSET              ( UA_NVRAM_OFFSET + SA_NVRAM_APID_AREA_OFFSET )
#define UA_NVRAM_SPW_PATH_AREA_OFFSET          ( UA_NVRAM_OFFSET + SA_NVRAM_SPW_PATH_AREA_OFFSET )
#define UA_NVRAM_CPDU_ROUTING_AREA_OFFSET      ( UA_NVRAM_OFFSET + SA_NVRAM_CPDU_ROUTING_AREA_OFFSET )
#define UA_NVRAM_TM_ERR_ROUTING_AREA_OFFSET    ( UA_NVRAM_OFFSET + SA_NVRAM_TM_ERR_ROUTING_AREA_OFFSET )
#define UA_NVRAM_TC_ERR_ROUTING_AREA_OFFSET    ( UA_NVRAM_OFFSET + SA_NVRAM_TC_ERR_ROUTING_AREA_OFFSET )
#define UA_NVRAM_TM_CONFIG_AREA_OFFSET         ( UA_NVRAM_OFFSET + SA_NVRAM_TM_CONFIG_AREA_OFFSET  )
#define UA_NVRAM_TC_CONFIG_AREA_OFFSET         ( UA_NVRAM_OFFSET + SA_NVRAM_TC_CONFIG_AREA_OFFSET )
#define UA_NVRAM_TC_APID_AREA_OFFSET           ( UA_NVRAM_OFFSET + SA_NVRAM_TC_APID_CFG_OFFSET )

/* MM Area Offset */
#define UA_NVRAM_MM_BAD_BLOCKS_AREA_OFFSET     ( 0x2000 )
#define UA_NVRAM_MM_OP_AREA_OFFSET             ( 0x2400 )
#define UA_NVRAM_MM_WR_PTRS_AREA_OFFSET        ( 0x24D0 )

#endif
