/******************************************************************************
 * Copyright (C) 2005-2018 ÅAC Microtec AB
 *
 *
 * Filename:     nvram.h
 *
 * Author(s):    PeBr
 * Support:      support@aacmicrotec.com
 * Description:  Interface file for accessing NVRAM
 * Requirements:
 *****************************************************************************/
#ifndef _NVRAM_H_
#define _NVRAM_H_
#include <rtems.h>
#include "nvram_common.h"

/* Return codes */
#define NVRAM_APID_MCM      ( 4 )
#define NVRAM_APID_SCM      ( 3 )
#define NVRAM_APID_PAYLOAD  ( 2 )
#define NVRAM_APID_TCM      ( 1 )
#define NVRAM_OK            ( 0 )
#define NVRAM_ERROR         ( -1 )
#define NVRAM_BLOCK_BAD     ( 1 )
#define NVRAM_BLOCK_OK      ( 0 )

#ifdef __cplusplus
extern "C"  {
#endif

/*
 * Name:        nvram_init
 * @brief       Initialize the NVRAM
 * @return      NVRAM_OK    All ok.
 * @return      NVRAM_ERROR with errno EAGAIN: NVRAM access failure.
 * @return      NVRAM_ERROR with errno ENOMSG: NVRAM init success, but NVRAM is not
 *              configured.
 * @return      NVRAM_ERROR with errno EBADF: NVRAM init success, but version mismatch
 *              between nvram and application
 */
int32_t nvram_init(void);

/*
 * Name:        nvram_fallback_init
 * @brief       A fallback solution that will use hard-coded, fallback
 *              parameters. This function can be called if opening
 *              of the NVRAM-device fails.    
 */
void nvram_fallback_init(void);

/** Read data from NVRAM with fallback.
 *
 * The NVRAM access is mutex-protected.
 *
 * If thew NVRAM read fails, and the area is one which has a fallback defined, the fallback
 * data will be returned.
 *
 * When returning fallback data, the size returned may be different from the size requsted,
 * and hence mismatched size should normally still be treated as a success.
 *
 * If the NVRAM read reports correctable EDAC errors, the corrected data is re-written to
 * NVRAM, and success reported, regardless if the re-writing of data succeeded or not.
 *
 * @param       area - The area on NVRAM to access
 * @param       offset - The offset within the area to access
 * @param       length - Number of bytes to read from area
 * @param       buf - read data
 *
 * @retval      >=0 Success, number of bytes read (may differ from requested size).
 * @retval      NVRAM_ERROR with errno EINVAL: Invalid parameters.
 * @retval      NVRAM_ERROR with errno EAGAIN: NVRAM access failure and no fallback
 *              available.
 */
int32_t nvram_get_data(nvram_area_t area, uint16_t offset, uint16_t length, uint8_t *buf);

/*
 * Name:        nvram_set_data
 * @brief       Writes data to NVRAM
 * @param       area - The area on NVRAM to access
 * @param       offset - The offset within the area to access
 * @param       length - Number of bytes to read from area
 * @param       buf - read data
 * @return      number of bytes written on success
 * @return      NVRAM_ERROR on error
 */
int32_t nvram_set_data(nvram_area_t area, uint16_t offset, uint16_t length,
  const uint8_t *buf);

/*
 * Name:        nvram_set_block_info
 * @brief       Sets block information of a block
 * @param       area - MM_BAD_BLOCKS_AREA or SF_BAD_BLOCKS_AREA
 * @param       block - (0-8191) the block number
 * @param       value - (0 - block OK, 1 - bad block)
 * @return      >=0 on Success
 * @return      NVRAM_ERROR on error
 */
int32_t nvram_set_block_info(nvram_area_t area, uint16_t block, uint8_t value);

/*
 * Name:        nvram_set_block_info
 * @brief       Sets block information of a block
 * @param       area - MM_BAD_BLOCKS_AREA or SF_BAD_BLOCKS_AREA
 * @param       block - (0-8191) the block number
 * @return      >=0 On succes (0 - Block OK, 1-Bad block)
 * @return      NVRAM_ERROR on error
 */
int32_t nvram_get_block_info(nvram_area_t area, uint16_t block);

/*
 * Name:        nvram_get_size
 * @brief       Initialize the NVRAM
 * @param       area - The area on NVRAM to access
 * @param       elem_size - Size of an element in the area
 * @param       no_of_elems - Number of elements in the area
 * @return      Number of available bytes on success
 * @return      NVRAM_ERROR Not ok.
 */
int32_t nvram_get_size(nvram_area_t area, uint16_t *elem_size, uint16_t * no_of_elems);

/*
 * Name:        nvram_get_version_info
 * @brief       Check if NVRAM is configured and get the version.
 * @param       version Pointer to version which will be available on success.
 * @return      NVRAM_OK Success.
 * @return      NVRAM_ERROR and errno EAGAIN: NVRAM read error.
 * @return      NVRAM_ERROR and errno ENOMSG: NVRAM is not configured.
 */
int32_t nvram_get_version_info(uint32_t *version);

/*
 * Name:        nvram_check_version
 * @brief       Check version
 * @return      NVRAM_OK    All ok.
 * @return      NVRAM_ERROR Wrong NVRAM version
 */
int32_t nvram_check_version(void);

#ifdef __cplusplus
}
#endif

#endif /* _NVRAM_H_ */
