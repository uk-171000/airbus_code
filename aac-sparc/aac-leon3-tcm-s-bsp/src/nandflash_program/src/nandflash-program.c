/*----------------------------------------------------------------------------
 * Copyright (C) 2016-2017 ÅAC Microtec AB
 *
 * Filename:     nandflash-program.c
 * Module name:  nandflash-program
 *
 * Author(s):    JuBa, DaHe, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  A program that will erase and program a NAND flash memory
 *               via the ÅAC NAND flash controller. It takes bad blocks into
 *               account.
 * Requirements: -
 *--------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdarg.h>
#include <errno.h>
#include "uart.h"
#include "spi_ram.h"
#include "system_flash.h"
#include "wdt.h"
#include "nandflash-program.h"

/* Starting page numbers for redundant boot images */
#define SAFE_IM_1                   0x00000
#define SAFE_IM_2                   0x20000
#define SAFE_IM_3                   0x40000
#define UPDATE_IM_1                 0x80000
#define UPDATE_IM_2                 0xA0000
#define UPDATE_IM_3                 0xC0000
#define NO_OF_IMAGES                6
#define AVAILABLE_BLOCKS_PER_IMAGE  1024

/* Bits, and hence blocks, per word  */
#define BB_TABLE_BLOCKS_PER_WORD (sizeof(uint32_t) * 8)

/* Length of bad block table in bytes */
#define BB_TABLE_WORDS (SYSFLASH_BLOCKS / BB_TABLE_BLOCKS_PER_WORD)

/* Bad block table position in NVRAM */
#define BB_TABLE_POS 0x0E00

/* Globals */
/* Use these as a sync between ISR callbacks and "normal" code */
volatile uint32_t sf_int_event;

extern uint32_t _nvram_base;
#define NVRAM_BASE _nvram_base

uint32_t bad_block_table[BB_TABLE_WORDS];

/*
 * Setup System Flash callback functions
 */
sysflash_callbacks_t sysflash_callbacks = {
  .sysflash_prog_page_done_irq_callback = prog_page_done,
  .sysflash_read_status_done_irq_callback = read_status_done,
  .sysflash_read_p_setup_done_irq_callback = read_page_done,
  .sysflash_erase_block_done_irq_callback = erase_block_done,
  .sysflash_reset_done_irq_callback = reset_done,
  .sysflash_set_feature_done_irq_callback = set_feature_done
};

/*
 * Setup UART callback functions
 */
uart_callbacks_t uart_callbacks = {
  .uart_rx_linestatus_callback = NULL,
  .uart_rx_data_callback = NULL,
  .uart_timeout_callback = NULL,
  .uart_tx_empty_callback = NULL,
  .uart_modem_callback = NULL
};

void finish(int test_res)
{
  while (1);
}

int main(void)
{
  uint32_t  image_length;
  uint32_t  page_programming_offset;
  uint8_t   prompt;
  uint8_t   input;
  uint32_t  block;
  int32_t   result;
  uint8_t   edac_status;
  spi_ram_config_t nvram_config;
  int i;

  wdt_disable();

#ifdef PROMPT
  prompt = 1;
#else
  prompt = 0;
#endif

#ifdef OR1K
  /* Init UART and clear any possible input */
  uart_init(UART_BASE, BAUDRATE);
  while (uart_check_for_char(UART_BASE) != 0) {
    i = uart_getc(UART_BASE);
    uart_putc(UART_BASE, i);
  }
#endif
  /* Initialise system flash */
  result = sysflash_init();
  if (result != ENOERR) {
    myprintf("Sysflash Init failed. Aborting...\r\n");
    return -1;
  }

  /* Install system flash interrupt handler (supplied by the driver) */
  aac_interrupt_handler_add(_sysflash_IRQ, sysflash_isr, NULL);
  aac_interrupt_enable(_sysflash_IRQ);
  aac_interrupts_enable();

  /* Get image length from markers in linker command file */
  image_length = (uint32_t) &end_nandprogram_data - (uint32_t) &nandprogram_data;

  myprintf("\r\n\nNAND flash programming app\r\n"
           "----------------------------\r\n\r\n");

  myprintf("Read bad block table from NVRAM\r\n\r\n");
  /* Set up NVRAM */
  spi_ram_init(NVRAM_BASE);
  nvram_config.divisor = 4;
  nvram_config.edac = SPI_RAM_EDAC_ENABLE;
  result = spi_ram_config(NVRAM_BASE, &nvram_config);
  if (result != ENOERR) {
    myprintf("Could not configure NVRAM. Aborting...\r\n\r\n");
    return -1;
  }

  /* Read bad block table from NVRAM */
  for (i = 0; i < BB_TABLE_WORDS; i++) {
    result = spi_ram_data_read(NVRAM_BASE,
                               (uint8_t *) &bad_block_table[i],
                               BB_TABLE_POS + i*sizeof(uint32_t),
                               sizeof(uint32_t));

    if (result != ENOERR) {
      myprintf("Unable to read NVRAM. Aborting...\r\n\r\n");
      return -1;
    }

    /* Check NVRAM EDAC */
    result = spi_ram_edac_status_read(NVRAM_BASE, &edac_status);

    if (result != ENOERR) {
      myprintf("Unable to read NVRAM EDAC status. Aborting...\r\n\r\n");
      return -1;
    }

    /* TODO: Retry once on EDAC multi error */
    /* TODO: What should be the default? No bad, all bad, whatever the NVRAM gives out? */
    if ((edac_status & SPI_RAM_EDAC_MULT_ERROR) != 0) {
      myprintf("Uncorrectable errors in NVRAM data word %d."
               " Assuming no bad blocks.\r\n\r\n", i);
      bad_block_table[i] = 0;
    }
  }

  /* Scan for factory marked bad blocks and merge that info into the table */
  myprintf("Scanning for factory marked bad blocks\r\n");

  for (block = 0; block < SYSFLASH_BLOCKS; block++) {
    myprintf("Block %"PRIu32"\r", block);

    result = sysflash_factory_bad_block_check(block);
    if (result < 0) {
      myprintf("\nError when checking block %"PRIu32": %"PRIi32"!\r\n",
	      block, result);
    } else if (result == SYSFLASH_FACTORY_BAD_BLOCK_MARKED) {
      myprintf("\nFound bad block: %"PRIu32"\r\n", block);

      /* Set flag in bad block table */
      bad_block_table[block / BB_TABLE_BLOCKS_PER_WORD] |=
          1 << (block % BB_TABLE_BLOCKS_PER_WORD);
    }
  }

  myprintf("\n----------------------------\r\n\r\n");

  if (prompt == 1) {
    myprintf("Press [Enter] when ready to program\r\n\r\n");

    input = 0;
#ifdef __or1k__
    while (input != '\r') {
      if (uart_check_for_char(UART_BASE) > 0) {
        input = uart_getc(UART_BASE);
        /* echo input */
        uart_putc(UART_BASE, input);
      }
    }
#endif

#ifdef __sparc__
    while (input != '\r') {
      input = getc(stdin);     
      /* echo input */
      putc(input, stdout);
    }
#endif
  }

#ifndef NO_SAFE_IM_1
  page_programming_offset = SAFE_IM_1;

  /* Program */
  program_image(page_programming_offset, (uint8_t *) &nandprogram_data,
                image_length);

  /* Verify */
  verify_image(page_programming_offset, (uint8_t *) &nandprogram_data,
               image_length);
#endif

#ifndef NO_SAFE_IM_2
  page_programming_offset = SAFE_IM_2;

  /* Program */
  program_image(page_programming_offset, (uint8_t *) &nandprogram_data,
                image_length);

  /* Verify */
  verify_image(page_programming_offset, (uint8_t *) &nandprogram_data,
               image_length);
#endif

#ifndef NO_SAFE_IM_3
  page_programming_offset = SAFE_IM_3;

  /* Program */
  program_image(page_programming_offset, (uint8_t *) &nandprogram_data,
                image_length);

  /* Verify */
  verify_image(page_programming_offset, (uint8_t *) &nandprogram_data,
               image_length);
#endif

#ifndef NO_UPDATE_IM_1
  page_programming_offset = UPDATE_IM_1;

  /* Program */
  program_image(page_programming_offset, (uint8_t *) &nandprogram_data,
                image_length);

  /* Verify */
  verify_image(page_programming_offset, (uint8_t *) &nandprogram_data,
               image_length);
#endif

#ifndef NO_UPDATE_IM_2
  page_programming_offset = UPDATE_IM_2;


  /* Program */
  program_image(page_programming_offset, (uint8_t *) &nandprogram_data,
                image_length);

  /* Verify */
  verify_image(page_programming_offset, (uint8_t *) &nandprogram_data,
               image_length);
#endif

#ifndef NO_UPDATE_IM_3
  page_programming_offset = UPDATE_IM_3;

  /* Program */
  program_image(page_programming_offset, (uint8_t *) &nandprogram_data,
                image_length);

  /* Verify */
  verify_image(page_programming_offset, (uint8_t *) &nandprogram_data,
               image_length);
#endif

  myprintf("\r\nWriting Bad Block table to NVRAM...\r\n\n");

  /* Enable write */
  result = spi_ram_write_protection(NVRAM_BASE,
      SPI_RAM_WRITE_PROTECTION_DISABLE);

  /* Write bad block table to NVRAM */
  result = spi_ram_data_write(NVRAM_BASE,
                              BB_TABLE_POS,
                              (uint8_t *) bad_block_table,
                              BB_TABLE_WORDS * sizeof(uint32_t));

  if (result != ENOERR) {
    myprintf("\r\nFailed to write Bad Block table to NVRAM!\r\n\n");
  }

  myprintf("\r\n----------------------------\r\n"
           "NAND flash programming done!\r\n");

  wdt_enable();

  /* Stop here */
  finish(0);

  return 0;
}

/* Callback functions for sysflash interrupt handler */
void reset_done(void *arg)
{
  sf_int_event = SF_RESET_DONE;
}

void read_status_done(void *arg)
{
  sf_int_event = SF_READ_STATUS_DONE;
}

void set_feature_done(void *arg)
{
  sf_int_event = SF_SET_FEATURE_DONE;
}

void erase_block_done(void *arg)
{
  sf_int_event = SF_ERASE_BLOCK_DONE;
}

void read_page_done(void *arg)
{
  sf_int_event = SF_READ_PAGE_SETUP_DONE;
}

void prog_page_done(void *arg)
{
  sf_int_event = SF_PROG_PAGE_DONE;
}

int32_t bad_block_check(uint32_t block)
{
  /* Sanity check block number */
  if (block < 0 || block > SYSFLASH_BLOCKS) {
    return -1;
  }

  /* Look up block in Bad Block table */
  if ((bad_block_table[block / BB_TABLE_BLOCKS_PER_WORD] &
       1 << (block % BB_TABLE_BLOCKS_PER_WORD)) != 0) {
    return BLOCK_BAD;
  }

  return BLOCK_OK;
}

int32_t verify_page(uint32_t page_num, uint8_t* data, uint32_t num_bytes)
{
  uint8_t  page_buf[SYSFLASH_PAGE_SIZE];
  int32_t  result;
  uint32_t i;
  uint32_t error_count;

  /* Reset event marker first */
  sf_int_event = 0;

  /* Enable interrupt */
  result = sysflash_interrupt_enable(SYSFLASH_IE_RPAGE_DONE);
  if (result != ENOERR) {
    myprintf("\nInterrupt enable failed! Driver returned '%"PRIi32"'.\r\n",  result);
    return -1;
  }

  result = sysflash_read_page_setup(page_num);
  if (result != ENOERR) {
    myprintf("Read page setup failed on page %"PRIu32".\r\n", page_num);
    /* Return as if all bytes were bad */
    return SYSFLASH_PAGE_SIZE;
  }
  /* Wait for page setup to finish */
  while (sf_int_event != SF_READ_PAGE_SETUP_DONE);

  /* Disable interrupt */
  result = sysflash_interrupt_enable(SYSFLASH_IE_RPAGE_DONE);
  if (result != ENOERR) {
    myprintf("\nInterrupt disable failed! Driver returned '%"PRIi32"'.\r\n",  result);
    return -1;
  }

  /* Get the actual page data */
  result = sysflash_read_page(page_buf, num_bytes);
  if (result != ENOERR) {
    myprintf("Failed to read data from page %"PRIu32", got %"PRIu32"\r\n",
            page_num, result);
    return SYSFLASH_PAGE_SIZE;
  }

  error_count = 0;

  for (i = 0; i < num_bytes; i++) {
    if (data[i] != page_buf[i])
      error_count++;
  }

  return error_count;
}

void verify_image(uint32_t base_page, uint8_t* data, uint32_t length_bytes)
{
  uint32_t num_pages;
  uint32_t page;
  uint32_t data_offset;
  uint32_t data_size;
  uint32_t verify_errors;
  int32_t  result;
  uint32_t block_ok;

  block_ok = 0;

  /* Check that base page is at a block boundary */
  if (base_page % SYSFLASH_PAGES_PER_BLOCK != 0) {
    myprintf("First page of image (%"PRIu32") should be the first of an"
             " erase block.\r\n", base_page);
    return;
  }

  myprintf("Verifying image from page %"PRIu32", size %"PRIu32"\r\n",
           base_page,
           length_bytes);

  num_pages = (length_bytes / SYSFLASH_PAGE_SIZE);
  if ((length_bytes % SYSFLASH_PAGE_SIZE) != 0)
    num_pages++;

  data_offset = 0;
  verify_errors = 0;

  for (page = base_page; page < (base_page + num_pages); page++)
  {
    /* At each block boundary, check if it's a bad block */
    if (page % SYSFLASH_PAGES_PER_BLOCK == 0) {
      result = bad_block_check(page / SYSFLASH_PAGES_PER_BLOCK);
      if (result < 0) {
        myprintf("ERROR: Block number out of range!\r\n");
        return;
      }
      else if (result == BLOCK_BAD) {
        myprintf("Skip bad block %"PRIu32" (page %"PRIu32" to %"PRIu32")\r\n",
                 page / SYSFLASH_PAGES_PER_BLOCK,
                 page,
                 page + SYSFLASH_PAGES_PER_BLOCK - 1);

        /* Skip block */
        page += SYSFLASH_PAGES_PER_BLOCK - 1;
        num_pages += SYSFLASH_PAGES_PER_BLOCK;

        block_ok = 0;
      }
      else {
        block_ok = 1;
      }
    }

    if (block_ok) {
      if (length_bytes < SYSFLASH_PAGE_SIZE) {
        data_size = length_bytes;
      } else {
        data_size = SYSFLASH_PAGE_SIZE;
      }

      result = verify_page(page, &data[data_offset], data_size);
      if (result < 0) {
        myprintf("\r\nCould not complete verification due to errors!\r\n");
        return;
      }
      else {
        verify_errors += result;
      }

      length_bytes -= data_size;
      data_offset +=  data_size;
    }
  }

  if (verify_errors != 0) {
    myprintf("Verify complete - %"PRIu32" byte errors were detected\r\n",
             verify_errors);
  } else {
    myprintf("Image at %"PRIu32" verified.\r\n", base_page);
  }
}

void program_image(uint32_t base_page, uint8_t* data, uint32_t length_bytes)
{
  int32_t  result;
  uint32_t num_pages;
  uint32_t page;
  uint32_t block;
  uint32_t data_offset;
  uint32_t block_ok;
  uint32_t data_size;
  uint32_t available_blocks;

  /* Check that base page is at a block boundary */
  if (base_page % SYSFLASH_PAGES_PER_BLOCK != 0) {
    myprintf("First page of image (%"PRIu32") should be the first of an erase block.\r\n"
             "Will not program image.\r\n\n", base_page);
    return;
  }

  myprintf("Programming image from page %"PRIu32", size %"PRIu32"\r\n",
           base_page,
           length_bytes);

  /* Get length in pages, rounded up */
  num_pages = (length_bytes / SYSFLASH_PAGE_SIZE);
  if ((length_bytes % SYSFLASH_PAGE_SIZE) != 0)
    num_pages++;

  myprintf("\r\nProgramming %ld pages\r\n", num_pages);

  data_offset = 0;
  page = base_page;
  available_blocks = AVAILABLE_BLOCKS_PER_IMAGE;

  block_ok = 0;
  block = page / SYSFLASH_PAGES_PER_BLOCK;
  
  while (page < base_page + num_pages){
    /* At each block boundary, check if it's a bad block and erase */
    if (page % SYSFLASH_PAGES_PER_BLOCK == 0) {
      if (available_blocks == 0) {
        myprintf("Out of blocks!\r\n");
        break;
      }
      else {

        available_blocks--;
        block = page / SYSFLASH_PAGES_PER_BLOCK;
        result = bad_block_check(block);

        if (result < 0) {
          myprintf("Programming failed, block number out of range!\r\n");
          return;
        }
        else if (result == BLOCK_BAD) {
          block_ok = 0;
          myprintf("Skip bad block %"PRIu32" (page %"PRIu32" to %"PRIu32")\r\n",
                   block,
                   page,
                   page + SYSFLASH_PAGES_PER_BLOCK - 1);
          /* Skip block, extend image size in flash */
          page += SYSFLASH_PAGES_PER_BLOCK;
          num_pages += SYSFLASH_PAGES_PER_BLOCK;
        }
        else {
          block_ok = 1;
        }

        if (block_ok) {
          myprintf("Erase block %"PRIu32".\r\n", block);

          /* Reset event marker first */
          sf_int_event = 0;

          /* Enable interrupt */
          result = sysflash_interrupt_enable(SYSFLASH_IE_ERASE_DONE);
          if (result != ENOERR) {
            myprintf("\nInterrupt enable failed! Driver returned '%"PRIi32"'.\r\n",
                     result);

            return;
          }

          result = sysflash_erase_block(block);
          if (result == -EINVAL) {
            myprintf("Erase failed, block number %"PRIu32" out of range.\r\n",
                     block);
            return;
          }
          else if (result == -EBUSY) {
            myprintf("Controller busy, cannot erase block.\r\n");
            return;
          }

          /* Wait for erase to finish */
          while (sf_int_event != SF_ERASE_BLOCK_DONE);

          /* Disable interrupt */
          result = sysflash_interrupt_disable(SYSFLASH_IE_ERASE_DONE);
          if (result != ENOERR) {
            myprintf("\nInterrupt disable failed! Driver returned '%"PRIi32"'.\r\n",
                     result);
            return;
          }

          /* Make sure the operation succeeded */
          result = sysflash_verify_command();
          if (result == -EIO) {
            myprintf("Erase failed on chip level, block %"PRIu32"\r\n",
                     block);

            /* Set flag in bad block table */
            bad_block_table[block / BB_TABLE_BLOCKS_PER_WORD] |=
              1 << (block % BB_TABLE_BLOCKS_PER_WORD);

            /* Step to next block, extend image size in flash */
            page += SYSFLASH_PAGES_PER_BLOCK;
            num_pages += SYSFLASH_PAGES_PER_BLOCK;

            block_ok = 0;
          }
          else if (result == -EBUSY) {
            myprintf("Error: Controller busy, cannot read chip status\r\n");
            return;
          }
        }
      }
    }

    if (block_ok) {
      if (length_bytes < SYSFLASH_PAGE_SIZE) {
        data_size = length_bytes;
      } else {
        data_size = SYSFLASH_PAGE_SIZE;
      }

      /* Reset event marker first */
      sf_int_event = 0;

      /* Enable interrupt */
      result = sysflash_interrupt_enable(SYSFLASH_IE_PROG_DONE);
      if (result != ENOERR) {
        myprintf("\nInterrupt enable failed! Driver returned '%"PRIi32"'.\r\n",  result);
        return;
      }

      result = sysflash_program_page(page, &data[data_offset], data_size);
      if (result == -EINVAL) {
        myprintf("Programming page %"PRIu32" failed.\r\n", page);
        myprintf("Block, page or size out of range, aborting programming!\r\n\n");
        return;
      }
      else if (result == -EBUSY) {
        myprintf("Programming page %"PRIu32" failed.\r\n", page);
        myprintf("Controller busy, aborting programming!\r\n\n");
        return;
      }

      /* Wait for program page to finish */
      while (sf_int_event != SF_PROG_PAGE_DONE);

      /* Disable interrupt */
      result = sysflash_interrupt_disable(SYSFLASH_IE_PROG_DONE);
      if (result != ENOERR) {
        myprintf("\nInterrupt disable failed! Driver returned '%"PRIi32"'.\r\n",  result);
        return;
      }

      /* Make sure the operation succeeded */
      result = sysflash_verify_command();
      if (result == -EIO) {
        myprintf("Program failed on block %"PRIu32", skipping to next block.\r\n",
                 block);

        /* Set flag in bad block table */
        bad_block_table[block / BB_TABLE_BLOCKS_PER_WORD] |=
            1 << (block % BB_TABLE_BLOCKS_PER_WORD);

        /* Revert to start of block */
        page -= page % SYSFLASH_PAGES_PER_BLOCK;
        length_bytes += (page % SYSFLASH_PAGES_PER_BLOCK) * SYSFLASH_PAGE_SIZE;
        data_offset -= (page % SYSFLASH_PAGES_PER_BLOCK) * SYSFLASH_PAGE_SIZE;

        /* Step to next block, extend image size in flash */
        page += SYSFLASH_PAGES_PER_BLOCK;
        num_pages += SYSFLASH_PAGES_PER_BLOCK;
      }
      else if (result == -EBUSY) {
        myprintf("Error: Controller busy, cannot read chip status\r\n");
        return;
      }
      else {
        length_bytes -= data_size;
        data_offset += data_size;
        page++;
      }
    }
  }

  if (length_bytes != 0) {
    myprintf("ERROR: Failed to program full image at %"PRIu32"!\r\n\n",
             base_page);
  }
  else {
    myprintf("Program image complete at %"PRIu32"!\r\n\n", base_page);
  }
}

void myprintf(const char *fmt, ...)
{
  char    out_str[MAX_OUT_STR_LEN];
  va_list argp;
  int     i;

  va_start(argp, fmt);

  vsnprintf(out_str, MAX_OUT_STR_LEN, fmt, argp);

  /* Output everything up to the NULL marker */
  i = 0;
  while ((out_str[i] != '\0') && (i < MAX_OUT_STR_LEN)) {
#ifdef OR1K
    uart_putc(UART_BASE, out_str[i++]);
#endif

#ifdef LEON3
    putc(out_str[i++], stdout);
#endif      
  }
}
