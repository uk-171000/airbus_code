/*----------------------------------------------------------------------------
 * Copyright (C) 2016-2017 ÅAC Microtec AB
 *
 * Filename:     nandflash-program.h
 * Module name:  nandflash_program
 *
 * Author(s):    JuBa, DaHe, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  Header file for nandflash_program
 * Requirements: -
 *--------------------------------------------------------------------------*/
#include <toolchain_support.h>

/* Program image data defined by the Makefile */
extern uint32_t nandprogram_data;
extern uint32_t end_nandprogram_data;

#ifdef OR1K
  /* UART address from board definition file */
  #ifndef UART_BASE
  extern uint32_t _or1k_board_uart_base;
  #define UART_BASE               _or1k_board_uart_base
  #endif
  #ifndef BAUDRATE
  extern uint32_t _or1k_board_uart_baud;
  #define BAUDRATE                _or1k_board_uart_baud
  #endif
#endif

#define BLOCK_BAD               (0x00)
#define BLOCK_OK                (0x01)

/* Event flags */
#define SF_RESET_DONE           (1<<0)
#define SF_READ_PAGE_SETUP_DONE (1<<1)
#define SF_PROG_PAGE_DONE       (1<<2)
#define SF_ERASE_BLOCK_DONE     (1<<3)
#define SF_READ_ID_DONE         (1<<4)
#define SF_READ_STATUS_DONE     (1<<5)
#define SF_SET_FEATURE_DONE     (1<<6)

/* Callback functions */
void reset_done(void *arg);
void prog_page_done(void *arg);
void read_page_done(void *arg);
void erase_block_done(void *arg);
void read_status_done(void *arg);
void set_feature_done(void *arg);


/*
 * Name:        bad_block_check
 * Description: Checks if a block is marked as bad in the table.
 * Arguments:   Block number to check.
 * Return:      BLOCK_OK  if block is OK.
 *              BLOCK_BAD if block was marked as bad.
 *              -1        if block number is out-of-range.
 */
int32_t bad_block_check(uint32_t block);

/*
 * Name:        verify_page
 * Description: Reads out a page to an internal buffer and compares it to the
 *              supplied data buffer.
 * Arguments:   Page number to verify.
 *              Buffer to verify against.
 *              Number of bytes to verify.
 * Return:      Number of detected byte errors, or
 *              -1 on internal error.
 */
int32_t verify_page(uint32_t page_num, uint8_t* data, uint32_t num_bytes);

/*
 * Name:        program_image
 * Description: Programs the supplied data buffer to flash.
 * Arguments:   Starting page for the image in flash.
 *              Buffer to program.
 *              Number of bytes to program.
 * Return:
 */
void program_image(uint32_t base_page, uint8_t* data, uint32_t length_bytes);

/*
 * Name:        verify_image
 * Description: Reads out an image and compares it to the supplied data buffer.
 * Arguments:   Starting page of the image in flash.
 *              Buffer to verify against.
 *              Number of bytes to verify.
 * Return:
 */
void verify_image(uint32_t base_page, uint8_t* data, uint32_t length_bytes);

/*
 * Name:        myprintf
 * Description: A wrapper function to make it easier to get output to any UART,
 *              not just debug. Maximum output length is controlled by the
 *              define MAX_OUT_STR_LEN.
 * Arguments:   A format string and a variable number of data variables.
 * Return:
 */
#define MAX_OUT_STR_LEN 256
void myprintf(const char *fmt, ...);
