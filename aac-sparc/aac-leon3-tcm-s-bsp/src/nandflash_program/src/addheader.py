#!/usr/bin/env python
#-----------------------------------------------------------------------------
# Copyright (C) 2016 AAC Microtec AB
#
#
# Filename:     addheader.py
# Module name:  nandflash_program
#
#  Author(s):    DaHe
#  Support:      support@aacmicrotec.com
#  Description:  Add a header to the program image to write to flash.
#                The script reads an image file and outputs the data into a
#                new file with a 64-bit header that contains the length in
#                bytes and a checksum of all 32-bit words. The size is stored
#                as a big endian 32-bit unsigned integer and is included in
#                the checksum.
#
#  Requirements: See AAC Bootrom SW Requirements
#-----------------------------------------------------------------------------
import os
import shutil
import struct
import sys

if (len(sys.argv) > 2):
    infile_name = sys.argv[1]
    outfile_name = sys.argv[2]
    if (len(sys.argv) > 3):
        corrupt_crc = 1
    else:
        corrupt_crc = 0
else:
    print "Not enough command line arguments!"
    print "Expecting " + sys.argv[0] + " <input file> <output file>"
    exit(1)

filesize = os.stat(infile_name).st_size

# Init checksum for calculation
chksum = 0

with open(outfile_name, 'w+b') as outfile:
    # Write size and checksum at start of file as
    # 4 byte big endian unsigned integers
    outfile.seek(0,0)
    outfile.write(struct.pack(">I", filesize))
    outfile.write(struct.pack(">I", chksum))

    if corrupt_crc == 0:
        chksum ^= filesize

    with open(infile_name, 'r') as infile:
        # Loop over file, add each 32-bit word to checksum,
        # and write to output file
        next_word = infile.read(4)
        while next_word:
            chksum ^= struct.unpack(">I", next_word)[0]
            outfile.write(next_word)
            next_word = infile.read(4)

    # Overwrite initial checksum with computed
    outfile.seek(4,0)
    outfile.write(struct.pack(">I", chksum))
