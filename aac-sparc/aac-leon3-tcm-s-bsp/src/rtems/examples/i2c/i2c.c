#include <rtems.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <bsp/i2c.h>
#include <rtems/libi2c.h>
#include <rtems/libio.h>

#define I2C_ADDR           0x55
#define I2C_DEV_M          0
#define I2C_DEV_S          1
#define I2C_NODE_M         "/dev/i2c-0-test"
#define I2C_NODE_S         "/dev/i2c-1-test"

/*******************************************************************************************/

#define CMD_GET_SENTENCE        0x11
#define CMD_SET_SENTENCE        0x22
#define CMD_GET_COUNTER         0x33
#define CMD_DEC_COUNTER         0x44
#define CMD_INC_COUNTER         0x55

static const char * cmd_to_string(uint8_t cmd)
{
  switch(cmd)
  {
    case CMD_GET_SENTENCE:      return "GET_SENTENCE";
    case CMD_SET_SENTENCE:      return "SET_SENTENCE";
    case CMD_GET_COUNTER:       return "GET_COUNTER";
    case CMD_DEC_COUNTER:       return "DEC_COUNTER";
    case CMD_INC_COUNTER:       return "INC_COUNTER";
    default:                    return "UNKNOWN";
  }
}

static int16_t  g_counter       = 0;
static char     g_sentence[64]  = "This is the default slave device's sentence.\0garbage";

static enum {
  S_IDLE,

  S_SENTENCE_READ,

  S_SENTENCE_WRITE,
  S_COUNTER_WRITE
} g_state = S_IDLE;

/*******************************************************************************************/

void i2c_read(uint8_t byte, void *userdata) {
  static char in_buf[64];
  static off_t off = 0;

  switch(g_state) {
  default:
    g_state = S_IDLE;
  case S_IDLE:
    printk("[S] Received command: %s\r\n", cmd_to_string(byte));
    switch(byte) {
    case CMD_GET_SENTENCE:
      g_state = S_SENTENCE_WRITE;
      break;

    case CMD_SET_SENTENCE:
      g_state = S_SENTENCE_READ;
      break;

    case CMD_GET_COUNTER:
      g_state = S_COUNTER_WRITE;
      break;

    case CMD_DEC_COUNTER:
      --g_counter;
      break;

    case CMD_INC_COUNTER:
      ++g_counter;
      break;

    default:
      break;
    }
    break;

  case S_SENTENCE_READ:
    if(off < sizeof(in_buf)) {
      in_buf[off] = (char)byte;
      ++off;
    }
    if(byte == 0) {
      in_buf[off - 1] = '\0';
      strncpy(g_sentence, in_buf, sizeof(g_sentence));
      off = 0;
      g_state = S_IDLE;
    }

    break;
  }
}

rtems_status_code i2c_write(uint8_t *byte, void *userdata) {
  static uint8_t *c = NULL;

  switch(g_state) {
  case S_SENTENCE_WRITE:
    if(c == NULL)
      c = (uint8_t *)g_sentence;

    *byte = *c;
    if(*c == 0)
    {
      c = NULL;
      g_state = S_IDLE;
    }
    else
      ++c;

    break;

  case S_COUNTER_WRITE:
    if(c == NULL)
      c = (uint8_t *)&g_counter;

    *byte = *c;
    ++c;
    if(c - (uint8_t *)&g_counter == sizeof(g_counter)) {
      c = NULL;
      g_state = S_IDLE;
    }
    break;

  default:
    return RTEMS_TOO_MANY;
    break;
  }

  return RTEMS_SUCCESSFUL;
}

rtems_task slave_task(rtems_task_argument ignored) {
  or1k_aac_i2c_set_slave_addr(I2C_DEV_S, I2C_ADDR);
  or1k_aac_i2c_set_read_callback(I2C_DEV_S, i2c_read, NULL);
  or1k_aac_i2c_set_write_callback(I2C_DEV_S, i2c_write, NULL);

  for(;;) {
    sleep(1);
  }
}

/*******************************************************************************************/

rtems_task master_task(rtems_task_argument ignored)
{
  int           fd;
  int           rv              = 0;
  uint8_t       cmd;
  char          buf[64];
  const char    new_sentence[]  = "Hello, world!";
  int16_t       counter         = 0;
  int           i;
  int           n;

  dev_t dev = rtems_filesystem_make_dev_t(rtems_libi2c_major,
                                          RTEMS_LIBI2C_MAKE_MINOR(I2C_DEV_M, I2C_ADDR));

  rv = mknod(I2C_NODE_M, S_IFCHR | 0666, dev);
  if(rv) {
    printf("\033[32;1m[M] error: mknod: %d (%d, %s)\033[0m\r\n", rv, errno, strerror(errno));
    exit(1);
  }

  fd = open(I2C_NODE_M, O_RDWR);
  if(fd < 0) {
    printf("\033[32;1m[M] error: open: %d (%d, %s)\033[0m\r\n", fd, errno, strerror(errno));
    exit(1);
  }

  cmd = CMD_GET_SENTENCE;
  printf("\033[32;1m[M] Sending command: %s\033[0m\r\n", cmd_to_string(cmd));
  write(fd, &cmd, sizeof(cmd));
  rv = read(fd, buf, sizeof(buf));
  buf[sizeof(buf)-1] = '\0';
  if(rv <= 0)
    printf("\033[32;1m[M] Invalid sentence: read: %d (%d, %s)\033[0m\r\n", rv, errno, strerror(errno));
  else
    printf("\033[32;1m[M] Sentence: %s\033[0m\r\n", buf);

  sleep(1);

  cmd = CMD_SET_SENTENCE;
  printf("\033[32;1m[M] Sending command: %s\033[0m\r\n", cmd_to_string(cmd));
  write(fd, &cmd, sizeof(cmd));
  write(fd, new_sentence, sizeof(new_sentence));

  sleep(1);

  cmd = CMD_GET_SENTENCE;
  printf("\033[32;1m[M] Sending command: %s\033[0m\r\n", cmd_to_string(cmd));
  write(fd, &cmd, sizeof(cmd));
  rv = read(fd, buf, sizeof(buf));
  buf[sizeof(buf)-1] = '\0';
  if(rv <= 0)
    printf("\033[32;1m[M] Invalid sentence: read: %d (%d, %s)\033[0m\r\n", rv, errno, strerror(errno));
  else
    printf("\033[32;1m[M] Sentence: %s\033[0m\r\n", buf);

  sleep(1);

  n = 5;
  cmd = CMD_INC_COUNTER;
  printf("\033[32;1m[M] Sending command: %s (%d times)\033[0m\r\n", cmd_to_string(cmd), n);
  for(i = 0; i < n; ++i)
    write(fd, &cmd, sizeof(cmd));


  cmd = CMD_GET_COUNTER;
  printf("\033[32;1m[M] Sending command: %s\033[0m\r\n", cmd_to_string(cmd));
  write(fd, &cmd, sizeof(cmd));
  rv = read(fd, &counter, sizeof(counter));
  if(rv != sizeof(counter))
    printf("\033[32;1m[M] Invalid counter value: read: %d (%d, %s)\033[0m\r\n", rv, errno, strerror(errno));
  else
    printf("\033[32;1m[M] Counter: %d\033[0m\r\n", counter);

  sleep(1);

  n = 8;
  cmd = CMD_DEC_COUNTER;
  printf("\033[32;1m[M] Sending command: %s (%d times)\033[0m\r\n", cmd_to_string(cmd), n);
  for(i = 0; i < n; ++i)
    write(fd, &cmd, sizeof(cmd));

  sleep(1);

  cmd = CMD_GET_COUNTER;
  printf("\033[32;1m[M] Sending command: %s\033[0m\r\n", cmd_to_string(cmd));
  write(fd, &cmd, sizeof(cmd));
  rv = read(fd, &counter, sizeof(counter));
  if(rv != sizeof(counter))
    printf("\033[32;1m[M] Invalid counter value: read: %d (%d, %s)\033[0m\r\n", rv, errno, strerror(errno));
  else
    printf("\033[32;1m[M] Counter: %d\033[0m\r\n", counter);

  exit(0);
}

/*******************************************************************************************/

rtems_task Init(rtems_task_argument ignored)
{
  rtems_id      task_id[2];
  rtems_name    task_name[2];

  printf("\r\nI2C TEST\r\n\r\n");

  or1k_aac_i2c_register_buses();

  task_name[0] = rtems_build_name('T', 'M', 'A', 'S');
  task_name[1] = rtems_build_name('T', 'S', 'L', 'A');

  rtems_task_create(task_name[0], 1, RTEMS_MINIMUM_STACK_SIZE * 2, RTEMS_DEFAULT_MODES,
                    RTEMS_DEFAULT_ATTRIBUTES, &task_id[0]);
  rtems_task_create(task_name[1], 1, RTEMS_MINIMUM_STACK_SIZE * 2, RTEMS_DEFAULT_MODES,
                    RTEMS_DEFAULT_ATTRIBUTES, &task_id[1]);

  rtems_task_start(task_id[1], slave_task, 1);
  sleep(1);
  rtems_task_start(task_id[0], master_task, 0);

  rtems_task_delete(RTEMS_SELF);
}

/*******************************************************************************************/

#include <bsp.h>

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_MAXIMUM_DRIVERS 10
#define CONFIGURE_MAXIMUM_SEMAPHORES 10
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 20
#define CONFIGURE_INIT_TASK_STACK_SIZE (32 * 1024)

#define CONFIGURE_INIT
#include <rtems/confdefs.h>
/* end of file */
