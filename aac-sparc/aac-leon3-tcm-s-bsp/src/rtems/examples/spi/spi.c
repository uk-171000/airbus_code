/*
 * This example reads status register from RS256A FRAM chip connected to SPI0
 * and its CS' pin to GPIO 12
 */

#include <rtems.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <bsp/spi.h>
#include <errno.h>
#include <rtems/libi2c.h>
#include <rtems/libio.h>
#include <time.h>

#define SPI_NODE        "/dev/spi-0-test"

#define GPIO_DIR(n)     *((volatile unsigned char *)(0x9A000000 + 7 + (n)))
#define GPIO_VAL(n)     *((volatile unsigned char *)(0x9A000000 + (n)))

/*******************************************************************************************/

rtems_task Init(rtems_task_argument ignored)
{
  int fd = -1;
  int rv = 0;
  rtems_libi2c_tfr_mode_t tfr;
  rtems_libi2c_read_write_t rw;
  unsigned char rd_buf[2];
  unsigned char wr_buf[2] = {0x05, 0x00};
  unsigned char buf;

  or1k_aac_spi_register_buses();

  GPIO_DIR(4) = 0x02;
  GPIO_VAL(4) = 0x02;

  dev_t dev = rtems_filesystem_make_dev_t(rtems_libi2c_major, RTEMS_LIBI2C_MAKE_MINOR(0, 1));
  rv = mknod(SPI_NODE, S_IFCHR | 0666, dev);

  if(rv) {
    printf("[A] error: mknod: %d (%d, %s)\r\n", rv, errno, strerror(errno));
    exit(1);
  }
  fd = open(SPI_NODE, O_RDWR);
  if(fd < 0) {
    printf("[A] error: open: %d (%d, %s)\r\n", fd, errno, strerror(errno));
    exit(1);
  }

  tfr.clock_inv = false;
  tfr.clock_phs = false;
  tfr.idle_char = 0xAA;
  tfr.baudrate = 18000000 / 4096;

  for(;;) {
    if(tfr.baudrate > 18000000)
      tfr.baudrate = 18000000 / 4096;
    rv = rtems_libi2c_ioctl(RTEMS_LIBI2C_MAKE_MINOR(0, 1), RTEMS_LIBI2C_IOCTL_SET_TFRMODE,
                            &tfr);
    tfr.baudrate *= 2;
    printf("*** speed request: no more than %u\r\n", tfr.baudrate);

    GPIO_VAL(4) = 0x00;
    write(fd, "\x05", 1);
    read(fd, &buf, sizeof(buf));
    GPIO_VAL(4) = 0x02;

    printf("write; read: %02x\r\n", buf);
    sleep(1);

    rw.rd_buf = rd_buf;
    rw.wr_buf = wr_buf;
    rw.byte_cnt = sizeof(wr_buf);

    GPIO_VAL(4) = 0x00;
    rv = rtems_libi2c_ioctl(RTEMS_LIBI2C_MAKE_MINOR(0, 1), RTEMS_LIBI2C_IOCTL_READ_WRITE,
                            &rw);
    GPIO_VAL(4) = 0x02;

    printf("read+write (write: %02x %02x): %02x %02x\r\n", wr_buf[0], wr_buf[1],
           rd_buf[0], rd_buf[1]);

    sleep(1);
  }

  exit(0);
}

/*******************************************************************************************/

#include <bsp.h>

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_MAXIMUM_DRIVERS 10
#define CONFIGURE_MAXIMUM_SEMAPHORES 10
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 20
#define CONFIGURE_INIT_TASK_STACK_SIZE (32 * 1024)

#define CONFIGURE_INIT
#include <rtems/confdefs.h>
