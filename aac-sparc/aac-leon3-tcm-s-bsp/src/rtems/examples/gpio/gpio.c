/*
 * This example turns on and off GPIO 30, and reads GPIO 31. On the ethernet
 * expansion board these are marked as GPIO's 15,14.
 *
 * The best way to test it is to actually connect them together.
 */

#include <rtems.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <bsp.h>
#include <unistd.h>

/*******************************************************************************************/

rtems_task Init(rtems_task_argument ignored)
{
  /* set 30 to output */
  aac_gpio_set_dir(30, AAC_GPIO_DIR_OUT);

  /* set 31 to input */
  aac_gpio_set_dir(31, AAC_GPIO_DIR_IN);

  int s = 1;
  while (1) {
    printf("Setting pin 30 to %d\n", s);
    aac_gpio_set_value(30, s);

    printf("Reading pin 31: %d\n", aac_gpio_get_value(31));

    usleep(500000);

    if(s > 0)
      s = 0x00;
    else
      s = 0x01;
  }

  exit(0);
}

/*******************************************************************************************/

#include <bsp.h>

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_MAXIMUM_DRIVERS 4
#define CONFIGURE_MAXIMUM_SEMAPHORES 4

#define CONFIGURE_MAXIMUM_TASKS             1

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#define CONFIGURE_INIT
#include <rtems/confdefs.h>
