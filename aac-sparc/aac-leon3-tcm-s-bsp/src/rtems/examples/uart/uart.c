#include <rtems.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <rtems/libio.h>
#include <time.h>
#include <bsp/uart.h>
#include <termios.h>

static const char UART_NODE[] = "/dev/ttyS2";
static int fd = -1;

/*******************************************************************************************/

rtems_task RdTask(rtems_task_argument ignored)
{
  unsigned char buf;
  unsigned char c;
  int rd_cnt;

  for(;;) {
    rd_cnt = read(fd, &buf, sizeof(buf));
    c = buf;
    if(buf < 0x20 || buf > 0x7E)
      c = '.';
    printf("[r] %s: read: %d (0x%02x [%c])\r\n", UART_NODE, rd_cnt, buf, c);
  }

  exit(0);
}

/*******************************************************************************************/

rtems_task WrTask(rtems_task_argument ignored)
{
  int wr_cnt;
  const char sample_msg[10] = "Hello.\r\n";
  const char sample_msg_2[10] = "World.\r\n";
  struct termios termcfg;

  for(;;) {
    /* Change UART mode to RS422 */
    tcgetattr(fd, &termcfg);
    termcfg.c_cflag |= OR1K_AAC_TERMIOS_CRS422;
    tcsetattr(fd, TCSANOW, &termcfg);

    wr_cnt = write(fd, sample_msg, sizeof(sample_msg));
    printf("[w] %s: write: %d\r\n", UART_NODE, wr_cnt);

    usleep(1000000);

    /* Change UART mode to RS485 (i.e. disable RS422 mode) */
    tcgetattr(fd, &termcfg);
    termcfg.c_cflag &= ~OR1K_AAC_TERMIOS_CRS422;
    tcsetattr(fd, TCSANOW, &termcfg);

    wr_cnt = write(fd, sample_msg_2, sizeof(sample_msg_2));
    printf("[w] %s: write: %d\r\n", UART_NODE, wr_cnt);

    usleep(1000000);
  }

  exit(0);
}

/*******************************************************************************************/

rtems_task Init(rtems_task_argument ignored)
{
  struct termios termcfg;

  rtems_id      task_id[2];
  rtems_name    task_name[2];

  task_name[0] = rtems_build_name('T', 'A', 'W', 'R');
  task_name[1] = rtems_build_name('T', 'A', 'R', 'D');

  rtems_task_create(task_name[0], 1, RTEMS_MINIMUM_STACK_SIZE * 2, RTEMS_DEFAULT_MODES,
                    RTEMS_DEFAULT_ATTRIBUTES, &task_id[0]);
  rtems_task_create(task_name[1], 1, RTEMS_MINIMUM_STACK_SIZE * 2, RTEMS_DEFAULT_MODES,
                    RTEMS_DEFAULT_ATTRIBUTES, &task_id[1]);

  fd = open(UART_NODE, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if(fd < 0) {
    printf("[i] error: open %s: %d (%d, %s)\r\n", UART_NODE, fd,
       errno, strerror(errno));
    exit(1);
  } else {
    printf("[i] open %s ok\r\n", UART_NODE);

    tcgetattr(fd, &termcfg);
    termcfg.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | INPCK | ISTRIP | INLCR | ICRNL | IXON);
    termcfg.c_oflag = 0;
    termcfg.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
    termcfg.c_cflag &= ~(CSIZE | PARENB);
    termcfg.c_cflag |= CS8;
    termcfg.c_cc[VMIN] = 1;
    termcfg.c_cc[VTIME] = 0;
    tcsetattr(fd, TCSANOW, &termcfg);

    cfsetospeed(&termcfg, B9600);
    cfsetispeed(&termcfg, B9600);
    tcsetattr(fd, TCSANOW, &termcfg);
  }

  rtems_task_start(task_id[0], WrTask, 0);
  rtems_task_start(task_id[1], RdTask, 0);

  rtems_task_delete(RTEMS_SELF);
}

/*******************************************************************************************/

#include <bsp.h>

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_MAXIMUM_DRIVERS 10
#define CONFIGURE_MAXIMUM_SEMAPHORES 10
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 20
#define CONFIGURE_INIT_TASK_STACK_SIZE (32 * 1024)

#define CONFIGURE_INIT
#include <rtems/confdefs.h>
