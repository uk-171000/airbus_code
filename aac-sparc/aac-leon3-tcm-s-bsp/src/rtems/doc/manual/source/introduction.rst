Introduction
============

This is the documentation for the RTEMS BSP for OBC-lite from ÅAC Microtec AB.

Version information
-------------------

.. csv-table::
   :header-rows: 1
   :delim: ;

   Date       ; Author         ; Version ; Changes
   2014-10-14 ; Karol Gugala   ; 0.1.0   ; Initial draft
   2014-12-12 ; Michael Gielda ; 0.1.1   ; Review

