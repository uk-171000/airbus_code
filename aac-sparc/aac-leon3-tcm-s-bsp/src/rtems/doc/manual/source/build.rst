Building the BSP
================

Prerequisites 
--------------

RTEMS source builder
~~~~~~~~~~~~~~~~~~~~

This is a set of tools required for building the RTEMS system. It can be acquired with:

.. code-block:: bash

   git clone git://git.rtems.org/rtems-source-builder.git

Next step is to build those tools with:

.. code-block:: bash

  cd rtems-source-builder/rtems
  ../source-builder/sb-set-builder --log=l-or1k.txt \ 
  --prefix=/path/to/your/rtems_build --with-rtems 4.11/rtems-or1k

The last parameter determines the platform for which the tools are being built (in this case rtems-or1k). 
Note that you have to change the prefix to the required location.

Before attempting to build the system add the ``bin`` folder location the PATH variable.
This folder can be found in the previously selected prefix location.

.. code-block:: bash

   export PATH=/path/to/your/rtems_build/bin:$PATH

RTEMS source code
~~~~~~~~~~~~~~~~~

Clone the RTEMS repository with:

.. code-block:: bash

   git clone https://dev.antmicro.com/git/rtems-openrisc

Building the RTEMS
------------------

To build the RTEMS kernel follow these steps:

* Change directory to previously cloned RTEMS source
* Run bootstrap scrip from within this directory (this will run ``autoconf`` for all the BSPs)
* Create a new build directory and ``cd`` there (the best choice is to locate the ``build`` directory outside the RTEMS repository)
* Run the ``configure`` script (located in the RTEMS repository directory) with the proper switches (see below). This script must be invoked from the ``build`` directory. This will generate the necessary ``Makefiles``.
* Run ``make``. Note that RTEMS does not properly handle multi-threaded ``make``, thus if you invoke ``make`` with the ``-jX`` switch there might be some errors. A workaround for this issue is to invoke ``make -jX`` a couple of times until the system is built properly, or run ``make`` without the ``-jX`` switch (this will take much more time).

An example script for compiling the RTEMS kernel:

.. code-block:: bash
   
   cd rtems
   ./bootstrap
   cd ..
   mkdir or1k #build directory
   cd or1k
   ../rtems/configure --target=or1k-rtems4.11 --enable-rtemsbsp=aac-obc \
   --enable-tests=samples
   make -j9

Note that passing the option ``--enable-tests=samples`` to the configure script will generate (and build in the end) sample RTEMS tests.

Building custom application
---------------------------

In order to build a custom application within the RTEMS system it is required that that ``--prefix=`` option is set while configuring the system and running ``make install`` after building the system.

.. code-block:: bash 
  
   ../rtems-openrisc/configure --target=or1k-rtems4.11 --enable-rtemsbsp=aac-obc \
    --enable-tests=samples --prefix=/path/to/your/install_point
   make -j9
   make install

The next important step is setting the ``RTEMS_MAKEFILE_PATH`` environment variable.

.. code-block:: bash
    
   export RTEMS_MAKEFILE_PATH=/path/to/your/install_point/or1k-rtems4.11/aac-obc

See the example application with the required Makefile in the ``examples`` directory in the RTEMS repository.

