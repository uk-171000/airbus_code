/*
 * COPYRIGHT (c) 2014 Hesham ALMatary <heshamelmatary@gmail.com>
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE.
 */

#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <rtems/score/cpu.h>
#include <rtems/bspIo.h>

void _CPU_Exception_frame_print( const CPU_Exception_frame *frame )
{
  uint32_t i;

  /* r0 is a special case (always zero) */
  printk( "r%02i = 0x%016x\n", 0, 0);
  for ( i = 1; i < 32; ++i ) {
      printk( "r%02i = 0x%016x\n", i, frame->r[i]);
  }

  printk( "EPCR = 0x%016x\n", frame->epcr);
  printk( "EEAR = 0x%016x\n", frame->eear);
  printk( "ESR  = 0x%016x\n", frame->esr);
}
