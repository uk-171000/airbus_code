/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2018 ÅAC Microtec AB
 *
 * Filename:     uart.c
 * Module name:  uart
 *
 * Author(s):    PeBr, ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  UART bare metal driver implementation
 * Requirements: ÅAC UART Requirement specification, Rev. A
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <errno.h>
#include "aac_uart.h"

#define CLK_FREQ        (50000000)

/* The IP is a bit stupid and won't allow read-back of the FCR register, so we need
   to keep track of these values for any changes we do to the register. */
static uint8_t trigger_level;
static uint8_t buffer_depth;

void aac_uart_init(uint32_t uart_base, uint32_t bitrate)
{
  /* Disable all interrupts */
  REG8(uart_base + AAC_UART_IER) = 0x00;

  /* Set 8 bit char, 1 stop bit, no parity */
  REG8(uart_base + AAC_UART_LCR) = (AAC_UART_LCR_WLEN8 | AAC_UART_LCR_STOP_1 |
                                    AAC_UART_LCR_PDIS);

  /* Set up default FCR values */
  trigger_level = AAC_UART_FCR_TRIGGER_14;
  buffer_depth = AAC_UART_FCR_BUFFER_16;

  /* Reset FIFOs and set up default settings */
  REG8(uart_base + AAC_UART_FCR) = (AAC_UART_FCR_ENABLE_FIFO | AAC_UART_FCR_CLEAR_RCVR |
                                    AAC_UART_FCR_CLEAR_XMIT | trigger_level |
                                    buffer_depth);

  aac_uart_set_bitrate(uart_base, bitrate);

  /* Clear sending and receiving control registers */
  REG8(uart_base + AAC_UART_RXR) = AAC_UART_RXR_DISABLE;
  REG8(uart_base + AAC_UART_TXR) = AAC_UART_TXR_DISABLE;
}

void aac_uart_set_bitrate(uint32_t uart_base, uint32_t bitrate)
{
  uint32_t      divisor;
  float         float_divisor;

  /* Reset receiver and transmitter FIFOs */
  REG8(uart_base + AAC_UART_FCR) = (AAC_UART_FCR_ENABLE_FIFO | AAC_UART_FCR_CLEAR_RCVR |
                                    AAC_UART_FCR_CLEAR_XMIT | trigger_level |
                                    buffer_depth);

  /* Set bitrate */
  float_divisor = (float) CLK_FREQ / (16 * bitrate);

  /* Ensure round up */
  float_divisor += 0.50f;
  divisor = (uint32_t) float_divisor;

  REG8(uart_base + AAC_UART_LCR) |= AAC_UART_LCR_DLAB;
  REG8(uart_base + AAC_UART_DLL) = divisor & AAC_UART_DLL_MASK;
  REG8(uart_base + AAC_UART_DLM) = (divisor >> AAC_UART_DLM_SHAMT) & AAC_UART_DLM_MASK;
  REG8(uart_base + AAC_UART_LCR) &= ~(AAC_UART_LCR_DLAB);
}

inline void aac_uart_putc(uint32_t uart_base, uint8_t c)
{
  /* Wait for transmit FIFO empty to make sure there's free space */
  while ((REG8(uart_base + AAC_UART_LSR) & AAC_UART_LSR_THRE) != AAC_UART_LSR_THRE);

  REG8(uart_base + AAC_UART_THR) = c;
}

/* Only used when we know transmit FIFO is empty, typically in interrupt */
inline void aac_uart_putc_nonblocking(uint32_t uart_base, uint8_t c)
{
  REG8(uart_base + AAC_UART_THR) = c;
}

inline void aac_uart_flush(uint32_t uart_base)
{
  while ((REG8(uart_base + AAC_UART_LSR) & (AAC_UART_LSR_TEMT | AAC_UART_LSR_THRE)) !=
         (AAC_UART_LSR_TEMT | AAC_UART_LSR_THRE));
}

inline uint8_t aac_uart_getc(uint32_t uart_base)
{
  return REG8(uart_base + AAC_UART_RBR);
}

inline uint8_t aac_uart_check_for_char(uint32_t uart_base)
{
  return REG8(uart_base + AAC_UART_BCR);
}

inline void aac_uart_rxint_enable(uint32_t uart_base)
{
  REG8(uart_base + AAC_UART_IER) |= AAC_UART_IER_RDI;
}

inline void aac_uart_rxint_disable(uint32_t uart_base)
{
  REG8(uart_base + AAC_UART_IER) &= ~(AAC_UART_IER_RDI);
}

inline void aac_uart_txint_enable(uint32_t uart_base)
{
  REG8(uart_base + AAC_UART_IER) |= AAC_UART_IER_THRI;
}

inline void aac_uart_txint_disable(uint32_t uart_base)
{
  REG8(uart_base + AAC_UART_IER) &= ~(AAC_UART_IER_THRI);
}

inline uint8_t aac_uart_get_iir(uint32_t uart_base)
{
  return REG8(uart_base + AAC_UART_IIR);
}

inline uint8_t aac_uart_get_lsr(uint32_t uart_base)
{
  return REG8(uart_base + AAC_UART_LSR);
}

inline uint8_t aac_uart_get_msr(uint32_t uart_base)
{
  return REG8(uart_base + AAC_UART_MSR);
}

inline uint8_t aac_uart_get_lcr(uint32_t uart_base)
{
  return REG8(uart_base + AAC_UART_LCR);
}

inline void aac_uart_clear_rx_fifo(uint32_t uart_base)
{
  REG8(uart_base + AAC_UART_FCR) = (AAC_UART_FCR_ENABLE_FIFO | AAC_UART_FCR_CLEAR_RCVR |
                                    trigger_level | buffer_depth);
}

inline void aac_uart_clear_tx_fifo(uint32_t uart_base)
{
  REG8(uart_base + AAC_UART_FCR) = (AAC_UART_FCR_ENABLE_FIFO | AAC_UART_FCR_CLEAR_XMIT |
                                    trigger_level | buffer_depth);
}

inline void aac_uart_lsint_enable(uint32_t uart_base)
{
  REG8(uart_base + AAC_UART_IER) |= AAC_UART_IER_RLSI;
}

inline void aac_uart_lsint_disable(uint32_t uart_base)
{
  REG8(uart_base + AAC_UART_IER) &= ~(AAC_UART_IER_RLSI);
}

inline void aac_uart_set_txr(uint32_t uart_base, uint8_t ctrl)
{
  REG8(uart_base + AAC_UART_TXR) = ctrl;
}

inline void aac_uart_set_rxr(uint32_t uart_base, uint8_t ctrl)
{
  REG8(uart_base + AAC_UART_RXR) = ctrl;
}

inline void aac_uart_set_moder(uint32_t uart_base, uint8_t mode)
{
  REG8(uart_base + AAC_UART_MODER) = mode;
}

inline void aac_uart_set_mcr(uint32_t uart_base, uint8_t val)
{
  REG8(uart_base + AAC_UART_MCR) = val;
}

inline void aac_uart_set_lcr(uint32_t uart_base, uint8_t val)
{
  REG8(uart_base + AAC_UART_LCR) = val;
}

int32_t aac_uart_set_trigger_level(uint32_t uart_base, uint8_t val)
{
  switch (val) {
    case AAC_UART_TRIGGER_14:
      trigger_level = AAC_UART_FCR_TRIGGER_14;
      break;

    case AAC_UART_TRIGGER_8:
      trigger_level = AAC_UART_FCR_TRIGGER_8;
      break;

    case AAC_UART_TRIGGER_4:
      trigger_level = AAC_UART_FCR_TRIGGER_4;
      break;

    case AAC_UART_TRIGGER_1:
      trigger_level = AAC_UART_FCR_TRIGGER_1;
      break;

    default:
      /* Invalid value */
      return -EINVAL;
      break;
  }

  REG8(uart_base + AAC_UART_FCR) = (AAC_UART_FCR_ENABLE_FIFO | trigger_level |
                                    buffer_depth);

  return 0;
}

uint8_t aac_uart_get_trigger_level(uint32_t uart_base)
{
  switch (trigger_level) {
    case AAC_UART_FCR_TRIGGER_14:
      return AAC_UART_TRIGGER_14;
      break;

    case AAC_UART_FCR_TRIGGER_8:
      return AAC_UART_TRIGGER_8;
      break;

    case AAC_UART_FCR_TRIGGER_4:
      return AAC_UART_TRIGGER_4;
      break;

    case AAC_UART_FCR_TRIGGER_1:
      return AAC_UART_TRIGGER_1;
      break;

    default:
      /* Something is scarily wrong, reset to standard value */
      trigger_level = AAC_UART_FCR_TRIGGER_14;
      REG8(uart_base + AAC_UART_FCR) = (AAC_UART_FCR_ENABLE_FIFO | trigger_level |
                                        buffer_depth);
      return AAC_UART_TRIGGER_14;
  }
}

int32_t aac_uart_set_buffer_depth(uint32_t uart_base, uint8_t val)
{
  switch (val) {
    case AAC_UART_BUFFER_128:
      buffer_depth = AAC_UART_FCR_BUFFER_128;
      break;

    case AAC_UART_BUFFER_64:
      buffer_depth = AAC_UART_FCR_BUFFER_64;
      break;

    case AAC_UART_BUFFER_32:
      buffer_depth = AAC_UART_FCR_BUFFER_32;
      break;

    case AAC_UART_BUFFER_16:
      buffer_depth = AAC_UART_FCR_BUFFER_16;
      break;

    default:
      /* Invalid value */
      return -EINVAL;
      break;
  }

  REG8(uart_base + AAC_UART_FCR) = (AAC_UART_FCR_ENABLE_FIFO | trigger_level |
                                    buffer_depth);

  return 0;
}

uint8_t aac_uart_get_buffer_depth(uint32_t uart_base)
{
  switch (buffer_depth) {
    case AAC_UART_FCR_BUFFER_128:
      return AAC_UART_BUFFER_128;
      break;

    case AAC_UART_FCR_BUFFER_64:
      return AAC_UART_BUFFER_64;
      break;

    case AAC_UART_FCR_BUFFER_32:
      return AAC_UART_BUFFER_32;
      break;

    case AAC_UART_FCR_BUFFER_16:
      return AAC_UART_BUFFER_16;
      break;

    default:
      /* Something is scarily wrong, reset to standard value */
      buffer_depth = AAC_UART_FCR_BUFFER_16;
      REG8(uart_base + AAC_UART_FCR) = (AAC_UART_FCR_ENABLE_FIFO | trigger_level |
                                        buffer_depth);
      return AAC_UART_BUFFER_16;
  }
}
