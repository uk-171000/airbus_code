/**
 * @file
 *
 * @ingroup bsp_clock
 *
 * @brief generic_or1k clock support.
 */

/*
 * generic_or1k Clock driver
 *
 * COPYRIGHT (c) 2014-2015 Hesham ALMatary <heshamelmatary@gmail.com>
 * Copyright (c) 2014-2016 ÅAC Microtec AB <www.aacmicrotec.com>
 *                         Jakob Viketoft <jakob.viketoft@aacmicrotec.com>
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE
 */

#include <rtems.h>
#include <bsp.h>
#include <bsp/irq.h>
#include <bsp/generic_or1k.h>
#include <rtems/score/cpu.h>
#include <rtems/score/or1k-utility.h>
#include <rtems/timecounter.h>

/* The number of clock cycles before generating a tick timer interrupt. */
#define OR1K_CLOCK_CYCLE_TIME_NANOSECONDS     (1000000000 / OR1K_BSP_CLOCK_FREQ)

static struct timecounter generic_or1k_tc;
static CPU_Counter_ticks cpu_counter_ticks;
static uint32_t timer_counts_per_clock_tick;

/* These prototypes are added here to avoid warnings */
void Clock_isr(void *arg);
static uint32_t generic_or1k_get_timecount(struct timecounter *tc);

static void generic_or1k_clock_at_tick(void)
{
  uint32_t ttmr;

  /* Get TTMR value */
  ttmr = _OR1K_mfspr(CPU_OR1K_SPR_TTMR);

  /* Clear interrupt */
  ttmr &=  ~(CPU_OR1K_SPR_TTMR_IP);

  /* Write it back */
  _OR1K_mtspr(CPU_OR1K_SPR_TTMR, ttmr);

  cpu_counter_ticks += timer_counts_per_clock_tick;
}

static void generic_or1k_clock_handler_install(
  proc_ptr new_isr,
   proc_ptr old_isr
)
{
  old_isr = NULL;
  _CPU_ISR_install_vector(OR1K_EXCEPTION_TICK_TIMER,
                          new_isr,
                          old_isr);
}

static void generic_or1k_clock_initialize(void)
{
  uint32_t ttmr;

  /* Calculate timer value for given time per clock tick */
  timer_counts_per_clock_tick = (1000 * rtems_configuration_get_microseconds_per_tick()) /
    OR1K_CLOCK_CYCLE_TIME_NANOSECONDS;

 /* For TTMR register,
  * The least significant 28 bits are the number of clock cycles
  * before generating a tick timer interrupt. While the most
  * significant 4 bits are used for mode configuration, tick timer
  * interrupt enable and pending interrupts status.
  */

  /* FIXME: Long interval should pass since initializing the tick timer
   * registers fires exceptions dispite interrupts has not been enabled yet.
   */
  ttmr = (CPU_OR1K_SPR_TTMR_MODE_RESTART | CPU_OR1K_SPR_TTMR_IE |
          (timer_counts_per_clock_tick & CPU_OR1K_SPR_TTMR_TP_MASK)) &
    ~(CPU_OR1K_SPR_TTMR_IP);

  _OR1K_mtspr(CPU_OR1K_SPR_TTMR, ttmr);
  _OR1K_mtspr(CPU_OR1K_SPR_TTCR, 0);

  /* Initialize CPU Counter */
  cpu_counter_ticks = 0;

  /* Initialize timecounter */
  generic_or1k_tc.tc_get_timecount = generic_or1k_get_timecount;
  generic_or1k_tc.tc_counter_mask = 0xffffffff;
  generic_or1k_tc.tc_frequency = OR1K_BSP_CLOCK_FREQ;
  generic_or1k_tc.tc_quality = RTEMS_TIMECOUNTER_QUALITY_CLOCK_DRIVER;
  rtems_timecounter_install(&generic_or1k_tc);
}

static void generic_or1k_clock_cleanup(void)
{
  uint32_t sr;

  sr = _OR1K_mfspr(CPU_OR1K_SPR_SR);

  /* Disable tick timer exceptions */
  _OR1K_mtspr(CPU_OR1K_SPR_SR, sr & ~CPU_OR1K_SPR_SR_TEE);

  /* Invalidate tick timer config registers */
  _OR1K_mtspr(CPU_OR1K_SPR_TTCR, 0);
  _OR1K_mtspr(CPU_OR1K_SPR_TTMR, 0);
}

static uint32_t generic_or1k_get_timecount(struct timecounter *tc)
{
  uint32_t counts_since_last_timer_interrupt;

  counts_since_last_timer_interrupt = _OR1K_mfspr(CPU_OR1K_SPR_TTCR);

  return cpu_counter_ticks + counts_since_last_timer_interrupt;
}

CPU_Counter_ticks _CPU_Counter_read(void)
{
  return generic_or1k_get_timecount(NULL);
}

CPU_Counter_ticks _CPU_Counter_difference(
  CPU_Counter_ticks second,
  CPU_Counter_ticks first
)
{
  return second - first;
}

#define Clock_driver_support_at_tick() generic_or1k_clock_at_tick()

#define Clock_driver_support_initialize_hardware() generic_or1k_clock_initialize()

#define Clock_driver_support_install_isr(isr, old_isr) \
  do {                                                 \
    old_isr = NULL;                                    \
    generic_or1k_clock_handler_install(isr, old_isr);  \
  } while (0)

#define Clock_driver_support_shutdown_hardware() generic_or1k_clock_cleanup()

#include "../../../shared/clockdrv_shell.h"
