/**
 * @file
 *
 * @ingroup or1k_aac_uart
 *
 * @brief Console Configuration.
 */

/*
 * Copyright (c) 2017 - 2018 ÅAC Microtec AB <www.aacmicrotec.com>
 *
 * Author(s):
 *  Martin Erik Werner <martin.werner@aacmicrotec.com>
 *  Jakob Viketoft <jakob.viketoft@aacmicrotec.com>
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE
 *
 */

#include <rtems/termiostypes.h>

#include <bsp.h>
#include <bsp/fatal.h>
#include <libchip/aac_uart.h>

typedef struct {
  rtems_termios_device_context base;
  aac_uart_args_t uart_args;
  volatile bool poll_initialised;
} console_context_t;

static console_context_t console_context = {
  .base = RTEMS_TERMIOS_DEVICE_CONTEXT_INITIALIZER("CONSOLE UART"),
  .uart_args = {
    .base = AAC_BSP_UARTDBG_BASE,
    .info = NULL
  },
  .poll_initialised = false
};

static void uart_poll_write(
    rtems_termios_device_context *base,
    const char *buf,
    size_t bytes
    )
{
  console_context_t *ctx;

  ctx = (console_context_t *)base;

  while (bytes--) {
    aac_uart_putc(ctx->uart_args.base, *buf++);
  }
}

static int uart_poll_read(rtems_termios_device_context *base)
{
  console_context_t *ctx;

  ctx = (console_context_t *)base;

  if (aac_uart_check_for_char(ctx->uart_args.base)) {
    return aac_uart_getc(ctx->uart_args.base);
  } else {
    /* no characters available */
    return -1;
  }
}

static void uart_poll_initialise(rtems_termios_device_context *base)
{
  console_context_t *ctx;

  ctx = (console_context_t *)base;

  aac_uart_set_moder(ctx->uart_args.base, AAC_UART_MODER_RS422);
  aac_uart_init(ctx->uart_args.base, AAC_BSP_UARTDBG_BAUD);

  aac_uart_set_txr(ctx->uart_args.base, AAC_UART_TXR_ENABLE);
  aac_uart_set_rxr(ctx->uart_args.base, AAC_UART_RXR_ENABLE);
}

static bool uart_poll_first_open(
    rtems_termios_tty *tty,
    rtems_termios_device_context *base,
    struct termios *term,
    rtems_libio_open_close_args_t *args
    )
{
  console_context_t *ctx;
  rtems_status_code sc;

  ctx = (console_context_t *)base;

  sc = rtems_termios_set_initial_baud(tty, AAC_BSP_UARTDBG_BAUD);
  if (sc != RTEMS_SUCCESSFUL) {
    bsp_fatal(BSP_FATAL_CONSOLE_REGISTER_DEV_0);
  }

  if (!ctx->poll_initialised) {
    uart_poll_initialise(base);
    ctx->poll_initialised = true;
  }

  return true;
}

static void uart_poll_last_close(
    rtems_termios_tty *tty,
    rtems_termios_device_context *base,
    rtems_libio_open_close_args_t *args
    )
{
  console_context_t *ctx;

  ctx = (console_context_t *)base;

  aac_uart_flush(ctx->uart_args.base);
  aac_uart_set_rxr(ctx->uart_args.base, AAC_UART_RXR_DISABLE);
  aac_uart_set_txr(ctx->uart_args.base, AAC_UART_TXR_DISABLE);

  ctx->poll_initialised = false;
}

static bool uart_set_attributes(
    rtems_termios_device_context *base,
    const struct termios         *term
    )
{
  console_context_t *ctx;
  rtems_termios_baud_t baudrate_number;

  ctx = (console_context_t *)base;


  baudrate_number = rtems_termios_baud_to_number(term->c_cflag);
  if (!baudrate_number) {
    return false;
  }
  aac_uart_set_bitrate(ctx->uart_args.base, baudrate_number);

  return true;
}

const rtems_termios_device_handler uart_poll_handler = {
  .first_open = uart_poll_first_open,
  .last_close = uart_poll_last_close,
  .poll_read = uart_poll_read,
  .write = uart_poll_write,
  .set_attributes = uart_set_attributes,
  .mode = TERMIOS_POLLED
};

rtems_device_driver console_initialize(
    rtems_device_major_number major,
    rtems_device_minor_number minor,
    void *arg
    )
{
  rtems_status_code sc;

  rtems_termios_initialize();

  sc = rtems_termios_device_install(
      CONSOLE_DEVICE_NAME,
      major,
      minor,
      &uart_poll_handler,
      NULL,
      &console_context.base
      );
  if (sc != RTEMS_SUCCESSFUL) {
    bsp_fatal(BSP_FATAL_CONSOLE_INSTALL_0);
  }

  if (link("/dev/console", "/dev/tty_debug") < 0) {
    bsp_fatal(BSP_FATAL_CONSOLE_INSTALL_1);
  }

  return RTEMS_SUCCESSFUL;
}

static void bsp_out_char(char c)
{
  rtems_interrupt_lock_context lock_context;

  rtems_termios_device_lock_acquire(&console_context.base, &lock_context);
  if (!console_context.poll_initialised) {
    uart_poll_initialise(&console_context.base);
    console_context.poll_initialised = true;
  }
  rtems_termios_device_lock_release(&console_context.base, &lock_context);

  if (c == '\n') {
    const char cr = '\r';
    uart_poll_write(&console_context.base, &cr, 1);
  }

  uart_poll_write(&console_context.base, &c, 1);
}

BSP_output_char_function_type BSP_output_char = bsp_out_char;

BSP_polling_getchar_function_type BSP_poll_char = NULL;
