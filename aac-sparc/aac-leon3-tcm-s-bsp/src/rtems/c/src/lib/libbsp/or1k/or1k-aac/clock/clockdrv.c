/**
 * @file
 *
 * @ingroup bsp_clock
 *
 * @brief or1k_aac clock support.
 */

/*
 * or1k_aac Clock driver
 *
 * Copyright (c) 2014 - 2016 ÅAC Microtec AB <www.aacmicrotec.com>
 * Author(s):
 *  Jakob Viketoft <support@aacmicrotec.com>
 *
 * based on c/src/lib/libbsp/or1k/generic_or1k/clock/clockdrv.c
 * Copyright (c) 2014 - 2015 Hesham ALMatary <heshamelmatary@gmail.com>
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE
 *
 */

#include <rtems.h>
#include <bsp.h>
#include <bsp/irq.h>
#include <rtems/score/cpu.h>
#include <rtems/score/or1k-utility.h>
#include <rtems/counter.h>
#include <rtems/timecounter.h>

/* The number of clock cycles before generating a tick timer interrupt. */
#define OR1K_AAC_CLOCK_CYCLE_TIME_NANOSECONDS (1000000000 / AAC_BSP_CLOCK_FREQ)

static rtems_timecounter_simple or1k_aac_tc_simple;
static CPU_Counter_ticks cpu_counter_ticks;
static uint32_t timer_counts_per_clock_tick;

/* These prototypes are added here to avoid warnings */
void Clock_isr(void *arg);

static void or1k_aac_clock_at_tick(void)
{
  uint32_t ttmr;

  /* Get TTMR value */
  ttmr = _OR1K_mfspr(CPU_OR1K_SPR_TTMR);

  /* Clear interrupt */
  ttmr &=  ~(CPU_OR1K_SPR_TTMR_IP);

  /* Write it back */
  _OR1K_mtspr(CPU_OR1K_SPR_TTMR, ttmr);

  cpu_counter_ticks += timer_counts_per_clock_tick;
}

static void or1k_aac_tc_at_tick(rtems_timecounter_simple *tc)
{
  /* Nothing to do */
}

static void or1k_aac_clock_handler_install(proc_ptr new_isr, proc_ptr old_isr)
{
  old_isr = NULL;
  _CPU_ISR_install_vector(OR1K_EXCEPTION_TICK_TIMER,
                          new_isr,
                          old_isr);
}

static void or1k_aac_clock_cleanup(void)
{
  uint32_t sr;

  sr = _OR1K_mfspr(CPU_OR1K_SPR_SR);

  /* Disable tick timer exceptions */
  _OR1K_mtspr(CPU_OR1K_SPR_SR, sr & ~CPU_OR1K_SPR_SR_TEE);

  /* Invalidate tick timer config registers */
  _OR1K_mtspr(CPU_OR1K_SPR_TTCR, 0);
  _OR1K_mtspr(CPU_OR1K_SPR_TTMR, 0);
}

static uint32_t or1k_aac_get_tc_simple(rtems_timecounter_simple *tc)
{
  return _OR1K_mfspr(CPU_OR1K_SPR_TTCR);
}

static bool or1k_aac_tc_is_pending(rtems_timecounter_simple *tc)
{
  if (_OR1K_mfspr(CPU_OR1K_SPR_TTMR) & CPU_OR1K_SPR_TTMR_IP) {
    return 1;
  } else {
    return 0;
  }
}

static uint32_t or1k_aac_tc_get_timecount(struct timecounter *tc)
{
  return rtems_timecounter_simple_upcounter_get(tc,
                                                or1k_aac_get_tc_simple,
                                                or1k_aac_tc_is_pending);
}

static void or1k_aac_tc_tick(void)
{
  rtems_timecounter_simple_upcounter_tick(&or1k_aac_tc_simple,
                                          or1k_aac_get_tc_simple,
                                          or1k_aac_tc_at_tick);
}

CPU_Counter_ticks _CPU_Counter_read(void)
{
  return or1k_aac_get_tc_simple(NULL);
}

CPU_Counter_ticks _CPU_Counter_difference(CPU_Counter_ticks second,
                                          CPU_Counter_ticks first)
{
  return second - first;
}

static void or1k_aac_clock_initialize(void)
{
  uint32_t ttmr;

  /* Calculate timer value for given time per clock tick */
  timer_counts_per_clock_tick = (1000 * rtems_configuration_get_microseconds_per_tick()) /
    OR1K_AAC_CLOCK_CYCLE_TIME_NANOSECONDS;

  /* For TTMR register,
   * The least significant 28 bits are the number of clock cycles
   * before generating a tick timer interrupt. While the most
   * significant 4 bits are used for mode configuration, tick timer
   * interrupt enable and pending interrupts status.
   */

  ttmr = (CPU_OR1K_SPR_TTMR_MODE_RESTART | CPU_OR1K_SPR_TTMR_IE |
          (timer_counts_per_clock_tick & CPU_OR1K_SPR_TTMR_TP_MASK)) &
    ~(CPU_OR1K_SPR_TTMR_IP);

  _OR1K_mtspr(CPU_OR1K_SPR_TTMR, ttmr);
  _OR1K_mtspr(CPU_OR1K_SPR_TTCR, 0);

  /* Initialize CPU Counter */
  cpu_counter_ticks = 0;

  /* Initialize timecounter */
  rtems_timecounter_simple_install(&or1k_aac_tc_simple,
                                   AAC_BSP_CLOCK_FREQ,
                                   timer_counts_per_clock_tick,
                                   or1k_aac_tc_get_timecount);

  rtems_counter_initialize_converter(AAC_BSP_CLOCK_FREQ);
}

#define Clock_driver_timecounter_tick() or1k_aac_tc_tick()

#define Clock_driver_support_at_tick() or1k_aac_clock_at_tick()

#define Clock_driver_support_initialize_hardware() or1k_aac_clock_initialize()

#define Clock_driver_support_install_isr(isr, old_isr)  \
  do {                                                  \
    or1k_aac_clock_handler_install(isr, old_isr);       \
    old_isr = NULL;                                     \
  } while (0)

#define Clock_driver_support_shutdown_hardware() or1k_aac_clock_cleanup()

#include "../../../shared/clockdrv_shell.h"
