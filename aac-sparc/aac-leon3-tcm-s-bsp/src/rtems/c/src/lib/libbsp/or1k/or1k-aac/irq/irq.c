/**
 * @file
 *
 * @ingroup or1k_interrupt
 *
 * @brief Interrupt support.
 */

/*
 * Copyright (c) 2014 ÅAC Microtec AB <www.aacmicrotec.com>
 *  Karol Gugala <kgugala@antmicro.com>
 *  Tomasz Gorochowik <tgorochowik@antmicro.com>
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE.
 */

#include <bsp/irq.h>
#include <bsp/irq-generic.h>
#include <assert.h>

static int or1k_external_interrupt_handler(rtems_vector_number vector)
{
  uint32_t picsr;
  uint32_t int_num;

  /* Get picsr */
  picsr = _OR1K_mfspr(CPU_OR1K_SPR_PICSR);

  /* Make sure we have a pending interrupt */
  assert(picsr != 0);

  /* Go through all set interrupts in a round-robin style */
  while (picsr) {
    /* Find pending interrupt with lowest number */
    int_num = _OR1K_Find_First_One(picsr);

    /* Adjust vector number with a start from 0 */
    int_num--;

    /* Call the interrupt handler */
    bsp_interrupt_handler_dispatch((rtems_vector_number) int_num);

    /* Clear the interrupt */
    picsr &= ~(1 << int_num);
    _OR1K_mtspr(CPU_OR1K_SPR_PICSR, picsr);
  }

  return 0;
}

void bsp_interrupt_handler_default(rtems_vector_number vector)
{
  printk("Unhandled interrupt, number: %u\n", vector);
}

rtems_status_code bsp_interrupt_facility_initialize()
{
  /* Install exception handler for external interrupt exception */
  _CPU_ISR_install_vector(OR1K_EXCEPTION_IRQ, or1k_external_interrupt_handler, NULL);

  /* Clear all pending interrupts */
  _OR1K_mtspr(CPU_OR1K_SPR_PICSR, 0);

  return RTEMS_SUCCESSFUL;
}

rtems_status_code bsp_interrupt_vector_enable(rtems_vector_number vector)
{
  uint32_t picmr;

  picmr = _OR1K_mfspr(CPU_OR1K_SPR_PICMR);
  picmr |= (1 << vector);
  _OR1K_mtspr(CPU_OR1K_SPR_PICMR, picmr);

  return RTEMS_SUCCESSFUL;
}

rtems_status_code bsp_interrupt_vector_disable(rtems_vector_number vector)
{
  uint32_t picmr;

  picmr = _OR1K_mfspr(CPU_OR1K_SPR_PICMR);
  picmr &= ~(1 << vector);
  _OR1K_mtspr(CPU_OR1K_SPR_PICMR, picmr);

  return RTEMS_SUCCESSFUL;
}
