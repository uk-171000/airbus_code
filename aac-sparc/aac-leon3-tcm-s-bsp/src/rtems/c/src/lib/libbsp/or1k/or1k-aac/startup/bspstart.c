/**
 * @file
 *
 * @ingroup or1k_aac
 *
 * @brief BSP Startup
 */

/*
 * Copyright (c) 2014 ÅAC Microtec AB <www.aacmicrotec.com>
 *
 * Author(s):
 *  Tomasz Gorochowik <tgorochowik@antmicro.com>
 *  Karol Gugala <kgugala@antmicro.com>
 *
 * based on  c/src/lib/libbsp/or1k/ork1sim/startup/bspstart.c
 * Copyright (c) 2014 Hesham ALMatary <heshamelmatary@gmail.com>
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE
 *
 */

#include <bsp.h>
#include <bsp/bootcard.h>
#include <bsp/irq.h>
#include <bsp/irq-generic.h>
#include <libcpu/cache.h>

void bsp_start( void )
{
  /* Data and instruction disabling and flushing is handled by the startup
     assembler code, no need to replicate this here. */

  /* Enable caches */
  _CPU_cache_enable_instruction();
  _CPU_cache_enable_data();

  /* Initialize external interrupts */
  bsp_interrupt_initialize();
}
