/**
 * @file
 *
 * @ingroup or1k_aac_interrupt
 *
 * @brief Interrupt definitions.
 */

/**
 * Copyright (c) 2014 ÅAC Microtec AB <www.aacmicrotec.com>
 *
 * Author(s):
 *  Tomasz Gorochowik <tgorochowik@antmicro.com>
 *  Karol Gugala <kgugala@antmicro.com>
 *
 * based on  c/src/lib/libbsp/or1k/ork1sim/include/irq.h
 * Copyright (c) 2014 Hesham ALMatary <heshamelmatary@gmail.com>
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE
 *
 */

#ifndef LIBBSP_OR1K_AAC_IRQ_H
#define LIBBSP_OR1K_AAC_IRQ_H

#ifndef ASM

#include <rtems.h>
#include <rtems/irq.h>
#include <rtems/irq-extension.h>

#define BSP_INTERRUPT_VECTOR_MIN  0
#define BSP_INTERRUPT_VECTOR_MAX  30

#define BSP_INTERRUPT_USE_INDEX_TABLE
#define BSP_INTERRUPT_HANDLER_TABLE_SIZE 31

#endif /* ASM */
#endif /* LIBBSP_OR1K_AAC_IRQ_H */
