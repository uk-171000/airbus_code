/**
 * Copyright (c) 2014 - 2016 ÅAC Microtec AB <www.aacmicrotec.com>
 *
 * Author(s):
 *  Jakob Viketoft <jakob.viketoft@aacmicrotec.com>
 *
 * based on  c/src/lib/libbsp/or1k/generic_ork1/start/start.S
 * Copyright (c) 2014 Hesham ALMatary <heshamelmatary@gmail.com>
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE
 */
#include <bsp/linker-symbols.h>
#include <rtems/score/or1k-utility.h>

  .extern _ISR_Handler

/* The following macro defines the first instructions every exception
 * should execute before jumping to its handler function from the
 * exception vector table. r3 is saved into the stack and loaded with
 * vector number before jumping to _ISR_Handler. r3 value is restored
 * back from _ISR_Handler after handling the exception and before
 * returning from interrupt.
 */
#define EXCEPTION_SETUP(vector) \
  l.nop   ; \
  /* Add space for redzone and make space on the current stack \
   * (from the interrupted thread) */ \
  l.addi  r1, r1, -(REDZONE_SIZE + EX_FRAME_SIZE); \
  l.sw    OR1K_EC_GPR3(r1), r3; \
  l.addi  r3, r0, vector; \
  l.j     _ISR_Handler; \
  l.nop

  .extern boot_card
  .extern _OR1K_Exception_default

  /* Global symbols */
  .global _start
  .global bsp_start_vector_table_begin

/* Populate HW vector table */

.section .vector, "ax"

.org 0x100
_reset:
  l.j _start
  l.nop

.org 0x200
_buserr:
  EXCEPTION_SETUP(2)

.org 0x300
_dPageFault:
  EXCEPTION_SETUP(3)

.org 0x400
_iPageFault:
  EXCEPTION_SETUP(4)

.org 0x500
_timer:
  EXCEPTION_SETUP(5)

.org 0x600
_unalign:
  EXCEPTION_SETUP(6)

.org 0x700
_undefIns:
  EXCEPTION_SETUP(7)

.org 0x800
_exInt:
  EXCEPTION_SETUP(8)

.org 0x900
_dTLB:
  EXCEPTION_SETUP(9)

.org 0xA00
_iTLB:
  EXCEPTION_SETUP(10)

.org 0xB00
_range:
  EXCEPTION_SETUP(11)

.org 0xC00
_syscall:
  EXCEPTION_SETUP(12)

.org 0xD00
_fp:
  EXCEPTION_SETUP(13)

.org 0xE00
_trap:
  EXCEPTION_SETUP(14)

.org 0xF00
_res_future1:
  EXCEPTION_SETUP(15)

.org 0x1000
_res_future2:
  EXCEPTION_SETUP(16)

.org 0x1100
_res_future3:
  EXCEPTION_SETUP(17)

.org 0x1200
_res_future4:
  EXCEPTION_SETUP(18)

.org 0x1300
_res_future5:
  EXCEPTION_SETUP(19)

.org 0x1400
_res_future6:
  EXCEPTION_SETUP(20)

.org 0x1500
_res_impspec1:
  EXCEPTION_SETUP(21)

.org 0x1600
_res_impspec2:
  EXCEPTION_SETUP(22)

.org 0x1700
_res_impspec3:
  EXCEPTION_SETUP(23)

.org 0x1800
_res_impspec4:
  EXCEPTION_SETUP(24)

.org 0x1900
_res_custom1:
  EXCEPTION_SETUP(25)

.org 0x1A00
_res_custom2:
  EXCEPTION_SETUP(26)

.org 0x1B00
_res_custom3:
  EXCEPTION_SETUP(27)

.org 0x1C00
_res_custom4:
  EXCEPTION_SETUP(28)

.org 0x1D00
_res_custom5:
  EXCEPTION_SETUP(29)

.org 0x1E00
_res_custom6:
  EXCEPTION_SETUP(30)

.org 0x1F00
_res_custom7:
  EXCEPTION_SETUP(31)

.org 0x2000

bsp_start_vector_table_begin:

  .word 0
  .word _start /* Reset */
  .word _OR1K_Exception_default /* Bus Error */
  .word _OR1K_Exception_default /* Data Page Fault */
  .word _OR1K_Exception_default /* Instruction Page Fault */
  .word _OR1K_Exception_default /* Tick timer */
  .word _OR1K_Exception_default /* Alignment */
  .word _OR1K_Exception_default /* Undefined Instruction */
  .word _OR1K_Exception_default /* External Interrupt */
  .word _OR1K_Exception_default /* Data TLB Miss */
  .word _OR1K_Exception_default /* Instruction TLB Miss */
  .word _OR1K_Exception_default /* Range Exception */
  .word _OR1K_Exception_default /* System Call */
  .word _OR1K_Exception_default /* Floating Point Exception */
  .word _OR1K_Exception_default /* Trap */
  .word _OR1K_Exception_default /* Reserved for future use */
  .word _OR1K_Exception_default /* Reserved for future use */
  .word _OR1K_Exception_default /* Reserved for future use */
  .word _OR1K_Exception_default /* Reserved for future use */
  .word _OR1K_Exception_default /* Reserved for future use */
  .word _OR1K_Exception_default /* Reserved for future use */
  .word _OR1K_Exception_default /* Reserved for implementation-specific */
  .word _OR1K_Exception_default /* Reserved for implementation-specific */
  .word _OR1K_Exception_default /* Reserved for implementation-specific */
  .word _OR1K_Exception_default /* Reserved for implementation-specific */
  .word _OR1K_Exception_default /* Reserved for custom exceptions. */
  .word _OR1K_Exception_default /* Reserved for custom exceptions. */
  .word _OR1K_Exception_default /* Reserved for custom exceptions. */
  .word _OR1K_Exception_default /* Reserved for custom exceptions. */
  .word _OR1K_Exception_default /* Reserved for custom exceptions. */
  .word _OR1K_Exception_default /* Reserved for custom exceptions. */
  .word _OR1K_Exception_default /* Reserved for custom exceptions. */

bsp_start_vector_table_end:

  .section  ".bsp_start_text", "ax"
  .type _start, @function

_start:
  /* Clear r0 */
  l.movhi r0, 0

  /* Set SR register to Supervision mode */
  l.ori   r1, r0, CPU_OR1K_SPR_SR_SM
  l.mtspr r0, r1, CPU_OR1K_SPR_SR

  /* Clear all registers */
  l.movhi r1, 0
  l.movhi r2, 0
  l.movhi r3, 0
  l.movhi r4, 0
  l.movhi r5, 0
  l.movhi r6, 0
  l.movhi r7, 0
  l.movhi r8, 0
  l.movhi r9, 0
  l.movhi r10, 0
  l.movhi r11, 0
  l.movhi r12, 0
  l.movhi r13, 0
  l.movhi r14, 0
  l.movhi r15, 0
  l.movhi r16, 0
  l.movhi r17, 0
  l.movhi r18, 0
  l.movhi r19, 0
  l.movhi r20, 0
  l.movhi r21, 0
  l.movhi r22, 0
  l.movhi r23, 0
  l.movhi r24, 0
  l.movhi r25, 0
  l.movhi r26, 0
  l.movhi r27, 0
  l.movhi r28, 0
  l.movhi r29, 0
  l.movhi r30, 0
  l.movhi r31, 0

  /* Disable all interrupts */
  l.mtspr r0, r0, CPU_OR1K_SPR_PICMR

  /* Clear all pending interrupts */
  l.mtspr r0, r0, CPU_OR1K_SPR_PICSR

  /* Disable Tick-Timer */
  l.mtspr r0, r0, CPU_OR1K_SPR_TTMR
  /* Zero the counter */
  l.mtspr r0, r0, CPU_OR1K_SPR_TTCR

  /* Check if IC present and skip otherwise */
  l.mfspr r24, r0, CPU_OR1K_SPR_UPR
  l.andi  r26, r24, CPU_OR1K_SPR_UPR_ICP
  l.sfeq  r26, r0
  l.bf    9f
  l.nop

  /* Disable I-Cache */
  l.mfspr r13, r0, CPU_OR1K_SPR_SR
  l.addi  r11, r0, -1
  l.xori  r11, r11, CPU_OR1K_SPR_SR_ICE
  l.and   r11, r13, r11
  l.mtspr r0, r11, CPU_OR1K_SPR_SR

  /* Establish cache block size
     If BS=0, 16;
     If BS=1, 32;
     r14 contain block size
  */
  l.mfspr r24, r0, CPU_OR1K_SPR_ICCFGR
  l.andi  r26, r24, CPU_OR1K_SPR_ICCFGR_CBS
  l.srli  r28, r26, 7
  l.ori   r30, r0, 16
  l.sll   r14, r30, r28

  /* Establish number of cache sets
     r16 contains number of cache sets
     r28 contains log(# of cache sets)
  */
  l.andi  r26, r24, CPU_OR1K_SPR_ICCFGR_NCS
  l.srli  r28, r26, 3
  l.ori   r30, r0, 1
  l.sll   r16, r30, r28

  /* Invalidate IC */
  l.addi  r6, r0, 0
  l.sll   r5, r14, r28
1:
  l.mtspr r0, r6, CPU_OR1K_SPR_ICBIR
  l.sfne  r6, r5
  l.bf    1b
  l.add   r6, r6, r14

9:
  /* Check if DC present and skip otherwise */
  l.mfspr r24, r0, CPU_OR1K_SPR_UPR
  l.andi  r26, r24, CPU_OR1K_SPR_UPR_DCP
  l.sfeq  r26, r0
  l.bf    9f
  l.nop

  /* Disable DC */
  l.mfspr r6, r0, CPU_OR1K_SPR_SR
  l.addi  r5, r0, -1
  l.xori  r5, r5, CPU_OR1K_SPR_SR_DCE
  l.and   r5, r6, r5
  l.mtspr r0, r5, CPU_OR1K_SPR_SR

  /* Establish cache block size
     If BS=0, 16;
     If BS=1, 32;
     r14 contain block size
  */
  l.mfspr r24, r0,CPU_OR1K_SPR_DCCFGR
  l.andi  r26, r24, CPU_OR1K_SPR_DCCFGR_CBS
  l.srli  r28, r26, 7
  l.ori   r30, r0, 16
  l.sll   r14, r30, r28

  /* Establish number of cache sets
     r16 contains number of cache sets
     r28 contains log(# of cache sets)
  */
  l.andi  r26, r24, CPU_OR1K_SPR_DCCFGR_NCS
  l.srli  r28, r26, 3
  l.ori   r30, r0, 1
  l.sll   r16, r30, r28

  /* Invalidate DC */
  l.addi  r6, r0, 0
  l.sll   r5, r14, r28
1:
  l.mtspr r0, r6, CPU_OR1K_SPR_DCBIR
  l.sfne  r6, r5
  l.bf    1b
  l.add   r6, r6, r14

9:
  /* load stack and frame pointers */
  l.movhi r1, hi(bsp_section_stack_begin)
  l.ori   r1, r1, lo(bsp_section_stack_begin)
  l.add   r2, r0, r1

  /* Clearing .bss */
  l.movhi r13, hi(bsp_section_bss_begin)
  l.ori   r13, r13, lo(bsp_section_bss_begin)
  l.movhi r15, hi(bsp_section_bss_end)
  l.ori   r15, r15, lo(bsp_section_bss_end)

_loop_clear_bss:
  l.sfgeu r13, r15
  l.bf    _end_clear_bss
  l.addi  r13, r13, 4
  l.sw    0(r13), r0
  l.j     _loop_clear_bss
  l.nop
_end_clear_bss:

  l.j boot_card
  l.nop

/* Temporary code for unhandled exceptions */
.section .text
.align
.global _unhandled_exception

unhandled_exception:
  l.nop
