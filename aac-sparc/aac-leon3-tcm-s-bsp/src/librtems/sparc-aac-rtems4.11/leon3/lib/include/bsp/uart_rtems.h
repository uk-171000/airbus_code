/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2018 ÅAC Microtec AB
 *
 * Filename:     uart_rtems.h
 * Module name:  uart
 *
 * Author(s):    ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  ÅAC UART RTEMS driver declarations
 * Requirements: Adheres to the ÅAC UART Driver Requirement Specification Rev. A
 *----------------------------------------------------------------------------*/

#ifndef _RTEMS_UART_H_
#define _RTEMS_UART_H_

#define UART_B1200                      (0)
#define UART_B2400                      (1)
#define UART_B4800                      (2)
#define UART_B9600                      (3)
#define UART_B19200                     (4)
#define UART_B38400                     (5)
#define UART_B57600                     (6)
#define UART_B76800                     (7)
#define UART_B115200                    (8)
#define UART_B153600                    (9)
#define UART_B375000                    (10)

#define UART_DEFAULT_BITRATE            (115200)

#define UART_BUFFER_DEPTH_16            (0)
#define UART_BUFFER_DEPTH_32            (1)
#define UART_BUFFER_DEPTH_64            (2)
#define UART_BUFFER_DEPTH_128           (3)

#define UART_TRIGGER_LEVEL_1            (0)
#define UART_TRIGGER_LEVEL_4            (1)
#define UART_TRIGGER_LEVEL_8            (2)
#define UART_TRIGGER_LEVEL_14           (3)

#define UART_IOCTL_SET_BITRATE          _IOW('W', 1, uint32_t)
#define UART_IOCTL_MODE_SELECT          _IOW('W', 2, uint32_t)
#define UART_IOCTL_RX_FLUSH             _IOW('W', 3, uint32_t)
#define UART_IOCTL_SET_PARITY           _IOW('W', 4, uint32_t)
#define UART_IOCTL_SET_BUFFER_DEPTH     _IOW('W', 5, uint32_t)
#define UART_IOCTL_GET_BUFFER_DEPTH     _IOW('R', 6, uint32_t)
#define UART_IOCTL_SET_TRIGGER_LEVEL    _IOW('W', 7, uint32_t)
#define UART_IOCTL_GET_TRIGGER_LEVEL    _IOW('R', 8, uint32_t)

#define UART_RTEMS_MODE_RS422           (0x00)
#define UART_RTEMS_MODE_RS485           (0x01)
#define UART_RTEMS_MODE_LOOPBACK        (0x02)

#define UART_PARITY_NONE                (0)
#define UART_PARITY_ODD                 (1)
#define UART_PARITY_EVEN                (2)

rtems_device_driver rtems_uart_initialize(rtems_device_major_number major,
                                              rtems_device_minor_number minor,
                                              void *args);

/* As the driver registers as an IMFS generic node, the driver table only needs
   to handle the initialization */
#define AAC_UART_DRIVER_TABLE_ENTRY {       \
      rtems_uart_initialize,                \
      NULL,                                 \
      NULL,                                 \
      NULL,                                 \
      NULL,                                 \
      NULL }

#endif /* _RTEMS_UART_H_ */
