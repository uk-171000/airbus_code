/*------------------------------------------------------------------------------
 * Copyright (C) 2015 - 2016 ÅAC Microtec AB
 *
 * Filename:     bsp_confdefs.h
 * Module name:  RTEMS
 *
 * Author(s):    JaVi
 * Support:      support@aacmicrotec.com
 * Description:  A file with configuration defines for using drivers with RTEMS
 * Requirements: -
 *----------------------------------------------------------------------------*/

#ifndef BSP_CONFDEFS_H
#define BSP_CONFDEFS_H

#include <rtems.h>
#include <rtems/score/apimutex.h>
#include <rtems/score/wkspace.h>

#define CONFIGURE_HAS_OWN_DEVICE_DRIVER_TABLE

#ifdef CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
  #include <rtems/console.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
  #include <rtems/clockdrv.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_ADC_DRIVER
  #include <bsp/adc_rtems.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_CCSDS_DRIVER
  #include <bsp/ccsds_rtems.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_ERROR_MANAGER_DRIVER
  #include <bsp/error_manager_rtems.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_GPIO_DRIVER
  #include <bsp/gpio_rtems.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_MASSMEM_FLASH_DRIVER
  #include <bsp/massmem_flash_rtems.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_SCET_DRIVER
  #include <bsp/scet_rtems.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_SPACEWIRE_DRIVER
  #include <bsp/spacewire_node_rtems.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_SPI_RAM_DRIVER
  #include <bsp/spi_ram_rtems.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_SYSTEM_FLASH_DRIVER
  #include <bsp/system_flash_rtems.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_UART_DRIVER
  #include <bsp/uart_rtems.h>
#endif

#ifdef CONFIGURE_APPLICATION_NEEDS_WDT_DRIVER
  #include <bsp/wdt_rtems.h>
#endif

#ifdef CONFIGURE_INIT
  rtems_driver_address_table
    _IO_Driver_address_table[ CONFIGURE_MAXIMUM_DRIVERS ] = {
    #ifdef CONFIGURE_BSP_PREREQUISITE_DRIVERS
      CONFIGURE_BSP_PREREQUISITE_DRIVERS,
    #endif
    #ifdef CONFIGURE_APPLICATION_PREREQUISITE_DRIVERS
      CONFIGURE_APPLICATION_PREREQUISITE_DRIVERS,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
      CONSOLE_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
      CLOCK_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_RTC_DRIVER
      RTC_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_STUB_DRIVER
      DEVNULL_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_ZERO_DRIVER
      DEVZERO_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_IDE_DRIVER
      IDE_CONTROLLER_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_ATA_DRIVER
      ATA_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_FRAME_BUFFER_DRIVER
      FRAME_BUFFER_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_EXTRA_DRIVERS
      CONFIGURE_APPLICATION_EXTRA_DRIVERS,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_ADC_DRIVER
      AAC_ADC_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_CCSDS_DRIVER
      AAC_CCSDS_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_GPIO_DRIVER
      AAC_GPIO_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_ERROR_MANAGER_DRIVER
      AAC_ERROR_MANAGER_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_MASSMEM_FLASH_DRIVER
      AAC_MASSMEM_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_SCET_DRIVER
      AAC_SCET_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_SPACEWIRE_DRIVER
      AAC_SPWN_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_SPI_RAM_DRIVER
      AAC_SPI_RAM_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_SYSTEM_FLASH_DRIVER
      AAC_SYSTEM_FLASH_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_UART_DRIVER
      AAC_UART_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_WDT_DRIVER
      AAC_WATCHDOG_DRIVER_TABLE_ENTRY,
    #endif
    #ifdef CONFIGURE_APPLICATION_NEEDS_NULL_DRIVER
      NULL_DRIVER_TABLE_ENTRY
    #elif !defined(CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER) && \
        !defined(CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER) && \
        !defined(CONFIGURE_APPLICATION_NEEDS_RTC_DRIVER) && \
        !defined(CONFIGURE_APPLICATION_NEEDS_STUB_DRIVER) && \
        !defined(CONFIGURE_APPLICATION_NEEDS_ZERO_DRIVER) && \
        !defined(CONFIGURE_APPLICATION_NEEDS_IDE_DRIVER) && \
        !defined(CONFIGURE_APPLICATION_NEEDS_ATA_DRIVER) && \
        !defined(CONFIGURE_APPLICATION_NEEDS_FRAME_BUFFER_DRIVER) && \
        !defined(CONFIGURE_APPLICATION_EXTRA_DRIVERS)
      NULL_DRIVER_TABLE_ENTRY
    #endif
  };

  const size_t _IO_Number_of_drivers =
    RTEMS_ARRAY_SIZE( _IO_Driver_address_table );
#endif

#endif /* BSP_CONFDEFS_H */
