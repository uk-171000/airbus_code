/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2016 ÅAC Microtec AB
 *
 * Filename:     uart.h
 * Module name:  uart
 *
 * Author(s):    PeBr, ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  UART bare metal driver definitions
 * Requirements: ÅAC UART Requirement Specification, Rev. A
 *----------------------------------------------------------------------------*/

#ifndef _UART_H_
#define _UART_H_

#include <stdint.h>
#include <toolchain_support.h>

/*
 * UART register map
 */
#define UART_RBR        (0x0)   /* R:  Receive buffer (DLAB = 0) */
#define UART_THR        (0x0)   /* W:  Transmit buffer (DLAB = 0) */
#define UART_DLL        (0x0)   /* RW: Divisor Latch Low (DLAB = 1) */
#define UART_DLM        (0x1)   /* RW: Divisor Latch High (DLAB = 1) */
#define UART_IER        (0x1)   /* RW: Interrupt Enable Register */
#define UART_IIR        (0x2)   /* R:  Interrupt ID Register */
#define UART_FCR        (0x2)   /* W:  FIFO Control Register */
#define UART_LCR        (0x3)   /* RW: Line Control Register */
#define UART_MCR        (0x4)   /* RW: Modem Control Register */
#define UART_LSR        (0x5)   /* RW: Line Status Register */
#define UART_MSR        (0x6)   /* RW: Modem Status Register */
#define UART_MODER      (0x7)   /* RW: RS485/RS422 mode control Register */
#define UART_TXR        (0x8)   /* RW: Enable/disable Line Driver control */
#define UART_RXR        (0x9)   /* RW: Enable/disable Line Receiver control */
#define UART_BCR        (0xA)   /* R:  Read out the current receive buffer count */ 

/*
 * These are the definitions for the Divisor Latch Low register
 */
#define UART_DLL_MASK   (0xFF)

/*
 * These are the definitions for the Divisor Latch High register
 */
#define UART_DLM_MASK   (0xFF)
#define UART_DLM_SHAMT  (8)     /* Shift amount for the part of the divisor in DLM */

/*
 * These are the definitions for the Interrupt Enable Register
 */
#define UART_IER_RDI    (0x01)  /* Enable receiver data interrupt */
#define UART_IER_THRI   (0x02)  /* Enable Transmitter holding register int. */
#define UART_IER_RLSI   (0x04)  /* Enable receiver line status interrupt */
#define UART_IER_MSI    (0x08)  /* Enable Modem status interrupt */

/*
 * These are the definitions for the Interrupt Identification Register
 */
#define UART_IIR_RLSI   (0x01)  /* Receiver line status interrupt */
#define UART_IIR_RDI    (0x02)  /* Receiver data interrupt */
#define UART_IIR_TOI    (0x04)  /* Receive timeout interrupt */
#define UART_IIR_THRI   (0x08)  /* Transmitter holding register empty */
#define UART_IIR_MSI    (0x10)  /* Modem status interrupt */

/*
 * These are the definitions for the FIFO Control Register
 */
#define UART_FCR_ENABLE_FIFO    (0x01) /* Enable the FIFO */
#define UART_FCR_CLEAR_RCVR     (0x02) /* Clear the RCVR FIFO */
#define UART_FCR_CLEAR_XMIT     (0x04) /* Clear the XMIT FIFO */
#define UART_FCR_DMA_SELECT     (0x08) /* For DMA applications */
#define UART_FCR_BUFFER_MASK    (0x30) /* Mask for the chosen buffer depth */
#define UART_FCR_BUFFER_16      (0x00) /* Buffer depth of 16 */
#define UART_FCR_BUFFER_32      (0x10) /* Buffer depth of 32 */
#define UART_FCR_BUFFER_64      (0x20) /* Buffer depth of 64 */
#define UART_FCR_BUFFER_128     (0x30) /* Buffer depth of 128 */
#define UART_FCR_TRIGGER_MASK   (0xC0) /* Mask for the FIFO trigger range */
#define UART_FCR_TRIGGER_1      (0x00) /* Trigger set at 1 byte */
#define UART_FCR_TRIGGER_4      (0x40) /* Trigger set at 1/4 of buffer depth  */
#define UART_FCR_TRIGGER_8      (0x80) /* Trigger set at 1/2 of buffer depth */
#define UART_FCR_TRIGGER_14     (0xC0) /* Trigger set at 2 bytes from buffer top */

/*
 * These are the definitions for the Line Control Register
 */
#define UART_LCR_WLS    (0x03)  /* Mask for the Word length select bits */
#define UART_LCR_WLEN5  (0x00)  /* Word length of 5 bits */
#define UART_LCR_WLEN6  (0x01)  /* Word length of 6 bits */
#define UART_LCR_WLEN7  (0x02)  /* Word length of 7 bits */
#define UART_LCR_WLEN8  (0x03)  /* Word length of 8 bits */
#define UART_LCR_STB    (0x04)  /* Mask for stop bits */
#define UART_LCR_STOP_1 (0x00)  /* 1 stop bit */
#define UART_LCR_STOP_2 (0x00)  /* 1.5 stop bits for WLEN5, 2 stop bits otherwise */
#define UART_LCR_PEN    (0x08)  /* Parity Enable */
#define UART_LCR_PDIS   (0x00)  /* Parity Disable */
#define UART_LCR_EPS    (0x10)  /* Even parity select */
#define UART_LCR_SPAR   (0x20)  /* Stick parity */
#define UART_LCR_SBC    (0x40)  /* Set break control */
#define UART_LCR_DLAB   (0x80)  /* Divisor latch access bit */

/*
 * These are the definitions for the Modem Control Register
 */
#define UART_MCR_DTR    (0x01)  /* DTR complement */
#define UART_MCR_RTS    (0x02)  /* RTS complement */
#define UART_MCR_OUT1   (0x04)  /* Out1 complement */
#define UART_MCR_OUT2   (0x08)  /* Out2 complement */
#define UART_MCR_LOOP   (0x10)  /* Enable loopback test mode */

/*
 * These are the definitions for the Line Status Register
 */
#define UART_LSR_DR     (0x01)  /* Receiver data ready */
#define UART_LSR_OE     (0x02)  /* Overrun error indicator */
#define UART_LSR_PE     (0x04)  /* Parity error indicator */
#define UART_LSR_FE     (0x08)  /* Framing error indicator */
#define UART_LSR_BI     (0x10)  /* Break interrupt indicator */
#define UART_LSR_THRE   (0x20)  /* Transmit-holding-register empty */
#define UART_LSR_TEMT   (0x40)  /* Transmitter empty */
#define UART_LSR_RCVR   (0x80)  /* Error in receiver FIFO */

/*
 * These are the definitions for the Modem Status Register
 */
#define UART_MSR_DCTS   (0x01)  /* Delta CTS */
#define UART_MSR_DDSR   (0x02)  /* Delta DSR */
#define UART_MSR_TERI   (0x04)  /* Trailing edge ring indicator */
#define UART_MSR_DDCD   (0x08)  /* Delta DCD */
#define UART_MSR_CTS    (0x10)  /* Clear to Send */
#define UART_MSR_DSR    (0x20)  /* Data Set Ready */
#define UART_MSR_RI     (0x40)  /* Ring Indicator */
#define UART_MSR_DCD    (0x80)  /* Data Carrier Detect */

/*
 * These are the definitions for the mode control registers
 */
#define UART_MODER_RS422        (0x00)
#define UART_MODER_RS485        (0x01)
#define UART_MODER_LOOPBACK     (0x02)

/*
 * These are the definitions for the transmit line driver control registers
 */
#define UART_TXR_DISABLE        (0x00)
#define UART_TXR_ENABLE         (0x01)

/*
 * These are the definitions for the receiver line driver control registers
 */
#define UART_RXR_DISABLE        (0x00)
#define UART_RXR_ENABLE         (0x01)

/*
 * These are the definitions of buffer depth selections
 */
#define UART_BUFFER_16          (0)
#define UART_BUFFER_32          (1)
#define UART_BUFFER_64          (2)
#define UART_BUFFER_128         (3)

/*
 * These are the definitions of trigger level selections
 */
#define UART_TRIGGER_1          (0)
#define UART_TRIGGER_4          (1)
#define UART_TRIGGER_8          (2)
#define UART_TRIGGER_14         (3)

/*
 * Name:        uart_args_t
 * @brief       Interrupt argument struct
 */
typedef struct uart_args
{
  uint32_t      base;
  void          *info;
} uart_args_t;

/*
 * Name:        uart_callbacks_t
 * @brief       Interrupt callback struct with all callback functions
 */
typedef struct uart_callbacks
{
  /*
   * Name:      uart_rx_linestatus_callback
   * @brief     Callback for receiver line status (error) event
   * @param[in] uart_args_t with info specific for this UART
   */
  void (*uart_rx_linestatus_callback)(uart_args_t *arg);

  /*
   * Name:      uart_rx_data_callback
   * @brief     Callback for receiver data available event
   * @param[in] uart_args_t with info specific for this UART
   */
  void (*uart_rx_data_callback)(uart_args_t *arg);

  /*
   * Name:      uart_timeout_callback
   * @brief     Callback for character timeout indication event
   * @param[in] uart_args_t with info specific for this UART
   */
  void (*uart_timeout_callback)(uart_args_t *arg);

  /*
   * Name:      uart_tx_empty_callback
   * @brief     Callback for transmitter holding register empty event
   * @param[in] uart_args_t with info specific for this UART
   */
  void (*uart_tx_empty_callback)(uart_args_t *arg);

  /*
   * Name:      uart_modem_callback
   * @brief     Callback for MODEM status
   * @param[in] uart_args_t with info specific for this UART
   */
  void (*uart_modem_callback)(uart_args_t *arg);
} uart_callbacks_t;

/* Name:        uart_init
 * @brief       Initializes the uart to its default state
 * @param[in]   Base address of the uart
 * @param[in]   Bitrate to configure
 */
void uart_init(uint32_t uart_base, uint32_t bitrate);

/* Name:        uart_set_bitrate
 * @brief       Set the UART bitrate
 * @param[in]   Base address of the uart
 * @param[in]   Bitrate to configure
 */
void uart_set_bitrate(uint32_t uart_base, uint32_t bitrate);

/* Name:        uart_putc
 * @brief       Blocks until TX FIFO is empty and then writes character
 * @param[in]   Base address of the uart
 * @param[in]   Byte to send
 */
void uart_putc(uint32_t uart_base, uint8_t c);

/* Name:        uart_putc_nonblocking
 * @brief       Writes to the TX FIFO without checking (waiting for it to be empty)
 * @param[in]   Base address of the uart
 * @param[in]   Byte to send
 */
void uart_putc_nonblocking(uint32_t uart_base, uint8_t c);

/* Name:        uart_flush
 * @brief       Blocks until all data in transmit FIFO have been sent
 * @param[in]   Base address of the uart
 */
void uart_flush(uint32_t uart_base);

/* Name:        uart_getc
 * @brief       Gets a byte from the UART RX FIFO
 * @param[in]   Base address of the uart
 * @return      Byte retrieved
 */
uint8_t uart_getc(uint32_t uart_base);

/* Name:        uart_check_for_char
 * @brief       Checks if there is data in the UART RX FIFO
 * @param[in]   Base address of the uart
 * @return      0 = No data in RX FIFO, >0 = Amount of data in FIFO
 */
uint8_t uart_check_for_char(uint32_t uart_base);

/* Name:        uart_rxint_enable
 * @brief       Enables RX side interrupt
 * @param[in]   Base address of the uart
 */
void uart_rxint_enable(uint32_t uart_base);

/* Name:        uart_rxint_disable
 * @brief       Disables RX side interrupt
 * @param[in]   Base address of the uart
 */
void uart_rxint_disable(uint32_t uart_base);

/* Name:        uart_lsint_enable
 * @brief       Enables receiver line status interrupt
 * @param[in]   Base address of the uart
 */
void uart_lsint_enable(uint32_t uart_base);

/* Name:        uart_lsint_disable
 * @brief       Disables receiver line status interrupt
 * @param[in]   Base address of the uart
 */
void uart_lsint_disable(uint32_t uart_base);

/* Name:        uart_txint_enable
 * @brief       Enables transmitter holding register empty interrupt
 * @param[in]   Base address of the uart
 */
void uart_txint_enable(uint32_t uart_base);

/* Name:        uart_txint_disable
 * @brief       Enables transmitter holding register empty interrupt
 * @param[in]   Base address of the uart
 */
void uart_txint_disable(uint32_t uart_base);

/* Name:        uart_get_iir
 * @brief       Get the Interrupt Identification Register (IIR)
 * @param[in]   Base address of the uart
 * @return      Interrupt status register
 */
uint8_t uart_get_iir(uint32_t uart_base);

/* Name:        uart_get_lsr
 * @brief       Get the Line Status Register (LSR)
 * @param[in]   Base address of the uart
 */
uint8_t uart_get_lsr(uint32_t uart_base);

/* Name:        uart_get_msr
 * @brief       Get the MODEM Status Register (MSR)
 * @param[in]   Base address of the uart
 * @return      Content of modem status register
 */
uint8_t uart_get_msr(uint32_t uart_base);

/* Name:        uart_get_lcr
 * @brief       Get the Line Control Register (LCR)
 * @param[in]   Base address of the uart
 * @return      Content of line control register
 */
uint8_t uart_get_lcr(uint32_t uart_base);

/* Name:        uart_clear_rx_fifo
 * @brief       Clears the RX FIFO
 * @param[in]   Base address of the uart
 */
void uart_clear_rx_fifo(uint32_t uart_base);

/* Name:        uart_clear_tx_fifo
 * @brief       Clears the TX FIFO
 * @param[in]   Base address of the uart
 */
void uart_clear_tx_fifo(uint32_t uart_base);

/* Name:        uart_set_txr
 * @brief       Sets the tx register
 * @param[in]   Base address of the uart
 */
void uart_set_txr(uint32_t uart_base, uint8_t ctrl);

/* Name:        uart_set_rxr
 * @brief       Sets the rx register
 * @param[in]   Base address of the uart
 */
void uart_set_rxr(uint32_t uart_base, uint8_t ctrl);

/* Name:        uart_set_moder
 * @brief       Set the mode register
 * @param[in]   Base address of the uart
 */
void uart_set_moder(uint32_t uart_base, uint8_t ctrl);

/* Name:        uart_set_mcr
 * @brief       Set the Modem Control Register (MCR)
 * @param[in]   Base address of the uart
 * @param[in]   Register value to write
 */
void uart_set_mcr(uint32_t uart_base, uint8_t val);

/* Name:        uart_set_lcr
 * @brief       Set the Line Control Register (LCR)
 * @param[in]   Base address of the uart
 * @param[in]   Register value to write
 */
void uart_set_lcr(uint32_t uart_base, uint8_t val);

/* Name:        uart_set_trigger_level
 * @brief       Set the RX FIFO trigger level
 * @param[in]   Base address of the uart
 * @param[in]   Trigger level to write
 * @return      0 on success, -EINVAL on error
 */
int32_t uart_set_trigger_level(uint32_t uart_base, uint8_t val);

/* Name:        uart_get_trigger_level
 * @brief       Get the RX FIFO trigger level
 * @param[in]   Base address of the uart
 * @return      Current trigger level
 */
uint8_t uart_get_trigger_level(uint32_t uart_base);

/* Name:        uart_set_buffer_depth
 * @brief       Set the RX FIFO buffer depth
 * @param[in]   Base address of the uart
 * @param[in]   Buffer depth to write
 * @return      0 on success, -EINVAL on error
 */
int32_t uart_set_buffer_depth(uint32_t uart_base, uint8_t val);

/* Name:        uart_get_buffer_depth
 * @brief       Get the RX FIFO buffer depth
 * @param[in]   Base address of the uart
 * @return      Current buffer depth
 */
uint8_t uart_get_buffer_depth(uint32_t uart_base);

/* Name:        uart_isr
 * @brief       UART interrupt handler
 * @param[in]   void-casted uart_args_t struct
 */
AAC_INTERRUPT_HANDLER_SIGNATURE(uart_isr, data_arg);

#endif // __UART_H_
