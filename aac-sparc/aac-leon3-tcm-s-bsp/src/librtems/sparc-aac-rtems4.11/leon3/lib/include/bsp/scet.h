/*-----------------------------------------------------------------------------
 * Copyright (C) 2005-2016 ÅAC Microtec AB
 *
 * Filename:     scet.h
 * Module name:  scet
 *
 * Author(s):    PeBr, ErZa
 * Support:      support@aacmicrotec.com
 * Description:  ÅAC SCET bare-metal driver header file
 * Requirements: ÅAC SCET Driver Detailed Design Document
 *----------------------------------------------------------------------------*/

#ifndef _SCET_H_
#define _SCET_H_

#include <stdint.h>
#include "toolchain_support.h"

extern uint32_t _scet_base;
#define SCET_BASE _scet_base

extern uint32_t _scet_IRQ;
#define SCET_IRQ _scet_IRQ

/* Name:        scet_callbacks_t
 * @brief       Struct containing the callback pointers for all interrupt sources
 */
typedef struct scet_callbacks
{
  /*
   * Name:      scet_pps_arrived
   * @brief     Callback for PPS arrived event
   */
  void (*scet_pps_arrived)(void);

  /*
   * Name:      scet_pps_lost
   * @brief     Callback for PPS lost event
   */
  void (*scet_pps_lost)(void);

  /*
   * Name:      scet_pps_found
   * @brief     Callback for PPS found event
   */
  void (*scet_pps_found)(void);

  /*
   * Name:      scet_gp_trig0
   * @brief     Callback for GP trigger 0 event
   */
  void (*scet_gp_trig0)(void);

  /*
   * Name:      scet_gp_trig1
   * @brief     Callback for GP trigger 1 event
   */
  void (*scet_gp_trig1)(void);

  /*
   * Name:      scet_gp_trig2
   * @brief     Callback for GP trigger 2 event
   */
  void (*scet_gp_trig2)(void);

  /*
   * Name:      scet_gp_trig3
   * @brief     Callback for GP trigger 3 event
   */
  void (*scet_gp_trig3)(void);

  /*
   * Name:      scet_gp_trig4
   * @brief     Callback for GP trigger 4 event
   */
  void (*scet_gp_trig4)(void);

  /*
   * Name:      scet_gp_trig5
   * @brief     Callback for GP trigger 5 event
   */
  void (*scet_gp_trig5)(void);

  /*
   * Name:      scet_gp_trig6
   * @brief     Callback for GP trigger 6 event
   */
  void (*scet_gp_trig6)(void);

  /*
   * Name:      scet_gp_trig7
   * @brief     Callback for GP trigger 7 event
   */
  void (*scet_gp_trig7)(void);
} scet_callbacks_t;

/*
 * Name:       scet_read_seconds
 * @brief      Reads seconds from the SCET
 * @return     The number of seconds
 */
uint32_t scet_read_seconds(void);

/*
 * Name:       scet_read_subseconds
 * @brief      Reads sub-seconds from the SCET
 * @return     The number of subseconds
 */
uint16_t scet_read_subseconds(void);

/*
 * Name:       scet_write_seconds
 * @brief      Write seconds to the SCET
 * @param[in]  new_sec the number of seconds
 */
void scet_write_seconds(int32_t new_sec);

/*
 * Name:       scet_write_subseconds
 * @brief      Write subseconds to the SCET
 * @param[in]  new_subsec the number of subseconds
 */
void scet_write_subseconds(int16_t new_subsec);

/*
 * Name:       scet_read_reg
 * @brief      Reads value from register
 * @param[in]  offset the register offset
 * @return     Read value of the register
 */
uint32_t scet_read_reg(uint32_t offset);

/*
 * Name:       scet_write_reg
 * @brief      Writes a value to a register
 * @param[in]  offset the register offset
 *             value the value to write
 * @return
 */
void scet_write_reg(uint32_t offset, uint32_t value);

/*
 * Name:       scet_interrupt_handler
 * @brief      Interrupt handler for the scet
 * @param[in]  args Ignored
 */
AAC_INTERRUPT_HANDLER_SIGNATURE(scet_interrupt_handler, args);

/*
 * Name:       scet_set_interrupt_srcs
 * @brief      Sets the interrupt sources
 * @param[in]  new_srcs bit map of the interupt sources to enable
 */
void scet_set_interrupt_srcs(uint32_t new_srcs);

/*
 * Name:       scet_latch_and_get_time
 * @brief      Latches the current time and returns it
 * @param[in]  Pointer to 6 octet area where timestamp will be stored.
 *             4 least significant octets contain the seconds stored in
 *             little endian
 *             2 most significant octets contain the subseconds stored in
 *             little endian
 */
void scet_latch_and_get_time(uint8_t *timestamp);

#endif
