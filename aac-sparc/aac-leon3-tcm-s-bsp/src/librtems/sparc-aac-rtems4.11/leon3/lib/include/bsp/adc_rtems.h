/*------------------------------------------------------------------------------
 * Copyright (C) 2016 ÅAC Microtec AB
 *
 * Filename:     adc_rtems.h
 * Module name:  adc
 *
 * Author(s):    CoSj
 * Support:      support@aacmicrotec.com
 * Description:  ADC bare-metal driver implementation
 * Requirements: Adheres to the ÅAC ADC Driver Detailed Design Description Rev. A
 *----------------------------------------------------------------------------*/

#ifndef _RTEMS_ADC_H_
#define _RTEMS_ADC_H_

/* ADC sample rates */
typedef enum {
  ADC_IOCTL_SPS_31250,
  ADC_IOCTL_SPS_15625,
  ADC_IOCTL_SPS_10417,
  ADC_IOCTL_SPS_5208,
  ADC_IOCTL_SPS_2597,
  ADC_IOCTL_SPS_1007,
  ADC_IOCTL_SPS_503_8,
  ADC_IOCTL_SPS_381,
  ADC_IOCTL_SPS_200_3,
  ADC_IOCTL_SPS_100_5,
  ADC_IOCTL_SPS_59_52,
  ADC_IOCTL_SPS_49_68,
  ADC_IOCTL_SPS_20_01,
  ADC_IOCTL_SPS_16_63,
  ADC_IOCTL_SPS_10,
  ADC_IOCTL_SPS_5,
  ADC_IOCTL_SPS_2_5,
  ADC_IOCTL_SPS_1_25
} adc_ioctl_sample_rate_e;

#define ADC_DEVICE_NAME      "/dev/adc"

#define ADC_CHANNELS            (16)
#define ADC_SAMPLE_SIZE         (4)
#define ADC_SAMPLE_PRECISION    (24)
#define ADC_CHANNEL_MASK        (0x0000000F)
#define ADC_CHANNEL_OFFSET      (0)
#define ADC_STATUS_MASK         (0x000000F0)
#define ADC_STATUS_OFFSET       (4)
#define ADC_DATA_MASK           (0xFFFFFF00)
#define ADC_DATA_OFFSET         (8)

#define ADC_SET_SAMPLE_RATE_IOCTL     _IOW('G', 1, uint32_t)
#define ADC_GET_SAMPLE_RATE_IOCTL     _IOR('G', 2, uint32_t)
#define ADC_SET_CLOCK_DIVISOR_IOCTL   _IOW('G', 3, uint32_t)
#define ADC_GET_CLOCK_DIVISOR_IOCTL   _IOR('G', 4, uint32_t)
#define ADC_ENABLE_CHANNEL_IOCTL      _IOW('G', 5, uint32_t)
#define ADC_DISABLE_CHANNEL_IOCTL     _IOW('G', 6, uint32_t)

rtems_device_driver rtems_adc_initialize(rtems_device_major_number major,
                                         rtems_device_minor_number minor,
                                         void *args);

rtems_device_driver rtems_adc_open(rtems_device_major_number major,
                                   rtems_device_minor_number minor,
                                   void *args);

rtems_device_driver rtems_adc_close(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *args);

rtems_device_driver rtems_adc_read(rtems_device_major_number major,
                                   rtems_device_minor_number minor,
                                   void *args);

rtems_device_driver rtems_adc_write(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *args);

rtems_device_driver rtems_adc_control(rtems_device_major_number major,
                                      rtems_device_minor_number minor,
                                      void *args);

#define AAC_ADC_DRIVER_TABLE_ENTRY { \
      rtems_adc_initialize,          \
      rtems_adc_open,                \
      rtems_adc_close,               \
      rtems_adc_read,                \
      rtems_adc_write,               \
      rtems_adc_control }

#endif /* _RTEMS_ADC_H_ */
