/*----------------------------------------------------------------------
 * Copyright (C) 2015 - 2017 ÅAC Microtec AB
 *
 * Filename:     system_flash_rtems.h
 * Module name:  system_flash
 *
 * Author(s):    OdMa, JoBe, MaWe
 * Support:      support@aacmicrotec.com
 * Description:  NAND System flash controller driver declarations
 * Requirements: Adheres to the ÅAC System Flash Driver Requirement
 *               Specification Rev. A
 *--------------------------------------------------------------------*/

#ifndef _RTEMS_SYSTEM_FLASH_H_
#define _RTEMS_SYSTEM_FLASH_H_

#include <rtems.h>

#include "system_flash_common.h"

#define SYSFLASH_DEVICE_NAME                   "/dev/sysflash"
#define SYSFLASH_OP_SUCCESS                    0xE0

#define SYSFLASH_IOCTL_GROUP                   'G'
#define SYSFLASH_IO_RESET                      _IO (SYSFLASH_IOCTL_GROUP, 22)
#define SYSFLASH_IO_READ_CHIP_STATUS           _IOR(SYSFLASH_IOCTL_GROUP, 23, uint8_t*)
#define SYSFLASH_IO_READ_CTRL_STATUS           _IOR(SYSFLASH_IOCTL_GROUP, 24, uint8_t*)
#define SYSFLASH_IO_READ_ID                    _IOR(SYSFLASH_IOCTL_GROUP, 26, uint8_t*)
#define SYSFLASH_IO_ERASE_BLOCK                _IOW(SYSFLASH_IOCTL_GROUP, 27, uint32_t)
#define SYSFLASH_IO_READ_SPARE_AREA            _IOR(SYSFLASH_IOCTL_GROUP, 28, void*)
#define SYSFLASH_IO_WRITE_SPARE_AREA           _IOR(SYSFLASH_IOCTL_GROUP, 29, void*)
#define SYSFLASH_IO_BAD_BLOCK_CHECK            _IOR(SYSFLASH_IOCTL_GROUP, 30, uint32_t)

/* Supported IO-controls */
typedef struct
{
  uint32_t page_num;
  uint32_t raw;
  uint8_t *data_buf;
  uint32_t size;
} sysflash_ioctl_spare_area_args_t;


rtems_device_driver sysflash_initialize(rtems_device_major_number major,
                                        rtems_device_minor_number unused ,
                                        void *args);

rtems_device_driver sysflash_open(rtems_device_major_number major,
                                  rtems_device_minor_number unused,
                                  void *args);

rtems_device_driver sysflash_close(rtems_device_major_number major,
                                   rtems_device_minor_number unused,
                                   void *args);

rtems_device_driver sysflash_read(rtems_device_major_number major,
                                  rtems_device_minor_number unused,
                                  void *args);

rtems_device_driver sysflash_write(rtems_device_major_number major,
                                   rtems_device_minor_number unused,
                                   void *args);

rtems_device_driver sysflash_control(rtems_device_major_number major,
                                     rtems_device_minor_number unused,
                                     void *args);

#define AAC_SYSTEM_FLASH_DRIVER_TABLE_ENTRY { \
    sysflash_initialize,                      \
    sysflash_open,                            \
    sysflash_close,                           \
    sysflash_read,                            \
    sysflash_write,                           \
    sysflash_control }


#endif /* _RTEMS_SYSTEM_FLASH_H_ */
