/*-----------------------------------------------------------------------------
 * Copyright (C) 2018 ÅAC Microtec AB
 *
 * Filename:     spi_ram.h
 * Module name:  spi_ram
 *
 * Author(s):    CoSj, JoZe, DaHe
 * Support:      support@aacmicrotec.com
 * Description:  SPI RAM bare metal driver definitions
 * Requirements: Adheres to the ÅAC SPI RAM Driver Detailed Design Description Rev. A
 *----------------------------------------------------------------------------*/

#ifndef _SPI_RAM_H_
#define _SPI_RAM_H_
#include <stdbool.h>

/* SPI RAM memory offsets  */
#define SPI_RAM_EDAC_ON_MAX_RANGE       (0x4000)
#define SPI_RAM_EDAC_OFF_MAX_RANGE      (0x8000)

/* Status register bits */
#define SPI_RAM_WRITE_ENABLE_LATCH      (0x02)

/* EDAC Status register bits */
#define SPI_RAM_EDAC_MULT_ERROR         (0x04)
#define SPI_RAM_EDAC_DOUBLE_ERROR       (0x02)
#define SPI_RAM_EDAC_SINGLE_ERROR       (0x01)


/* SPI RAM EDAC */
typedef enum {
  SPI_RAM_EDAC_ENABLE  = 0,
  SPI_RAM_EDAC_DISABLE = 1
} spi_ram_edac_e;

/* SPI RAM Write Protection */
typedef enum {
  SPI_RAM_WRITE_PROTECTION_ENABLE  = 0,
  SPI_RAM_WRITE_PROTECTION_DISABLE = 1,
} spi_ram_write_protection_e;

typedef struct spi_ram_config {
  uint16_t divisor;
  uint8_t  edac;
} spi_ram_config_t;

/* Name:        spi_ram_init
 * @brief       Initializes the SPI RAM to its default state.
 * @param[in]   base_addr  Base address of the SPI RAM.
 * @return      0          Operation was successful.
 *              -EINVAL    Invalid parameter.
 */
int32_t spi_ram_init(uint32_t base_addr);

/* Name:        spi_ram_config
 * @brief       Configures the SPI RAM.
 * @param[in]   base_addr  Base address of the SPI RAM.
 * @param[in]   config     Pointer to a configuration for the SPI RAM.
 * @return      0          Operation was successful.
 *              -EINVAL    Invalid parameter.
 */
int32_t spi_ram_config(uint32_t base_addr, const spi_ram_config_t *config);

/* Name:        spi_ram_write_protection
 * @brief       Configure the SPI RAM write protection.
 * @param[in]   base_addr        Base address of the SPI RAM.
 * @param[in]   write_protection Write protection configuration.
 * @return      0                Operation was successful.
 *              -EINVAL          Invalid parameter.
 */
int32_t spi_ram_write_protection(uint32_t base_addr, uint8_t write_protection);

/* Name:        spi_ram_status_reg_read
 * @brief       Read the SPI RAM status register.
 * @param[in]   base_addr  Base address of the SPI RAM.
 * @param[out]  value      Status register value.
 * @return      0          Operation was successful.
 *              -EINVAL    Invalid parameter.
 */
int32_t spi_ram_status_reg_read(uint32_t base_addr, uint8_t *value);

/* Name:        spi_ram_status_reg_write
 * @brief       Write the SPI RAM status register.
 * @param[in]   base_addr  Base address of the SPI RAM.
 * @param[in]   value      The new status register value.
 */
void spi_ram_status_reg_write(uint32_t base_addr, uint8_t value);

/* Name:        spi_ram_data_read
 * @brief       Read data from the SPI RAM.
 * @param[in]   base_addr  Base address of the SPI RAM.
 * @param[in]   dest       Pointer to memory to start write data into.
 * @param[in]   src        SPI RAM address to start read data from.
 * @param[in]   size       Length of the SPI RAM memory to read.
 * @return      0          Operation was successful.
 *              -EINVAL    Invalid parameter.
 */
int32_t spi_ram_data_read(uint32_t base_addr,
                          uint8_t *dest,
                          uint16_t src,
                          uint16_t size);

/* Name:        spi_ram_data_read
 * @brief       Write data into the SPI RAM.
 * @param[in]   base_addr  Base address of the SPI RAM.
 * @param[in]   dest       SPI RAM address to start write data into.
 * @param[in]   src        Pointer to memory to start read data from.
 * @param[in]   size       Length of the SPI RAM memory to write.
 * @return      0          Operation was successful.
 *              -EINVAL    Invalid parameter.
 */
int32_t spi_ram_data_write(uint32_t base_addr,
                           uint16_t dest,
                           const uint8_t *src,
                           uint16_t size);

/* Name:        spi_ram_edac_status_read
 * @brief       Read EDAC status register.
 * @param[in]   base_addr   Base address of the SPI RAM.
 * @param[out]  value       EDAC status register value.
 * @return      0           Operation was successful.
 *              -EINVAL     Invalid parameter.
 */
int32_t spi_ram_edac_status_read(uint32_t base_addr, uint8_t *value);

/* Name:        spi_ram_debug_detect_read
 * @brief       Read debug detect status register.
 * @param[in]   base_addr   Base address of the SPI RAM.
 * @param[out]  value       Debug detect status. Detected=true.
 * @return      0           Operation was successful.
 *              -EINVAL     Invalid parameter.
 */
int32_t spi_ram_debug_detect_read(uint32_t base_addr, bool *value);

/* Name:        spi_ram_busy_read
 * @brief       Read busy bit in status register.
 * @param[in]   base_addr   Base address of the SPI RAM.
 * @param[out]  value       Busy status bit value.
 * @return      0           Operation was successful.
 *              -EINVAL     Invalid parameter.
 */
int32_t spi_ram_busy_read(uint32_t base_addr, bool *value);

#endif // _SPI_RAM_H_
