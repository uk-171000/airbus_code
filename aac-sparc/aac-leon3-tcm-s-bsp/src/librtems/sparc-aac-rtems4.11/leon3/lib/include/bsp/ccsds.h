/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2016 ÅAC Microtec AB
 *
 * Filename:     ccsds.h
 * Module name:  ccsds
 *
 * Author(s):    PeBr, AnSi
 * Support:      support@aacmicrotec.com
 * Description:  CCSDS bare metal driver interface
 * Requirements: ÅAC CCSDS Requirement specification
 *----------------------------------------------------------------------------*/
#ifndef _CCSDS_H_
#define _CCSDS_H_

#include <stdint.h>
#include "ccsds_defs.h"

/* Default values */
#define CCSDS_DEF_TM_CLK_DIVISOR            ( 0x00190000 )

/* Definitions for General IP Setup register*/
#define CCSDS_TM_RS_BYPASS                  ( 1 << 0 )
#define CCSDS_TM_PSEUDO_BYPASS              ( 1 << 1 )
#define CCSDS_TM_CONV_BYPASS                ( 1 << 2 )
#define CCSDS_TC_DERANDOMIZER_BYPASS        ( 1 << 4 )
#define CCSDS_IDLE_FRAME_ENABLE             ( 1 << 8 )
#define CCSDS_MC_CNT_ENABLE                 ( 1 << 9 )
#define CCSDS_FECF_EN                       ( 1 << 10 )
#define CCSDS_OCF_CLCW_EN                   ( 1 << 11 )
#define CCSDS_TM_EN                         ( 1 << 15 )
#define CCSDS_CLK_DIVISOR_MASK              ( 0xFFFF0000 )
#define CCSDS_TM_RS_BYPASS_BIT_POS          ( 0 )
#define CCSDS_TM_PSEUDO_BYPASS_BIT_POS      ( 1 )
#define CCSDS_TM_CONV_BYPASS_BIT_POS        ( 2 )
#define CCSDS_TC_DERAND_BYPASS_BIT_POS      ( 4 )
#define CCSDS_IDLE_FRAME_EN_BIT_POS         ( 8 )
#define CCSDS_MC_CNT_EN_BIT_POS             ( 9 )
#define CCSDS_FECF_EN_BIT_POS               ( 10 )
#define CCSDS_OCF_CLCW_EN_BIT_POS           ( 11 )
#define CCSDS_TM_EN_BIT_POS                 ( 15 )
#define CCSDS_CLK_DIV_BIT_POS               ( 16 )

/* Definitions for DMA Control register */
#define CCSDS_TM_DMA_EN                     ( 1 << 0 )
#define CCSDS_TM_DMA_RST                    ( 1 << 1 )

/* Definitons for TM Status register*/
#define CCSDS_TM_FIFO_ERR                   ( 1 << 1 )
#define CCSDS_TM_DMA_DESC_VC0_MASK          ( 0x00000300 )
#define CCSDS_TM_DMA_DESC_VC1_MASK          ( 0x00000C00 )
#define CCSDS_TM_DMA_DESC_VC2_MASK          ( 0x00003000 )
#define CCSDS_TM_DMA_DESC_VC3_MASK          ( 0x0000C000 )
#define CCSDS_TM_DMA_DESC_VC0_BIT_POS       ( 8 )
#define CCSDS_TM_DMA_DESC_VC1_BIT_POS       ( 10 )
#define CCSDS_TM_DMA_DESC_VC2_BIT_POS       ( 12 )
#define CCSDS_TM_DMA_DESC_VC3_BIT_POS       ( 14 )

/* Definitions for Interrupts Enable and Interrups Status register */
#define CCSDS_TC_RECEIVED                   ( 1 << 0 )
#define CCSDS_TC_BUFFER_OVERFLOW            ( 1 << 1 )
#define CCSDS_TC_BUFFER_PARERR              ( 1 << 2 )
#define CCSDS_TC_REJECTED                   ( 1 << 3 )
#define CCSDS_TM_ERR                        ( 1 << 9 )
#define CCSDS_TM_FRAME                      ( 1 << 10 )
#define CCSDS_VC0_TS                        ( 1 << 11 )
#define CCSDS_CPDU                          ( 1 << 16 )
#define CCSDS_DMA_FINISHED_VC0              ( 1 << 17 )
#define CCSDS_DMA_FINISHED_VC1              ( 1 << 18 )
#define CCSDS_DMA_FINISHED_VC0_BIT_POS      ( 17 )
#define CCSDS_DMA_FINISHED_VC1_BIT_POS      ( 18 )


/* Definitions for TC Frame Control register */
#define CCSDS_TC_BUFFER_RST                 ( 1 << 0 )
#define CCSDS_TC_BUFFER_RD                  ( 1 << 1 )

/* Definitions for TC Frame Status register */
#define CCSDS_TC_FRAME_COUNT_MASK           ( 0x00FF0000 )
#define CCSDS_TC_BUFFER_COUNT_MASK          ( 0x000007FF )
#define CCSDS_TC_FRAME_COUNT_BIT_POS        ( 16 )

/* Definitions for TC Error counters register */
#define CCSDS_TC_OVERFLOW_MASK              ( 0xFF000000 )
#define CCSDS_TC_CPDU_REJECTED_MASK         ( 0x00FF0000 )
#define CCSDS_TC_BUF_REJECTED_MASK          ( 0x0000FF00 )
#define CCSDS_TC_PARERR_MASK                ( 0x000000FF )
#define CCSDS_TC_OVERFLOW_BIT_POS           ( 24 )
#define CCSDS_TC_CPDU_REJ_BIT_POS           ( 16 )
#define CCSDS_TC_BUF_REJ_BIT_POS            ( 8 )
#define CCSDS_TC_PARERR_BIT_POS             ( 0 )

/* Definitions for CPDU Status register */
#define CCSDS_CPDU_LINE_STATUS_11           ( 1 << 15 )
#define CCSDS_CPDU_LINE_STATUS_10           ( 1 << 14 )
#define CCSDS_CPDU_LINE_STATUS_9            ( 1 << 13 )
#define CCSDS_CPDU_LINE_STATUS_8            ( 1 << 12 )
#define CCSDS_CPDU_LINE_STATUS_7            ( 1 << 11 )
#define CCSDS_CPDU_LINE_STATUS_6            ( 1 << 10 )
#define CCSDS_CPDU_LINE_STATUS_5            ( 1 << 9 )
#define CCSDS_CPDU_LINE_STATUS_4            ( 1 << 8 )
#define CCSDS_CPDU_LINE_STATUS_3            ( 1 << 7 )
#define CCSDS_CPDU_LINE_STATUS_2            ( 1 << 6 )
#define CCSDS_CPDU_LINE_STATUS_1            ( 1 << 5 )
#define CCSDS_CPDU_LINE_STATUS_0            ( 1 << 4 )
#define CCSDS_CPDU_LINE_STATUS_MASK         ( 0x0000FFF0 )
#define CCSDS_CPDU_BYPASS_CNT_MASK          ( 0x0000000F )
#define CCSDS_CPDU_LINE_STATUS_BIT_POS      ( 4 )

/* Definitions for TM DMA Descriptors register */
#define CCSDS_RAW_DATA                      ( 1 << 20 )
#define CCSDS_WRAP                          ( 1 << 19 )
#define CCSDS_IRQ_EN                        ( 1 << 18 )
#define CCSDS_TM_PRESENT                    ( 1 << 17 )
#define CCSDS_DESC_LENGTH_MASK              ( 0x0001FFFF )
#define CCSDS_DESC_IRQ_EN                   ( 1 )
#define CCSDS_DESC_IRQ_DIS                  ( 0 )

/* Definitions for Timestamp copntrol register */
#define CCSDS_NO_TIMESTAMPS                 ( 0 )

/* CCSDS callback struct */
typedef struct ccsds_callbacks
{
  /*
   * Name:    cpdu_callback_function
   * @brief   Callback function for cpdu interrupt
   * @param   cdpdu_status [in] Status of CPDU
  */
  void (*cpdu_callback_function)(const uint32_t cpdu_status);

  /*
   * Name:    vco_ts_callback_function
   * @brief   Callback function for VC0 timestamp
  */
  void (*vco_ts_callback_function)(void);

  /*
   * Name:    dma_fin_callback_function
   * @brief   Callback function for dma finished interrupt
   * @param   tm_status [in] The status of the TM path
  */
  void (*dma_fin_callback_function)(const uint32_t tm_status);

  /*
   * Name:    tm_err_callback_function
   * @brief   Callback function for TM error interrupt
   * @param   tm_err_status [in] The error status of the TM path
  */
  void (*tm_err_callback_function)(const uint32_t tm_err_status);

  /*
   * Name:    tc_rejected_callback_function
   * @brief   Callback function for tc rejected interrupt
   * @param   tc_err_status [in] The TC error counter
  */
  void (*tc_rejected_callback_function)(const uint32_t tc_err_status);

  /*
   * Name:    tc_buf_par_err_callback_function
   * @brief   Callback function for TC buffer parity error interrupt
   * @param   tc_err_status [in] The TC error counter
  */
  void (*tc_buf_par_err_callback_function)(const uint32_t tc_err_status);

  /*
   * Name:    tc_buf_overflow_callback_function
   * @brief   Callback function for TC Buffer Overflow interrupt
   * @param   tc_err_status [in] The TC error counter
  */
  void (*tc_buf_overflow_callback_function)(const uint32_t tc_err_status);

  /*
   * Name:    tc_rec_callback_function
   * @brief   Callback function for TC received interrupt
   * @param   tc_frame_status [in] The TC Frame status
  */
  void (*tc_rec_callback_function)(const uint32_t tc_frame_status);

  /*
   * Name:    tm_frame_sent_callback_function
   * @brief   Callback function for TM Frame send
  */
  void (*tm_frame_sent_callback_function)(void);
} ccsds_callbacks_t;

/*
 * Name:    ccsds_init
 * @brief   Initialize CCSDS with a default configuration:
 *          - FECF is enabled
            - Master Channel Frame counter is enabled
            - Generation of Idle frames is enabled
            - Derandomization of telecommands is disabled
            - Inclusion of CLCW in TM Frames is enabled
            - Pseudo randomization of telemetry is disabled
            - Reed Solomon encoding of telemetry is enabled
            - Convolutional encoding of telemetry is disabled
            - Generation of TM is enabled
            - DMA transfers are enabled
            - A default TM clock divisor of 25
            - All available interrupts from IP are enabled
            - All available descriptors for all VC are configured.
 * @return  void
*/
void ccsds_init(void);

/*
 * Name:    ccsds_interrupt_handler
 * @brief   ISR for IRQ's generated by IP
 * @param   args [in] Not used.
*/
AAC_INTERRUPT_HANDLER_SIGNATURE(ccsds_interrupt_handler, args);

/*
 * Name:    ccsds_set_ip_config
 * @brief   Sets a configuration of IP config-reg
 * @param   configuration [in] The configuration to write
 * @return  void
*/
void ccsds_set_ip_config(const uint32_t configuration);

/*
 * Name:    ccsds_get_ip_config
 * @brief   Gets a configuration of IP config-reg
 * @return  The configuration of register IP config
*/
uint32_t ccsds_get_ip_config(void);

/*
 * Name:    ccsds_set_dma_control
 * @brief   Sets a configuration of DMA control register
 * @param   control [in] The configuration to write
 * @return  void
*/
void ccsds_set_dma_control(const uint32_t control);

/*
 * Name:    ccsds_get_dma_control
 * @brief   Gets the configuration of DMA control register
 * @return  The configuration of the DMA control register
*/
uint32_t ccsds_get_dma_control(void);

/*
 * Name:    ccsds_set_timestamp_control
 * @brief   Sets a configuration of timestamp ctrl of VC0
 * @param   timestamp_ctrl [in] How often the timestamp occurs
                0 - No timestamp
                0x01 - Every VC0
                0x02 - Every 2:nd VC0
                ...
                0xFF - Every 255:th VC0
 * @return  void
*/
void ccsds_set_timestamp_control(const uint32_t timestamp_ctrl);

/*
 * Name:    ccsds_get_timestamp_control
 * @brief   Sets a configuration of timestamp ctrl of VC0
 * @return   How often the time stamp occurs
                0 - No timestamp
                0x01 - Every VC0
                0x02 - Every 2:nd VC0
                ...
                0xFF - Every 255:th VC0
*/
uint32_t ccsds_get_timestamp_control(void);

/*
 * Name:    ccsds_set_ie_config
 * @brief   Sets a configuration of Interrupt Enable register
 * @param   configuration [in] The configuration to write
 * @return  void
*/
void ccsds_set_ie_config(const uint32_t configuration);

/*
 * Name:    ccsds_get_ie_config
 * @brief   Gets the configuration of Interrupt Enable register
 * @return  The configuration of Interrupt Enable Register
*/
uint32_t ccsds_get_ie_config(void);

/*
 * Name:    ccsds_set_dma_desc
 * @brief   Sets configuration of a DMA-descriptor
 * @param   vc_no [in] The Virtual Channel to configure
 * @param   desc_no [in] The DMA descriptor to configure
 * @param   descriptor [in] The configuration to write for the descriptor
 * @return  void
*/
void ccsds_set_dma_desc(const uint32_t vc_no,
                        const uint32_t desc_no,
                        const uint32_t descriptor);

/*
 * Name:    ccsds_get_dma_desc
 * @brief   Gets the configuration of a DMA-descriptor
 * @param   vc_no [in] The Virtual Channel to use
 * @param   desc_no [in] The DMA descriptor to get
 * @return  The configuration of the descriptor
*/
uint32_t ccsds_get_dma_desc(const uint32_t vc_no,
                            const uint32_t desc_no);

/*
 * Name:    ccsds_set_dma_desc_adr
 * @brief   Sets adress in SDRAM for DMA transfer to use
 * @param   vc_no [in] The Virtual Channel to configure
 * @param   desc_no [in] The DMA descriptor adress no
 * @param   desc_address [in] The address in SDRAM to use
 * @return  void
*/
void ccsds_set_dma_desc_adr(const uint32_t vc_no,
                            const uint32_t desc_no,
                            const uint32_t desc_address);

/*
 * Name:    ccsds_get_DMA_desc_adr
 * @brief   Gets adress in SDRAM used for a DMA Adress descriptor
 * @param   vc_no [in] The Virtual Channel
 * @param   desc_no [in] The DMA descriptor adress no
 * @return  The configured address in SDRAM for the descriptor
*/
uint32_t ccsds_get_dma_desc_adr(const uint32_t vc_no,
                                const uint32_t desc_no);

/*
 * Name:    ccsds_get_interrupt_status
 * @brief   Gets the interrupt status
 * @return  The interrupt status
*/
uint32_t ccsds_get_interrupt_status(void);

/*
 * Name:    ccsds_ack_interrupt
 * @brief   Acknowledge interrupts
 * @param   interrupts [in] The interrupts to acknowledge
 * @return  void
*/
void ccsds_ack_interrupt(const uint32_t interrupts);

/*
 * Name:    ccsds_get_tm_status
 * @brief   Gets the status of the TM path
 * @return  The status of the TM path
*/
uint32_t ccsds_get_tm_status(void);

/*
 * Name:    ccsds_get_cpdu_status
 * @brief   Gets CPDU status
 * @return  The status of CDPDU
*/
uint32_t ccsds_get_cpdu_status(void);

/*
 * Name:    ccsds_get_ip_configs1
 * @brief   Gets the configuration of regsiter IP config 1
 * @return  The configuration of IP config 1
*/
uint32_t ccsds_get_ip_configs1(void);

/*
 * Name:    ccsds_get_ip_configs2
 * @brief   Gets the configuration of register IP config 2
 * @return  The configuration of register IP config 2
*/
uint32_t ccsds_get_ip_configs2(void);

/*
 * Name:    ccsds_get_tm_error_cnt
 * @brief   Gets the error counter of the TM path
 * @return  The error counter of the TM path
*/
uint32_t ccsds_get_tm_error_cnt(void);

/*
 * Name:    ccsds_get_tc_error_cnt
 * @brief   Gets the error counter of the TC path
 * @return  The error counter of the TC path
*/
uint32_t ccsds_get_tc_error_cnt(void);

/*
 * Name:    ccsds_set_tc_frame_control
 * @brief   Sets a configuration of the TC frame control
 * @param   tc_frame_control [in] The configuration to write
 * @return  void
*/
void ccsds_set_tc_frame_control(const uint32_t tc_frame_control);

/*
 * Name:    ccsds_read_tc_data_buffer
 * @brief   Reads the content of the TC FIFO
 * @param   buffer [out] The buffer contaning the read data
 * @param   length [out] Number of 32-bit words to read
*/
void ccsds_read_tc_data_buffer(uint32_t *buffer,
                              const uint16_t length);

/*
 * Name:    ccsds_set_clcw
 * @brief   Sets the CLCW
 * @param   clcw [in] The clcw value
 * @return  void
*/
void ccsds_set_clcw(const uint32_t clcw);

/*
 * Name:    ccsds_get_clcw
 * @brief   Gets the CLCW
 * @return  The CLCW
*/
uint32_t ccsds_get_clcw(void);

/*
 * Name:    ccsds_configure_dma_descriptors
 * @brief   Configures the DMA descriptors
 * @param   generate_irq [in]
 *            0 - No DMA_FIN interrupt is generated when DMA transfer done
 *            1 - A DMA_FIN interrupt is generated when DMA transfer done
 * @return  0       Call successful
 *          -EINVAL Wrong argument
*/
int32_t ccsds_configure_dma_descriptors(const uint32_t generate_irq);

/*
 * Name:    ccsds_reset_dma_descriptors
 * @brief   Resets all DMA-descriptors
 * @return  void
*/
void ccsds_reset_dma_descriptors(void);

/*
 * Name:    ccsds_enable_dma_transfer
 * @brief   Enables DMA transfers. If DMA descriptors are
 *          set up correct and TM is enabled. TM frames will be
 *          transferred
 * @return  void
*/
void ccsds_enable_dma_transfer(void);

/*
 * Name:    ccsds_disable_dma_transfer
 * @brief   Disables DMA transfers.
 * @return  void
*/
void ccsds_disable_dma_transfer(void);

/*
 * Name:    ccsds_enable_tm
 * @brief   Enables the TM path.
 * @return  void
*/
void ccsds_enable_tm(void);

/*
 * Name:    ccsds_disable_tm
 * @brief   Enables the TM path.
 * @return  void
*/
void ccsds_disable_tm(void);

/*
 * Name:    ccsds_write_tm
 * @brief   Writes data to be send as TM on a virtual channel.
 * @param   vc_no [in]  The virtual channel to write data to
 *          tm_data[in] The data to write. Data shall be formatted as one
 *          or several TM PUS packets
 *          count[in]   Number of bytes of data to write
 * @retval  0-3         Success, value indicates which dma descriptor was used.
 * @retval  -EACCESS    TM is not enabled
 * @retval  -EAGAIN     No descriptors available
 * @retval  -EINVAL     count argument out of range
*/
int32_t ccsds_write_tm(const uint32_t vc_no, uint8_t *tm_data, uint32_t count);

/*
 * Name:    ccsds_get_tc_frame_status
 * @brief   Reads the TC frame status
 * @return  the value of TC frame status
*/
uint32_t ccsds_get_tc_frame_status(void);

/*
 * Name:    ccsds_get_next_tc_length
 * @brief   Reads the length of the next TC
 * @return  Length in bytes of rec TC
*/
uint32_t ccsds_get_next_tc_length(void);

/*
 * Name:    ccsds_get_radio_status
 * @brief   Reads Radio status
 * @return  The Radio Status
*/
uint32_t ccsds_get_radio_status(void);

#endif
