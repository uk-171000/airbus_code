/*
 * Copyright (C) 2016-2018 ÅAC Microtec AB
 *
 * Filename:    error_manager_rtems.h
 * Module name: error_manager
 *
 * Author(s):    ErZa, LiJo
 * Description:
 * Requirements:
 */

#ifndef __RTEMS_ERRMAN_H__
#define __RTEMS_ERRMAN_H__

#include "error_manager_regs.h"

#define RTEMS_ERRMAN_DEVICE_NAME "/dev/error_manager"

#define ERRMAN_IRQ_POWER_LOSS 0
#define ERRMAN_IRQ_PULSE_COMMAND 1
#define ERRMAN_IRQ_EDAC_MULTIPLE_ERR_OTHER 2

rtems_device_driver rtems_error_manager_initialize(rtems_device_major_number major,
                                                   rtems_device_minor_number minor,
                                                   void *args);

#define AAC_ERROR_MANAGER_DRIVER_TABLE_ENTRY { \
      rtems_error_manager_initialize,          \
      NULL,                                    \
      NULL,                                    \
      NULL,                                    \
      NULL,                                    \
      NULL }

#define ERRMAN_GET_SR_IOCTL                   _IOR('E',  1, uint32_t)
#define ERRMAN_GET_CF_IOCTL                   _IOR('E',  2, uint32_t)
#define ERRMAN_GET_SELFW_IOCTL                _IOR('E',  3, uint32_t)
#define ERRMAN_GET_RUNFW_IOCTL                _IOR('E',  4, uint32_t)
#define ERRMAN_GET_SCRUBBER_IOCTL             _IOR('E',  5, uint32_t)
#define ERRMAN_GET_RESET_ENABLE_IOCTL         _IOR('E',  6, uint32_t)
#define ERRMAN_GET_WDT_ERRCNT_IOCTL           _IOR('E',  7, uint32_t)
#define ERRMAN_GET_EDAC_SINGLE_ERRCNT_IOCTL   _IOR('E',  8, uint32_t)
#define ERRMAN_GET_EDAC_MULTI_ERRCNT_IOCTL    _IOR('E',  9, uint32_t)
#define ERRMAN_GET_CPU_PARITY_ERRCNT_IOCTL    _IOR('E', 10, uint32_t)
#define ERRMAN_GET_SYS_SINGLE_ERRCNT_IOCTL    _IOR('E', 11, uint32_t)
#define ERRMAN_GET_SYS_MULTI_ERRCNT_IOCTL     _IOR('E', 12, uint32_t)
#define ERRMAN_GET_MMU_SINGLE_ERRCNT_IOCTL    _IOR('E', 13, uint32_t)
#define ERRMAN_GET_MMU_MULTI_ERRCNT_IOCTL     _IOR('E', 14, uint32_t)
#define ERRMAN_GET_POWER_LOSS_ENABLE_IOCTL    _IOR('E', 15, uint32_t)

#define ERRMAN_SET_SR_IOCTL                   _IOW('E', 16, uint32_t)
#define ERRMAN_SET_CF_IOCTL                   _IOW('E', 17, uint32_t)
#define ERRMAN_SET_SELFW_IOCTL                _IOW('E', 18, uint32_t)
#define ERRMAN_SET_RUNFW_IOCTL                _IOW('E', 19, uint32_t)
#define ERRMAN_SET_SCRUBBER_IOCTL             _IOW('E', 20, uint32_t)
#define ERRMAN_SET_RESET_ENABLE_IOCTL         _IOW('E', 21, uint32_t)
#define ERRMAN_SET_WDT_ERRCNT_IOCTL           _IOW('E', 22, uint32_t)
#define ERRMAN_SET_EDAC_SINGLE_ERRCNT_IOCTL   _IOW('E', 23, uint32_t)
#define ERRMAN_SET_EDAC_MULTI_ERRCNT_IOCTL    _IOW('E', 24, uint32_t)
#define ERRMAN_SET_CPU_PARITY_ERRCNT_IOCTL    _IOW('E', 25, uint32_t)
#define ERRMAN_SET_SYS_SINGLE_ERRCNT_IOCTL    _IOW('E', 26, uint32_t)
#define ERRMAN_SET_SYS_MULTI_ERRCNT_IOCTL     _IOW('E', 27, uint32_t)
#define ERRMAN_SET_MMU_SINGLE_ERRCNT_IOCTL    _IOW('E', 28, uint32_t)
#define ERRMAN_SET_MMU_MULTI_ERRCNT_IOCTL     _IOW('E', 29, uint32_t)

#define ERRMAN_SET_POWER_LOSS_ENABLE_IOCTL    _IOW('E', 30, uint32_t)

#define ERRMAN_RESET_SYSTEM_IOCTL             _IOW('E', 31, uint32_t)

#define ERRMAN_GET_NVRAM_SINGLE_ERRCNT_IOCTL  _IOR('E', 32, uint32_t)
#define ERRMAN_GET_NVRAM_DOUBLE_ERRCNT_IOCTL  _IOR('E', 33, uint32_t)
#define ERRMAN_GET_NVRAM_MULTI_ERRCNT_IOCTL   _IOR('E', 34, uint32_t)

#define ERRMAN_SET_NVRAM_SINGLE_ERRCNT_IOCTL  _IOW('E', 35, uint32_t)
#define ERRMAN_SET_NVRAM_DOUBLE_ERRCNT_IOCTL  _IOW('E', 36, uint32_t)
#define ERRMAN_SET_NVRAM_MULTI_ERRCNT_IOCTL   _IOW('E', 37, uint32_t)


#endif /* __RTEMS_ERRMAN_H__ */
