/******************************************************************************
 * Copyright (C) 2015 ÅAC Microtec AB
 *
 *
 * Filename: wdt_rtems.h
 *
 * Author(s): Erik Zachrisson <erik.zachrisson@aacmicrotec.com>
 * Description: AAC watchdog RTEMS driver declarations
 * Requirements: Bare metal watchdog driver
 *****************************************************************************/

#ifndef _RTEMS_WATCHDOG_H_
#define _RTEMS_WATCHDOG_H_

#include "wdt_regs.h"

#define RTEMS_WATCHDOG_DEVICE_NAME "/dev/watchdog"

rtems_device_driver rtems_watchdog_initialize(rtems_device_major_number major,
                                              rtems_device_minor_number unused,
                                              void *args);

rtems_device_driver rtems_watchdog_open(rtems_device_major_number major,
                                        rtems_device_minor_number unused,
                                        void *args);

rtems_device_driver rtems_watchdog_close(rtems_device_major_number major,
                                         rtems_device_minor_number unused,
                                         void *args);

rtems_device_driver rtems_watchdog_read(rtems_device_major_number major,
                                        rtems_device_minor_number unused,
                                        void *args);

rtems_device_driver rtems_watchdog_write(rtems_device_major_number major,
                                         rtems_device_minor_number unused,
                                         void *args);

rtems_device_driver rtems_watchdog_control(rtems_device_major_number major,
                                           rtems_device_minor_number unused,
                                           void *args);

#define AAC_WATCHDOG_DRIVER_TABLE_ENTRY {       \
      rtems_watchdog_initialize,                \
      rtems_watchdog_open,                      \
      rtems_watchdog_close,                     \
      rtems_watchdog_read,                      \
      rtems_watchdog_write,                     \
      rtems_watchdog_control }

#define WATCHDOG_ENABLE                        (1)
#define WATCHDOG_DISABLE                       (0)
#define WATCHDOG_KICK                          (1)

#define WATCHDOG_ENABLE_IOCTL                  _IOW('W', 1, uint32_t)
#define WATCHDOG_SET_TIMEOUT_IOCTL             _IOW('W', 2, uint32_t)

#endif /* _RTEMS_WATCHDOG_H_ */
