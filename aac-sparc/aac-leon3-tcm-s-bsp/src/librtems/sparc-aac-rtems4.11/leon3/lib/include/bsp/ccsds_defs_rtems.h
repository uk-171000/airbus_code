/******************************************************************************
 * Copyright (C) 2005-2015 ÅAC Microtec AB
 *
 *
 * Filename:     ccsds_defs_rtems.h
 *
 * Author(s):    Pebr
 * Support:      support@aacmicrotec.com
 * Description:  
 * Requirements: Adheres to the Requirement Specification for CCSDS
 *****************************************************************************/

#ifndef _CCSDS_DEFS_RTEMS_H_
#define _CCSDS_DEFS_RTEMS_H_
typedef struct
{
  rtems_id id;
  rtems_id sem_id;
} ccsds_task_t;

typedef struct
{
  /* Transmission task */
  ccsds_task_t  task_tx;

  /* Reception task */
  ccsds_task_t task_rx;
  
  /* Length of last rec TC */
  uint16_t tc_length;
}ccsds_ctrl_t;

static volatile ccsds_ctrl_t ccsds_ctrl;
#endif 
