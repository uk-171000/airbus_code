/*-------------------------------------------------------------------------
 * Copyright (C) 2015 - 2017 ÅAC Microtec AB
 *
 * Filename:     massmem_flash.c
 * Module name:  massmem_flash
 *
 * Author(s):    Dahe, MaWe, InSi, OdMa, JoBe
 * Support:      support@aacmicrotec.com
 * Description:  NAND mass memory flash controller driver declarations.
 * Requirements: Adheres to the ÅAC Mass Memory Flash Driver Requirement
 *               Specification Rev. A
 *-----------------------------------------------------------------------*/

#ifndef _MASSMEM_FLASH_H_
#define _MASSMEM_FLASH_H_

#include <stdint.h>
#include <toolchain_support.h>

#include "massmem_flash_common.h"

extern uint32_t _massmem_base;
extern uint32_t _massmem_IRQ;

#define MASSMEM_TIMEOUT                    (0x800000)

#ifndef MASSMEM_ENABLE_32GB
  /* Alias for backwards-compatibility */
  #define MASSMEM_SPARE_AREA_SIZE_BYTES    (MASSMEM_SPARE_AREA_BYTES)
#endif

#define MASSMEM_PAGES_PER_LUN              (MASSMEM_BLOCKS_PER_LUN * \
                                            MASSMEM_PAGES_PER_BLOCK)

/* Chip status register flags/bit masks
 * Since the status is read from all 4 data chips in parallel
 * 32-bit bit masks are also needed */
/* Write Protect, 0 => Protected */
#define MASSMEM_STATUS_WP                  (0x80)
#define MASSMEM_STATUS_WP_32               (0x80808080)
/* Ready/Busy, 0 => Busy */
#define MASSMEM_STATUS_RDY                 (0x40)
#define MASSMEM_STATUS_RDY_32              (0x40404040)
/* Ready/busy for array operations, 0 => Busy */
#define MASSMEM_STATUS_ARDY                (0x20)
#define MASSMEM_STATUS_ARDY_32             (0x20202020)
/* Previous op (N-1) Pass/Fail, 0 => Pass */
#define MASSMEM_STATUS_FAILC               (0x02)
#define MASSMEM_STATUS_FAILC_32            (0x02020202)
/* Latest op (N) Pass/Fail, 0 => Pass */
#define MASSMEM_STATUS_FAIL                (0x01)
#define MASSMEM_STATUS_FAIL_32             (0x01010101)

/* Controller registers */
#define MASSMEM_CMD                        (0x00*4)
#define MASSMEM_SR                         (0x01*4)
#define MASSMEM_IE                         (0x02*4)
#define MASSMEM_TS                         (0x03*4)
#define MASSMEM_ADDR                       (0x04*4)
#define MASSMEM_DIO                        (0x05*4)
#define MASSMEM_FEATURE                    (0x06*4)
#define MASSMEM_DMA_BASE_ADDR              (0x07*4)
#define MASSMEM_DMA_LENGTH                 (0x08*4)
#define MASSMEM_MEMORY_SELECT              (0x09*4)
#define MASSMEM_EDAC                       (0x0A*4)
#define MASSMEM_ERR_CNT                    (0x0B*4)

/* Register flags */
#define MASSMEM_CMD_PROG_PAGE              (1<<0)
#define MASSMEM_CMD_READ_STATUS            (1<<1)
#define MASSMEM_CMD_READ_PAGE              (1<<2)
#define MASSMEM_CMD_ERASE_BLOCK            (1<<3)
#define MASSMEM_CMD_READ_ID                (1<<4)
#define MASSMEM_CMD_RESET                  (1<<5)
#define MASSMEM_CMD_STOP_RP                (1<<6)
#define MASSMEM_CMD_SET_FEATURE            (1<<8)
#define MASSMEM_CMD_DMA_ON                 (1<<9)

#define MASSMEM_SR_PROG_PAGE_DONE          (1<<0)
#define MASSMEM_SR_READ_STATUS_DONE        (1<<1)
#define MASSMEM_SR_READ_P_SETUP_DONE       (1<<2)
#define MASSMEM_SR_ERASE_BLOCK_DONE        (1<<3)
#define MASSMEM_SR_READ_ID_DONE            (1<<4)
#define MASSMEM_SR_RESET_DONE              (1<<5)
#define MASSMEM_SR_BUSY                    (1<<7)
#define MASSMEM_SR_SET_FEATURE_DONE        (1<<8)
/* Status Register Interrupt Mask Definitions */
#define MASSMEM_SR_VALID_BITS_MASK         (MASSMEM_SR_PROG_PAGE_DONE    | \
                                            MASSMEM_SR_READ_STATUS_DONE  | \
                                            MASSMEM_SR_READ_P_SETUP_DONE | \
                                            MASSMEM_SR_ERASE_BLOCK_DONE  | \
                                            MASSMEM_SR_READ_ID_DONE      | \
                                            MASSMEM_SR_RESET_DONE        | \
                                            MASSMEM_SR_BUSY              | \
                                            MASSMEM_SR_SET_FEATURE_DONE)

#define MASSMEM_IE_PROG_DONE               (1<<0)
#define MASSMEM_IE_RSTAT_DONE              (1<<1)
#define MASSMEM_IE_RPAGE_DONE              (1<<2)
#define MASSMEM_IE_ERASE_DONE              (1<<3)
#define MASSMEM_IE_RID_DONE                (1<<4)
#define MASSMEM_IE_RESET_DONE              (1<<5)
#define MASSMEM_IE_SFEATURE_DONE           (1<<8)

/* Interrupt Enable Register Mask Definitions */
#define MASSMEM_INT_EN_REG_MASK            (MASSMEM_IE_PROG_DONE  | \
                                            MASSMEM_IE_RSTAT_DONE | \
                                            MASSMEM_IE_RPAGE_DONE | \
                                            MASSMEM_IE_ERASE_DONE | \
                                            MASSMEM_IE_RID_DONE   | \
                                            MASSMEM_IE_RESET_DONE | \
                                            MASSMEM_IE_SFEATURE_DONE)

/* Feature Definitions */
#define MASSMEM_FEATURE_TIMING_MODE        (0x0201)
#define MASSMEM_FEATURE_OUT_DRIVE_STRENGTH (0x0210)
#define MASSMEM_FEATURE_PULLDOWN_STRENGTH  (0x0081)

/* Extended address register (also known as target select register) */
/* Target select sets which Logical Unit are being addressed */
#define MASSMEM_TS_TARGET_SHIFT                (0)
#define MASSMEM_TS_TARGET                      (1 << MASSMEM_TS_TARGET_SHIFT)
#define MASSMEM_TS_TARGET_CE                   (0)
#define MASSMEM_TS_TARGET_CE2                  (1)
/* Extended address is is used for extra MSB bit 32 when addressing 8GB chips */
#define MASSMEM_TS_32GB_EXTENDED_ADDRESS_SHIFT (8)
#define MASSMEM_TS_32GB_EXTENDED_ADDRESS       (1 << MASSMEM_TS_32GB_EXTENDED_ADDRESS_SHIFT)

/* Bit mask for the EDAC data */
#define MASSMEM_EDAC_DATA_MASK             (0xFF)

/* Memory select adapts the RTL to the chip type hardware being used */
#define MASSMEM_MEMORY_SELECT_MT29F32G08AFABA (0)
#define MASSMEM_MEMORY_SELECT_MT29F64G08AFAAA (1)

#ifndef ENOERR
#define ENOERR                             (0)
#endif

#define MASSMEM_EDAC_INTERLEAVING 0
#define MASSMEM_RAW 1

typedef struct
{
  /*
   * Name:        massmem_interrupt_callback
   * @brief       Interrupt callback function.
   * @param[in]   arg  Pointer to function arguments.
   */
  void (*massmem_prog_page_done_irq_callback)(void *arg);
  void (*massmem_read_status_done_irq_callback)(void *arg);
  void (*massmem_read_p_setup_done_irq_callback)(void *arg);
  void (*massmem_erase_block_done_irq_callback)(void *arg);
  void (*massmem_read_id_done_irq_callback)(void *arg);
  void (*massmem_reset_done_irq_callback)(void *arg);
  void (*massmem_set_feature_done_irq_callback)(void *arg);
} massmem_callbacks_t;

/*
 * Name:        massmem_init
 * @brief:      Initialize massmem. Performs reset of targets and sets default features.
 *
 * @param[in]:  -
 * @return:     ENOERR if OK
 *              -EIO    if timeout.
 */
int32_t massmem_init(void);

/*
 * Name:        massmem_set_features_default
 * @brief:      Set sub features to default values.
 * @param[in]:  -
 * @return:     ENOERR if OK.
 *              -EIO   if timeout.
 */
int32_t massmem_set_features_default(void);

void massmem_set_status(uint32_t status);

uint32_t massmem_get_status(void);

/* Name:        massmem_get_interrupt_status
 * @brief       Gets status for interrupts parts of massmem status register.
 * @param[in]:  -
 * @return      Content of the status register (interrupt status part).
 */
uint32_t massmem_get_interrupt_status(void);


void massmem_set_target(uint32_t target);


uint32_t massmem_get_interrupt_enable(void);


/* Name:        massmem_interrupt_enable
 * @brief       Enables massmem interrupt.
 * @param[in]   int_source Interrupt source(s) to enable.
 * @return      ENOERR     Enabling massmem interrupts was successful.
 *              -EINVAL    Invalid parameter.
 */
int32_t massmem_interrupt_enable(const int32_t int_source);


/* Name:        massmem_interrupt_disable
 * @brief       Disables massmem interrupt.
 * @param[in]   int_source Interrupt source(s) to disable.
 * @return      ENOERR     Disabling massmem interrupts was successful.
 *              -EINVAL    Invalid parameter.
 */
int32_t massmem_interrupt_disable(const int32_t int_source);


/*
 * Name:        massmem_isr
 * @brief:      Interrupt service routine for mass memory interrupts.
 *              The function decodes active interrupts, activates a
 *              callback routine (if it is registered)
 *              and acknowledges the status register for
 *              active interrupts. It is a bit mask of the corresponding interrupt
 *              sources in the interrupt
 *              enable register where
 *                '0' indicates that the corresponding source does not have an
 *                    active interrupt
 *                '1' indicates that the corresponding source does have an
 *                    active interrupt
 * @param[in]:  Data argument defined due to RTEMS prototype (not used)
 * @return:     -
 */
AAC_INTERRUPT_HANDLER_SIGNATURE(massmem_isr, data_arg);


/*
 * Name:        massmem_reset_target
 * @brief:      Sends the reset command to the selected target chip
 *              Intended to be used when corresponding interrupt is enabled.
 * @param[in]:  target to be reset, either MASSMEM_TS_TARGET_CE or
 *              MASSMEM_TS_TARGET_CE2
 * @return:     ENOERR  if OK
 *              -EINVAL  if target is neither low or high target
 */
int32_t massmem_reset_target(int32_t target);


/*
 * Name:        massmem_stop_read_program
 * @brief:      Sends the stop read/program command to the flash controller
 * @param[in]:  -
 * @return:     -
 */
void massmem_stop_read_program(void);

/*
 * Name:        massmem_set_page_spare_area_addr
 * @brief:      Set the address to the page spare area
 * @param[in]:  page_num Page number
 * @param[in]:  offset Offset into spare area
 * @return:     ENOERR if OK
 *              -EINVAL if page number is out-of-range
 */
int32_t massmem_set_page_spare_area_addr(uint32_t page_num, uint32_t offset);

/*
 * Name:        massmem_set_page_addr
 * @brief:      Sets the target and starting address for the page. Pages are
 *              counted continuously over the whole device, low numbers select
 *              the low target, high numbers select the high target.
 * @param[in]:  Page number
 * @return:     ENOERR if OK
 *              -EINVAL if page number is out-of-range
 */
int32_t massmem_set_page_addr(uint32_t page_num);

/*
 * Name:        massmem_read_status
 * @brief:      Reads the flash chip status register, edac status and IP status register.
 *              Intended to be used when corresponding interrupt is enabled.
 * @param[in]:  Data chip status buffer pointer.
 *              MM_DAC chip status buffer pointer.
 *              Controller status buffer pointer.
 * @return:     ENOERR if OK.
 *              -EBUSY if controller is busy.
 */
int32_t massmem_read_status(uint32_t *data_status,
                            uint8_t *edac_status,
                            uint16_t *ctrl_status);

/*
 * Name:        massmem_read_id
 * @brief:      Reads the ID data from the flash chips
 *              Intended to be used when corresponding interrupt is disabled.
 * @param[in]:  Pointer to Chip ID data structure
 * @return:     ENOERR if OK.
 *              -EBUSY if controller busy.
 */
int32_t massmem_read_id(massmem_cid_t *cid);


/*
 * Name:        massmem_verify_command
 * @brief:      Check Pass/Fail flag in the status register of the flash chip
 * @param[in]:  -
 * @return:     ENOERR if last Page Program or Block Erase passed.
 *              -EIO if last Page Program or Block Erase failed.
 */
int32_t massmem_verify_command(void);


/*
 * Name:        massmem_erase_block
 * @brief:      Start erasing a block of the flash without bad block check
 *              Intended to be used when corresponding interrupt is enabled.
 *              CAUTION: Only use on blocks that have already been checked,
 *                       bad blocks should never be erased or programmed!
 * @param[in]:  Block number to erase
 * @return:     ENOERR if Ok.
 *              -EINVAL if block number is out-of-range.
 *              -EBUSY if controller busy.
 */
int32_t massmem_erase_block(uint32_t block_num);


/*
 * Name:        massmem_read_page_dma
 * @brief:      Reads a page from the flash using DMA.
 * @param[in]:  Page number to read
 *              Buffer to store the data
 *              Number of bytes to read, note that the IP reads hole
 *              words of size 4 bytes, therefore size must be modulo 4.
 * @return:     ENOERR if operation was successful
 *              -EBUSY  if controller busy
 *              -EINVAL if page number is out-of-range, if data_buf
 *                      pointer is null pointer, not 4 byte aligned,
 *                      or if size is larger than one page or not
 *                      modulo 4 bytes, since read is done in 32 bit words.
 */
int32_t massmem_read_page_dma(uint32_t page_num,
                              uint8_t *data_buf,
                              uint32_t size);

/*
 * Name:        massmem_bad_block_factory_check
 * Description: Checks if a block was marked as bad from factory (in spare area)
 * Arguments:   Block number to check (blocks are counted continuously
 *              over the whole device)
 * Return:      MASSMEM_FACTORY_BAD_BLOCK_CLEARED if block is OK
 *              MASSMEM_FACTORY_BAD_BLOCK_MARKED  if block was marked as bad
 *              -EIO     if timeout
 *              -EBUSY   if controller busy
 *              -EINVAL  if block number is out-of-range
 */
int32_t massmem_factory_bad_block_check(uint32_t block_num);

/*
 * Name:        massmem_read_spare_area_poll
 * @brief:      Reads spare area for a page from the flash
 *              Intended to be used when corresponding interrupt is disabled.
 * @param[in]:  page_num Page number to read
 *              offset   Offset into spare area to read at
 *              mode     With EDAC and interleaving if set to MASSMEM_EDAC_INTERLEAVING,
 *                       without if set to MASSMEM_RAW.
 *              data_buf Buffer to store the data
 *              edac_buf Buffer to store the EDAC data (NULL if not wanted)
 *              size     Number of bytes to read
 * @return:     ENOERR if OK.
 *              -EBUSY if controller busy
 *              -EIO if timeout
 *              -EINVAL if page number or number of bytes was out-of-range
 *                      or if size is larger than one page or not
 *                      modulo 4 bytes, since program is done in 32 bit words.
 */
int32_t massmem_read_spare_area_poll(uint32_t page_num,
                                     uint32_t offset,
                                     uint32_t mode,
                                     uint8_t *data_buf,
                                     uint8_t *edac_buf,
                                     uint32_t size);


/*
 * Name:        massmem_program_page_dma
 * @brief:      Program a page to the flash using DMA
 *              Intended to be used when corresponding interrupt is enabled.
 * @param[in]:  Page number to program
 *              Buffer containing the data
 *              Number of bytes to program
 * @return:     ENOERR if OK.
 *              -EBUSY if controller busy.
 *              -EINVAL if page number or number of bytes was out-of-range
 *                      or if size is larger than one page or not
 *                      modulo 4 bytes, since program is done in 32 bit words.
 */
int32_t massmem_program_page_dma(uint32_t page_num,
                                 const uint8_t *data_buf,
                                 uint32_t size);


/*
 * Name:        massmem_program_page_poll
 * @brief:      Program a page to the flash without DMA
 *              Intended to be used when corresponding interrupt is disabled.
 * @param[in]:  Page number to program
 *              Pointer to Buffer containing the data
 *              Pointer to struct containing error injection vector, should be
 *                     set to NULL if not using error injection.
 *              Number of bytes to program
 * @return:     ENOERR if OK.
 *              -EBUSY if controller busy.
 *              -EIO if timeout.
 *              -EINVAL if page number or number of bytes was out-of-range
 *                      or if size is larger than one page or not
 *                      modulo 4 bytes, since program is done in 32 bit words.
 */
int32_t massmem_program_page_poll(uint32_t page_num,
                                  const uint8_t *data_buf,
                                  massmem_error_injection_t *error_injection,
                                  uint32_t size);


/*
 * Name:        massmem_program_spare_area_dma
 * @brief:      Program the spare area of a page to the flash using DMA.
 *              Intended to be used when corresponding interrupt is enabled.
 *              Note that using offset < 32 will likely overwrite the factory
 *              bad block information, due to interleaving.
 *              Note that interleaving is enabled when writing, which means
 *              that to get expected results you need to program in
 *              32 word "chunks" at 32 word offsets. Which is why that is
 *              required by this function.
 * @param[in]:  Page number to program
 *              Offset into spare area
 *              Buffer containing the data
 *              Number of bytes to program
 * @return:     ENOERR if OK.
 *              -EBUSY if controller busy
 *              -EIO if timeout
 *              -EINVAL if page number or number of bytes was out-of-range.
 *                      If size+offset is larger than spare area.
 *                      size not modulo 32 words (of 4 bytes each).
 *                      offset not module 32 words (of 4 bytes each).
 */
int32_t massmem_program_spare_area_dma(uint32_t page_num,
                                       uint32_t offset,
                                       const uint8_t *data_buf,
                                       uint32_t size);

/**
 * @brief Get page size in bytes.
 *
 * @return Available page size in bytes.
 */
size_t massmem_get_page_bytes(void);

/**
 * @brief Get spare area size in bytes.
 *
 * @return Available page size in bytes.
 */
size_t massmem_get_spare_area_bytes(void);
#endif
