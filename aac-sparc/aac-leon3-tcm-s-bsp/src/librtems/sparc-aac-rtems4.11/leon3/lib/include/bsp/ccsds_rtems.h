/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2016 ÅAC Microtec AB
 *
 * Filename:     ccsds_rtems.h
 * Module name:  ccsds
 *
 * Author(s):    PeBr, AnSi
 * Support:      support@aacmicrotec.com
 * Description:  CCSDS RTEMS Driver Interface
 * Requirements: ÅAC CCSDS Requirement specification
 *----------------------------------------------------------------------------*/

#ifndef CCSDS_RTEMS_H
#define CCSDS_RTEMS_H

#include "ccsds_defs.h"

/* DMA transfer status */
#define NOT_SEND           ( 0 )
#define SEND_FINISHED      ( 1 )
#define SEND_ERROR         ( 2 )

/* Supported IO-controls */
#define CCSDS_SET_TM_CONFIG         _IOW('G', 1, tm_config_t*)
#define CCSDS_GET_TM_CONFIG         _IOR('G', 2, tm_config_t*)
#define CCSDS_SET_TC_CONFIG         _IOW('G', 3, tc_config_t*)
#define CCSDS_GET_TC_CONFIG         _IOR('G', 4, tc_config_t*)
#define CCSDS_GET_RADIO_STATUS      _IOR('G', 7, radio_status_t*)
#define CCSDS_GET_TM_STATUS         _IOR('G', 8, tm_status_t*)
#define CCSDS_GET_TM_ERR_CNT        _IOR('G', 9, tm_error_cnt_t*)
#define CCSDS_GET_TC_ERR_CNT        _IOR('G', 10, tc_error_cnt_t*)
#define CCSDS_GET_TC_STATUS         _IOR('G', 11, tc_status_t*)
#define CCSDS_SET_TC_FRAME_CTRL     _IOW('G', 12, uint32_t)
#define CCSDS_ENABLE_TM             _IOW('G', 13, void*)
#define CCSDS_DISABLE_TM            _IOW('G', 14, void*)
#define CCSDS_INIT                  _IOW('G', 15, void*)
#define CCSDS_SET_CLCW              _IOW('G', 16, uint32_t)
#define CCSDS_GET_CLCW              _IOR('G', 17, uint32_t*)
#define CCSDS_SET_TM_TIMESTAMP      _IOW('G', 18, uint32_t)
#define CCSDS_GET_TM_TIMESTAMP      _IOR('G', 19, uint32_t*)

#define CCSDS_NAME        "/dev/ccsds"
#define CCSDS_NAME_TM     "/dev/ccsds-tm"
#define CCSDS_NAME_TC     "/dev/ccsds-tc"
#define CCSDS_NAME_TM_VC0   "/dev/ccsds-tm0"
#define CCSDS_NAME_TM_VC1   "/dev/ccsds-tm1"
#define CCSDS_NAME_TM_VC2   "/dev/ccsds-tm2"
#define CCSDS_NAME_TM_VC3   "/dev/ccsds-tm3"
#define CCSDS_NAME_TM_VC4   "/dev/ccsds-tm4"
#define CCSDS_NAME_TM_VC5   "/dev/ccsds-tm5"
#define CCSDS_NAME_TM_VC6   "/dev/ccsds-tm6"
#define CCSDS_NAME_TC_VC0   "/dev/ccsds-tc0"


#define AAC_CCSDS_DRIVER_TABLE_ENTRY { \
    ccsds_initialize, ccsds_open, ccsds_close, ccsds_read, \
    ccsds_write, ccsds_control }

/* DMA descriptor control block */
typedef struct
{
  uint32_t adress;
  uint32_t length;
  uint8_t vc;
  uint8_t status;
} dma_transfer_cb_t;

/* DMA descriptors */
typedef struct
{
  uint32_t desc_no;
  uint32_t desc_config;
  uint32_t desc_adress;
} dma_descriptor_t;

/* TM configuration */
typedef struct
{
  uint16_t clk_divisor;
  uint8_t tm_enabled;
  uint8_t ocf_clcw_enabled;
  uint8_t fecf_enabled;
  uint8_t mc_cnt_enabled;
  uint8_t idle_frame_enabled;
  uint8_t tm_conv_bypassed;
  uint8_t tm_pseudo_rand_bypassed;
  uint8_t tm_rs_bypassed;
} tm_config_t;

/* TC configuration */
typedef struct
{
  uint8_t tc_derandomizer_bypassed;
} tc_config_t;

/* TM status */
typedef struct
{
  uint16_t dma_desc_addr;
  uint8_t tm_fifo_err;
  uint8_t tm_busy;
} tm_status_t;

/* TC status */
typedef struct
{
  uint16_t tc_buffer_cnt;
  uint16_t cpdu_line_status;
  uint8_t cpdu_bypass_cnt;
  uint8_t tc_frame_cnt;
} tc_status_t;

/* TC Error Counter */
typedef struct
{
  uint8_t tc_overflow_cnt;
  uint8_t tc_cpdu_rej_cnt;
  uint8_t tc_buf_rej_cnt;
  uint8_t tc_par_err_cnt;
} tc_error_cnt_t;

/* TM Error Counter */
typedef struct
{
  uint8_t tm_par_err_cnt;
} tm_error_cnt_t;


/* Radio Status */
typedef struct
{
  uint8_t tc_sub_carrier;
  uint8_t tc_carrier;
} radio_status_t;

/*
 * Name:    ccsds_initialize
 * @brief   Initialize function. Creates dev-files
 *
 * @param   rtems_device_major_number [in] Major number
 * @param   rtems_device_minor_number [in] Minor number
 * @param   void [in]
*/
rtems_device_driver ccsds_initialize(
  rtems_device_major_number, rtems_device_minor_number, void *);

/*
 * Name:    ccsds_open
 * @brief   Opens a device descriptor
 *
 * @param   rtems_device_major_number [in] Major number
 * @param   rtems_device_minor_number [in] Minor number
 * @param   void [in]
*/
rtems_device_driver ccsds_open(
  rtems_device_major_number, rtems_device_minor_number, void *);

/*
 * Name:    ccsds_close
 * @brief   Close a device
 *
 * @param   rtems_device_major_number [in] Major number
 * @param   rtems_device_minor_number [in] Minor number
 * @param   void [in]
*/
rtems_device_driver ccsds_close(
  rtems_device_major_number, rtems_device_minor_number, void *);

/*
 * Name:    ccsds_read
 * @brief   Reads from a device
 *
 * @param   rtems_device_major_number [in] Major number
 * @param   rtems_device_minor_number [in] Minor number
 * @param   void [in]
*/
rtems_device_driver ccsds_read(
  rtems_device_major_number, rtems_device_minor_number, void *);

/*
 * Name:    ccsds_write
 * @brief   Writes to a device
 *
 * @param   rtems_device_major_number [in] Major number
 * @param   rtems_device_minor_number [in] Minor number
 * @param   void [in]
*/
rtems_device_driver ccsds_write(
  rtems_device_major_number, rtems_device_minor_number, void *);

/*
 * Name:    ccsds_control
 * @brief   Provides ioctls of a device
 *
 * @param   rtems_device_major_number [in] Major number
 * @param   rtems_device_minor_number [in] Minor number
 * @param   void [in]
*/
rtems_device_driver ccsds_control(
  rtems_device_major_number, rtems_device_minor_number, void *);

#endif
