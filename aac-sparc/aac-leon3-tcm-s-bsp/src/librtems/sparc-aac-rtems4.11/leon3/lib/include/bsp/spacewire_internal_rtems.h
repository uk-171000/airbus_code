/*-----------------------------------------------------------------------------
 * Copyright (C) 2015 - 2018 ÅAC Microtec AB
 *
 * Filename:     spwn_internal_rtems.h
 * Module name:  SpaceWire
 *
 * Author(s):    InSi, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  Spacewire node RTEMS driver internal declarations
 * Requirements: N/A
 *---------------------------------------------------------------------------*/

#ifndef __SPACEWIRE_INTERNAL_RTEMS_H__
#define __SPACEWIRE_INTERNAL_RTEMS_H__

#define SPWN_MAX_LOG_ADDRESSES      (255)
#define SPWN_RESERVED_LOG_ADDRESSES (32) /* 0-31 are reserved for spacewire router */
#define SPWN_NR_OF_LOG_ADDRESSES    (SPWN_MAX_LOG_ADDRESSES - SPWN_RESERVED_LOG_ADDRESSES)
#define SPWN_ADDR_Q_SIZE            (10)

typedef enum
{
  SPWN_STATE_PKT_PROCESSED = 0, /* Packet processed normally. */
  SPWN_STATE_PKT_IN_PROCESS     /* Packet being processed */
} spwn_state_pkt_enum;

typedef enum
{
  SPWN_STATE_LINK_UNKNOWN = 0,
  SPWN_STATE_LINK_RESET,
  SPWN_STATE_LINK_UP,
  SPWN_STATE_LINK_DOWN,
  SPWN_STATE_LINK_ERROR
} spwn_state_link_enum;

typedef struct
{
  spwn_state_pkt_enum   state;
  /* The semaphores are only used for RX */
  rtems_id              incoming_semaphore;
  rtems_id              complete_semaphore;
  uint16_t              status;
  uint16_t              size;
  uint8_t               *buffer;
} spwn_info_t;

typedef struct
{
  uint8_t               index;
  int                   reader_fd;
  int                   writer_fd;
  volatile spwn_info_t  rx;
  volatile spwn_info_t  tx;
} spwn_dev_ctx_t;

typedef struct
{
  volatile uint8_t      front;
  volatile uint8_t      rear;
  volatile uint8_t      items;
  volatile spwn_info_t  *infos[SPWN_ADDR_Q_SIZE];
} spwn_queue_t;

bool spwn_q_put(spwn_queue_t* q, volatile spwn_info_t *info_item);
bool spwn_q_get(spwn_queue_t* q, volatile spwn_info_t **info_item);

#endif /* __SPACEWIRE_INTERNAL_RTEMS_H__ */
