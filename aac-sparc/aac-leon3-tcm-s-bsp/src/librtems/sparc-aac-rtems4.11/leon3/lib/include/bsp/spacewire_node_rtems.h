/*-----------------------------------------------------------------------------
 * Copyright (C) 2015 - 2018 ÅAC Microtec AB
 *
 * Filename:     spacewire_node_rtems.h
 * Module name:  SpaceWire
 *
 * Author(s):    InSi, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  Spacewire node RTEMS driver declarations
 * Requirements: Adheres to the ÅAC Spacewire Node Driver Requirement Specification Rev. A
 *----------------------------------------------------------------------------*/

#ifndef __RTEMS_SPACEWIRE_NODE_H__
#define __RTEMS_SPACEWIRE_NODE_H__

#include <bsp.h>

#define SPWN_MAX_PACKET_SIZE            (0xFFFC)
#define SPWN_DEVICE_0_NAME_PREFIX       "/dev/spwn0-"
#define SPWN_DEVICE_NAME_LENGTH         (14) /* prefix + 3 digits*/

/* Supported IO-controls */
#define SPWN_IOCTL_GROUP                'G'
#define SPWN_IOCTL_MODE_OFF             (0x00)
#define SPWN_IOCTL_MODE_NORMAL          (0x01)
#define SPWN_IOCTL_MODE_LOOPBACK        (0x02)

#define SPWN_IOCTL_MODE_SET             _IOW(SPWN_IOCTL_GROUP, 1, uint32_t)


rtems_device_driver spwn_initialize(rtems_device_major_number major,
                                    rtems_device_minor_number minor,
                                    void *args);

#define AAC_SPWN_DRIVER_TABLE_ENTRY { \
      spwn_initialize,                \
      NULL,                           \
      NULL,                           \
      NULL,                           \
      NULL,                           \
      NULL }

#endif /* __RTEMS_SPACEWIRE_NODE_H__ */
