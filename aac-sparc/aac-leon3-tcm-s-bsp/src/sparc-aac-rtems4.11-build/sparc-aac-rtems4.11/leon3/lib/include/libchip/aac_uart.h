/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2018 ÅAC Microtec AB
 *
 * Filename:     uart.h
 * Module name:  uart
 *
 * Author(s):    PeBr, ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  UART bare metal driver definitions
 * Requirements: ÅAC UART Requirement Specification, Rev. A
 *----------------------------------------------------------------------------*/

#ifndef _AAC_UART_H_
#define _AAC_UART_H_

#include <stdint.h>

#define REG8(addr)  *((volatile uint8_t *) (addr))
#define REG16(addr) *((volatile uint16_t *) (addr))
#define REG32(addr) *((volatile uint32_t *) (addr))

/*
 * AAC_UART register map
 */
#define AAC_UART_RBR        (0x0)   /* R:  Receive buffer (DLAB = 0) */
#define AAC_UART_THR        (0x0)   /* W:  Transmit buffer (DLAB = 0) */
#define AAC_UART_DLL        (0x0)   /* RW: Divisor Latch Low (DLAB = 1) */
#define AAC_UART_DLM        (0x1)   /* RW: Divisor Latch High (DLAB = 1) */
#define AAC_UART_IER        (0x1)   /* RW: Interrupt Enable Register */
#define AAC_UART_IIR        (0x2)   /* R:  Interrupt ID Register */
#define AAC_UART_FCR        (0x2)   /* W:  FIFO Control Register */
#define AAC_UART_LCR        (0x3)   /* RW: Line Control Register */
#define AAC_UART_MCR        (0x4)   /* RW: Modem Control Register */
#define AAC_UART_LSR        (0x5)   /* RW: Line Status Register */
#define AAC_UART_MSR        (0x6)   /* RW: Modem Status Register */
#define AAC_UART_MODER      (0x7)   /* RW: RS485/RS422 mode control Register */
#define AAC_UART_TXR        (0x8)   /* RW: Enable/disable Line Driver control */
#define AAC_UART_RXR        (0x9)   /* RW: Enable/disable Line Receiver control */
#define AAC_UART_BCR        (0xA)   /* R:  Read out the current receive buffer count */ 

/*
 * These are the definitions for the Divisor Latch Low register
 */
#define AAC_UART_DLL_MASK   (0xFF)

/*
 * These are the definitions for the Divisor Latch High register
 */
#define AAC_UART_DLM_MASK   (0xFF)
#define AAC_UART_DLM_SHAMT  (8)     /* Shift amount for the part of the divisor in DLM */

/*
 * These are the definitions for the Interrupt Enable Register
 */
#define AAC_UART_IER_RDI    (0x01)  /* Enable receiver data interrupt */
#define AAC_UART_IER_THRI   (0x02)  /* Enable Transmitter holding register int. */
#define AAC_UART_IER_RLSI   (0x04)  /* Enable receiver line status interrupt */
#define AAC_UART_IER_MSI    (0x08)  /* Enable Modem status interrupt */

/*
 * These are the definitions for the Interrupt Identification Register
 */
#define AAC_UART_IIR_NO_INT (0x01)  /* No interrupts pending */
#define AAC_UART_IIR_ID     (0x0f)  /* Mask for the interrupt ID */
#define AAC_UART_IIR_MSI    (0x00)  /* Modem status interrupt */
#define AAC_UART_IIR_THRI   (0x02)  /* Transmitter holding register empty */
#define AAC_UART_IIR_TOI    (0x0c)  /* Receive timeout interrupt */
#define AAC_UART_IIR_RDI    (0x04)  /* Receiver data interrupt */
#define AAC_UART_IIR_RLSI   (0x06)  /* Receiver line status interrupt */

/*
 * These are the definitions for the FIFO Control Register
 */
#define AAC_UART_FCR_ENABLE_FIFO    (0x01) /* Enable the FIFO */
#define AAC_UART_FCR_CLEAR_RCVR     (0x02) /* Clear the RCVR FIFO */
#define AAC_UART_FCR_CLEAR_XMIT     (0x04) /* Clear the XMIT FIFO */
#define AAC_UART_FCR_DMA_SELECT     (0x08) /* For DMA applications */
#define AAC_UART_FCR_BUFFER_MASK    (0x30) /* Mask for the chosen buffer depth */
#define AAC_UART_FCR_BUFFER_16      (0x00) /* Buffer depth of 16 */
#define AAC_UART_FCR_BUFFER_32      (0x10) /* Buffer depth of 32 */
#define AAC_UART_FCR_BUFFER_64      (0x20) /* Buffer depth of 64 */
#define AAC_UART_FCR_BUFFER_128     (0x30) /* Buffer depth of 128 */
#define AAC_UART_FCR_TRIGGER_MASK   (0xC0) /* Mask for the FIFO trigger range */
#define AAC_UART_FCR_TRIGGER_1      (0x00) /* Trigger set at 1 byte */
#define AAC_UART_FCR_TRIGGER_4      (0x40) /* Trigger set at 1/4 of buffer depth  */
#define AAC_UART_FCR_TRIGGER_8      (0x80) /* Trigger set at 1/2 of buffer depth */
#define AAC_UART_FCR_TRIGGER_14     (0xC0) /* Trigger set at 2 bytes from buffer top */

/*
 * These are the definitions for the Line Control Register
 */
#define AAC_UART_LCR_WLS    (0x03)  /* Mask for the Word length select bits */
#define AAC_UART_LCR_WLEN5  (0x00)  /* Word length of 5 bits */
#define AAC_UART_LCR_WLEN6  (0x01)  /* Word length of 6 bits */
#define AAC_UART_LCR_WLEN7  (0x02)  /* Word length of 7 bits */
#define AAC_UART_LCR_WLEN8  (0x03)  /* Word length of 8 bits */
#define AAC_UART_LCR_STB    (0x04)  /* Mask for stop bits */
#define AAC_UART_LCR_STOP_1 (0x00)  /* 1 stop bit */
#define AAC_UART_LCR_STOP_2 (0x00)  /* 1.5 stop bits for WLEN5, 2 stop bits otherwise */
#define AAC_UART_LCR_PEN    (0x08)  /* Parity Enable */
#define AAC_UART_LCR_PDIS   (0x00)  /* Parity Disable */
#define AAC_UART_LCR_EPS    (0x10)  /* Even parity select */
#define AAC_UART_LCR_SPAR   (0x20)  /* Stick parity */
#define AAC_UART_LCR_SBC    (0x40)  /* Set break control */
#define AAC_UART_LCR_DLAB   (0x80)  /* Divisor latch access bit */

/*
 * These are the definitions for the Modem Control Register
 */
#define AAC_UART_MCR_DTR    (0x01)  /* DTR complement */
#define AAC_UART_MCR_RTS    (0x02)  /* RTS complement */
#define AAC_UART_MCR_OUT1   (0x04)  /* Out1 complement */
#define AAC_UART_MCR_OUT2   (0x08)  /* Out2 complement */
#define AAC_UART_MCR_LOOP   (0x10)  /* Enable loopback test mode */

/*
 * These are the definitions for the Line Status Register
 */
#define AAC_UART_LSR_DR     (0x01)  /* Receiver data ready */
#define AAC_UART_LSR_OE     (0x02)  /* Overrun error indicator */
#define AAC_UART_LSR_PE     (0x04)  /* Parity error indicator */
#define AAC_UART_LSR_FE     (0x08)  /* Framing error indicator */
#define AAC_UART_LSR_BI     (0x10)  /* Break interrupt indicator */
#define AAC_UART_LSR_THRE   (0x20)  /* Transmit-holding-register empty */
#define AAC_UART_LSR_TEMT   (0x40)  /* Transmitter empty */
#define AAC_UART_LSR_RCVR   (0x80)  /* Error in receiver FIFO */

/*
 * These are the definitions for the Modem Status Register
 */
#define AAC_UART_MSR_DCTS   (0x01)  /* Delta CTS */
#define AAC_UART_MSR_DDSR   (0x02)  /* Delta DSR */
#define AAC_UART_MSR_TERI   (0x04)  /* Trailing edge ring indicator */
#define AAC_UART_MSR_DDCD   (0x08)  /* Delta DCD */
#define AAC_UART_MSR_CTS    (0x10)  /* Clear to Send */
#define AAC_UART_MSR_DSR    (0x20)  /* Data Set Ready */
#define AAC_UART_MSR_RI     (0x40)  /* Ring Indicator */
#define AAC_UART_MSR_DCD    (0x80)  /* Data Carrier Detect */

/*
 * These are the definitions for the mode control registers
 */
#define AAC_UART_MODER_RS422        (0x00)
#define AAC_UART_MODER_RS485        (0x01)
#define AAC_UART_MODER_LOOPBACK     (0x02)

/*
 * These are the definitions for the transmit line driver control registers
 */
#define AAC_UART_TXR_DISABLE        (0x00)
#define AAC_UART_TXR_ENABLE         (0x01)

/*
 * These are the definitions for the receiver line driver control registers
 */
#define AAC_UART_RXR_DISABLE        (0x00)
#define AAC_UART_RXR_ENABLE         (0x01)

/*
 * These are the definitions of buffer depth selections
 */
#define AAC_UART_BUFFER_16          (0)
#define AAC_UART_BUFFER_32          (1)
#define AAC_UART_BUFFER_64          (2)
#define AAC_UART_BUFFER_128         (3)

/*
 * These are the definitions of trigger level selections
 */
#define AAC_UART_TRIGGER_1          (0)
#define AAC_UART_TRIGGER_4          (1)
#define AAC_UART_TRIGGER_8          (2)
#define AAC_UART_TRIGGER_14         (3)

/*
 * Name:        aac_uart_args_t
 * @brief       Interrupt argument struct
 */
typedef struct aac_uart_args
{
  uint32_t      base;
  void          *info;
} aac_uart_args_t;

/*
 * Name:        aac_uart_callbacks_t
 * @brief       Interrupt callback struct with all callback functions
 */
typedef struct aac_uart_callbacks
{
  /*
   * Name:      aac_uart_rx_linestatus_callback
   * @brief     Callback for receiver line status (error) event
   * @param[in] aac_uart_args_t with info specific for this UART
   */
  void (*aac_uart_rx_linestatus_callback)(aac_uart_args_t *arg);

  /*
   * Name:      aac_uart_rx_data_callback
   * @brief     Callback for receiver data available event
   * @param[in] aac_uart_args_t with info specific for this UART
   */
  void (*aac_uart_rx_data_callback)(aac_uart_args_t *arg);

  /*
   * Name:      aac_uart_timeout_callback
   * @brief     Callback for character timeout indication event
   * @param[in] aac_uart_args_t with info specific for this UART
   */
  void (*aac_uart_timeout_callback)(aac_uart_args_t *arg);

  /*
   * Name:      aac_uart_tx_empty_callback
   * @brief     Callback for transmitter holding register empty event
   * @param[in] aac_uart_args_t with info specific for this UART
   */
  void (*aac_uart_tx_empty_callback)(aac_uart_args_t *arg);

  /*
   * Name:      aac_uart_modem_callback
   * @brief     Callback for MODEM status
   * @param[in] aac_uart_args_t with info specific for this UART
   */
  void (*aac_uart_modem_callback)(aac_uart_args_t *arg);
} aac_uart_callbacks_t;

/* Name:        aac_uart_init
 * @brief       Initializes the aac_uart to its default state
 * @param[in]   Base address of the UART
 * @param[in]   Bitrate to configure
 */
void aac_uart_init(uint32_t aac_uart_base, uint32_t bitrate);

/* Name:        aac_uart_set_bitrate
 * @brief       Set the UART bitrate
 * @param[in]   Base address of the UART
 * @param[in]   Bitrate to configure
 */
void aac_uart_set_bitrate(uint32_t aac_uart_base, uint32_t bitrate);

/* Name:        aac_uart_putc
 * @brief       Blocks until TX FIFO is empty and then writes character
 * @param[in]   Base address of the UART
 * @param[in]   Byte to send
 */
void aac_uart_putc(uint32_t aac_uart_base, uint8_t c);

/* Name:        aac_uart_putc_nonblocking
 * @brief       Writes to the TX FIFO without checking (waiting for it to be empty)
 * @param[in]   Base address of the UART
 * @param[in]   Byte to send
 */
void aac_uart_putc_nonblocking(uint32_t aac_uart_base, uint8_t c);

/* Name:        aac_uart_flush
 * @brief       Blocks until all data in transmit FIFO have been sent
 * @param[in]   Base address of the UART
 */
void aac_uart_flush(uint32_t aac_uart_base);

/* Name:        aac_uart_getc
 * @brief       Gets a byte from the AAC_UART RX FIFO
 * @param[in]   Base address of the UART
 * @return      Byte retrieved
 */
uint8_t aac_uart_getc(uint32_t aac_uart_base);

/* Name:        aac_uart_check_for_char
 * @brief       Checks if there is data in the AAC_UART RX FIFO
 * @param[in]   Base address of the UART
 * @return      0 = No data in RX FIFO, >0 = Amount of data in FIFO
 */
uint8_t aac_uart_check_for_char(uint32_t aac_uart_base);

/* Name:        aac_uart_rxint_enable
 * @brief       Enables RX side interrupt
 * @param[in]   Base address of the UART
 */
void aac_uart_rxint_enable(uint32_t aac_uart_base);

/* Name:        aac_uart_rxint_disable
 * @brief       Disables RX side interrupt
 * @param[in]   Base address of the UART
 */
void aac_uart_rxint_disable(uint32_t aac_uart_base);

/* Name:        aac_uart_lsint_enable
 * @brief       Enables receiver line status interrupt
 * @param[in]   Base address of the UART
 */
void aac_uart_lsint_enable(uint32_t aac_uart_base);

/* Name:        aac_uart_lsint_disable
 * @brief       Disables receiver line status interrupt
 * @param[in]   Base address of the UART
 */
void aac_uart_lsint_disable(uint32_t aac_uart_base);

/* Name:        aac_uart_txint_enable
 * @brief       Enables transmitter holding register empty interrupt
 * @param[in]   Base address of the UART
 */
void aac_uart_txint_enable(uint32_t aac_uart_base);

/* Name:        aac_uart_txint_disable
 * @brief       Enables transmitter holding register empty interrupt
 * @param[in]   Base address of the UART
 */
void aac_uart_txint_disable(uint32_t aac_uart_base);

/* Name:        aac_uart_get_iir
 * @brief       Get the Interrupt Identification Register (IIR)
 * @param[in]   Base address of the UART
 * @return      Interrupt status register. Bit 7 and 6 are always set
 */
uint8_t aac_uart_get_iir(uint32_t aac_uart_base);

/* Name:        aac_uart_get_lsr
 * @brief       Get the Line Status Register (LSR)
 * @param[in]   Base address of the UART
 * @return      Content of line status register
 */
uint8_t aac_uart_get_lsr(uint32_t aac_uart_base);

/* Name:        aac_uart_get_msr
 * @brief       Get the MODEM Status Register (MSR)
 * @param[in]   Base address of the UART
 * @return      Content of modem status register
 */
uint8_t aac_uart_get_msr(uint32_t aac_uart_base);

/* Name:        aac_uart_get_lcr
 * @brief       Get the Line Control Register (LCR)
 * @param[in]   Base address of the UART
 * @return      Content of line control register
 */
uint8_t aac_uart_get_lcr(uint32_t aac_uart_base);

/* Name:        aac_uart_clear_rx_fifo
 * @brief       Clears the RX FIFO
 * @param[in]   Base address of the UART
 */
void aac_uart_clear_rx_fifo(uint32_t aac_uart_base);

/* Name:        aac_uart_clear_tx_fifo
 * @brief       Clears the TX FIFO
 * @param[in]   Base address of the UART
 */
void aac_uart_clear_tx_fifo(uint32_t aac_uart_base);

/* Name:        aac_uart_set_txr
 * @brief       Sets the tx register
 * @param[in]   Base address of the UART
 */
void aac_uart_set_txr(uint32_t aac_uart_base, uint8_t ctrl);

/* Name:        aac_uart_set_rxr
 * @brief       Sets the rx register
 * @param[in]   Base address of the UART
 */
void aac_uart_set_rxr(uint32_t aac_uart_base, uint8_t ctrl);

/* Name:        aac_uart_set_moder
 * @brief       Set the mode register
 * @param[in]   Base address of the UART
 */
void aac_uart_set_moder(uint32_t aac_uart_base, uint8_t ctrl);

/* Name:        aac_uart_set_mcr
 * @brief       Set the Modem Control Register (MCR)
 * @param[in]   Base address of the UART
 * @param[in]   Register value to write
 */
void aac_uart_set_mcr(uint32_t aac_uart_base, uint8_t val);

/* Name:        aac_uart_set_lcr
 * @brief       Set the Line Control Register (LCR)
 * @param[in]   Base address of the UART
 * @param[in]   Register value to write
 */
void aac_uart_set_lcr(uint32_t aac_uart_base, uint8_t val);

/* Name:        aac_uart_set_trigger_level
 * @brief       Set the RX FIFO trigger level
 * @param[in]   Base address of the UART
 * @param[in]   Trigger level to write
 * @return      0 on success, -EINVAL on error
 */
int32_t aac_uart_set_trigger_level(uint32_t aac_uart_base, uint8_t val);

/* Name:        aac_uart_get_trigger_level
 * @brief       Get the RX FIFO trigger level
 * @param[in]   Base address of the UART
 * @return      Current trigger level
 */
uint8_t aac_uart_get_trigger_level(uint32_t aac_uart_base);

/* Name:        aac_uart_set_buffer_depth
 * @brief       Set the RX FIFO buffer depth
 * @param[in]   Base address of the UART
 * @param[in]   Buffer depth to write
 * @return      0 on success, -EINVAL on error
 */
int32_t aac_uart_set_buffer_depth(uint32_t aac_uart_base, uint8_t val);

/* Name:        aac_uart_get_buffer_depth
 * @brief       Get the RX FIFO buffer depth
 * @param[in]   Base address of the UART
 * @return      Current buffer depth
 */
uint8_t aac_uart_get_buffer_depth(uint32_t aac_uart_base);

#endif // __AAC_UART_H_
