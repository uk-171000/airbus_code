/*-----------------------------------------------------------------------------
 * Copyright (C) 2017 ÅAC Microtec AB
 *
 * Filename:     spacewire.c
 * Module name:  RTEMS example
 *
 * Author(s):    JaVi
 * Support:      support@aacmicrotec.com
 * Description:  This program shows how to send and receive SpaceWire
 *               packets.
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

/* Define the ÅAC drivers we need */
#define CONFIGURE_APPLICATION_NEEDS_SPACEWIRE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_WDT_DRIVER

/* Define the RTEMS drivers we need */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

/* Set up application specific resources */
#define CONFIGURE_MAXIMUM_DRIVERS 11
#define CONFIGURE_MAXIMUM_SEMAPHORES 5
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 10
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 0
#define CONFIGURE_MAXIMUM_TIMERS 0
#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 3

#ifdef LEON3
#define DEFAULT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#else
#define DEFAULT_TASK_ATTRIBUTES RTEMS_DEFAULT_ATTRIBUTES
#endif
#define CONFIGURE_INIT_TASK_ATTRIBUTES DEFAULT_TASK_ATTRIBUTES

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

#include <bsp/spacewire_node_rtems.h>

/* Local parameters */
#define BUFFER_SIZE     16384

uint8_t __attribute__ ((aligned (4))) rd_buf[BUFFER_SIZE];
uint8_t wr_buf[BUFFER_SIZE];

rtems_task spw_reader_task(rtems_task_argument ignore);

rtems_task Init(rtems_task_argument ignored)
{
  rtems_status_code     status;
  rtems_id              rd_task_id;
  int                   fd_wdt;
  int                   fd_spw;
  ssize_t               sent_size;
  ssize_t               wr_size;

  printf("SpaceWire example application started\n");

  /* Create SpaceWire reader task */
  status = rtems_task_create(rtems_build_name('R', 'S', 'P', 'W'),
                             100,
                             RTEMS_CONFIGURED_MINIMUM_STACK_SIZE,
                             RTEMS_DEFAULT_MODES,
                             DEFAULT_TASK_ATTRIBUTES,
                             &rd_task_id);

  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to create SpaceWire reader task (%d : %s).",
           status, rtems_status_text(status));
    return;
  }

  /* Open SpaceWire device for writing on logical address 63 */
  fd_spw = open(SPWN_DEVICE_0_NAME_PREFIX"63", O_WRONLY);
  if (fd_spw < 0) {
    printf("Failed to open SpaceWire '%s' for writing (%d).",
           SPWN_DEVICE_0_NAME_PREFIX"63", fd_spw);
  }

  /* Start SpaceWire reader task */
  status = rtems_task_start(rd_task_id,
                            spw_reader_task,
                            (rtems_task_argument) NULL);
  if(status != RTEMS_SUCCESSFUL) {
    printf("Failed to start SpaceWire reader task (%d : %s).",
           status, rtems_status_text(status));
    return;
  }

  /* Disable the watchdog */
  fd_wdt = open(RTEMS_WATCHDOG_DEVICE_NAME, O_WRONLY);
  if (fd_wdt < 0) {
    printf("Failed to open watchdog '%s' (%d).", RTEMS_WATCHDOG_DEVICE_NAME, fd_wdt);
  }
  status = ioctl(fd_wdt, WATCHDOG_ENABLE_IOCTL, WATCHDOG_DISABLE);
  if (status < 0) {
    printf("Could not disable watchdog (%d : %s).", errno, strerror(errno));
  }

  /* Set up a SpaceWire address so that all packets are routed back to us.
     The router is set up to use path addressing, so the SpaceWire addresses
     are simply the port of the router that we want the packet sent to. Let's
     assign port 3, i.e. ourselves (according to the manual, section 6). */

  /* Address */
  wr_buf[0] = 3;
  wr_buf[1] = 63;

  /* Data content */
  wr_buf[2] = 'D';
  wr_buf[3] = 'A';
  wr_buf[4] = 'T';
  wr_buf[5] = 'A';
  wr_buf[6] = '\0';

  wr_size = 7;

  /* Send SpaceWire packets continuously */
  while (true) {

    sent_size = write(fd_spw, &wr_buf, wr_size);

    if (sent_size < 0) {
      printf("Unable to write packet, got error %d\n", (int) sent_size);
    } else if (sent_size != wr_size) {
      printf("Unable to write entire packet, tried %d, managed %d\n",
             (int) wr_size, (int) sent_size);
    }

    sleep(1);
  }
}

rtems_task spw_reader_task(rtems_task_argument ignored)
{
  int                   fd_spw;
  ssize_t               rd_size;
  uint32_t              i;

  /* Open SpaceWire device for reading on logical address 63 */
  fd_spw = open(SPWN_DEVICE_0_NAME_PREFIX"63", O_RDONLY);

  while (true) {
    /* Keep reading */
    rd_size = read(fd_spw, &rd_buf, BUFFER_SIZE);
    if (rd_size < 0) {
      printf("Error in reading from the SpaceWire device, got error %d\n",
             (int) rd_size);
    } else {
      /* Print the received packet */
      printf("Got new SpaceWire packet (size %d): ", (int) rd_size);
      for (i = 0; i < rd_size; i++) {
        printf("%c", (char) rd_buf[i]);
      }
      printf("\n");
    }
  }

}
