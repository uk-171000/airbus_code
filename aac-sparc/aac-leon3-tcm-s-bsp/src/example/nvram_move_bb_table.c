/*-----------------------------------------------------------------------------
 * Copyright (C) 2016 - 2018 ÅAC Microtec AB
 *
 * Filename:     nvram_move_bb_table.c
 * Module name:  RTEMS example
 *
 * Author(s):    ErZa, JaVi, DaHe
 * Support:      support@aacmicrotec.com
 * Description:  Interactive program to view/move bad block table from old to new NVRAM
 *               structure.
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>

#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <bsp/toolchain_support.h>

#define CONFIGURE_APPLICATION_NEEDS_SPI_RAM_DRIVER

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_UNIFIED_WORK_AREAS
#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_MAXIMUM_DRIVERS
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS SHRT_MAX

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#ifdef LEON3
#define CONFIGURE_INIT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#endif

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

#include <bsp/spi_ram_rtems.h>

#define BB_TABLE_WORDS      (8192/32)
#define BB_TABLE_OFFSET_OLD (0x1000)
#define BB_TABLE_OFFSET_NEW (0x0E00)

static uint32_t bb_table[BB_TABLE_WORDS];

rtems_task Init(rtems_task_argument ignored)
{
  int      nvram_fd;
  off_t    seek_res;
  ssize_t  rw_res;
  uint32_t i;
  uint32_t read_errors = 0;
  int input;

  printf("Bad Block table test application started\n");

  nvram_fd = open(SPI_RAM_DEVICE_NAME, O_RDWR);
  if (nvram_fd < 0) {
    DBG(DBG_SEVERITY_ERR, "Failed to open NVRAM (%d:%s)\n", errno, strerror(errno));
    return;
  }


  printf("Read old Bad Block table (NVRAM offset 0x%04X)\n", BB_TABLE_OFFSET_OLD);
  seek_res = lseek(nvram_fd, BB_TABLE_OFFSET_OLD, SEEK_SET);
  if (seek_res != BB_TABLE_OFFSET_OLD) {
    DBG(DBG_SEVERITY_ERR, "Failed to seek to BB table!");
  }
  else {
    for (i = 0; i < BB_TABLE_WORDS; i++) {
      errno = 0;
      rw_res = read(nvram_fd, &bb_table[i], sizeof(*bb_table));
      if (rw_res != sizeof(*bb_table)) {
        DBG(DBG_SEVERITY_ERR, "Failed to read BB table word %d!", i);
      }
      if (errno != 0) {
        read_errors++;
        DBG(DBG_SEVERITY_ERR,
            "Errno for word %d: %d (%s)!",
            i,
            errno,
            strerror(errno));
      }
    }
  }

  printf("\nRead errors: %" PRIu32 "\n", read_errors);

  printf("\nBad block table:\n----------------");
  for (i = 0; i < BB_TABLE_WORDS; i++) {
    if (i%8 == 0) {
      printf("\n");
    }
    printf(" %08" PRIX32 "", bb_table[i]);
  }
  printf("\n\n");


  printf("\nWrite the Bad Block table to NVRAM offset 0x%04X (y/n)?\n",
         BB_TABLE_OFFSET_NEW);
  input = getchar();

  printf("\n\ninput = 0x%X\n\n", input);

  if((input == 'y') || (input == 'Y')) {
    printf("Write new Bad Block table (NVRAM offset 0x%04X)\n", BB_TABLE_OFFSET_NEW);
    seek_res = lseek(nvram_fd, BB_TABLE_OFFSET_NEW, SEEK_SET);
    if (seek_res != BB_TABLE_OFFSET_NEW) {
      DBG(DBG_SEVERITY_ERR, "Failed to seek to BB table!");
    }
    else {
      for (i = 0; i < BB_TABLE_WORDS; i++) {
        rw_res = write(nvram_fd, &bb_table[i], sizeof(*bb_table));
        if (rw_res != sizeof(*bb_table)) {
          DBG(DBG_SEVERITY_ERR, "Failed to write BB table word %d!", i);
        }
        else {
          if(i%80 == 0) {
            printf("\n");
          }
          printf(".");
        }
      }
      printf("\n\nBad Block table moved!\n\n");
    }
  }

  printf("\nBad Block table view/move application done!\n");

  rtems_task_delete(RTEMS_SELF);
}
