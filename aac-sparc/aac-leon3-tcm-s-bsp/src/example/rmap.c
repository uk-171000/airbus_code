/*-----------------------------------------------------------------------------
 * Copyright (C) 2005 - 2017 ÅAC Microtec AB
 *
 * Filename:     rmap.h
 * Module name:  RTEMS example
 *
 * Author(s):    PeBr, InSi, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  RMAP protocol helper functions
 *               Note: This code assumes a big endian CPU
 * Requirements: Adheres to ECSS-E-ST-50-52C (5 February 2010)
 *---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "rmap.h"
#include "rmap_crc.h"

int32_t rmap_add_header(const rmap_header_t * hdr,
                        const uint32_t data_offs,
                        uint8_t * data_buf)
{
  uint32_t      type;
  uint32_t      wr_cmd;
  uint32_t      hdr_size;
  uint32_t      reply_addr_len;
  uint32_t      offset;
  uint32_t      i;
  uint8_t       hdr_crc;
  uint32_t      tmp;

  type = hdr->cmd & RMAP_TYPE_MASK;
  wr_cmd = hdr->cmd & RMAP_CMD_WR_MASK;

  if (type == RMAP_TYPE_CMD) {
    /* The reply address length can only be in 0, 4, 8 or 12 bytes */
    if (hdr->reply_addr.len == 0) {
      reply_addr_len = 0;
    } else if (hdr->reply_addr.len <= 4) {
      reply_addr_len = 1;
    } else if (hdr->reply_addr.len <= 8) {
      reply_addr_len = 2;
    } else if (hdr->reply_addr.len <= 12) {
      reply_addr_len = 3;
    } else {
      printf("SpaceWire address is too long to fit in header\n");
      return -1;
    }
  } else {
    reply_addr_len = 0;
  }

  /* Check that the header fits */
  if (type == RMAP_TYPE_CMD) {
    /* Read or write command */
    hdr_size = RMAP_HEADER_CMD_SIZE_FIXED + hdr->tgt_addr.len + 4*reply_addr_len;
  } else if (wr_cmd == RMAP_CMD_WRITE) {
    /* Write reply */
    hdr_size = RMAP_HEADER_REPLY_SIZE_FIXED + hdr->tgt_addr.len;
  } else {
    /* Read reply */
    hdr_size = RMAP_HEADER_REPLY_SIZE_FIXED + hdr->tgt_addr.len + 4;
  }
  if (data_offs < hdr_size) {
    printf("Space given for adding RMAP header is too small\n");
    return -1;
  }

  /* Calculate start of header */
  offset = data_offs - hdr_size;

  /* Add SpaceWire address first */
  memcpy(&data_buf[offset], &hdr->tgt_addr.addr[0], hdr->tgt_addr.len);
  offset += hdr->tgt_addr.len;

  /* Initialize header CRC calculations */
  hdr_crc = 0;

  /* Add target logical address (reverse meaning for replies) */
  data_buf[offset++] = hdr->tgt_lgl_addr;
  hdr_crc = RMAP_CalculateCRC(hdr_crc, hdr->tgt_lgl_addr);

  /* Add protocol identifier */
  data_buf[offset++] = RMAP_PROTOCOL_ID;
  hdr_crc = RMAP_CalculateCRC(hdr_crc, RMAP_PROTOCOL_ID);

  /* Add instruction (the reply address length is zero for replies) */
  data_buf[offset++] = hdr->cmd | reply_addr_len;
  hdr_crc = RMAP_CalculateCRC(hdr_crc, hdr->cmd | reply_addr_len);

  /* Add key for commands */
  if (type == RMAP_TYPE_CMD) {
    data_buf[offset++] = hdr->key;
    hdr_crc = RMAP_CalculateCRC(hdr_crc, hdr->key);
  }

  /* Add status for replies */
  if (type == RMAP_TYPE_REPLY) {
    data_buf[offset++] = hdr->status;
    hdr_crc = RMAP_CalculateCRC(hdr_crc, hdr->status);
  }

  /* Add reply address for commands */
  if (type == RMAP_TYPE_CMD) {
    for (i = 0; i < 4*reply_addr_len - hdr->reply_addr.len; i++) {
      /* Pad all unused address field bytes with zeroes */
      data_buf[offset++] = 0;
      hdr_crc = RMAP_CalculateCRC(hdr_crc, 0);
    }
    for (i = 0; i < hdr->reply_addr.len; i++) {
      /* Add the reply address */
      data_buf[offset++] = hdr->reply_addr.addr[i];
      hdr_crc = RMAP_CalculateCRC(hdr_crc, hdr->reply_addr.addr[i]);
    }
  }

  /* Add initiator logical address (reverse meaning for replies) */
  data_buf[offset++] = hdr->init_addr;
  hdr_crc = RMAP_CalculateCRC(hdr_crc, hdr->init_addr);

  /* Add transaction identifier */
  data_buf[offset++] = (hdr->trans_id >> 8) & 0xFF;
  hdr_crc = RMAP_CalculateCRC(hdr_crc, (hdr->trans_id >> 8) & 0xFF);
  data_buf[offset++] = hdr->trans_id & 0xFF;
  hdr_crc = RMAP_CalculateCRC(hdr_crc, hdr->trans_id & 0xFF);

  /* Add extended address for commands */
  if (type == RMAP_TYPE_CMD) {
    data_buf[offset++] = hdr->ext_addr;
    hdr_crc = RMAP_CalculateCRC(hdr_crc, hdr->ext_addr);
  }

  /* Add RMAP address for commands */
  if (type == RMAP_TYPE_CMD) {
    tmp = hdr->addr;
    for (i = 0; i < 4; i++) {
      data_buf[offset++] = (tmp >> 24) & 0xFF;
      hdr_crc = RMAP_CalculateCRC(hdr_crc, (tmp >> 24) & 0xFF);
      tmp = tmp << 8;
    }
  }

  /* Add data length for commands and read replies */
  if ((type == RMAP_TYPE_CMD) || (wr_cmd == RMAP_CMD_READ)) {
    tmp = hdr->data_len << 8;
    for (i = 0; i < 3; i++) {
      data_buf[offset++] = (tmp >> 24) & 0xFF;
      hdr_crc = RMAP_CalculateCRC(hdr_crc, (tmp >> 24) & 0xFF);
      tmp = tmp << 8;
    }
  }

  /* Add header CRC */
  data_buf[offset] = hdr_crc;

  return hdr_size;
}

int32_t rmap_parse_header(rmap_header_t * hdr,
                          const uint8_t * data_buf)
{
  uint32_t      type;
  uint32_t      wr_cmd;
  uint32_t      hdr_size;
  uint32_t      reply_addr_len;
  uint32_t      offset;
  uint32_t      i;
  uint32_t      j;
  uint8_t       hdr_crc;

  /* Start parsing at the beginning of the buffer */
  offset = 0;

  /* Initialize header CRC calculations */
  hdr_crc = 0;

  /* Parse target logical address (reverse meaning for replies) */
  hdr->tgt_lgl_addr = data_buf[offset];
  hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);

  /* Parse protocol identifier */
  if (data_buf[offset] != RMAP_PROTOCOL_ID) {
    printf("Invalid RMAP protocol ID\n");
    return -1;
  }
  hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);

  /* Parse instruction */
  hdr->cmd = data_buf[offset] & ~RMAP_REPLY_LEN_MASK;
  reply_addr_len = data_buf[offset] & RMAP_REPLY_LEN_MASK;
  hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);

  type = hdr->cmd & RMAP_TYPE_MASK;
  wr_cmd = hdr->cmd & RMAP_CMD_WR_MASK;

  /* Parse header size */
  if (type == RMAP_TYPE_CMD) {
    /* Read or write command */
    hdr_size = RMAP_HEADER_CMD_SIZE_FIXED + hdr->tgt_addr.len + 4*reply_addr_len;
  } else if (wr_cmd == RMAP_CMD_WRITE) {
    /* Write reply */
    hdr_size = RMAP_HEADER_REPLY_SIZE_FIXED + hdr->tgt_addr.len;
  } else {
    /* Read reply */
    hdr_size = RMAP_HEADER_REPLY_SIZE_FIXED + hdr->tgt_addr.len + 4;
  }

  /* Parse key for commands */
  if (type == RMAP_TYPE_CMD) {
    hdr->key = data_buf[offset];
    hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);
  }

  /* Parse status for replies */
  if (type == RMAP_TYPE_REPLY) {
    hdr->status = data_buf[offset];
    hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);
  }

  /* Parse reply address for commands */
  if (type == RMAP_TYPE_CMD) {
    i = 0;
    while ((data_buf[offset] == 0) && (i < 4*reply_addr_len)) {
      /* Ignore all zero fields except in CRC calculation */
      hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);
    }
    for (j = 0; j < 4*reply_addr_len - i; j++) {
      hdr->reply_addr.addr[j] = data_buf[offset];
      hdr->reply_addr.len = hdr->reply_addr.len + 1;
      hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);
    }
    /* Make sure we allow for at least one zero in case of non-zero reply length,
       see RMAP specification section 5.1.6 for details */
    if ((reply_addr_len != 0) && (hdr->reply_addr.len == 0)) {
      hdr->reply_addr.len = 1;
      hdr->reply_addr.addr[0] = 0;
    }
  }

  /* Parse initiator logical address (reverse meaning for replies) */
  hdr->init_addr = data_buf[offset];
  hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);

  /* Parse transaction identifier */
  hdr->trans_id = data_buf[offset] & 0xFF;
  hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);
  hdr->trans_id = (hdr->trans_id << 8) | (data_buf[offset] & 0xFF);
  hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);

  /* Skip reserved field for read replies except for CRC calculation */
  if ((type == RMAP_TYPE_REPLY) && (wr_cmd == RMAP_CMD_READ)) {
    hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);
  }

  /* Parse extended address for commands */
  if (type == RMAP_TYPE_CMD) {
    hdr->ext_addr = data_buf[offset];
    hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);
  }

  /* Parse RMAP address for commands */
  if (type == RMAP_TYPE_CMD) {
    hdr->addr = 0;
    for (i = 0; i < 4; i++) {
      hdr->addr = (hdr->addr << 8) | (data_buf[offset] & 0xFF);
      hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);
    }
  }

  /* Parse data length for commands and read replies */
  if ((type == RMAP_TYPE_CMD) || (wr_cmd == RMAP_CMD_READ)) {
    hdr->data_len = 0;
    for (i = 0; i < 3; i++) {
      hdr->data_len = (hdr->data_len << 8) | (data_buf[offset] & 0xFF);
      hdr_crc = RMAP_CalculateCRC(hdr_crc, data_buf[offset++]);
    }
  }

  /* Check header CRC */
  if (data_buf[offset] != hdr_crc) {
    printf("Header CRC didn't match calculated value %d %d\n",
           (int) data_buf[offset], (int) hdr_crc);
    return -1;
  }

  return hdr_size;
}
