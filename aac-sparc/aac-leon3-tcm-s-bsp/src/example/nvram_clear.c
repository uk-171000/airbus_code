/*-----------------------------------------------------------------------------
 * Copyright (C) 2016 - 2018 ÅAC Microtec AB
 *
 * Filename:     nvram.c
 * Module name:  RTEMS example
 *
 * Author(s):    ErZa, JaVi, PeBr
 * Support:      support@aacmicrotec.com
 * Description:  A NVRAM program that clears the content of the whole NVRAM
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

/* These are strictly here only for demonstrational purposes */
#define CONFIGURE_APPLICATION_NEEDS_SPI_RAM_DRIVER

/* These are probably minimal defines */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_MAXIMUM_DRIVERS 11
#define CONFIGURE_MAXIMUM_SEMAPHORES 50
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 10
#define CONFIGURE_MAXIMUM_TIMERS 0

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 20

#ifdef LEON3
#define CONFIGURE_INIT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#endif

#define CONFIGURE_INIT

#define NVRAM_MAX_RANGE ( 16384 )

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

#include <bsp/spi_ram_rtems.h>

static int nvram_fd;
static uint8_t temp_buffer[NVRAM_MAX_RANGE];

rtems_task Init(rtems_task_argument ignored)
{
  off_t loc_offset;
  ssize_t bytes_written;

  printf("NVRAM clean application started. A debugger must be connected.\n");

  memset(&temp_buffer[0], 0, NVRAM_MAX_RANGE);

  nvram_fd = open(SPI_RAM_DEVICE_NAME, O_RDWR);
  if (nvram_fd < 0) {
    printf("Failed to open NVRAM (%d:%s)\n", errno, strerror(errno));
    return;
  }

  /* Set pointer to start of NVRAM */
  loc_offset = lseek(nvram_fd, 0, SEEK_SET);
  if (loc_offset != 0) {
    printf("Failed to set offset in NVRAM (%d:%s)\n", errno, strerror(errno));
  }

  bytes_written = write(nvram_fd, &temp_buffer[0], NVRAM_MAX_RANGE);
  if (bytes_written != NVRAM_MAX_RANGE) {
    printf("Failed to write data to NVRAM (%d:%s)\n", errno, strerror(errno));
  }

  printf("Clearing finished.\n");
  rtems_task_delete(RTEMS_SELF);
}
