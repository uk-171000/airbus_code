/*-----------------------------------------------------------------------------
 * Copyright (C) 2016 - 2017 ÅAC Microtec AB
 *
 * Filename:     rmap.h
 * Module name:  RTEMS example
 *
 * Author(s):    PeBr, InSi, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  RMAP protocol declarations
 *               Note: This code assumes a big endian CPU
 * Requirements: Adheres to ECSS-E-ST-50-52C (5 February 2010)
 *---------------------------------------------------------------------------*/

#ifndef RMAP_H
#define RMAP_H

#include <stdint.h>

#define RMAP_PROTOCOL_ID                (0x01)

#define RMAP_HEADER_CMD_SIZE_FIXED      (16)
#define RMAP_HEADER_REPLY_SIZE_FIXED    (8)

/* RMAP commands and replies */
#define RMAP_TYPE_OFFSET                (6)
#define RMAP_TYPE_CMD                   (1 << RMAP_TYPE_OFFSET)
#define RMAP_TYPE_REPLY                 (0 << RMAP_TYPE_OFFSET)
#define RMAP_TYPE_MASK                  (1 << RMAP_TYPE_OFFSET)
#define RMAP_CMD_WR_OFFSET              (5)
#define RMAP_CMD_WR_MASK                (1 << RMAP_CMD_WR_OFFSET)
#define RMAP_CMD_WRITE                  (1 << RMAP_CMD_WR_OFFSET)
#define RMAP_CMD_READ                   (0 << RMAP_CMD_WR_OFFSET)
#define RMAP_CMD_VERIFY                 (1 << 4)
#define RMAP_CMD_REPLY                  (1 << 3)
#define RMAP_CMD_INCR                   (1 << 2)
#define RMAP_REPLY_LEN_OFFSET           (0)
#define RMAP_REPLY_LEN_MASK             (3 << RMAP_REPLY_LEN_OFFSET)

typedef struct {
  uint8_t len;
  uint8_t addr[255];
} spw_address_t;

typedef struct {
  spw_address_t tgt_addr;
  uint8_t       tgt_lgl_addr;
  uint8_t       cmd;
  uint8_t       status;
  uint8_t       key;
  spw_address_t reply_addr;
  uint8_t       init_addr;
  uint16_t      trans_id;
  uint8_t       ext_addr;
  uint32_t      addr;
  uint32_t      data_len;
} rmap_header_t;

int32_t rmap_add_header(const rmap_header_t * hdr,
                        const uint32_t data_offs,
                        uint8_t * data_buf);
int32_t rmap_parse_header(rmap_header_t * hdr,
                          const uint8_t * data_buf);

#endif /* RMAP_H */
