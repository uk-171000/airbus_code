/*-----------------------------------------------------------------------------
 * Copyright (C) 2017 - 2018 ÅAC Microtec AB
 *
 * Filename:     spacewire-tmtc.c
 * Module name:  RTEMS example
 *
 * Author(s):    JaVi
 * Support:      support@aacmicrotec.com
 * Description:  This program shows how to send TM packets to the TCM and
 *               receive TC packets.
 *               packets.
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

/* Define the ÅAC drivers we need */
#define CONFIGURE_APPLICATION_NEEDS_SPACEWIRE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_WDT_DRIVER

/* Define the RTEMS drivers we need */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

/* Set up application specific resources */
#define CONFIGURE_MAXIMUM_DRIVERS 11
#define CONFIGURE_MAXIMUM_SEMAPHORES 4
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 10
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 0
#define CONFIGURE_MAXIMUM_TIMERS 0
#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 3

#ifdef LEON3
#define DEFAULT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#else
#define DEFAULT_TASK_ATTRIBUTES RTEMS_DEFAULT_ATTRIBUTES
#endif
#define CONFIGURE_INIT_TASK_ATTRIBUTES DEFAULT_TASK_ATTRIBUTES

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

#include <bsp/spacewire_node_rtems.h>
#include "rmap.h"
#include "rmap_crc.h"
#include "pus.h"

/* Local parameters */
#define BUFFER_SIZE     16384

uint8_t __attribute__ ((aligned (4))) rd_buf[BUFFER_SIZE];
uint8_t wr_buf[BUFFER_SIZE];

rtems_task spw_reader_task(rtems_task_argument spw_fildes);

rtems_task Init(rtems_task_argument ignored)
{
  rtems_status_code     status;
  rtems_id              rd_task_id;
  int                   fd_wdt;
  int                   fd_spw;
  ssize_t               wr_size;
  ssize_t               sent_size;
  int32_t               rmap_hdr_size;
  rmap_header_t         rmap_hdr;
  spacepkt_header_t     spacepkt_hdr;
  int32_t               tm_hdr_size;
  uint32_t              i;
  uint8_t               data_crc;
  printf("SpaceWire example application started\n");

  /* Create SpaceWire reader task */
  status = rtems_task_create(rtems_build_name('R', 'S', 'P', 'W'),
                             100,
                             RTEMS_CONFIGURED_MINIMUM_STACK_SIZE,
                             RTEMS_DEFAULT_MODES,
                             DEFAULT_TASK_ATTRIBUTES,
                             &rd_task_id);

  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to create SpaceWire reader task (%d : %s).",
           status, rtems_status_text(status));
    return;
  }

  /* Open SpaceWire device for reading and writing on logical address 254 */
  fd_spw = open(SPWN_DEVICE_0_NAME_PREFIX"254", O_RDWR);
  if (fd_spw < 0) {
    printf("Failed to open SpaceWire '%s' for writing (%d).",
           SPWN_DEVICE_0_NAME_PREFIX"254", fd_spw);
  }

  /* Start SpaceWire reader task */
  status = rtems_task_start(rd_task_id,
                            spw_reader_task,
                            (rtems_task_argument) &fd_spw);
  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to start SpaceWire reader task (%d : %s).",
           status, rtems_status_text(status));
    return;
  }

  /* Disable the watchdog */
  fd_wdt = open(RTEMS_WATCHDOG_DEVICE_NAME, O_WRONLY);
  if (fd_wdt < 0) {
    printf("Failed to open watchdog '%s' (%d).", RTEMS_WATCHDOG_DEVICE_NAME, fd_wdt);
  }
  status = ioctl(fd_wdt, WATCHDOG_ENABLE_IOCTL, WATCHDOG_DISABLE);
  if (status < 0) {
    printf("Could not disable watchdog (%d : %s).", errno, strerror(errno));
  }

  /* Create a SpaceWire RMAP packet for sending telemetry data through the TCM,
     assuming that the TCM is connected on port 1. Command specifications are from
     The Sirius Product User Manual, section 7.11. */

  rmap_hdr.tgt_addr.addr[0] = 2;
  rmap_hdr.tgt_addr.addr[1] = 3;
  rmap_hdr.tgt_addr.len = 2;
  rmap_hdr.tgt_lgl_addr = 0x42;
  rmap_hdr.cmd = RMAP_TYPE_CMD | RMAP_CMD_WRITE;
  rmap_hdr.key = 0x30;
  rmap_hdr.reply_addr.addr[0] = 1;
  rmap_hdr.reply_addr.addr[1] = 3;
  rmap_hdr.reply_addr.len = 2;
  rmap_hdr.init_addr = 254;
  rmap_hdr.trans_id = 0;
  rmap_hdr.ext_addr = 0xFF;
  rmap_hdr.addr = 0x00001000;
  rmap_hdr.data_len = SPACE_HDR_SZ_B + 4;

  /* Create a dummy Space packet for transmission */
  spacepkt_hdr.pkt_type = SPACE_TYPE_TM;
  spacepkt_hdr.apid = 175;
  spacepkt_hdr.seq_count = 0;
  spacepkt_hdr.data_len = 4;

  /* Set up dummy payload data */
  wr_buf[100] = 'D';
  wr_buf[101] = 'A';
  wr_buf[102] = 'T';
  wr_buf[103] = 'A';
  wr_buf[104] = 0x00;

  /* Add the space packet header data in the offset margin before the packet data */
  tm_hdr_size = spacepkt_add_header(&spacepkt_hdr, 100, wr_buf);

  /* Send SpaceWire packets continuously */
  while (true) {

    /* Add the RMAP header data in the offset margin before the packet data (space packet) */
    rmap_hdr_size = rmap_add_header(&rmap_hdr, 100 - tm_hdr_size, wr_buf);

    data_crc = 0;
    for (i = 100 - tm_hdr_size; i < 104; i++) {
      data_crc = RMAP_CalculateCRC(data_crc, wr_buf[i]);
    }
    wr_buf[104] = data_crc;

    if (rmap_hdr_size < 0) {
      printf("Failed to create RMAP header\n");
    } else {

      wr_size = rmap_hdr_size + rmap_hdr.data_len + 1;

      sent_size = write(fd_spw, &wr_buf[100 - rmap_hdr_size - tm_hdr_size], wr_size);

      printf("Sent %d (%d, %d):\n", (int) sent_size, (int) rmap_hdr_size,
             (int) tm_hdr_size);
      for (i = 0; i < sent_size; i++) {
        printf("%.02X ", wr_buf[100 - rmap_hdr_size - tm_hdr_size + i]);
      }
      printf("\n");

      if (sent_size != wr_size) {
        printf("Unable to write entire packet, tried %d, managed %d\n",
               (int) wr_size, (int) sent_size);
      }

    }

    /* Increase transaction ID for each request */
    rmap_hdr.trans_id = rmap_hdr.trans_id + 1;

    sleep(1);
  }
}

rtems_task spw_reader_task(rtems_task_argument spw_fildes)
{
  int                   fd_spw;
  ssize_t               rd_size;
  uint32_t              i;
  rmap_header_t         rmap_hdr;
  int32_t               rmap_hdr_size;

  /* Get SpaceWire file handle from task argument */
  fd_spw =  *((int *) spw_fildes);

  while (true) {
    /* Keep reading */
    rd_size = read(fd_spw, rd_buf, BUFFER_SIZE);

    /* Print the received packet */
    printf("Got new SpaceWire packet (size %d)\n", (int) rd_size);
    printf("----------------------------------\n");
    rmap_hdr_size = rmap_parse_header(&rmap_hdr, rd_buf);
    if (rmap_hdr_size < 0) {
      printf("Couldn't parse RMAP header\n");
    } else {
      /* Print data portion of the packet according to section 7.11.4.16 of the manual */
      for (i = 0; i < rmap_hdr_size; i++) {
        printf("0x%02X", rd_buf[i]);
      }
      printf("\n\n");
    }
  }

}
