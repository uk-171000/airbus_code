/*-----------------------------------------------------------------------------
 * Copyright (C) 2016 - 2018 ÅAC Microtec AB
 *
 * Filename:     gpio.c
 * Module name:  RTEMS example
 *
 * Author(s):    ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  A simple GPIO test program
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>

#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

/* These are strictly here only for demonstrational purposes */
#define CONFIGURE_APPLICATION_NEEDS_GPIO_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_SCET_DRIVER

/* These are probably minimal defines */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_MAXIMUM_DRIVERS 11
#define CONFIGURE_MAXIMUM_SEMAPHORES 50
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 10
#define CONFIGURE_MAXIMUM_TIMERS 0

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 20

#ifdef LEON3
#define DEFAULT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#else
#define DEFAULT_TASK_ATTRIBUTES RTEMS_DEFAULT_ATTRIBUTES
#endif
#define CONFIGURE_INIT_TASK_ATTRIBUTES DEFAULT_TASK_ATTRIBUTES

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>
#include <bsp/gpio_rtems.h>

static int gpio_fd;

/* Interrupt service notification task */
rtems_task gpio_reader(rtems_task_argument ignore)
{
  ssize_t       rd_len;
  uint8_t       buf;

  while (true) {
    rd_len = read(gpio_fd, &buf, 1);
    if (rd_len > 0) {
      printf("Got interrupt for GPIO, value %d\r\n", buf);
    } else {
      printf("Failed reading from GPIO, got %zu.\r\n", rd_len);
    }
  }
}

rtems_task Init(rtems_task_argument ignored)
{
  uint32_t              ioctl_data;
  uint32_t              ioctl_result;
  rtems_id              gpio_reader_id;
  rtems_status_code     task_status;

  printf("GPIO example application started\n");
  gpio_fd = open("/dev/gpio0", O_RDWR);
  if (gpio_fd < 0) {
    printf("Failed to open GPIO device\n");
    return;
  }

  /* Set pin 0 as input */
  ioctl_data = GPIO_DIRECTION_IN;
  ioctl_result = (int32_t) ioctl(gpio_fd, GPIO_IOCTL_SET_DIRECTION, &ioctl_data);
  if (ioctl_result < 0) {
    printf("Failed direction ioctl operation on GPIO0, got %"PRIi32"\r\n", ioctl_result);
    return;
  }

  /* Setting up edge detection on falling edge for pin 0 */
  ioctl_data = 1;
  ioctl_result = (int32_t) ioctl(gpio_fd, GPIO_IOCTL_SET_FALL_EDGE_DETECTION,
                                 &ioctl_data);
  if (ioctl_result < 0) {
    printf("Failed falling edge ioctl operation on GPIO0, got %"PRIi32"\r\n",
           ioctl_result);
    return;
  }

  /* Setting up edge detection on rising edge for pin 0 */
  ioctl_data = 1;
  ioctl_result = (int32_t) ioctl(gpio_fd, GPIO_IOCTL_SET_RISE_EDGE_DETECTION,
                                 &ioctl_data);
  if (ioctl_result < 0) {
    printf("Failed rising edge ioctl operation on GPIO0, got %"PRIi32"\r\n",
           ioctl_result);
    return;
  }

 /* Create a reader task for pin 0 */
  task_status = rtems_task_create(rtems_build_name('G', 'P', 'I', 'R'),
                                  200,
                                  RTEMS_CONFIGURED_MINIMUM_STACK_SIZE,
                                  RTEMS_INTERRUPT_LEVEL(0),
                                  DEFAULT_TASK_ATTRIBUTES,
                                  &gpio_reader_id);

  if (task_status != RTEMS_SUCCESSFUL) {
    printf("Failed to create reader task for GPIO0, got %s.\r\n",
           rtems_status_text(task_status));
    return;
  }

  /* Now, start the task */
  task_status = rtems_task_start(gpio_reader_id, gpio_reader, (rtems_task_argument) NULL);
  if (task_status != RTEMS_SUCCESSFUL) {
    printf("Failed to run reader task for GPIO0, got %s.\r\n",
           rtems_status_text(task_status));
    return;
  }

  /* Wait for any user input */
  getchar();

  /* Kill the task */
  task_status = rtems_task_delete(gpio_reader_id);
  if (task_status != RTEMS_SUCCESSFUL) {
    printf("Failed to delete isn task for GPIO0, got %s.\r\n",
           rtems_status_text(task_status));
    return;
  }

  /* Close gpio device */
  close(gpio_fd);

  rtems_task_delete(RTEMS_SELF);
}
