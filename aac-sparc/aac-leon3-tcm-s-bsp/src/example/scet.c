/*-----------------------------------------------------------------------------
 * Copyright (C) 2016 - 2018 ÅAC Microtec AB
 *
 * Filename:     scet.c
 * Module name:  RTEMS example
 *
 * Author(s):    ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  A simple SCET program example
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

/* These are strictly here only for demonstrational purposes */
#define CONFIGURE_APPLICATION_NEEDS_SCET_DRIVER

/* These are probably minimal defines */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_MAXIMUM_DRIVERS 11
#define CONFIGURE_MAXIMUM_SEMAPHORES 50
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 10
#define CONFIGURE_MAXIMUM_TIMERS 0

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 20

#ifdef LEON3
#define CONFIGURE_INIT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#endif

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

#include <bsp/scet_rtems.h>

static int scet_fd;

rtems_task Init(rtems_task_argument ignored)
{
  rtems_status_code     status;
  int                   result;
  rtems_id              scet_q_id;
  uint32_t              message;
  size_t                message_size;
  ssize_t               rd_size;
  uint8_t __attribute__ ((aligned(4))) buffer[SCET_RW_CNT];
  uint32_t              sec;
  uint16_t              subsec;

  printf("SCET example application started\n");

  scet_fd = open(RTEMS_SCET_DEVICE_NAME, O_RDWR);
  if (scet_fd < 0) {
    printf("Failed to open scet (%d:%s)\n", errno, strerror(errno));
    return;
  }

  /* Set PPS threshold to maximum */
  result = ioctl(scet_fd, SCET_SET_PPS_THRESHOLD_IOCTL, 0xFFFE);
  if (result < 0) {
    printf("Failed to set PPS threshold\n");
    return;
  }

  /* Enable master mode */
  result = ioctl(scet_fd, SCET_SET_PPS_O_EN_IOCTL, 1);
  if (result < 0) {
    printf("Failed to enable PPS drive enable %d\n", result);
    return;
  }

  /* Find the PPS driver message queue for notifications */
  status = rtems_message_queue_ident(rtems_build_name('S', 'P', 'P', 'S'),
                                     RTEMS_SEARCH_ALL_NODES,
                                     &scet_q_id);
  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to find scet message queue, errcode: %d, errstring: %s\n",
           status, rtems_status_text(status));
    return;
  }

  /* Check to see if we can find an incoming PPS event */
  message = 0xFF;
  while ((status == RTEMS_SUCCESSFUL) && (message != SCET_INTERRUPT_STATUS_PPS_FOUND)) {
    status = rtems_message_queue_receive(scet_q_id,
                                         &message,
                                         &message_size,
                                         RTEMS_WAIT,
                                         200);
  }
  if (message == SCET_INTERRUPT_STATUS_PPS_FOUND) {
    printf("Found external PPS signal!\n");
  } else {
    printf("No external PPS signal found!\n");
  }

  /* Get current SCET time */
  rd_size = read(scet_fd, buffer, sizeof(buffer));
  if (rd_size < SCET_RW_CNT) {
    printf("Failed to read SCET time, (%d : %s)\n", errno, strerror(errno));
    return;
  }

  memcpy(&sec, buffer + SCET_SECOND_OFFS, sizeof(sec));
  memcpy(&subsec, buffer + SCET_SUBSECOND_OFFS, sizeof(subsec));

  printf("SCET time is %lu.%u\n", sec, subsec);

  rtems_task_delete(RTEMS_SELF);
}
