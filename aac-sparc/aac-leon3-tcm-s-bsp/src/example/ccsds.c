/*-----------------------------------------------------------------------------
 * Copyright (C) 2017 ÅAC Microtec AB
 *
 * Filename:     ccsds.c
 * Module name:  RTEMS example
 *
 * Author(s):    JaVi
 * Support:      support@aacmicrotec.com
 * Description:  This program shows how to send and receive CCSDS packets on
 *               a TCM-S without running the TCM core application.
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

/* Define the ÅAC drivers we need */
#define CONFIGURE_APPLICATION_NEEDS_CCSDS_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_WDT_DRIVER

/* Define the RTEMS drivers we need */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

/* Set up application specific resources */
#define CONFIGURE_MAXIMUM_DRIVERS 11
#define CONFIGURE_MAXIMUM_SEMAPHORES 4
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 10
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 1
#define CONFIGURE_MAXIMUM_TIMERS 0
#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 3

#ifdef LEON3
#define DEFAULT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#else
#define DEFAULT_TASK_ATTRIBUTES RTEMS_DEFAULT_ATTRIBUTES
#endif
#define CONFIGURE_INIT_TASK_ATTRIBUTES DEFAULT_TASK_ATTRIBUTES

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

#include <bsp/ccsds_rtems.h>
#include "pus.h"

/* Local parameters */
#define BUFFER_SIZE     16384

uint8_t __attribute__ ((aligned (4))) rd_buf[BUFFER_SIZE];
uint8_t wr_buf[BUFFER_SIZE];

rtems_task tc_reader_task(rtems_task_argument ignored);

rtems_task Init(rtems_task_argument ignored)
{
  rtems_status_code     status;
  rtems_id              rd_task_id;
  int                   fd_wdt;
  int                   fd_tm;
  int                   fd_tm0;
  ssize_t               wr_size;
  ssize_t               sent_size;
  tm_config_t           tm_config;
  int32_t               tm_hdr_size;
  spacepkt_header_t     spacepkt_hdr;

  printf("CCSDS example application started\n");

  /* Do general TM & TC setup first */
  fd_tm = open(CCSDS_NAME_TM, O_RDWR);
  if (fd_tm < 0) {
    printf("Failed to open CCSDS TM device %s (%d).\n", CCSDS_NAME_TM, fd_tm);
    return;
  }

  /* For illustrative purposes, let's start by reconfiguring TM. This means we have to
     disable TM (default on), write an updated configuration, wait a bit to allow it
     to set and then enable TM again. */

  /* Disable TM (default on) */
  status = ioctl(fd_tm, CCSDS_DISABLE_TM, NULL);
  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to disable TM\n");
    return;
  }

  /* Get current TM configuration */
  status = ioctl(fd_tm, CCSDS_GET_TM_CONFIG, &tm_config);
  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to get current TM configuration\n");
    return;
  }

  /* Set new TM configuration */
  tm_config.clk_divisor = 25; /* Set bitrate to 25 MHz / 25 => 1 Mbit */
  status = ioctl(fd_tm, CCSDS_SET_TM_CONFIG, &tm_config);
  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to set new TM configuration\n");
    return;
  }

  sleep(1);

  /* Enable TM again */
  status = ioctl(fd_tm, CCSDS_ENABLE_TM, NULL);
  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to enable TM\n");
    return;
  }

  /* Create TC reader task */
  status = rtems_task_create(rtems_build_name('T', 'C', '0', 'R'),
                             100,
                             RTEMS_CONFIGURED_MINIMUM_STACK_SIZE,
                             RTEMS_DEFAULT_MODES,
                             DEFAULT_TASK_ATTRIBUTES,
                             &rd_task_id);

  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to create TC reader task (%d : %s).\n",
           status, rtems_status_text(status));
    return;
  }

  /* Start TC reader task */
  status = rtems_task_start(rd_task_id,
                            tc_reader_task,
                            (rtems_task_argument) NULL);
  if(status != RTEMS_SUCCESSFUL) {
    printf("Failed to start TC reader task (%d : %s).\n",
           status, rtems_status_text(status));
    return;
  }

  /* Open TM virtual channel 0 for writing */
  fd_tm0 = open(CCSDS_NAME_TM_VC0, O_WRONLY);
  if (fd_tm0 < 0) {
    printf("Failed to open TM Virtual channel 0 for writing (%d).\n", fd_tm0);
    return;
  }

  /* Disable the watchdog */
  fd_wdt = open(RTEMS_WATCHDOG_DEVICE_NAME, O_WRONLY);
  if (fd_wdt < 0) {
    printf("Failed to open watchdog '%s' (%d).\n", RTEMS_WATCHDOG_DEVICE_NAME, fd_wdt);
  }
  status = ioctl(fd_wdt, WATCHDOG_ENABLE_IOCTL, WATCHDOG_DISABLE);
  if (status < 0) {
    printf("Could not disable watchdog (%d : %s).\n", errno, strerror(errno));
  }

  /* Create a dummy Space packet for transmission */
  spacepkt_hdr.pkt_type = SPACE_TYPE_TM;
  spacepkt_hdr.apid = 11;
  spacepkt_hdr.seq_count = 0;
  spacepkt_hdr.data_len = 4;

  /* Set up dummy payload data */
  wr_buf[100] = 'D';
  wr_buf[101] = 'A';
  wr_buf[102] = 'T';
  wr_buf[103] = 'A';

  /* Send TM packets continuously */
  while (true) {

    tm_hdr_size = spacepkt_add_header(&spacepkt_hdr, 100, wr_buf);

    if (tm_hdr_size < 0) {
      printf("Failed to create TM space packet header\n");
    } else {

      wr_size = tm_hdr_size + spacepkt_hdr.data_len;

      sent_size = write(fd_tm0, &wr_buf[100 - tm_hdr_size], wr_size);

      if (sent_size != wr_size) {
        printf("Unable to write entire packet, tried %d, managed %d\n",
               (int) wr_size, (int) sent_size);
      }

    }

    /* Increase sequence count for each packet */
    spacepkt_hdr.seq_count = spacepkt_hdr.seq_count + 1;

    sleep(1);
  }
}

rtems_task tc_reader_task(rtems_task_argument ignored)
{
  int                   fd_tc0;
  ssize_t               rd_size;
  uint32_t              i;

  /* Open TC virtual channel 0 for reading */
  fd_tc0 = open(CCSDS_NAME_TC_VC0, O_RDONLY);
  if (fd_tc0 < 0) {
    printf("Failed to open TC Virtual channel 0 for reading (%d).\n", fd_tc0);
    return;
  }

  while (true) {
    /* Keep reading */
    rd_size = read(fd_tc0, rd_buf, BUFFER_SIZE);

    /* Print the received packet */
    printf("Got new TC packet (size %d)\n", (int) rd_size);
    printf("-----------------------------\n");
    for (i = 0; i < rd_size; i++) {
      printf("%.02X ", rd_buf[i]);
    }
  }

}
