/*-----------------------------------------------------------------------------
 * Copyright (C) 2017 ÅAC Microtec AB
 *
 * Filename:     pus.h
 * Module name:  RTEMS example
 *
 * Author(s):    ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  C functions for creating TM and TC PUS packets.
 * Requirements: -
 *---------------------------------------------------------------------------*/

#ifndef PUS_H
#define PUS_H

#include <stdint.h>
#include <stdio.h>

/* Size constants for the packet header */
#define SPACE_VERSION_SZ                (3)
#define SPACE_TYPE_SZ                   (1)
#define SPACE_DATA_FIELD_HDR_FLG_SZ     (1)
#define SPACE_APID_SZ                   (11)
#define SPACE_GRP_FLG_SZ                (2)
#define SPACE_SEQ_CNT_SZ                (14)
#define SPACE_PKT_LEN_SZ                (16)

#define SPACE_HDR_SZ                    (SPACE_VERSION_SZ + \
                                         SPACE_TYPE_SZ + \
                                         SPACE_DATA_FIELD_HDR_FLG_SZ + \
                                         SPACE_APID_SZ + \
                                         SPACE_GRP_FLG_SZ + \
                                         SPACE_SEQ_CNT_SZ + \
                                         SPACE_PKT_LEN_SZ)

#define SPACE_HDR_SZ_B                  (SPACE_HDR_SZ / 8)

/* Offset constants for the packet header */
#define SPACE_VERSION_OFFS              (8 - SPACE_VERSION_SZ)
#define SPACE_TYPE_OFFS                 (SPACE_VERSION_OFFS - SPACE_TYPE_SZ)
#define SPACE_DATA_FIELD_HDR_FLG_OFFS   (SPACE_TYPE_OFFS - SPACE_DATA_FIELD_HDR_FLG_SZ)
#define SPACE_APID_OFFS                 (0)
#define SPACE_GRP_FLG_OFFS              (8 - SPACE_GRP_FLG_SZ)
#define SPACE_SEQ_CNT_OFFS              (0)

/* Mask constants for the packet header (for fields split over several bytes) */
#define SPACE_APID_HIGH_MASK            (0x0700)
#define SPACE_APID_LOW_MASK             (0x00FF)
#define SPACE_SEQ_CNT_HIGH_MASK         (0x3F00)
#define SPACE_SEQ_CNT_LOW_MASK          (0x00FF)
#define SPACE_PKT_LEN_HIGH_MASK         (0xFF00)
#define SPACE_PKT_LEN_LOW_MASK          (0x00FF)

/* Value constants for the packet header */
#define SPACE_VERSION                   (0x00)
#define SPACE_TYPE_TM                   (0x00)
#define SPACE_TYPE_TC                   (0x01)
#define SPACE_DATA_FIELD_HDR_FLG        (0x01)
#define SPACE_GRP_FLG_FIRST             (0x01)
#define SPACE_GRP_FLG_CONTINUE          (0x00)
#define SPACE_GRP_FLG_LAST              (0x02)
#define SPACE_GRP_FLG_STANDALONE        (0x03)

/* Size constants for the data field header */
#define PUS_SPARE_7_FLG_SZ              (1)
#define PUS_PKT_PUS_VERSION_SZ          (3)
#define PUS_SPARE_3_0_SZ                (4)
#define PUS_SERVICE_TYPE_SZ             (8)
#define PUS_SERVICE_SUBTYPE_SZ          (8)

#define PUS_DATA_FIELD_HDR_SZ           (PUS_SPARE_7_FLG_SZ + \
                                         PUS_PKT_PUS_VERSION_SZ + \
                                         PUS_SPARE_3_0_SZ + \
                                         PUS_SERVICE_TYPE_SZ + \
                                         PUS_SERVICE_SUBTYPE_SZ)

#define PUS_DATA_FIELD_HDR_SZ_B         (PUS_DATA_FIELD_HDR_SZ / 8)

/* Offset constants for the data field header */
#define PUS_SPARE_7_FLG_OFFS            (8 - PUS_SPARE_7_FLG_SZ)
#define PUS_PKT_PUS_VERSION_OFFS        (PUS_SPARE_7_FLG_OFFS - PUS_PKT_PUS_VERSION_SZ)
#define PUS_SPARE_3_0_OFFS              (0)

/* Value constants for the data field header */
#define PUS_SPARE_7_FLG                 (0x00)
#define PUS_PKT_PUS_VERSION             (0x01)
#define PUS_SPARE_3_0                   (0x00)

/* Other constants */
#define PUS_PEC_SZ_B                    (2)

#define PUS_SZ_B                        (SPACE_HDR_SZ_B + \
                                         PUS_DATA_FIELD_HDR_SZ_B + \
                                         PUS_PEC_SZ_B)

typedef struct {
  uint32_t      pkt_type;
  uint16_t      apid;
  uint16_t      seq_count;
  uint32_t      data_len;
} spacepkt_header_t;

typedef struct {
  uint32_t      pkt_type;
  uint16_t      apid;
  uint16_t      seq_count;
  uint32_t      data_len;
  uint8_t       pus_type;
  uint8_t       pus_subtype;
} puspkt_header_t;

int32_t spacepkt_add_header(const spacepkt_header_t * hdr,
                            uint32_t data_offs,
                            uint8_t * data_buf);

int32_t puspkt_add_header(const puspkt_header_t * hdr,
                          uint32_t data_offs,
                          uint8_t * data_buf);

#endif /* PUS_H */
