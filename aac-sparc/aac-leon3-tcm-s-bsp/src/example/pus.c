/*-----------------------------------------------------------------------------
 * Copyright (C) 2017 ÅAC Microtec AB
 *
 * Filename:     pus.c
 * Module name:  RTEMS example
 *
 * Author(s):    ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  C functions for creating TM and TC PUS packets.
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <stdint.h>
#include "pus.h"

/* A function to add a Space packet header (with parameters) to existing data */
int32_t spacepkt_add_header(const spacepkt_header_t * hdr,
                            uint32_t data_offs,
                            uint8_t * data_buf)
{
  uint32_t      length;
  uint32_t      offset;

  /* Check that the header fits */
  if (data_offs < SPACE_HDR_SZ_B) {
    printf("Data offset too small for space packet header.\n");
    return -1;
  }

  /* Calculate start of header */
  offset = data_offs - SPACE_HDR_SZ_B;

  length = hdr->data_len - 1;

  /* Create space packet header */
  data_buf[offset++] = ((SPACE_VERSION << SPACE_VERSION_OFFS) |
                        (hdr->pkt_type << SPACE_TYPE_OFFS) |
                        ((hdr->apid & SPACE_APID_HIGH_MASK) >> 8));
  data_buf[offset++] = hdr->apid & SPACE_APID_LOW_MASK;
  data_buf[offset++] = ((SPACE_GRP_FLG_STANDALONE << SPACE_GRP_FLG_OFFS) |
                        ((hdr->seq_count & SPACE_SEQ_CNT_HIGH_MASK) >> 8));
  data_buf[offset++] = hdr->seq_count & SPACE_SEQ_CNT_LOW_MASK;
  data_buf[offset++] = (length & SPACE_PKT_LEN_HIGH_MASK) >> 8;
  data_buf[offset++] = length & SPACE_PKT_LEN_LOW_MASK;

  return SPACE_HDR_SZ_B;
}

/* A function to add a PUS packet header (with parameters) to existing data */
int32_t puspkt_add_header(const puspkt_header_t * hdr,
                          uint32_t data_offs,
                          uint8_t * data_buf)
{
  uint32_t      length;
  uint32_t      offset;

  /* Check that the header fits */
  if (data_offs < PUS_SZ_B) {
    printf("Data offset too small for PUS packet header.\n");
    return -1;
  }

  offset = data_offs - PUS_SZ_B;

  length = hdr->data_len + PUS_DATA_FIELD_HDR_SZ_B + PUS_PEC_SZ_B - 1;

  /* Create PUS packet header */
  data_buf[offset++] = ((SPACE_VERSION << SPACE_VERSION_OFFS) |
                       (hdr->pkt_type << SPACE_TYPE_OFFS) |
                       (SPACE_DATA_FIELD_HDR_FLG << SPACE_DATA_FIELD_HDR_FLG_OFFS) |
                       ((hdr->apid & SPACE_APID_HIGH_MASK) >> 8));
  data_buf[offset++] = hdr->apid & SPACE_APID_LOW_MASK;
  data_buf[offset++] = ((SPACE_GRP_FLG_STANDALONE << SPACE_GRP_FLG_OFFS) |
                       ((hdr->seq_count & SPACE_SEQ_CNT_HIGH_MASK) >> 8));
  data_buf[offset++] = hdr->seq_count & SPACE_SEQ_CNT_LOW_MASK;
  data_buf[offset++] = (length & SPACE_PKT_LEN_HIGH_MASK) >> 8;
  data_buf[offset++] = length & SPACE_PKT_LEN_LOW_MASK;

  /* Packet data field header */
  data_buf[offset++] = ((PUS_SPARE_7_FLG << PUS_SPARE_7_FLG_OFFS) |
                        (PUS_PKT_PUS_VERSION << PUS_PKT_PUS_VERSION_OFFS) |
                        (PUS_SPARE_3_0 << PUS_SPARE_3_0_OFFS));
  data_buf[offset++] = hdr->pus_type & 0xFF;
  data_buf[offset++] = hdr->pus_subtype & 0xFF;

  return SPACE_HDR_SZ_B + PUS_DATA_FIELD_HDR_SZ_B;
}
