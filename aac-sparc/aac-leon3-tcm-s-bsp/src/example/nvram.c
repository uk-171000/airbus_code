/*-----------------------------------------------------------------------------
 * Copyright (C) 2016 - 2018 ÅAC Microtec AB
 *
 * Filename:     nvram.c
 * Module name:  RTEMS example
 *
 * Author(s):    ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  A NVRAM sample program
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

/* These are strictly here only for demonstrational purposes */
#define CONFIGURE_APPLICATION_NEEDS_SPI_RAM_DRIVER

/* These are probably minimal defines */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_MAXIMUM_DRIVERS 11
#define CONFIGURE_MAXIMUM_SEMAPHORES 50
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 10
#define CONFIGURE_MAXIMUM_TIMERS 0

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 20

#ifdef LEON3
#define CONFIGURE_INIT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#endif

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

#include <bsp/spi_ram_rtems.h>

static int nvram_fd;

rtems_task Init(rtems_task_argument ignored)
{
  uint8_t       wr_buf[8];
  uint8_t       rd_buf[8];
  uint32_t      i;

  printf("NVRAM example application started\n");

  nvram_fd = open(SPI_RAM_DEVICE_NAME, O_RDWR);
  if (nvram_fd < 0) {
    printf("Failed to open NVRAM (%d:%s)\n", errno, strerror(errno));
    return;
  }

  wr_buf[0] = 0x01;
  wr_buf[1] = 0x23;
  wr_buf[2] = 0x45;
  wr_buf[3] = 0x67;
  wr_buf[4] = 0x89;
  wr_buf[5] = 0xAB;
  wr_buf[6] = 0xCD;
  wr_buf[7] = 0xEF;

  /* Set an absolute write address and write */
  lseek(nvram_fd, 0x300, SEEK_SET);
  write(nvram_fd, &wr_buf[0], 4);

  /* Set a relative write address and write */
  lseek(nvram_fd, 0x100 - 4, SEEK_CUR);
  write(nvram_fd, &wr_buf[4], 4);

  /* Read back from the two locations using absolute addresses */
  lseek(nvram_fd, 0x300, SEEK_SET);
  read(nvram_fd, &rd_buf[0], 4);
  lseek(nvram_fd, 0x400, SEEK_SET);
  read(nvram_fd, &rd_buf[4], 4);

  for (i = 0; i < 8; i++) {
    printf("%.02X ", rd_buf[i]);
  }
  printf("\n");

  rtems_task_delete(RTEMS_SELF);
}
