/*-----------------------------------------------------------------------------
 * Copyright (C) 2018 ÅAC Microtec AB
 *
 * Filename:     sysflash-reader.c
 * Module name:  RTEMS example
 *
 * Author(s):    LiJo
 * Support:      support@aacmicrotec.com
 * Description:  An example to show reading from the System Flash.
 * Requirements: -
 *---------------------------------------------------------------------------*/
#define DEBUG

#include <inttypes.h>
#include <rtems.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>
#include <bsp.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <bsp.h>
#include <bsp/error_manager_rtems.h>
#include <bsp/spi_ram_rtems.h>
#include <bsp/system_flash_rtems.h>
#include <bsp/toolchain_support.h>
#include <rtems/counter.h>

#define BLOCK_BAD (0x00)
#define BLOCK_OK (0x01)

#define BLOCK_BUF_SIZE ((SYSFLASH_PAGE_SIZE / sizeof(uint32_t)) * SYSFLASH_PAGES_PER_BLOCK)

#define NVRAM_BB_TABLE_OFFSET (0x0E00)
#define BB_TABLE_BLOCKS_PER_WORD (32)
#define BB_TABLE_BIT_INDEX_MASK (0x1F)

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_SYSTEM_FLASH_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_ERROR_MANAGER_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_SPI_RAM_DRIVER

#define CONFIGURE_UNIFIED_WORK_AREAS
#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_MAXIMUM_DRIVERS
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS SHRT_MAX

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#ifdef LEON3
#define CONFIGURE_INIT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#endif

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

static uint32_t buffer[BLOCK_BUF_SIZE];

/* Help function declaration */
static int32_t read_block(int fd, int fd_errman, uint32_t block_number);
static int32_t check_bb_table(uint32_t block_no);
static int32_t check_bb_table_helper(int fd, uint32_t block_no);

rtems_task Init(rtems_task_argument ignored)
{

  int fd;
  int fd_errman;
  int i;
  int answer;
  int32_t status;
  int32_t status_bb;
  uint32_t block_number;
  long number;

  char inbuf[20];
  char *endptr;

  /* Open Error Manager */
  fd_errman = open(RTEMS_ERRMAN_DEVICE_NAME, O_RDONLY);
  if (fd_errman < 0) {
    DBG(DBG_SEVERITY_ERR, "Couldn't open device (%d:%d:%s).\n", fd_errman, errno,
        strerror(errno));
    return;
  }

  /* Open sysflash */
  fd = open(SYSFLASH_DEVICE_NAME, O_RDONLY);
  if (fd < 0) {
    DBG(DBG_SEVERITY_ERR, "Couldn't open device (%d:%d:%s).\n", fd, errno, strerror(errno));
    return;
  }

  while (true) {

    printf("\n\nEnter block number or press q to quit:\n");

    if (fgets(inbuf, sizeof(inbuf), stdin) == NULL) {
      DBG(DBG_SEVERITY_ERR, "fgets failed\n");
      continue;
    }

    if ((inbuf[0] == 'q') && (inbuf[1] == '\n')) {
      DBG(DBG_SEVERITY_INFO, "quitting");
      break;
    }

    number = 0;
    errno = 0;

    number = strtol(inbuf, &endptr, 10);

    /* sanity check */
    if (inbuf == endptr) {
      DBG(DBG_SEVERITY_INFO, "Input must be an integer.\n");
      continue;
    } else if (*endptr != '\n') {
      DBG(DBG_SEVERITY_INFO, "Input must be an integer.\n");
      continue;
    } else if (errno != 0) {
      DBG(DBG_SEVERITY_INFO, "Strtol failed, err: %ld, errstring: %s\n", errno,
          strerror(errno));
      continue;
    } else if ((number < 0) || (number > SYSFLASH_BLOCKS)) {
      DBG(DBG_SEVERITY_INFO, "Block number must be between 0 and 8192.\n");
      continue;
    }

    block_number = (uint32_t)number;

    /* Check for factory bad blocks */
    DBG(DBG_SEVERITY_INFO, "Check if block %" PRIu32 " is factory marked bad,"
                           " if bad, compare to table in NVRAM.\n",
        block_number);
    status = ioctl(fd, SYSFLASH_IO_BAD_BLOCK_CHECK, block_number);
    if (status < 0) {
      DBG(DBG_SEVERITY_ERR, "Failed to check bad block %lu, ERR: %ld, ERRSTRING: %s\n",
          block_number, status, strerror(errno));
    }

    /* Check flag in NVRAM bad block table */
    status_bb = check_bb_table(block_number);
    if (status_bb < 0) {
      DBG(DBG_SEVERITY_ERR, "Read from NVRAM failed");
      continue;
    }

    if ((status == SYSFLASH_FACTORY_BAD_BLOCK_MARKED) && (status_bb == BLOCK_OK)) {
      DBG(DBG_SEVERITY_INFO, "Factory bad block %" PRIu32 ", not marked in NVRAM table.\n",
          block_number);
      continue;
    } else if ((status == SYSFLASH_FACTORY_BAD_BLOCK_MARKED) && (status_bb == BLOCK_BAD)) {
      DBG(DBG_SEVERITY_INFO, "Factory bad block %" PRIu32 ", marked in NVRAM table.\n",
          block_number);
      continue;
    } else if ((status != SYSFLASH_FACTORY_BAD_BLOCK_MARKED) && (status_bb == BLOCK_BAD)) {
      DBG(DBG_SEVERITY_ERR,
          "Block %i not marked as factory bad, but is marked as bad in NVRAM bb table.\n",
          block_number);
      continue;
    }

    DBG(DBG_SEVERITY_INFO,
        "Block %i is not marked factory bad nor marked as bad in NVRAM bb table.\n",
        block_number);

    DBG(DBG_SEVERITY_INFO, "reading block.\n");

    /* Read block */
    status = read_block(fd, fd_errman, block_number);
    if (status < 0) {
      DBG(DBG_SEVERITY_ERR, "Failed to read block from system flash.");
    }

    /* Ask and print */
    printf("Print block?  (y or n)\n");
    answer = getchar();
    if (answer == 'y') {
      printf("\r\n Printing block %" PRIu32 "", block_number);
      for (i = 0; i < BLOCK_BUF_SIZE; ++i) {
        if (i % 6 == 0) {
          printf("\n");
        }
        printf("0X%08lX,  ", buffer[i]);
      }
    }

    /* Clear input buffer */
    while ((getchar()) != '\n')
      ;
  }

  rtems_task_delete(RTEMS_SELF);
}

static int32_t read_block(int fd, int fd_errman, uint32_t block_number)
{
  int page;
  int status;
  ssize_t reading_status;
  size_t buffer_offset;
  off_t offset;
  off_t page_offset;

  uint32_t single_error;
  uint32_t multiple_error;
  uint32_t single_error_read;
  uint32_t multiple_error_read;
  uint64_t timestamp_page;
  uint64_t timestamp_page_update;

  for (page = 0; page < SYSFLASH_PAGES_PER_BLOCK; ++page) {
    buffer_offset = page * SYSFLASH_PAGE_SIZE / sizeof(uint32_t);
    offset = block_number * SYSFLASH_PAGES_PER_BLOCK + page;

    page_offset = lseek(fd, offset, SEEK_SET);
    if (page_offset < 0) {
      DBG(DBG_SEVERITY_ERR, "Reset of seek pointer failed. Status %ld, %s\n", page_offset,
          rtems_status_text(page_offset));
      return -EIO;
    }
    if (page_offset != offset) {
      DBG(DBG_SEVERITY_WARN, "Did not seek the correct offset %lu, instead %ld\n", offset,
          page_offset);
    }

    /* Check single errors */
    status = ioctl(fd_errman, ERRMAN_GET_SYS_SINGLE_ERRCNT_IOCTL, &single_error);
    if (status < 0) {
      DBG(DBG_SEVERITY_ERR,
          "Failed to get system flash single error count, err: %ld, errstring: %s\n", status,
          strerror(errno));
      return -EIO;
    }

    /* Check multiple errors */
    status = ioctl(fd_errman, ERRMAN_GET_SYS_MULTI_ERRCNT_IOCTL, &multiple_error);
    if (status < 0) {
      DBG(DBG_SEVERITY_ERR, "Failed to get system flash multiple error count, err: "
                            "%ld, errstring: %s \n",
          status, strerror(errno));
      return -EIO;
    }

    /* page timestamp */
    timestamp_page = rtems_clock_get_uptime_nanoseconds();

    reading_status = read(fd, (buffer + buffer_offset), SYSFLASH_PAGE_SIZE);
    if (reading_status < 0) {
      DBG(DBG_SEVERITY_ERR, "Read from sysflash failed. Status %ld, %s\n", reading_status,
          rtems_status_text(reading_status));
      return -EIO;
    }

    if (reading_status != SYSFLASH_PAGE_SIZE) {
      DBG(DBG_SEVERITY_WARN, "Expected to read %ld bytes. Got %ld\n", SYSFLASH_PAGE_SIZE,
          reading_status);
    }

    timestamp_page_update = rtems_clock_get_uptime_nanoseconds();
    DBG(DBG_SEVERITY_INFO, "It took %" PRIu64 " ns to read page %" PRIu32 "\n",
        (timestamp_page_update - timestamp_page), offset);

    /* Check single errors */
    status = ioctl(fd_errman, ERRMAN_GET_SYS_SINGLE_ERRCNT_IOCTL, &single_error_read);
    if (status < 0) {
      DBG(DBG_SEVERITY_ERR,
          "Failed to get system flash single error count, ERR: %ld, ERRSTRING: %s\n", status,
          strerror(errno));
      return -EIO;
    }

    /* Check multiple errors */
    status = ioctl(fd_errman, ERRMAN_GET_SYS_MULTI_ERRCNT_IOCTL, &multiple_error_read);
    if (status < 0) {
      DBG(DBG_SEVERITY_ERR,
          "Failed to get system flash multiple error count, err: %ld, errstring: %s\n",
          status, strerror(errno));
      return -EIO;
    }

    /* Compare EDAC before and after read */
    if (single_error != single_error_read) {
      DBG(DBG_SEVERITY_ERR, "Correctable errors in flash read, page %i\n", page);
    }

    if (multiple_error != multiple_error_read) {
      DBG(DBG_SEVERITY_ERR, "Uncorrectable errors in flash read, page %i\n", page);
    }
  }

  return 0;
}

static int32_t check_bb_table(uint32_t block_no)
{
  int fd;
  int32_t myerrno;
  int32_t status;

  if (SYSFLASH_BLOCKS - 1 < block_no) {
    return -EINVAL;
  }

  fd = open(SPI_RAM_DEVICE_NAME, O_RDWR);
  if (0 > fd) {
    myerrno = errno;
    DBG(DBG_SEVERITY_ERR, "Unable to open NVRAM, error %d: %s", myerrno, strerror(myerrno));
    return -EIO;
  }

  status = check_bb_table_helper(fd, block_no);

  close(fd);

  return status;
}

static int32_t check_bb_table_helper(int fd, uint32_t block_no)
{
  off_t offset;
  off_t nvram_offset;
  int32_t status;
  int32_t myerrno;
  uint32_t bb_table_word;
  uint8_t bb_table_bit_index;
  ssize_t bytes_rw;
  ssize_t bytes_written;

  /* Get byte offset of 32-bit word containing block flag */
  nvram_offset =
      NVRAM_BB_TABLE_OFFSET + sizeof(bb_table_word) * (block_no / BB_TABLE_BLOCKS_PER_WORD);

  status = ioctl(fd, SPI_RAM_SET_EDAC_IOCTL, SPI_RAM_IOCTL_EDAC_ENABLE);
  if (status < 0) {
    printf("Failed to enable EDAC, err: %ld, errstring: %s\n", status, strerror(errno));
    return -EIO;
  }

  /* Seek to offset */
  DBG(DBG_SEVERITY_INFO, "Read from NVRAM offset 0x%X", (uint32_t)nvram_offset);
  errno = 0;
  offset = lseek(fd, nvram_offset, SEEK_SET);
  if (offset != nvram_offset) {
    myerrno = errno;
    DBG(DBG_SEVERITY_ERR, "lseek failed, error %d: %s", myerrno, strerror(myerrno));
    close(fd);
    return -EIO;
  }

  /* Read word */
  errno = 0;
  bytes_rw = read(fd, &bb_table_word, sizeof(bb_table_word));

  if (bytes_rw < 0) {
    DBG(DBG_SEVERITY_ERR, "Read from NVRAM failed, %d: %s", errno, strerror(errno));
    return bytes_rw;
  }

  /* Check for bit errors */
  if ((errno == EIO) && (bytes_rw < 0)) {
    DBG(DBG_SEVERITY_ERR, "Uncorrectable EDAC error when reading from nvram");
  } else if ((errno == EIO) && (bytes_rw >= 0)) {
    DBG(DBG_SEVERITY_WARN, "Correctable EDAC error when reading from nvram, re-writing");
    offset = lseek(fd, nvram_offset, SEEK_SET);

    if (offset != nvram_offset) {
      DBG(DBG_SEVERITY_ERR, "lseek of %s failed", SPI_RAM_DEVICE_NAME);
    }

    bytes_written = write(fd, &bb_table_word, sizeof(bb_table_word));
    if (bytes_written != sizeof(bb_table_word)) {
      DBG(DBG_SEVERITY_WARN, "Error when re-writing EDAC-corrected data to nvram");
    }
  }

  /* Check block flag in word */
  bb_table_bit_index = block_no % BB_TABLE_BLOCKS_PER_WORD;
  if ((bb_table_word & (1 << bb_table_bit_index)) != 0) {
    return BLOCK_BAD;
  } else {
    return BLOCK_OK;
  }
}
