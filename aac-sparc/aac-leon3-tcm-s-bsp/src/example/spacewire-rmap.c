/*-----------------------------------------------------------------------------
 * Copyright (C) 2017 - 2018 ÅAC Microtec AB
 *
 * Filename:     spacewire-rmap.c
 * Module name:  RTEMS example
 *
 * Author(s):    JaVi
 * Support:      support@aacmicrotec.com
 * Description:  This program shows how to use the SpaceWire driver to send
 *               RMAP commands to the TCM and receive responses.
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

/* Define the ÅAC drivers we need */
#define CONFIGURE_APPLICATION_NEEDS_SPACEWIRE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_WDT_DRIVER

/* Define the RTEMS drivers we need */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

/* Set up application specific resources */
#define CONFIGURE_MAXIMUM_DRIVERS 11
#define CONFIGURE_MAXIMUM_SEMAPHORES 4
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 10
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 0
#define CONFIGURE_MAXIMUM_TIMERS 0
#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 3

#ifdef LEON3
#define DEFAULT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#else
#define DEFAULT_TASK_ATTRIBUTES RTEMS_DEFAULT_ATTRIBUTES
#endif
#define CONFIGURE_INIT_TASK_ATTRIBUTES DEFAULT_TASK_ATTRIBUTES

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

#include <bsp/spacewire_node_rtems.h>
#include "rmap.h"
#include "rmap_crc.h"

/* Local parameters */
#define BUFFER_SIZE     16384

uint8_t __attribute__ ((aligned (4))) rd_buf[BUFFER_SIZE];
uint8_t wr_buf[BUFFER_SIZE];

rtems_task spw_reader_task(rtems_task_argument spw_fildes);

rtems_task Init(rtems_task_argument ignored)
{
  rtems_status_code     status;
  rtems_id              rd_task_id;
  int                   fd_wdt;
  int                   fd_spw;
  ssize_t               wr_size;
  ssize_t               sent_size;
  int32_t               rmap_hdr_size;
  rmap_header_t         rmap_hdr;

  printf("SpaceWire example application started\n");

  /* Create SpaceWire reader task */
  status = rtems_task_create(rtems_build_name('R', 'S', 'P', 'W'),
                             100,
                             RTEMS_CONFIGURED_MINIMUM_STACK_SIZE,
                             RTEMS_DEFAULT_MODES,
                             DEFAULT_TASK_ATTRIBUTES,
                             &rd_task_id);

  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to create SpaceWire reader task (%d : %s).",
           status, rtems_status_text(status));
    return;
  }

  /* Open SpaceWire device for reading and writing on logical address 63 */
  fd_spw = open(SPWN_DEVICE_0_NAME_PREFIX"63", O_RDWR);
  if (fd_spw < 0) {
    printf("Failed to open SpaceWire '%s' for writing (%d).",
           SPWN_DEVICE_0_NAME_PREFIX"63", fd_spw);
  }

  /* Start SpaceWire reader task */
  status = rtems_task_start(rd_task_id,
                            spw_reader_task,
                            (rtems_task_argument) &fd_spw);
  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to start SpaceWire reader task (%d : %s).",
           status, rtems_status_text(status));
    return;
  }

  /* Disable the watchdog */
  fd_wdt = open(RTEMS_WATCHDOG_DEVICE_NAME, O_WRONLY);
  if (fd_wdt < 0) {
    printf("Failed to open watchdog '%s' (%d).", RTEMS_WATCHDOG_DEVICE_NAME, fd_wdt);
  }
  status = ioctl(fd_wdt, WATCHDOG_ENABLE_IOCTL, WATCHDOG_DISABLE);
  if (status < 0) {
    printf("Could not disable watchdog (%d : %s).", errno, strerror(errno));
  }

  /* Create a SpaceWire RMAP packet for asking the TCM about housekeeping data,
     assuming that port 1 on this device is connected to port 1 on the TCM. Command
     specifications are from the Sirius Product User Manual, section 7.11. */

  rmap_hdr.tgt_addr.addr[0] = 1;
  rmap_hdr.tgt_addr.addr[1] = 3;
  rmap_hdr.tgt_addr.len = 2;
  rmap_hdr.tgt_lgl_addr = 0x42;
  rmap_hdr.cmd = RMAP_TYPE_CMD | RMAP_CMD_READ | RMAP_CMD_REPLY;
  rmap_hdr.key = 0x30;
  rmap_hdr.reply_addr.addr[0] = 1;
  rmap_hdr.reply_addr.addr[1] = 3;
  rmap_hdr.reply_addr.len = 2;
  rmap_hdr.init_addr = 63;
  rmap_hdr.trans_id = 0;
  rmap_hdr.ext_addr = 0xFF;
  rmap_hdr.addr = 0x02000000;
  rmap_hdr.data_len = 28;

  /* Send SpaceWire packets continuously */
  while (true) {

    /* We insert the RMAP header before our payload data, in this example we assume the
       payload data to start at the randomly chosen offset of 100 bytes. */
    rmap_hdr_size = rmap_add_header(&rmap_hdr, 100, wr_buf);

    if (rmap_hdr_size < 0) {
      printf("Failed to create RMAP header\n");
    } else {

      wr_size = rmap_hdr_size;

      sent_size = write(fd_spw, &wr_buf[100 - rmap_hdr_size], wr_size);

      if (sent_size < 0) {
        printf("Unable to write packet, got error %d\n", (int) sent_size);
      } else if (sent_size != wr_size) {
        printf("Unable to write entire packet, tried %d, managed %d\n",
               (int) wr_size, (int) sent_size);
      }

    }

    /* Increase transaction ID for each request */
    rmap_hdr.trans_id = rmap_hdr.trans_id + 1;

    sleep(1);
  }
}

rtems_task spw_reader_task(rtems_task_argument spw_fildes)
{
  int                   fd_spw;
  ssize_t               rd_size;
  uint32_t              i;
  rmap_header_t         rmap_hdr;
  int32_t               rmap_hdr_size;

  /* Use the SpaceWire file descriptor we got as argument */
  fd_spw =  *((int *) spw_fildes);

  while (true) {
    /* Keep reading (each read will block until data arrives) */
    rd_size = read(fd_spw, rd_buf, BUFFER_SIZE);
    if (rd_size < 0) {
      printf("Error in reading from the SpaceWire device, got error %d\n",
             (int) rd_size);
    } else {
      /* Print the received packet */
      printf("Got new SpaceWire packet (size %d)\n", (int) rd_size);
      printf("----------------------------------\n");
      rmap_hdr_size = rmap_parse_header(&rmap_hdr, rd_buf);
      if (rmap_hdr_size < 0) {
        printf("Couldn't parse RMAP header\n");
      } else {
        /* Print data portion of the packet according to the TCM HKData specification
           in the manual */
        i = rmap_hdr_size;
        printf("SCET seconds: %u\n", (rd_buf[i] << 24) | (rd_buf[i+1] << 16) |
               (rd_buf[i+2] << 8) | rd_buf[i+3]);
        printf("SCET subseconds: %u\n", (rd_buf[i+4] << 8) | rd_buf[i+5]);
        printf("Input voltage: %u\n", (rd_buf[i+6] << 8) | rd_buf[i+7]);
        printf("3V3: %u\n", (rd_buf[i+8] << 8) | rd_buf[i+9]);
        printf("2V5: %u\n", (rd_buf[i+10] << 8) | rd_buf[i+11]);
        printf("1V2: %u\n", (rd_buf[i+12] << 8) | rd_buf[i+13]);
        printf("Input current: %u\n", (rd_buf[i+14] << 8) | rd_buf[i+15]);
        printf("Temperature: %u\n", (rd_buf[i+16] << 24) | (rd_buf[i+17] << 16) |
               (rd_buf[i+18] << 8) | rd_buf[i+19]);
        printf("Padding: %u\n", rd_buf[i+20]);
        printf("SW major version: %u\n", rd_buf[i+21]);
        printf("SW minor version: %u\n", rd_buf[i+22]);
        printf("SW patch version: %u\n", rd_buf[i+23]);
        printf("CPU parity errors: %u\n", rd_buf[i+24]);
        printf("Watchdog trips: %u\n", rd_buf[i+25]);
        printf("Critical single SDRAM errors: %u\n", rd_buf[i+26]);
        printf("Other single SDRAM errors: %u\n", rd_buf[i+27]);
        printf("Critical multiple SDRAM errors: %u\n", rd_buf[i+28]);
        printf("Other multiple SDRAM errors: %u\n", rd_buf[i+29]);
        printf("\n\n");
      }
    }
  }

}
