/*-----------------------------------------------------------------------------
 * Copyright (C) 2016 - 2018 ÅAC Microtec AB
 *
 * Filename:     hello_world.c
 * Module name:  RTEMS example
 *
 * Author(s):    ErZa, JaVi
 * Support:      support@aacmicrotec.com
 * Description:  A simple hello world program
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <bsp.h>

/* These are strictly here only for demonstrational purposes */
#define CONFIGURE_APPLICATION_NEEDS_ADC_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_ERROR_MANAGER_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_GPIO_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_SYSTEM_FLASH_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_SCET_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_SPACEWIRE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_UART_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_WDT_DRIVER

/* These are probably minimal defines */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_MAXIMUM_DRIVERS 11
#define CONFIGURE_MAXIMUM_SEMAPHORES 50
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 10
#define CONFIGURE_MAXIMUM_TIMERS 0

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 20

#ifdef LEON3
#define CONFIGURE_INIT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#endif

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

rtems_task Init(rtems_task_argument ignored)
{
  printf("Hello world\n");
  rtems_task_delete(RTEMS_SELF);
}
