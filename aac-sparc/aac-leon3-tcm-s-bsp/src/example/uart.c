/*-----------------------------------------------------------------------------
 * Copyright (C) 2017 - 2018 ÅAC Microtec AB
 *
 * Filename:     uart.c
 * Module name:  RTEMS example
 *
 * Author(s):    JaVi
 * Support:      support@aacmicrotec.com
 * Description:  A simple uart test program
 * Requirements: -
 *---------------------------------------------------------------------------*/

#include <rtems.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <bsp.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

/* These are strictly here only for demonstrational purposes */
#define CONFIGURE_APPLICATION_NEEDS_UART_DRIVER

/* These are probably minimal defines */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_MAXIMUM_DRIVERS 10
#define CONFIGURE_MAXIMUM_SEMAPHORES 50
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 10
#define CONFIGURE_MAXIMUM_TIMERS 0

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_MAXIMUM_TASKS 20

#ifdef LEON3
#define DEFAULT_TASK_ATTRIBUTES (RTEMS_DEFAULT_ATTRIBUTES | RTEMS_FLOATING_POINT)
#else
#define DEFAULT_TASK_ATTRIBUTES RTEMS_DEFAULT_ATTRIBUTES
#endif
#define CONFIGURE_INIT_TASK_ATTRIBUTES DEFAULT_TASK_ATTRIBUTES

#define CONFIGURE_INIT

#include <bsp/bsp_confdefs.h>
#include <rtems/confdefs.h>

/* Local parameters */
#define BUFFER_SIZE     16384

uint8_t __attribute__ ((aligned (4))) rd_buf[BUFFER_SIZE];
uint8_t wr_buf[BUFFER_SIZE];

rtems_task uart_reader_task(rtems_task_argument uart_fildes);

rtems_task Init(rtems_task_argument ignored)
{
  rtems_status_code     status;
  rtems_id              rd_task_id;
  uint32_t              i;
  int                   fd_uart;
  uint8_t               buffer;

  printf("\nWelcome to UART testing!\n");

  /* Create SpaceWire reader task */
  status = rtems_task_create(rtems_build_name('R', 'S', 'P', 'W'),
                             100,
                             RTEMS_CONFIGURED_MINIMUM_STACK_SIZE,
                             RTEMS_DEFAULT_MODES,
                             DEFAULT_TASK_ATTRIBUTES,
                             &rd_task_id);

  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to create SpaceWire reader task (%d : %s).",
           status, rtems_status_text(status));
    return;
  }

  fd_uart = open("/dev/uart0", O_RDWR);
  if (fd_uart < 0) {
    printf("Failed to open UART0 %s\n", strerror(errno));
    return;
  }

  /* Start UART reader task */
  status = rtems_task_start(rd_task_id,
                            uart_reader_task,
                            (rtems_task_argument) &fd_uart);
  if (status != RTEMS_SUCCESSFUL) {
    printf("Failed to start UART reader task (%d : %s).",
           status, rtems_status_text(status));
    return;
  }

  for (i = 0; i < 100; i++) {
    /* Send an incrementing number with modulo to UART0 */
    buffer = '0' + i%10;
    write(fd_uart, &buffer, 1);
    sleep(1);
  }
}

rtems_task uart_reader_task(rtems_task_argument uart_fildes)
{
  int                   fd_uart;
  ssize_t               rd_size;
  int32_t               i;

  /* Get UART file handle from task argument */
  fd_uart =  *((int *) uart_fildes);

  while (true) {
    /* Keep reading */
    rd_size = read(fd_uart, rd_buf, BUFFER_SIZE);

    /* Print the received packet */
    printf("Got new UART data (size %d)\n", (int) rd_size);
    printf("----------------------------\n");

    /* Print UART data */
    for (i = 0; i < rd_size; i++) {
      printf("0x%02X", rd_buf[i]);
    }
    printf("\n\n");
  }
 
}
